import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Schriftteile_GatE
#import version_3.creation.Halbboegen_GatE
import Kanzlei_Schriftzuege_GatE

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Schriftteile_GatE)
#importlib.reload(version_3.creation.Halbboegen_GatE)
importlib.reload(Kanzlei_Schriftzuege_GatE)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Schriftteile_GatE import *
#from version_3.creation.Halbboegen_GatE import *
from Kanzlei_Schriftzuege_GatE import *


### True >>> modified code         >>> 1
### False >>> original Roßberg     >>> 0
version_mod = 1


if version_mod == True:
    print("Roßberg modifizert")
    import version_3.creation.Schriftteile_GatE_mod
    import Kanzlei_Schriftzuege_GatE_mod
    #import version_3.creation.Halbboegen_GatE_mod
    #importlib.reload(version_3.creation.Schriftteile_GatE_mod)
    importlib.reload(Kanzlei_Schriftzuege_GatE_mod)
    #importlib.reload(version_3.creation.Halbboegen_GatE_mod)
    from version_3.creation.Schriftteile_GatE_mod import *
    from Kanzlei_Schriftzuege_GatE_mod import *
    #from version_3.creation.Halbboegen_GatE_mod import *
else:
    print("Roßberg original")
    

from nibLib.pens.rectNibPen import RectNibPen
from math import radians
import math as m
import collections
import glyphContext








# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 14
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)




# _____________ Pen for drawing shape in glyph window _______________

penWidth = 10
penThickness = 4

#    +--------- width --------+
#    |                        |  thickness
#    +------------------------+





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)


# _______________________________________________________  






def drawKanzleiGatE_a(x, y):      
    
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1, outstrokeLen=3)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=0, outstrokeLen=1)
    
    KanzleiGatE_a = Bogen_links + Strich_rechts
    trans_scale(KanzleiGatE_a, valueToMoveGlyph) 
    return KanzleiGatE_a
    
   
   
    
    
def drawKanzleiGatE_b(x,y):        
    
    Strich_links = drawSchriftzug11(x, y, outstrokeLen=0)
    Bogen_rechts = drawSchriftzug29(x+1, y, instrokeLen=3)
    Raute_a, _, _, _ = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-5)
    
    KanzleiGatE_b = Strich_links + Bogen_rechts
    trans_scale(KanzleiGatE_b, valueToMoveGlyph)
    return KanzleiGatE_b
    
    
    
    
    
def drawKanzleiGatE_c(x,y):
           
    Bogen_links = drawSchriftzug15(x, y)
    Haeckchen_oben = drawGrundelementE(*Bogen_links.points[3])

    KanzleiGatE_c = Bogen_links + Haeckchen_oben
    trans_scale(KanzleiGatE_c, valueToMoveGlyph)
    return KanzleiGatE_c
    


def drawKanzleiGatE_ch(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, baseline-0.35)

    c_links = drawKanzleiGatE_c(x, y)  
    h_rechts = drawKanzleiGatE_h_decender(x+4, y)[0]
    valueToReturn = drawKanzleiGatE_h_decender(x+4, y)[1]
    KanzleiGatE_ch = c_links + h_rechts
    return KanzleiGatE_ch, valueToReturn
    
    



def drawKanzleiGatE_d(x, y, Endspitze=12):

    Bogen_links = drawSchriftzug15(x, y, instrokeLen=0.25, outstrokeLen=0)
    Bogen_rechts = drawSchriftzug31(x+2, y, outstrokeLen=2) 
    
    KanzleiGatE_d = Bogen_links + Bogen_rechts
    trans_scale(KanzleiGatE_d, valueToMoveGlyph)
    return KanzleiGatE_d
    
    
    

    
def drawKanzleiGatE_e(x, y):
        
    Bogen_links = drawSchriftzug15(x, y, outstrokeLen=1.5)
    Haken_rechts = drawSchriftteil8(*Bogen_links.points[3])
    
    #outstroke_oben = drawGrundelementA(Haken_rechts.points[-1][0]-offset[0],Haken_rechts.points[-1][1]-offset[1])
    outstroke_oben = drawOutstroke(*Haken_rechts.points[-1], 1.5, "down")
    
    #outstroke_unten = drawGrundelementA(Bogen_links.points[-2][0]-offset[0]/2, Bogen_links.points[-2][1]-offset[0]/4)
    #outstroke_unten = drawOutstroke(*Bogen_links.points[-2])

    KanzleiGatE_e = Bogen_links + Haken_rechts + outstroke_oben #+ outstroke_unten
    trans_scale(KanzleiGatE_e, valueToMoveGlyph)
    return KanzleiGatE_e
    
    
    
    
    
    
def drawKanzleiGatE_f(x,y):
           
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    trans_scale(Querstrich, valueToMoveGlyph)

    KanzleiGatE_longs = drawKanzleiGatE_longs(x, y)
    
    KanzleiGatE_longs = drawKanzleiGatE_longs(x, y)[0]
    values_to_return = drawKanzleiGatE_longs(x, y)[1]
    KanzleiGatE_f = KanzleiGatE_longs + Querstrich
    return KanzleiGatE_f, values_to_return
    



    
    
    
    
def drawKanzleiGatE_f_f(x, y):
     
    Querstrich_left = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=5)
    #Querstrich_right = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich = Querstrich_left #+ Querstrich_right 
    trans_scale(Querstrich, valueToMoveGlyph)
    
    longs_longs = drawKanzleiGatE_longs_longs(x, y)[0]
    values_to_return = drawKanzleiGatE_longs_longs(x, y)[1]
    
    KanzleiGatE_f_f = longs_longs + Querstrich
    return KanzleiGatE_f_f, values_to_return
    
    

def drawKanzleiGatE_f_t(x, y):
        
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    trans_scale(Querstrich, valueToMoveGlyph)
        
    glyph_longs_t = drawKanzleiGatE_longs_t(x, y)[0]
    values_to_return = drawKanzleiGatE_longs_t(x, y)[1]

    KanzleiGatE_f_t = glyph_longs_t + Querstrich
    return KanzleiGatE_f_t, values_to_return
    
    
    
    
    
    
    
def drawKanzleiGatE_f_f_t(x, y):
    
    KanzleiGatE_f_f_t = BezierPath()
    
    main_stroke_left = drawSchriftzug13(x, y, instrokeLen=1.75)
    pkt_Ausstrich = main_stroke_left.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    con = drawSchriftzug_longs_longs_con(x, y)
    main_stroke_middle = drawSchriftzug13(x+3, y, instrokeLen=1.25, PosStroke=False)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y+3)
    Schwung_left = drawSchneckenzug(*main_stroke_middle.points[3], UPPER_E, 4, HSL_size=1, HSL_start=9, clockwise=True, inward=True)
    Schwung_right = drawSchneckenzug(*Schwung_left.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=8.8, clockwise=False, inward=True)
    Schwung = Schwung_left + Schwung_right
    
    glyph_t = drawSchriftzug10(x+6, y, instrokeLen=0.5, PosStroke=False)
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=8)

    KanzleiGatE_f_f_t = main_stroke_left + con + main_stroke_middle + Schwung + glyph_t + Querstrich
    drawPath(KanzleiGatE_f_f_t)
    trans_scale(KanzleiGatE_f_f_t, valueToMoveGlyph)    
    return KanzleiGatE_f_f_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    

  

def drawKanzleiGatE_g(x, y):
    x+=2
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1, outstrokeLen=3) 
    Strich_rechts = drawSchriftzug_g_rechts(x+3, y)
    pkt_Auslauf = Strich_rechts.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KanzleiGatE_g = Bogen_links + Strich_rechts
    trans_scale(KanzleiGatE_g, valueToMoveGlyph)
    return KanzleiGatE_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    

def drawKanzleiGatE_g_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatE_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 3, HSL_size=1, HSL_start=24, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.2 * i      for  i in range(0, 4)])

    KanzleiGatE_g_thinStroke += Auslauf
    drawPath(KanzleiGatE_g_thinStroke)
    trans_thinStroke_down_left(KanzleiGatE_g_thinStroke) 
    trans_scale(KanzleiGatE_g_thinStroke, valueToMoveGlyph)
    return KanzleiGatE_g_thinStroke
    
    
    
    
    
    
def drawKanzleiGatE_h(x, y):      
    x+=2
    Strich_links = drawSchriftzug11(x, y)
    Bogen_rechts = drawSchriftzug30(x+1.5, y, "a", instrokeLen=3, outstrokeLen=0.8)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+1.5, baseline) 
    Fuss = drawSchriftteil7(*Raute_d) 
        
    KanzleiGatE_h = Strich_links + Bogen_rechts + Fuss
    trans_scale(KanzleiGatE_h, valueToMoveGlyph)    
    return KanzleiGatE_h
    
    
def drawKanzleiGatE_h_decender(x, y):      
    
    KanzleiGatE_h = BezierPath()
    Strich_links = drawSchriftzug11(x, y)
    Bogen_rechts = drawSchriftzug30(x+1.5, y, "c", instrokeLen=3, outstrokeLen=0) 
    pkt_Auslauf = Bogen_rechts.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
            
    KanzleiGatE_h = Strich_links + Bogen_rechts
    trans_scale(KanzleiGatE_h, valueToMoveGlyph)    
    return KanzleiGatE_h, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawKanzleiGatE_h_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatE_h_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 2, HSL_size=4, HSL_start=24, clockwise=True, inward=False, HSL_size_multipliers=[ 1+.2 * i      for  i in range(0, 3)])

          
    KanzleiGatE_h_thinStroke += Auslauf
    drawPath(KanzleiGatE_h_thinStroke)
    trans_thinStroke_down_left(KanzleiGatE_h_thinStroke) 
    trans_scale(KanzleiGatE_h_thinStroke, valueToMoveGlyph)
    return KanzleiGatE_h_thinStroke
    
    
     
    
def drawKanzleiGatE_i(x,y):       

    Strich = drawSchriftzug8(x, y, instrokeLen=1, outstrokeLen=1)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x,y)
    Punkt = drawGrundelementE(Raute_a[0], Raute_a[1]+modul_height*3)
        
    KanzleiGatE_i = Strich + Punkt
    trans_scale(KanzleiGatE_i, valueToMoveGlyph)
    return KanzleiGatE_i



def drawKanzleiGatE_j(x, y):      
    x += 4
    Strich = drawSchriftzug_g_rechts(x, y, instrokeLen=1)
    pkt_Auslauf = Strich.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x,y)
    Punkt = drawGrundelementE(Raute_a[0], Raute_a[1]+modul_height*3)
    
    KanzleiGatE_j = Strich + Punkt
    trans_scale(KanzleiGatE_j, valueToMoveGlyph)
    return KanzleiGatE_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    


def drawKanzleiGatE_k(x, y):
    
    Strich_links = drawSchriftzug11(x, y, outstrokeLen=1)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+1, y+1.5)
    Schleife = drawSchriftteil8(*Raute_b, instrokeLen=1, outstrokeLen=1.5)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)

    #Querstrich = drawGrundelementB(*Raute_a, 2)
    
    KanzleiGatE_k = Strich_links + Schleife + Querstrich
    trans_scale(KanzleiGatE_k, valueToMoveGlyph)
    return KanzleiGatE_k
    
    

        
        
def drawKanzleiGatE_l(x, y):    
    
    KanzleiGatE_l = drawSchriftzug11(x, y, outstrokeLen=1)
    
    trans_scale(KanzleiGatE_l, valueToMoveGlyph)
    return KanzleiGatE_l
    
    
    
    
       
        
        
def drawKanzleiGatE_m(x,y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1)
    Strich_mitte = drawSchriftzug9(x+3, y, instrokeLen=2.5)
    Strich_rechts = drawSchriftzug8(x+6, y, "a", instrokeLen=2.5, outstrokeLen=1)
    
    KanzleiGatE_m = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(KanzleiGatE_m, valueToMoveGlyph)
    return KanzleiGatE_m
    





def drawKanzleiGatE_n(x, y):      
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5, outstrokeLen=1)
    
    KanzleiGatE_n = Strich_links + Strich_rechts
    trans_scale(KanzleiGatE_n, valueToMoveGlyph)
    return KanzleiGatE_n
    




def drawKanzleiGatE_o(x, y):   
    
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1, outstrokeLen=1)
    Bogen_rechts = drawSchriftzug29(x+1, y, instrokeLen=1, outstrokeLen=1)
    
    KanzleiGatE_o = Bogen_links + Bogen_rechts
    trans_scale(KanzleiGatE_o, valueToMoveGlyph)
    return KanzleiGatE_o
    
    
    
    
    
def drawKanzleiGatE_p(x, y):
   
    Strich_links = drawSchriftzug7(x, y, instrokeLen=1, outstrokeLen=0)
    pkt_Ausstrich = Strich_links.points[-1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Bogen_rechts = drawSchriftzug29(x+1, y)
    
    Raute_a, _, _, _ = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)
    Signatur = drawGrundelementE(*Raute_a)
    Ausstrich = drawThinLineBtm(*Strich_links.points[-1])

    KanzleiGatE_p = Strich_links + Bogen_rechts + Signatur
    trans_scale(KanzleiGatE_p, valueToMoveGlyph)
    return KanzleiGatE_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
 
    
    
def drawKanzleiGatE_q(x, y):      
    
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1, outstrokeLen=2.5)
    Strich_rechts = drawSchriftzug14(x+3, y)
    pkt_Ausstrich = Strich_rechts.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    KanzleiGatE_q = Bogen_links + Strich_rechts
    trans_scale(KanzleiGatE_q, valueToMoveGlyph)
    return KanzleiGatE_q, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
    
def drawKanzleiGatE_r(x, y):       

    Strich = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=1)

    Raute_a, _, _, _ = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2, y)
    constroke = drawInstroke(*Raute_a, 1.5, "up")
    Haken = drawGrundelementE(*Raute_a)
    
    KanzleiGatE_r = Strich + constroke + Haken
    trans_scale(KanzleiGatE_r, valueToMoveGlyph)
    return KanzleiGatE_r    
    
    
    

def drawKanzleiGatE_rc(x, y):       
    
    drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)
    r_links = drawSchriftzug9(x, y, version="b", Fuss="long", instrokeLen=0.7, outstrokeLen=1)
    
    KanzleiGatE_rc = r_links
    trans_scale(KanzleiGatE_rc, valueToMoveGlyph) 
    
    letter_c = drawKanzleiGatE_c(x+2.5, y)
    KanzleiGatE_rc += letter_c
    return KanzleiGatE_rc
    
    
    
    

def drawKanzleiGatE_s(x, y):
    #x -= 8
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.5, y-3.25)
    Bogen_oben = drawHalbbogen8(*Grund_a, instrokeLen=2.08, outstrokeLen=2.5)
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.5, y-2.25)
    Bogen_unten = drawHalbbogen4(*Grund_b, outstrokeLen=1.85)
    
    Raute_a, _, _, _ = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2.5, y)
    Deckung_oben = drawGrundelementE(*Raute_a)
    
    _, _, _, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.5, baseline)
    Deckung_unten = drawGrundelementE(*Raute_d, 1, "unten")
    
    KanzleiGatE_s = Bogen_oben + Bogen_unten + Deckung_oben + Deckung_unten
    trans_scale(KanzleiGatE_s, valueToMoveGlyph)
    return KanzleiGatE_s
    
    
    
         
    

def drawKanzleiGatE_longs(x, y):
        
    stroke_down = drawSchriftzug13(x, y, instrokeLen=2.75)
    pkt_Ausstrich = stroke_down.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Spitze = drawGrundelementE(*stroke_down.points[-5])
    
    KanzleiGatE_longs = stroke_down + Spitze
    trans_scale(KanzleiGatE_longs, valueToMoveGlyph)
    return KanzleiGatE_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    

def drawKanzleiGatE_longs_longs(x, y, PosStroke=False):
        
    main_stroke_left = drawSchriftzug13(x, y, instrokeLen=1.75)
    pkt_Ausstrich = main_stroke_left.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    main_stroke_right = drawSchriftzug13(x+3, y, instrokeLen=2.75, PosStroke=PosStroke)

    Spitze = drawGrundelementE(*main_stroke_right.points[-13])
    
    con = drawSchriftzug_longs_longs_con(x, y)

    KanzleiGatE_longs_longs = main_stroke_left + main_stroke_right + con + Spitze
    trans_scale(KanzleiGatE_longs_longs, valueToMoveGlyph)
    return KanzleiGatE_longs_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    


def drawKanzleiGatE_longs_t(x, y):
    
    #### versucht mit Tab. 45 abzustimmen, aber es ist mir nicht gut gelungen.
    
    main_stroke_left = drawSchriftzug_ft_lig(x, y)
    pkt_Ausstrich = main_stroke_left.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    KanzleiGatE_longs_t = main_stroke_left
    trans_scale(KanzleiGatE_longs_t, valueToMoveGlyph)
        
    glyph_t = drawKanzleiGatE_t(x+3, y, instrokeLen=0)
    KanzleiGatE_longs_t += glyph_t
    return KanzleiGatE_longs_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    

def drawKanzleiGatE_germandbls(x,y):
    
    x += 1
    stroke_down = drawKanzleiGatE_longs(x, y)[0]
    trans_scale_invert(stroke_down, valueToMoveGlyph)
    pkt_Ausstrich = stroke_down.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    right = drawKanzleiGatE_z_fina(x+3, y)[0]
    trans_scale_invert(right, valueToMoveGlyph)
    pkt_Auslauf = right.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KanzleiGatE_germandbls = stroke_down + right
    trans_scale(KanzleiGatE_germandbls, valueToMoveGlyph)
    return KanzleiGatE_germandbls, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
def drawKanzleiGatE_germandbls_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatE_germandbls_Endspitze = BezierPath()   

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 3, HSL_size=2, HSL_start=24, clockwise=True, inward=False)
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)    

    KanzleiGatE_germandbls_Endspitze += Auslauf + Zierstrich
    drawPath(KanzleiGatE_germandbls_Endspitze)
    trans_thinStroke_down_left(KanzleiGatE_germandbls_Endspitze) 
    trans_scale(KanzleiGatE_germandbls_Endspitze, valueToMoveGlyph)
    return KanzleiGatE_germandbls_Endspitze 
    
    
    
    
    

      
    
    
def drawKanzleiGatE_t(x, y, instrokeLen=0.5):    
    
    Strich = drawSchriftzug10(x, y, instrokeLen=instrokeLen, outstrokeLen=1)
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    
    KanzleiGatE_t = Strich + Querstrich
    trans_scale(KanzleiGatE_t, valueToMoveGlyph)
    return KanzleiGatE_t
    
    
    
    

def drawKanzleiGatE_u(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=2.5)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=0, outstrokeLen=1)
    
    KanzleiGatE_u = Strich_links + Strich_rechts
    trans_scale(KanzleiGatE_u, valueToMoveGlyph)
    return KanzleiGatE_u
    
    
    
    
    
    

def drawKanzleiGatE_adieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_a = drawKanzleiGatE_a(x, y)     
    KanzleiGatE_adieresis = Grundschriftzug_a + Dieresis
    return KanzleiGatE_adieresis
    
    

def drawKanzleiGatE_odieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_o = drawKanzleiGatE_o(x, y)       
    KanzleiGatE_odieresis = Grundschriftzug_o + Dieresis
    return KanzleiGatE_odieresis
    

def drawKanzleiGatE_udieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_u = drawKanzleiGatE_u(x, y)   
    KanzleiGatE_udieresis = Grundschriftzug_u + Dieresis
    return KanzleiGatE_udieresis
    
    
    
    
    
    
    
    
    
def drawKanzleiGatE_v(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=1)
    Strich_rechts = drawSchriftzug29(x+1, y, instrokeLen=2.5)
    
    KanzleiGatE_v = Strich_links + Strich_rechts
    trans_scale(KanzleiGatE_v, valueToMoveGlyph)
    return KanzleiGatE_v
    
    
    

    
def drawKanzleiGatE_w(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=1)
    Strich_mitte = drawSchriftzug9(x+3, y, instrokeLen=2.5, outstrokeLen=1)
    Strich_rechts = drawSchriftzug29(x+4, y, instrokeLen=2.5)

    KanzleiGatE_w = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(KanzleiGatE_w, valueToMoveGlyph)
    return KanzleiGatE_w
    
    
    
    
    
def drawKanzleiGatE_x(x, y):
    
    Schnecke = drawSchriftzug_x_hook(x, y-5)
    trans_scale(Schnecke, valueToMoveGlyph)
    
    Hauptstrich_von_r = drawKanzleiGatE_r(x, y)
    KanzleiGatE_x = Hauptstrich_von_r + Schnecke
    return KanzleiGatE_x
    
    
    



def drawKanzleiGatE_y(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=1)
    Bogen_rechts = drawSchriftzug30(x+1, y, "c", instrokeLen=2.5)  ### stimmt dieser Abstand?
    pkt_Auslauf = Bogen_rechts.points[-2]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KanzleiGatE_y = Strich_links + Bogen_rechts
    trans_scale(KanzleiGatE_y, valueToMoveGlyph)
    return KanzleiGatE_y, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
        


def drawKanzleiGatE_z_init(x ,y):
        
    KanzleiGatE_z_init = BezierPath()
    #x +=4
    _, _, _, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+3)
    Halbbogen_oben = drawSchriftzug33(*Raute_d, outstrokeLen=0.75, EinsatzLen=0.12) 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.15, y-1.25)
    Halbbogen_unten = drawSchriftzug25(*Raute_a, outstrokeLen=0.8)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-2.5, y-5)
    Deckung = drawGrundelementE(*Raute_d, 1, "unten")
    KanzleiGatE_z_init += Deckung

    KanzleiGatE_z_init += Halbbogen_oben + Halbbogen_unten
    drawPath(KanzleiGatE_z_init)
    trans_scale(KanzleiGatE_z_init, valueToMoveGlyph)
    return KanzleiGatE_z_init
    
    
    
def drawKanzleiGatE_z_fina(x ,y):
        
    KanzleiGatE_z_fina = BezierPath()
    #x +=4

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    Halbbogen_oben = drawHalbbogen4(*Raute_a, instrokeLen=1, outstrokeLen=0) 

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-0.95, y-3)
    Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_E, 8, HSL_size=2.75, HSL_start=4, clockwise=True, inward=False) 
    pkt_Auslauf = Halbbogen_unten.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    KanzleiGatE_z_fina += Halbbogen_oben + Halbbogen_unten
    drawPath(KanzleiGatE_z_fina)
    trans_scale(KanzleiGatE_z_fina, valueToMoveGlyph)
    return KanzleiGatE_z_fina, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
    
def drawKanzleiGatE_z_fina_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatE_z_thinStroke = BezierPath()

    hook = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 8, HSL_size=1, HSL_start=18, clockwise=False, inward=True)

    KanzleiGatE_z_thinStroke += hook
    drawPath(KanzleiGatE_z_thinStroke)
    trans_thinStroke_down_left(KanzleiGatE_z_thinStroke) 
    trans_scale(KanzleiGatE_z_thinStroke, valueToMoveGlyph)
    return KanzleiGatE_z_thinStroke
    
    
    




    


    


def drawKanzleiGatE_thinstroke_Straight(x, y, *, pass_from_thick=None):     
    
    KanzleiGatE_thinstroke_Straight = BezierPath()
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
          
    KanzleiGatE_thinstroke_Straight = Zierstrich
    trans_thinStroke_down_left(KanzleiGatE_thinstroke_Straight) 
    trans_scale(KanzleiGatE_thinstroke_Straight, valueToMoveGlyph)
    return KanzleiGatE_thinstroke_Straight
    
    
    



# ________________________________________________________


margin_1modul = 60
margin_2modul = 120
marginStr = 30
marginRnd = 60 
margin_r=-25
add_temp=30 #### has to be solved by kerning!    

# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {
   #  'a' : [ drawKanzleiGatE_a, [temp_x, temp_y], marginRnd, marginStr ],
   #  'b' : [ drawKanzleiGatE_b, [temp_x, temp_y], marginStr, marginRnd ],
   #  'c' : [ drawKanzleiGatE_c, [temp_x, temp_y], marginRnd, margin_r+add_temp ],
   #  'd' : [ drawKanzleiGatE_d, [temp_x, temp_y, 12], marginRnd, marginRnd ],
   #  'e' : [ drawKanzleiGatE_e, [temp_x, temp_y], marginRnd, marginStr ],
   #'f' : [ drawKanzleiGatE_f, [temp_x, temp_y], marginStr, -114 ],
   #  'g' : [ drawKanzleiGatE_g, [temp_x, temp_y], marginRnd, marginStr+22 ],
   #  'h' : [ drawKanzleiGatE_h, [temp_x, temp_y], marginStr, marginRnd ],
   #  'h.dec' : [ drawKanzleiGatE_h_decender, [temp_x, temp_y], marginStr, marginRnd ],
   #  'i' : [ drawKanzleiGatE_i, [temp_x, temp_y], marginStr, marginStr ],
   #  'j' : [ drawKanzleiGatE_j, [temp_x, temp_y], marginStr-9, marginStr+6 ],
   #  'k' : [ drawKanzleiGatE_k, [temp_x, temp_y], marginStr, marginStr-30 ],
   #  'l' : [ drawKanzleiGatE_l, [temp_x, temp_y], marginStr, marginStr-55],
   #  'm' : [ drawKanzleiGatE_m, [temp_x, temp_y], marginStr, marginStr ],
   #  'n' : [ drawKanzleiGatE_n, [temp_x, temp_y], marginStr, marginStr ],
   #  'o' : [ drawKanzleiGatE_o, [temp_x, temp_y], marginRnd, marginRnd ],
   #  'p' : [ drawKanzleiGatE_p, [temp_x, temp_y], marginStr, marginRnd ],
   #  #'q' : [ drawKanzleiGatE_q, [temp_x, temp_y], marginRnd, marginStr ],
   #  'r' : [ drawKanzleiGatE_r, [temp_x, temp_y], marginStr, margin_r+add_temp ],
   #   's' : [ drawKanzleiGatE_s, [temp_x, temp_y], marginStr, marginStr ],
   # 'longs' : [ drawKanzleiGatE_longs, [temp_x, temp_y], marginStr, -114 ],
   # 'germandbls' : [ drawKanzleiGatE_germandbls, [temp_x, temp_y], marginStr++135, marginStr+210 ],
   #  't' : [ drawKanzleiGatE_t, [temp_x, temp_y], marginStr, marginStr-15],
   #  'u' : [ drawKanzleiGatE_u, [temp_x, temp_y], marginStr, marginStr ],
   #  'v' : [ drawKanzleiGatE_v, [temp_x, temp_y], marginStr, marginRnd ],
   #  'w' : [ drawKanzleiGatE_w, [temp_x, temp_y], marginStr, marginRnd ],
   #  'x' : [ drawKanzleiGatE_x, [temp_x, temp_y], marginStr-90, margin_r+add_temp ],
   #  'y' : [ drawKanzleiGatE_y, [temp_x, temp_y], marginStr, marginRnd ],
   #  'z' : [ drawKanzleiGatE_z_init, [temp_x, temp_y], marginRnd, marginRnd ],
   #  'z.init' : [ drawKanzleiGatE_z_init, [temp_x, temp_y], marginRnd, marginRnd ],
   #  'z.fina' : [ drawKanzleiGatE_z_fina, [temp_x, temp_y], marginStr, marginRnd ],

    # 'adieresis' : [ drawKanzleiGatE_adieresis, [temp_x, temp_y], marginRnd, marginStr ],
    # 'odieresis' : [ drawKanzleiGatE_odieresis, [temp_x, temp_y], marginRnd, marginRnd ],
    # 'udieresis' : [ drawKanzleiGatE_udieresis, [temp_x, temp_y], marginStr, marginStr ],

    
    # ##### Ligatures
    
    # 'r_c' : [ drawKanzleiGatE_rc, [temp_x, temp_y], marginStr, marginStr-60 ],
    # 'longs_longs' : [ drawKanzleiGatE_longs_longs, [temp_x, temp_y, True], marginStr-25, -78 ],
    # 'f_f' : [ drawKanzleiGatE_f_f, [temp_x, temp_y], marginStr-25, -78 ],
    # 'longs_t' : [ drawKanzleiGatE_longs_t, [temp_x, temp_y], marginStr, marginStr-15 ],
    # 'f_t' : [ drawKanzleiGatE_f_t, [temp_x, temp_y], marginStr, marginStr ],
    # 'f_f_t' : [ drawKanzleiGatE_f_f_t, [temp_x, temp_y], marginStr, marginStr-60 ],
    # 'c_h' : [ drawKanzleiGatE_ch, [temp_x, temp_y], marginRnd, marginRnd ],

    
    ###### Interpunction
    
    # # # # 'period' : [ drawKanzleiGatE_period, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'colon' : [ drawKanzleiGatE_colon, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'semicolon' : [ drawKanzleiGatE_semicolon, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'quoteright' : [ drawKanzleiGatE_quoteright, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'quotesingle' : [ drawKanzleiGatE_quotesingle, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'comma' : [ drawKanzleiGatE_comma, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'endash' : [ drawKanzleiGatE_endash, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'hyphen' : [ drawKanzleiGatE_hyphen, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'exclam' : [ drawKanzleiGatE_exclam, [temp_x, temp_y], margin_1modul, margin_1modul ],
    # # # # 'question' : [ drawKanzleiGatE_question, [temp_x, temp_y], margin_1modul, margin_1modul ],


    ##### Numbers
    
    # # # # 'zero' : [ drawKanzleiGatE_zero, [temp_x, temp_y], marginRnd, marginRnd-60 ],
    # # # # 'one' : [ drawKanzleiGatE_one, [temp_x, temp_y], marginStr+140, marginStr ],
    # # # # 'two' : [ drawKanzleiGatE_two, [temp_x, temp_y], marginStr+18, marginStr ],
    # # # # 'three' : [ drawKanzleiGatE_three, [temp_x, temp_y], marginStr+60, marginStr-60 ],
    # # # # 'four' : [ drawKanzleiGatE_four, [temp_x, temp_y], marginStr, marginStr-15 ],
    # # # # 'five' : [ drawKanzleiGatE_five, [temp_x, temp_y], marginStr+120, marginStr-210 ],
    # # # # 'six' : [ drawKanzleiGatE_six, [temp_x, temp_y], marginRnd, marginStr ],
    # # # # 'seven' : [ drawKanzleiGatE_seven, [temp_x, temp_y], marginStr, -120 ],
    # # # # 'eight' : [ drawKanzleiGatE_eight, [temp_x, temp_y], marginRnd, marginRnd-15 ],    
    # # # # 'nine' : [ drawKanzleiGatE_nine, [temp_x, temp_y], marginRnd, marginStr ],    



    ##### Uppercase
    
    # # # # 'A' : [ drawKanzleiGatE_A, [temp_x, temp_y], marginStr, marginStr-90 ],
    # # # # 'B' : [ drawKanzleiGatE_B, [temp_x, temp_y], marginStr, marginRnd ],
    # # # # 'C' : [ drawKanzleiGatE_C, [temp_x, temp_y], marginStr, marginStr-120 ],
    # # # # 'D' : [ drawKanzleiGatE_D, [temp_x, temp_y], marginStr, marginRnd ],
    # # # # 'E' : [ drawKanzleiGatE_E, [temp_x, temp_y], marginStr, marginStr-60 ],
    # # # # 'F' : [ drawKanzleiGatE_F, [temp_x, temp_y], marginStr, marginStr-90 ],
    # # # # 'G' : [ drawKanzleiGatE_G, [temp_x, temp_y], marginStr, marginRnd ],
    # # # # 'H' : [ drawKanzleiGatE_H, [temp_x, temp_y], marginStr, 0 ],
    # # # # 'I' : [ drawKanzleiGatE_I, [temp_x, temp_y], marginStr, marginStr-60],
    # # # # 'J' : [ drawKanzleiGatE_J, [temp_x, temp_y], marginStr, marginRnd-150],
    # # # # 'K' : [ drawKanzleiGatE_K, [temp_x, temp_y], marginRnd, marginStr-60 ],
    # # # # 'L' : [ drawKanzleiGatE_L, [temp_x, temp_y], marginStr+60, marginStr-60 ],
    # # # # 'M' : [ drawKanzleiGatE_M, [temp_x, temp_y], marginStr, marginStr-90 ],
    # # # # 'N' : [ drawKanzleiGatE_N, [temp_x, temp_y], marginStr, marginStr-90 ],
    # # # # 'O' : [ drawKanzleiGatE_O, [temp_x, temp_y], marginStr+60, marginStr+30 ],
    # # # # 'P' : [ drawKanzleiGatE_P, [temp_x, temp_y], marginStr+30, marginStr-90 ],
    # # # # 'Q' : [ drawKanzleiGatE_Q, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'R' : [ drawKanzleiGatE_R, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'S' : [ drawKanzleiGatE_S, [temp_x, temp_y], marginStr, marginRnd ],
    # # # # 'T' : [ drawKanzleiGatE_T, [temp_x, temp_y], marginStr, marginStr-120 ],
    # # # # 'U' : [ drawKanzleiGatE_U, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'V' : [ drawKanzleiGatE_V, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'W' : [ drawKanzleiGatE_W, [temp_x, temp_y], marginStr, marginStr-95 ],
    # # # # 'X' : [ drawKanzleiGatE_X, [temp_x, temp_y], marginStr, marginStr-90 ],
    # # # # 'Y' : [ drawKanzleiGatE_Y, [temp_x, temp_y], marginStr, marginStr-95 ],
    # # # # 'Z' : [ drawKanzleiGatE_Z, [temp_x, temp_y], marginStr, marginRnd ],
    
    

    
    }



drawFunctions_thinStroke = {

    'f' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'g' : [ drawKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],
    'h.dec' : [ drawKanzleiGatE_z_fina_thinStroke, [temp_x, temp_y] ],
    'j' : [ drawKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],
    'p' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'q' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'y' : [ drawKanzleiGatE_h_thinStroke, [temp_x, temp_y] ],
    'z.fina' : [ drawKanzleiGatE_z_fina_thinStroke, [temp_x, temp_y] ],

    'longs' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'longs_longs' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'longs_t' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'f_t' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f_t' : [ drawKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'c_h' : [ drawKanzleiGatE_z_fina_thinStroke, [temp_x, temp_y] ],

    'germandbls' : [ drawKanzleiGatE_germandbls_thinStroke, [temp_x, temp_y] ],
    # # # 'quotesingle' : [ drawKanzleiGatE_quotesingle_thinstroke, [temp_x, temp_y] ],  


    # # # 'B' : [ drawKanzleiGatE_B_thinStroke, [temp_x, temp_y] ],
    # # # 'C' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'D' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'E' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'F' : [ drawKanzleiGatE_F_thinStroke, [temp_x, temp_y] ],
    # # # 'G' : [ drawKanzleiGatE_G_thinStroke, [temp_x, temp_y] ],
    # # # 'H' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'I' : [ drawKanzleiGatE_F_thinStroke, [temp_x, temp_y] ],
    # # # 'J' : [ drawKanzleiGatE_J_thinStroke, [temp_x, temp_y] ],
    # # # 'L' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'M' : [ drawKanzleiGatE_M_thinStroke, [temp_x, temp_y] ],
    # # # 'N' : [ drawKanzleiGatE_M_thinStroke, [temp_x, temp_y] ],
    # # # 'O' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'P' : [ drawKanzleiGatE_P_thinStroke, [temp_x, temp_y] ],
    # # # 'Q' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'R' : [ drawKanzleiGatE_R_thinStroke, [temp_x, temp_y] ],
    # # # 'U' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'V' : [ drawKanzleiGatE_C_thinStroke, [temp_x, temp_y] ],
    # # # 'W' : [ drawKanzleiGatE_W_thinStroke, [temp_x, temp_y] ],
    # # # 'Y' : [ drawKanzleiGatE_Y_thinStroke, [temp_x, temp_y] ],
    # # # 'Z' : [ drawKanzleiGatE_Z_thinStroke, [temp_x, temp_y] ],

    # # # 'one' : [ drawKanzleiGatE_one_thinStroke, [temp_x, temp_y] ],
    # # # 'two' : [ drawKanzleiGatE_two_thinStroke, [temp_x, temp_y] ],
    # # # 'three' : [ drawKanzleiGatE_three_thinStroke, [temp_x, temp_y] ],
    # # # 'five' : [ drawKanzleiGatE_three_thinStroke, [temp_x, temp_y] ],
    # # # 'six' : [ drawKanzleiGatE_six_thinStroke, [temp_x, temp_y] ],
    # # # 'seven' : [ drawKanzleiGatE_seven_thinStroke, [temp_x, temp_y] ],
    # # # 'nine' : [ drawKanzleiGatE_nine_thinStroke, [temp_x, temp_y] ],


    }





for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	

    if key in drawFunctions_thinStroke:

        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    #assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    #print("margin left:", glyph.leftMargin,"_", "right:", glyph.rightMargin)
    #print("supposed to be:", drawFunctions[key][2],"_", drawFunctions[key][3])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_before = margin_ts - margin_fg
    #print("difference before:", difference_before)
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_mid = margin_ts - margin_fg
    #print("difference mid:", difference_mid)

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid
    #print("move thinStroke by:", move_thinStroke)
    

    glyph.copyLayerToLayer('foreground', 'background')
   
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): 
        margin_ts = 0 
        #print("(no thin line)")   
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_final = margin_ts - margin_fg
    #print("difference before:", difference_before, "=== difference final:", difference_final , "?")
    
    foreground.clear()

    
    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    #print("drawn with:  __Angle:", a, "__Width:", w)

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 

        #print("drawn with:  __Angle:", a, "__Width:", w)

        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    #glyph.removeOverlap(round=0)
    
    
        



