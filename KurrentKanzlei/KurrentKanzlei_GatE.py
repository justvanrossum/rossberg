import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatE
import version_3.creation.Halbboegen_GatE
import KurrentKanzlei_Schriftzuege_GatE

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatE)
importlib.reload(version_3.creation.Halbboegen_GatE)
importlib.reload(KurrentKanzlei_Schriftzuege_GatE)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatE import *
from version_3.creation.Halbboegen_GatE import *
from KurrentKanzlei_Schriftzuege_GatE import *

from nibLib.pens.rectNibPen import RectNibPen
from math import radians
import math as m
import collections
import glyphContext


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 13
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)



# _____________ Pen for drawing shape in glyph window _______________

penWidth = 10
penThickness = 4

#    +--------- width --------+
#    |                        |  thickness
#    +------------------------+





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 5

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)

# temporary to check height of numbers
#line((1*modul_width, 11*modul_height), (10*modul_width, 11*modul_height))



# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 2
temp_y = 8.5





# ________________________________________________________





def drawKurrentKanzleiGatE_a(x, y, version="isol"):     
    
    KurrentKanzleiGatE_a = BezierPath()
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    
    Bogen_links = drawSchriftzug25(x, y, outstrokeLen=1)
    Connection_up = drawConCurve_up(x, y)  
    
    if version == "isol":
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=1)
  
    if version == "iniA":    # ini und med sind gleich
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 4)
        KurrentKanzleiGatE_a += constroke

    if version == "iniH":    # ini und med sind gleich
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatE_a += constroke
        
    if version == "fina":
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=1)
        
    KurrentKanzleiGatE_a += Bogen_links + Connection_up + Strich_rechts
    trans_scale(KurrentKanzleiGatE_a, valueToMoveGlyph)
    return KurrentKanzleiGatE_a
    

def drawKurrentKanzleiGatE_adieresis(x, y, version="isol"):     
    
    KurrentKanzleiGatE_adieresis = BezierPath()
       
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    dieresis = drawDieresis(x+2, y)
    trans_scale(dieresis, valueToMoveGlyph)
        
    if version == "isol":
        glyph_a = drawKurrentKanzleiGatE_a(x, y, version="isol")
  
    if version == "iniA":    # ini und med sind gleich
        glyph_a = drawKurrentKanzleiGatE_a(x, y, version="iniA")

    if version == "iniH":    # ini und med sind gleich
        glyph_a = drawKurrentKanzleiGatE_a(x, y, version="iniH")
        
    if version == "fina":
        glyph_a = drawKurrentKanzleiGatE_a(x, y, version="fina")
        
    KurrentKanzleiGatE_adieresis += glyph_a + dieresis
    return KurrentKanzleiGatE_adieresis
    
    
    
    
         
        
        
    
    
    
    
def drawKurrentKanzleiGatE_b(x, y, version="isol"):        
    
    KurrentKanzleiGatE_b = BezierPath()
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
        
    if version == "isol":
        Bogen_links = drawSchriftzug17(x, y)
        con_btm = drawOutstroke(*Bogen_links.points[-1], 1.4)
        Strich_rechts = drawSchriftzug26(x+3.5, y, instrokeLen=0)
        hook = drawSchneckenzug(*Strich_rechts.points[0], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
        #hook = drawSchriftzug6(x+3.9, y+0.2, "b")

    if version == "iniA":   
        Bogen_links = drawSchriftzug17(x, y)
        con_btm = drawOutstroke(*Bogen_links.points[-1], 1.4)
        Strich_rechts = drawSchriftzug26(x+3.5, y, instrokeLen=0)  ### version="3.25"
        hook = drawSchneckenzug(*Strich_rechts.points[0], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])

    if version == "iniH":  
        Bogen_links = drawSchriftzug17(x, y)
        con_btm = drawOutstroke(*Bogen_links.points[-1], 1.4)
        Strich_rechts = drawSchriftzug26(x+3.5, y, instrokeLen=0)    ### version="3.25"
        hook = drawSchneckenzug(*Strich_rechts.points[0], UPPER_B, 5, HSL_size=2.45, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
        
    if version == "midA":   
        Bogen_links = drawSchriftzug17(x, y, version="con")
        con_btm = drawOutstroke(*Bogen_links.points[-1], 1.4)
        Strich_rechts = drawSchriftzug26(x+3.5, y, instrokeLen=0)  ### version="3.25"
        hook = drawSchneckenzug(*Strich_rechts.points[0], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])

    if version == "midH":  
        Bogen_links = drawSchriftzug17(x, y, version="con")
        con_btm = drawOutstroke(*Bogen_links.points[-1], 1.4)
        Strich_rechts = drawSchriftzug26(x+3.5, y, instrokeLen=0)  ### version="3.25"
        hook = drawSchneckenzug(*Strich_rechts.points[0], UPPER_B, 5, HSL_size=2.45, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
        
    if version == "fina":
        Bogen_links = drawSchriftzug17(x, y, version="con")
        con_btm = drawOutstroke(*Bogen_links.points[-1], 1.4)
        Strich_rechts = drawSchriftzug26(x+3.5, y, instrokeLen=0)     ### version="3.25"
        hook = drawSchneckenzug(*Strich_rechts.points[0], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
        
        
    KurrentKanzleiGatE_b += Bogen_links + con_btm + Strich_rechts + hook
    drawPath(KurrentKanzleiGatE_b)
    trans_scale(KurrentKanzleiGatE_b, valueToMoveGlyph)
    return KurrentKanzleiGatE_b
    
    
    
    
    
    


def drawKurrentKanzleiGatE_c(x,y, version="isol"):
           
    KurrentKanzleiGatE_c = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    Bogen_links = drawSchriftzug21(x, y,  version="for e")
    hook = drawGrundelementE(*Bogen_links.points[17])
  
    if version == "isol":  # wie fina
        outstroke = drawOutstroke(*Bogen_links.points[-1], 2) 
        KurrentKanzleiGatE_c += outstroke
        
    if version == "iniA":  # wie med
        constroke = drawConstroke(*Bogen_links.points[-1], "A", 4)
        KurrentKanzleiGatE_c += constroke

    if version == "iniH":  # wie med
        constroke = drawConstroke(*Bogen_links.points[-1], "H")
        KurrentKanzleiGatE_c += constroke

    KurrentKanzleiGatE_c += Bogen_links + hook
    trans_scale(KurrentKanzleiGatE_c, valueToMoveGlyph)
    return KurrentKanzleiGatE_c
    
    
    


def drawKurrentKanzleiGatE_d(x, y, version="isol"):   #Endspitze=12      

    KurrentKanzleiGatE_d = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    full_stroke = drawSchriftzug30(x+3, y, Spitze="long") # Spitze="long" Spitze="short"
  
    if version == "isol":  # wie fina
        pass
        
    if version == "iniA":  # wie med
        pass

    if version == "iniH":  # wie med
        pass

    KurrentKanzleiGatE_d += full_stroke
    trans_scale(KurrentKanzleiGatE_d, valueToMoveGlyph)
    return KurrentKanzleiGatE_d
    






def drawKurrentKanzleiGatE_e(x, y, version="isol"):  
    
    KurrentKanzleiGatE_e = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    Bogen_links = drawSchriftzug21(x, y,  version="for e")
    Bogen_rechts = drawSchriftzug4(x+2, y, "a")
    Mittelstrich = drawOutstroke(*Bogen_rechts.points[-1], 2, "down")
  
    if version == "isol":  # wie fina
        outstroke = drawOutstroke(*Bogen_links.points[-1], 2) 
        KurrentKanzleiGatE_e += outstroke
        
    if version == "iniA":  # wie med
        constroke = drawConstroke(*Bogen_links.points[-1], "A", 4)
        KurrentKanzleiGatE_e += constroke

    if version == "iniH":  # wie med
        #instroke = drawInstroke(*Grund_c, 2.5) 
        constroke = drawConstroke(*Bogen_links.points[-1], "H")
        KurrentKanzleiGatE_e += constroke


    KurrentKanzleiGatE_e += Bogen_links + Bogen_rechts + Mittelstrich
    trans_scale(KurrentKanzleiGatE_e, valueToMoveGlyph)
    return KurrentKanzleiGatE_e
    
#drawSchriftzug21(temp_x, temp_y)     # outstroke=True
    
    
    
    
       
def drawKurrentKanzleiGatE_f(x, y, version="isol"):
    
    KurrentKanzleiGatE_f = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    Grundstrich = drawKurrentKanzleiGatE_longs(x, y, version="pure")[0]
    values_to_return = drawKurrentKanzleiGatE_longs(x, y)[1]
    trans_scale_invert(Grundstrich, valueToMoveGlyph)
    Querstrich = drawGrundelementB(Grund_a[0]-modul_width, Grund_a[1], 2.5)
 
    
    # Poistionierung der Verbindungsstriche
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, baseline+0.5)
    
         
    if version == "isol":  # wie fina
        pass
        
    if version == "iniA":   # wie med
        constroke = drawConstroke(*Raute_d, "A", 3)
        KurrentKanzleiGatE_f += constroke

    if version == "iniH":   # wie med
        constroke = drawConstroke(*Raute_d, "H")
        KurrentKanzleiGatE_f += constroke

    drawPath(KurrentKanzleiGatE_f)
    KurrentKanzleiGatE_f += Grundstrich + Querstrich
    trans_scale(KurrentKanzleiGatE_f, valueToMoveGlyph)
    return KurrentKanzleiGatE_f, values_to_return
    
    
def drawKurrentKanzleiGatE_f_f(x, y, version="isol"):
    
    KurrentKanzleiGatE_f_f = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=0.75, version="short")
    pkt_Ausstrich = Hauptstrich_links.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    Hauptstrich_rechts = drawSchriftzug_f_f_2nd(x+1, y)
    
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+5.5, y+3.25)
    con = drawGrundelementA(*Raute_a, 2.25, "down")
    Signatur = drawSchriftzug35(x+6.35, y+3)

    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x-0.5, y)    
    Querstrich = drawGrundelementB(*Grund_a, 5)   

    if version == "isol":
        instroke = drawPosStroke(x, y, 0.5)
        KurrentKanzleiGatE_f_f += instroke
      
    if version == "iniA":
        instroke = drawPosStroke(x, y, 0.5)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5)
        KurrentKanzleiGatE_f_f += instroke + constroke
          
    if version == "iniH":       
        instroke = drawPosStroke(x, y, 0.5)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatE_f_f +=instroke + constroke

    if version == "midA":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5)
        KurrentKanzleiGatE_f_f += constroke

    if version == "midH":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatE_f_f += constroke
 
    KurrentKanzleiGatE_f_f += Hauptstrich_links + Hauptstrich_rechts + con + Signatur + Querstrich
    drawPath(KurrentKanzleiGatE_f_f)    
    trans_scale(KurrentKanzleiGatE_f_f, valueToMoveGlyph)
    return KurrentKanzleiGatE_f_f, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
def drawKurrentKanzleiGatE_f_f_t(x, y, version="isol"):
    
    KurrentKanzleiGatE_f_f_t = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1)
    pkt_Ausstrich = Hauptstrich_links.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    Hauptstrich_rechts = drawSchriftzug_longs_longs_2nd(x, y)
    
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+5.5, y+3.5)
    con = drawGrundelementA(*Raute_a, 1.75, "down")
    Signatur_left = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=1, HSL_start=6, clockwise=True, inward=False)
    Signatur_Einsatz = drawGrundelementE(*Signatur_left.points[-1], 0.15)
    Signatur_right = drawSchneckenzug(*Signatur_Einsatz.points[4], LOWER_A, 4, HSL_size=1, HSL_start=6, clockwise=False, inward=False)
    Signatur = Signatur_left + Signatur_Einsatz + Signatur_right + con
    
    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x-0.5, y)    
    Querstrich = drawGrundelementB(*Grund_a, 9)   


    if version == "isol":
        instroke = drawPosStroke(x, y, 0.5)
        KurrentKanzleiGatE_f_f_t += instroke
        glyph_t = drawSchriftzug19(x+6.75, y, version="short", outstrokeLen=0.5)
      
    if version == "iniA":
        instroke = drawPosStroke(x, y, 0.5)
        glyph_t = drawSchriftzug19(x+6.75, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-2], "A", 3)
        KurrentKanzleiGatE_f_f_t += instroke + constroke
                  
    if version == "iniH":       
        instroke = drawPosStroke(x, y, 0.5)
        glyph_t = drawSchriftzug19(x+6.75, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-2], "H")
        KurrentKanzleiGatE_f_f_t += instroke + constroke
        
    if version == "midA":
        glyph_t = drawSchriftzug19(x+6.75, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-2], "A", 3)
        KurrentKanzleiGatE_f_f_t += constroke

    if version == "midH":
        glyph_t = drawSchriftzug19(x+6.75, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-2], "H")
        KurrentKanzleiGatE_f_f_t += constroke

    if version == "fina":
        glyph_t = drawSchriftzug19(x+6.75, y, version="short", outstrokeLen=0.5)

     
    KurrentKanzleiGatE_f_f_t.line(Hauptstrich_rechts.points[-1], glyph_t.points[2]) 
    
    KurrentKanzleiGatE_f_f_t += Hauptstrich_links + Hauptstrich_rechts + Signatur + glyph_t + Querstrich
    drawPath(KurrentKanzleiGatE_f_f_t)    
    trans_scale(KurrentKanzleiGatE_f_f_t, valueToMoveGlyph)
    return KurrentKanzleiGatE_f_f_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    

    
def drawKurrentKanzleiGatE_g(x, y, version="isol"):
    
    KurrentKanzleiGatE_g = BezierPath()
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "isol":
        Bogen_links = drawSchriftzug25(x, y, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="fina")
        pkt_Auslauf = Strich_rechts.points[19]
        #text("pkt_Auslauf", pkt_Auslauf)
        
    if version == "iniA":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="loop_A")

    if version == "iniH":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="loop_H")
        
    if version == "fina":
        Bogen_links = drawSchriftzug25(x, y, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="fina")
        pkt_Auslauf = Strich_rechts.points[19]
        #text("pkt_Auslauf", pkt_Auslauf)
        
    KurrentKanzleiGatE_g += Bogen_links + Connection_up + Strich_rechts
    trans_scale(KurrentKanzleiGatE_g, valueToMoveGlyph)

    if version == "isol" or version == "fina":
        return KurrentKanzleiGatE_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    return KurrentKanzleiGatE_g
    

def drawKurrentKanzleiGatE_g_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentKanzleiGatE_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    #KurrentKanzleiGatE_g_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1)
    
    KurrentKanzleiGatE_g_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentKanzleiGatE_g_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatE_g_thinStroke) 
    trans_scale(KurrentKanzleiGatE_g_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_g_thinStroke
    

           
           


def drawKurrentKanzleiGatE_h(x, y, version="fina"):      
    
    KurrentKanzleiGatE_h = BezierPath()
    
    if version == "isol":
        pos_stroke = drawPosStroke(x, y-0.25)
        downstroke_left = drawSchriftzug11(x, y)
        connection = drawSchriftzug9(x, y, "b", outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")    
        KurrentKanzleiGatE_h += pos_stroke

    if version == "iniA":
        pos_stroke = drawPosStroke(x, y-0.25)
        downstroke_left = drawSchriftzug13(x, y)
        connection = drawSchriftzug9(x, y, "b", outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A", 5)
        KurrentKanzleiGatE_h += pos_stroke
        
    if version == "iniH":
        pos_stroke = drawPosStroke(x, y-0.25)
        downstroke_left = drawSchriftzug13(x, y)
        connection = drawSchriftzug9(x, y, "b", outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H", 5.8)
        KurrentKanzleiGatE_h += pos_stroke

    if version == "midA":
        downstroke_left = drawSchriftzug11(x, y)
        connection = drawSchriftzug9(x, y, "b", outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A")

    if version == "midH":
        downstroke_left = drawSchriftzug11(x, y)
        connection = drawSchriftzug9(x, y, "b", outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H", 5.8)
   
    if version == "fina":
        downstroke_left = drawSchriftzug11(x, y)
        connection = drawSchriftzug9(x, y, "b", outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")    

    pkt_Auslauf = bend_right.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KurrentKanzleiGatE_h += downstroke_left + connection + bend_right
    trans_scale(KurrentKanzleiGatE_h, valueToMoveGlyph)

    if version == "isol" or version == "fina":
        return KurrentKanzleiGatE_h, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    return KurrentKanzleiGatE_h

    
def drawKurrentKanzleiGatE_h_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatE_h_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 4, HSL_size=2, HSL_start=22, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
          
    KurrentKanzleiGatE_h_thinStroke += Auslauf
    drawPath(KurrentKanzleiGatE_h_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatE_h_thinStroke) 
    trans_scale(KurrentKanzleiGatE_h_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_h_thinStroke
    
    
    
    
        

def drawKurrentKanzleiGatE_i(x, y, version="isol"):      

    KurrentKanzleiGatE_i = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)

    ### Hilfslinie für i-Punkt
    #drawGrundelementG(*Grund_a, 3) 
    
    if version == "isol":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=1, outstrokeLen=1)

    if version == "iniA":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "A", 3.5)
        KurrentKanzleiGatE_i += constroke
        
    if version == "iniH":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "H")
        KurrentKanzleiGatE_i += constroke
    
    if version == "midA":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "A", 3.5)
        KurrentKanzleiGatE_i += constroke
        
    if version == "midH":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "H")
        KurrentKanzleiGatE_i += constroke
        
    if version == "fina":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=0, outstrokeLen=1)

  
    iPunkt = drawSchriftzug3(x+1.5, y+3, "a", instroke=False)

    KurrentKanzleiGatE_i += downstroke + iPunkt
    trans_scale(KurrentKanzleiGatE_i, valueToMoveGlyph)
    return KurrentKanzleiGatE_i
        
    
        

        
    
def drawKurrentKanzleiGatE_j(x, y, version="isol"):      
    
    KurrentKanzleiGatE_j = BezierPath()
    
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
            
    if version == "isol":
        instroke = drawInstroke((x-1)*modul_width, (y+0.5)*modul_height, 1)
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="fina")
        KurrentKanzleiGatE_j += instroke
        pkt_Auslauf = main_stroke.points[19]
        #text("pkt_Auslauf", pkt_Auslauf)

    if version == "iniA":
        instroke = drawInstroke((x-1)*modul_width, (y+0.5)*modul_height, 1)
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_A")
        KurrentKanzleiGatE_j += instroke

    if version == "iniH":
        instroke = drawInstroke((x-1)*modul_width, (y+0.5)*modul_height, 1)
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_H")
        KurrentKanzleiGatE_j += instroke
        
    if version == "midA":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_A")

    if version == "midH":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_H")
        
    if version == "fina":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="fina")
        pkt_Auslauf = main_stroke.points[19]
        #text("pkt_Auslauf", pkt_Auslauf)
            
    Punkt = drawGrundelementE((x+0.5)*modul_width, (y+3.5)*modul_height)
        
    KurrentKanzleiGatE_j += main_stroke + Punkt
    trans_scale(KurrentKanzleiGatE_j, valueToMoveGlyph)   

    if version == "isol" or version == "fina":
        return KurrentKanzleiGatE_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    return KurrentKanzleiGatE_j

    
    
    


def drawKurrentKanzleiGatE_k(x, y, version="isol"):
    
    KurrentKanzleiGatE_k = BezierPath()
    
    # Raute für Positionierung Querstrich
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x-1, y)
    # Raute für Positionierung Verbindungsstrich Fuß
    Raute_a_btm, Raute_b_btm, Raute_c_btm, Raute_d_btm = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)


    
    if version == "isol":
        main_stroke = drawSchriftzug18(x, y, outstroke=True)
        #hook = drawHalbbogen1((x+0.5)*modul_width, (y+2.75)*modul_width, 2)
        hook = drawSchriftzug4(x+1.25, y+2, "a", instroke=True, outstroke=True)
        Querstrich = drawGrundelementB(*Raute_a, 3)

    if version == "iniA":
        main_stroke = drawSchriftzug18(x, y)
        #hook = drawHalbbogen1((x+0.5)*modul_width, (y+2.75)*modul_width, 2)
        hook = drawSchriftzug4(x+1.25, y+2, "a", instroke=True, outstroke=True)
        constroke = drawConstroke(*Raute_d_btm, "A")
        Querstrich = drawGrundelementB(*Raute_a, 3)
        KurrentKanzleiGatE_k += constroke
        
    if version == "iniH":
        main_stroke = drawSchriftzug18(x, y)
        #hook = drawHalbbogen1((x+0.5)*modul_width, (y+2.75)*modul_width, 2)
        hook = drawSchriftzug4(x+1.25, y+2, "a", instroke=True, outstroke=True)
        constroke = drawConstroke(*Raute_d_btm, "H")
        Querstrich = drawGrundelementB(*Raute_a, 3)
        KurrentKanzleiGatE_k += constroke
    
    if version == "midA":
        main_stroke = drawSchriftzug13(x, y)
        constroke = drawConstroke(*main_stroke.points[3], "A")
        hook = drawSchriftzug4(x+1.75, y+2, "a", instroke=True, outstroke=True)
        Querstrich = drawGrundelementB(*Raute_a, 3)
        KurrentKanzleiGatE_k += constroke
        
    if version == "midH":
        main_stroke = drawSchriftzug13(x, y)
        constroke = drawConstroke(*main_stroke.points[3], "H")
        hook = drawSchriftzug4(x+1.75, y+2, "a", instroke=True, outstroke=True)
        Querstrich = drawGrundelementB(*Raute_a, 3)
        KurrentKanzleiGatE_k += constroke
        
    if version == "fina":
        main_stroke = drawSchriftzug13(x, y)
        hook = drawSchriftzug4(x+1.75, y+2, "a", instroke=True, outstroke=True)
        Querstrich = drawGrundelementB(*Raute_a, 3)
        outstroke = drawOutstroke(*main_stroke.points[3], 1)
        KurrentKanzleiGatE_k += outstroke


    KurrentKanzleiGatE_k += main_stroke + hook + Querstrich
    trans_scale(KurrentKanzleiGatE_k, valueToMoveGlyph)
    return KurrentKanzleiGatE_k
    
    
    
    
    
    
        
        
def drawKurrentKanzleiGatE_l(x, y, version="isol"):    
    
    KurrentKanzleiGatE_l = BezierPath()
    
    if version == "isol":
        pos_stroke = drawPosStroke(x+0.5, y, 0.5)
        downstroke_left = drawSchriftzug17(x, y)
        outstroke = drawOutstroke(*downstroke_left.points[-1],1)
        KurrentKanzleiGatE_l += pos_stroke + outstroke

    if version == "iniA":
        pos_stroke = drawPosStroke(x+0.5, y, 0.5)
        downstroke_left = drawSchriftzug17(x, y)
        constroke = drawConstroke(*downstroke_left.points[-1], "A", 4)
        KurrentKanzleiGatE_l += pos_stroke + constroke
    
    if version == "iniH":
        pos_stroke = drawPosStroke(x+0.5, y, 0.5)
        downstroke_left = drawSchriftzug17(x, y)
        constroke = drawConstroke(*downstroke_left.points[-1], "H", 4.5)
        KurrentKanzleiGatE_l += pos_stroke + constroke

    if version == "midA":
        downstroke_left = drawSchriftzug17(x, y, version="con")
        constroke = drawConstroke(*downstroke_left.points[-1], "A", 4)
        KurrentKanzleiGatE_l += constroke

    if version == "midH":
        downstroke_left = drawSchriftzug17(x, y, version="con")
        constroke = drawConstroke(*downstroke_left.points[-1], "H", 4.5)
        KurrentKanzleiGatE_l += constroke
        
    if version == "fina":
        downstroke_left = drawSchriftzug17(x, y, version="con")
        outstroke = drawOutstroke(*downstroke_left.points[-1],1)
        KurrentKanzleiGatE_l += outstroke  

    KurrentKanzleiGatE_l += downstroke_left
    trans_scale(KurrentKanzleiGatE_l, valueToMoveGlyph)
    return KurrentKanzleiGatE_l
    
    
    
        
        
def drawKurrentKanzleiGatE_m(x, y, version="fina"):
    
    KurrentKanzleiGatE_m = BezierPath()
    
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+7, y, "c", instrokeLen=1, outstrokeLen=1)
               
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3)
        KurrentKanzleiGatE_m += constroke

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatE_m += constroke

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3)
        KurrentKanzleiGatE_m += constroke

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatE_m += constroke
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+7, y, "b", instrokeLen=1, outstrokeLen=1)

    KurrentKanzleiGatE_m += downstroke_left + downstroke_mid + downstroke_right
    trans_scale(KurrentKanzleiGatE_m, valueToMoveGlyph)
    return KurrentKanzleiGatE_m






def drawKurrentKanzleiGatE_n(x, y, version="isol"):
    
    KurrentKanzleiGatE_n = BezierPath()
    
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "c", instrokeLen=1, outstrokeLen=1)
    
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3)
        KurrentKanzleiGatE_n += constroke

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatE_n += constroke

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3)
        KurrentKanzleiGatE_n += constroke

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatE_n += constroke
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "c", instrokeLen=1, outstrokeLen=1)

    KurrentKanzleiGatE_n += downstroke_left + downstroke_right
    trans_scale(KurrentKanzleiGatE_n, valueToMoveGlyph)
    return KurrentKanzleiGatE_n
    






def drawKurrentKanzleiGatE_o(x, y, version="isol"):
        
    KurrentKanzleiGatE_o = BezierPath()

    Bogen_links = drawSchriftzug21(x, y, version="standard", instrokeLen=0, outstrokeLen=1.3)
    
    ###  rechts oben am o müsste es laut TAB 51 noch weiter raus gehen. Aber das passt dann nicht mit
    ###  den anderen Buchstaben, wie zB n. Jetzt Zwischenlösung.
    #Bogen_links = drawSchriftzug21(x, y, version="for o", instrokeLen=0, outstrokeLen=1.3)
    
    Bogen_rechts = drawSchriftzug26(x+3, y, instrokeLen=0)

    if version == "isol":
        extra = drawGrundelementE(*Bogen_rechts.points[0], 0.5, "unten")
        KurrentKanzleiGatE_o += extra

    if version == "iniA":
        hook = drawSchriftzug6(x+3.4, y+0.2, "b")
        KurrentKanzleiGatE_o += hook

    if version == "iniH":
        #hook = drawHook_o(*Bogen_rechts.points[0])
        hook = drawHook_o(*Bogen_rechts.points[0], outstrokeLen=0.6)
        KurrentKanzleiGatE_o += hook

    if version == "midA":
        #hook = drawHook_o(*Bogen_rechts.points[0])
        hook = drawSchriftzug6(x+3.4, y+0.2, "b")
        KurrentKanzleiGatE_o += hook

    if version == "midH":
        #hook = drawHook_o(*Bogen_rechts.points[0])
        hook = drawHook_o(*Bogen_rechts.points[0], outstrokeLen=0.6)
        KurrentKanzleiGatE_o += hook
   
    if version == "fina":
        #hook = drawHook_o(*Bogen_rechts.points[0])
        hook = drawSchriftzug6(x+3.4, y+0.2, "b")
        KurrentKanzleiGatE_o += hook

    KurrentKanzleiGatE_o += Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatE_o, valueToMoveGlyph)   
    return KurrentKanzleiGatE_o
    
   
    
def drawKurrentKanzleiGatE_odieresis(x, y, version="isol"):     
    
    KurrentKanzleiGatE_odieresis = BezierPath()
       
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    dieresis = drawDieresis(x+2, y)
    trans_scale(dieresis, valueToMoveGlyph)
        
    if version == "isol":
        glyph_o = drawKurrentKanzleiGatE_o(x, y, version="isol")
  
    if version == "iniA":
        glyph_o = drawKurrentKanzleiGatE_o(x, y, version="iniA")

    if version == "iniH":
        glyph_o = drawKurrentKanzleiGatE_o(x, y, version="iniH")
        
    if version == "midA":
        glyph_o = drawKurrentKanzleiGatE_o(x, y, version="midA")

    if version == "midH":
        glyph_o = drawKurrentKanzleiGatE_o(x, y, version="midH")
        
    if version == "fina":
        glyph_o = drawKurrentKanzleiGatE_o(x, y, version="fina")
        
    KurrentKanzleiGatE_odieresis += glyph_o + dieresis
    return KurrentKanzleiGatE_odieresis
    
    


 
    
    
    

    
def drawKurrentKanzleiGatE_p(x, y, version="isol"):
        
    KurrentKanzleiGatE_p = BezierPath()
    
    if version == "isol":
        glyph_v = drawKurrentKanzleiGatE_v(x, y, version="isol")
    
    if version == "iniA":
        glyph_v = drawKurrentKanzleiGatE_v(x, y, version="iniA")
        
    if version == "iniH":
        glyph_v = drawKurrentKanzleiGatE_v(x, y, version="iniH")

    if version == "midA":
        glyph_v = drawKurrentKanzleiGatE_v(x, y, version="midA")

    if version == "midH":
        glyph_v = drawKurrentKanzleiGatE_v(x, y, version="midH")

    if version == "fina":
        glyph_v = drawKurrentKanzleiGatE_v(x, y, version="fina")

    _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, baseline+1)
    pkt_Ausstrich = Raute_d
    #text("pkt_Ausstrich", pkt_Ausstrich)

    KurrentKanzleiGatE_p += glyph_v
    return KurrentKanzleiGatE_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
    
def drawKurrentKanzleiGatE_q(x, y, version="isol"):     
    
    KurrentKanzleiGatE_q = BezierPath()
        
    Grund_q, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    
    Bogen_links = drawSchriftzug25(x, y, outstrokeLen=1)
    Connection_up = drawConCurve_up(x, y)  
    Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=0)

    if version == "isol":     # isol und fina sind gleich
        pass
        
    if version == "iniA":    # ini und med sind gleich
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 4)
        KurrentKanzleiGatE_q += constroke

    if version == "iniH":    # ini und med sind gleich
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatE_q += constroke
        
    pkt_Ausstrich = Strich_rechts.points[-1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
        
    KurrentKanzleiGatE_q += Bogen_links + Connection_up + Strich_rechts
    trans_scale(KurrentKanzleiGatE_q, valueToMoveGlyph)
    return KurrentKanzleiGatE_q, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    






def drawKurrentKanzleiGatE_r(x, y, version="isol"):       

    KurrentKanzleiGatE_r = BezierPath()

    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=1, outstrokeLen=0.5)
        hook = drawSchriftzug6(x+2.9, y+0.2, "b")

    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=1, outstrokeLen=0.5)
        hook = drawSchriftzug6(x+2.9, y+0.2, "b")

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=1, outstrokeLen=0.5)
        hook = drawSchriftzug6(x+2.9, y+0.2, "b")

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=0, outstrokeLen=0.5)
        hook = drawGrundelementE((x+1.5)*modul_width, (y+0.5)*modul_height, 1, "oben", outstrokeLen=1)

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=0, outstrokeLen=0.5)
        hook = drawGrundelementE((x+1.5)*modul_width, (y+0.5)*modul_height, 1, "oben", outstrokeLen=1)
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=0, outstrokeLen=0.5)
        hook = drawSchriftzug6(x+2.9, y+0.2, "b")

    KurrentKanzleiGatE_r += downstroke_left + hook
    trans_scale(KurrentKanzleiGatE_r, valueToMoveGlyph)
    return KurrentKanzleiGatE_r




def drawKurrentKanzleiGatE_rc(x, y):       
    
    r_links = drawSchriftzug8(x, y, instrokeLen=1, outstrokeLen=2, version="c")
    
    KurrentKanzleiGatE_rc = r_links
    trans_scale(KurrentKanzleiGatE_rc, valueToMoveGlyph) 
    
    letter_c = drawKurrentKanzleiGatE_c(x+3, y)
    KurrentKanzleiGatE_rc += letter_c
    return KurrentKanzleiGatE_rc

    
    
    
    
    
    
def drawKurrentKanzleiGatE_s(x, y, version="isol"):  
    #x +=2
    if version == "isol":
        final_s = drawSchriftzug29(x, y, version="isol")
        
    if version == "iniA":
        final_s = drawSchriftzug29(x, y, version="fina")

    if version == "iniH":
        final_s = drawSchriftzug29(x, y, version="iniH")

    if version == "midA":
        final_s = drawSchriftzug29(x, y, version="fina")

    if version == "midH":
        final_s = drawSchriftzug29(x, y, version="midH")
   
    if version == "fina":
        final_s = drawSchriftzug29(x, y, version="fina")
    
    KurrentKanzleiGatE_s = final_s
    trans_scale(KurrentKanzleiGatE_s, valueToMoveGlyph)
    return KurrentKanzleiGatE_s
    
      
  

    

def drawKurrentKanzleiGatE_longs(x, y, version="isol"):
    
    KurrentKanzleiGatE_longs = BezierPath()
    
    Hauptstrich = drawSchriftzug13(x, y, instrokeLen=1.5)
    Signatur = drawSchriftzug23(x+4.5, y+8.5)
    pkt_Ausstrich = Hauptstrich.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    if version == "isol":
        posStroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatE_longs += posStroke
      
    if version == "iniA":
        posStroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich.points[3], "A", 3)
        KurrentKanzleiGatE_longs += posStroke + constroke  
          
    if version == "iniH":
        posStroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich.points[3], "H")
        KurrentKanzleiGatE_longs += Hauptstrich + Signatur + posStroke + constroke 
        
    if version == "midA":
        constroke = drawConstroke(*Hauptstrich.points[3], "A", 3)
        KurrentKanzleiGatE_longs += Hauptstrich + Signatur + constroke

    if version == "midH":
        constroke = drawConstroke(*Hauptstrich.points[3], "H")
        KurrentKanzleiGatE_longs += constroke

    if version == "fina":    ### es gibt eigentlich kein langes s am Ende, aber als Grundlage für f benötigt
        pass
        
    if version == "pure":    ### für Grundlage f
        pass

    KurrentKanzleiGatE_longs += Hauptstrich + Signatur
    trans_scale(KurrentKanzleiGatE_longs, valueToMoveGlyph)
    return KurrentKanzleiGatE_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
    
    
def drawKurrentKanzleiGatE_germandbls(x, y, version="isol"):

    KurrentKanzleiGatE_germandbls = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1.6, version="germandbls")
    pkt_Ausstrich = Hauptstrich_links.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Bogen_rechts = drawSchriftzug_germandbls(x+3.25, y)
    pkt_Auslauf = Bogen_rechts.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
   
    if version == "isol":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatE_germandbls += instroke

    if version == "iniA":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatE_germandbls += instroke
          
    if version == "iniH":       
        instroke = drawPosStroke(x, y-0.25)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+6, y-0.5)
        constroke = drawConstroke(*Raute_a, "A", 1)
        KurrentKanzleiGatE_germandbls += instroke + constroke

    if version == "midA":
        pass
        
    if version == "midH":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+6, y-0.5)
        constroke = drawConstroke(*Raute_a, "A", 1)
        KurrentKanzleiGatE_germandbls += constroke         

    if version == "fina":
        pass        
    
    KurrentKanzleiGatE_germandbls += Hauptstrich_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatE_germandbls, valueToMoveGlyph)
    return KurrentKanzleiGatE_germandbls, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
def drawKurrentKanzleiGatE_germandbls_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentKanzleiGatE_germandbls_Endspitze = BezierPath()   
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 5, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
    #KurrentKanzleiGatE_germandbls_Endspitze.oval(Auslauf.points[-1][0], Auslauf.points[-1][1]-modul_height*0.48, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0], Auslauf.points[-1][1]-modul_height*0.48)
    
    KurrentKanzleiGatE_germandbls_Endspitze += Auslauf + Zierstrich + Endpunkt
    drawPath(KurrentKanzleiGatE_germandbls_Endspitze)
    trans_thinStroke_down_left(KurrentKanzleiGatE_germandbls_Endspitze) 
    trans_scale(KurrentKanzleiGatE_germandbls_Endspitze, valueToMoveGlyph)
    return KurrentKanzleiGatE_germandbls_Endspitze 
    
    
    


    
    
def drawKurrentKanzleiGatE_longs_longs(x, y, version="isol"):
    
    #y-=1
    KurrentKanzleiGatE_longs_longs = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1)
    pkt_Ausstrich = Hauptstrich_links.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    Hauptstrich_rechts = drawSchriftzug_longs_longs_2nd(x+0.25, y)
    
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+6.25, y+4)
    con = drawGrundelementA(*Raute_a, 2.5, "down")
    Signatur = drawSchriftteil10(*Raute_a)

    if version == "isol":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatE_longs_longs += instroke
      
    if version == "iniA":
        instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5)
        KurrentKanzleiGatE_longs_longs += instroke + constroke
          
    if version == "iniH":       
        instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatE_longs_longs +=instroke + constroke

    if version == "midA":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5)
        KurrentKanzleiGatE_longs_longs += constroke

    if version == "midH":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatE_longs_longs += constroke
 
    KurrentKanzleiGatE_longs_longs += Hauptstrich_links + Hauptstrich_rechts + con + Signatur
    drawPath(KurrentKanzleiGatE_longs_longs)    
    trans_scale(KurrentKanzleiGatE_longs_longs, valueToMoveGlyph)
    return KurrentKanzleiGatE_longs_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
def drawKurrentKanzleiGatE_longs_t(x, y, version="isol"):

    KurrentKanzleiGatE_longs_t = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1)
    pkt_Ausstrich = Hauptstrich_links.points[-15]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+2.85, y+3.625)
    Signatur = drawSchriftteil12(Raute_a[0]+modul_width, Raute_a[1])
    
    if version == "isol":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatE_longs_t += instroke
        glyph_t = drawSchriftzug19(x+3.7, y, version="short", outstrokeLen=0.5)

    if version == "iniA":
        instroke = drawPosStroke(x, y-0.25)
        glyph_t = drawSchriftzug19(x+3.7, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "A", 3)
        KurrentKanzleiGatE_longs_t += instroke + constroke

    if version == "iniH":       
        instroke = drawPosStroke(x, y-0.25)
        glyph_t = drawSchriftzug19(x+3.7, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "H")
        KurrentKanzleiGatE_longs_t += instroke + constroke

    if version == "midA":
        glyph_t = drawSchriftzug19(x+3.7, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "A", 3)
        KurrentKanzleiGatE_longs_t += constroke
        
    if version == "midH":
        glyph_t = drawSchriftzug19(x+3.7, y, version="short", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "H")
        KurrentKanzleiGatE_longs_t += constroke
        
    if version == "fina":
        glyph_t = drawSchriftzug19(x+3.7, y, version="short", outstrokeLen=0.5)
    
    KurrentKanzleiGatE_longs_t.line(Hauptstrich_links.points[-15], glyph_t.points[2])
    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x+3, y)    
    Querstrich = drawGrundelementB(*Grund_a, 2.5)
    
    KurrentKanzleiGatE_longs_t += Hauptstrich_links + Signatur + glyph_t + Querstrich
    drawPath(KurrentKanzleiGatE_longs_t)   
    trans_scale(KurrentKanzleiGatE_longs_t, valueToMoveGlyph)   
    return KurrentKanzleiGatE_longs_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
   

 
    



 
    

    
    
    
    
    
    
def drawKurrentKanzleiGatE_t(x, y, version="isol"):    
    
    KurrentKanzleiGatE_t = BezierPath()
    
    if version == "isol":
        #main_stroke = drawSchriftzug19(x, y, "b", outstroke=True)
        main_stroke = drawSchriftzug12(x, y, instroke=True, outstroke=True)
        Querstrich = drawGrundelementB((x-2.25)*modul_width, (y+0.5)*modul_width, 3)

    if version == "iniA":   # auch midA
        main_stroke = drawSchriftzug12(x, y, instroke=True, outstroke=True)
        Querstrich = drawGrundelementB((x-2.25)*modul_width, (y+0.5)*modul_width, 3)
        constroke = drawConstroke((x-1)*modul_width, (y-4)*modul_height, "A")
        KurrentKanzleiGatE_t += constroke
        
    if version == "iniH":    # auch midH
        main_stroke = drawSchriftzug12(x, y, instroke=True, outstroke=False)
        Querstrich = drawGrundelementB((x-2.25)*modul_width, (y+0.5)*modul_width, 3)
        constroke = drawConstroke((x-1)*modul_width, (y-4)*modul_height, "H")
        KurrentKanzleiGatE_t += constroke
        
    if version == "fina":
        main_stroke = drawSchriftzug12(x, y, instroke=True, outstroke=True)
        Querstrich = drawGrundelementB((x-2.25)*modul_width, (y+0.5)*modul_width, 3)

    KurrentKanzleiGatE_t += main_stroke + Querstrich
    trans_scale(KurrentKanzleiGatE_t, valueToMoveGlyph)
    return KurrentKanzleiGatE_t
    
    
    
    
    
    
    

def drawKurrentKanzleiGatE_u(x, y, version="isol"):
    
    KurrentKanzleiGatE_u = BezierPath()
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
        
    if version == "isol":
        Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+0.4, y)
        Strich_rechts = drawSchriftzug7(x+3.4, y, "a", instrokeLen=0, outstrokeLen=1)
  
    if version == "iniA":  
        Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+0.4, y)
        Strich_rechts = drawSchriftzug7(x+3.4, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 4)
        KurrentKanzleiGatE_u += constroke

    if version == "iniH": 
        Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+0.4, y)
        Strich_rechts = drawSchriftzug7(x+3.4, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatE_u += constroke
        
    if version == "midA":  
        Strich_links = drawSchriftzug10(x, y, instrokeLen=0, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+0.4, y)
        Strich_rechts = drawSchriftzug7(x+3.4, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 4)
        KurrentKanzleiGatE_u += constroke

    if version == "midH": 
        Strich_links = drawSchriftzug10(x, y, instrokeLen=0, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+0.4, y)
        Strich_rechts = drawSchriftzug7(x+3.4, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatE_u += constroke

    if version == "fina":
        Strich_links = drawSchriftzug10(x, y, instrokeLen=0, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+0.4, y)
        Strich_rechts = drawSchriftzug7(x+3.4, y, "a", instrokeLen=0, outstrokeLen=1)
        
    KurrentKanzleiGatE_u += Strich_links + Connection_up + Strich_rechts
    trans_scale(KurrentKanzleiGatE_u, valueToMoveGlyph)
    return KurrentKanzleiGatE_u
    

    
def drawKurrentKanzleiGatE_udieresis(x, y, version="isol"):     
    
    KurrentKanzleiGatE_udieresis = BezierPath()
       
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    dieresis = drawDieresis(x+2, y)
    trans_scale(dieresis, valueToMoveGlyph)
        
    if version == "isol":
        glyph_u = drawKurrentKanzleiGatE_u(x, y, version="isol")
  
    if version == "iniA":
        glyph_u = drawKurrentKanzleiGatE_u(x, y, version="iniA")

    if version == "iniH":
        glyph_u = drawKurrentKanzleiGatE_u(x, y, version="iniH")
        
    if version == "midA":
        glyph_u = drawKurrentKanzleiGatE_u(x, y, version="midA")

    if version == "midH":
        glyph_u = drawKurrentKanzleiGatE_u(x, y, version="midH")
        
    if version == "fina":
        glyph_u = drawKurrentKanzleiGatE_u(x, y, version="fina")
        
    KurrentKanzleiGatE_udieresis += glyph_u + dieresis
    return KurrentKanzleiGatE_udieresis
    
    
    
    
    
    
    
    
    
    
def drawKurrentKanzleiGatE_v(x, y, version="isol"):
    
    KurrentKanzleiGatE_v = BezierPath()
    
    ### Für diesen Buchstaben habe ich einen extra Downbend entworfen: b-2 der enger ist und mit 2 Abstand anliegt.
    
    # Raute Positionierung Verbindungsstrich A (weil weiter innen anfängt)
    Raute_a_forA, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2.5, y-3.5)

    # Raute Positionierung Verbindungsstrich H (weil manuell hingepflanzt – temporär)
    Raute_a_forH, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y-3.25)
            
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1, outstrokeLen=1.5)

    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1,  outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forA, "A", 3)
        KurrentKanzleiGatE_v += constroke

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1,  outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forH, "H", 3.1)
        KurrentKanzleiGatE_v += constroke

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0, outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forA, "A", 3)
        KurrentKanzleiGatE_v += constroke
        
    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0, outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forH, "H", 3.1)
        KurrentKanzleiGatE_v += constroke
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0, outstrokeLen=0)
    
    bend_right = drawSchriftzug26(x+3, y)

    KurrentKanzleiGatE_v += downstroke_left + bend_right
    trans_scale(KurrentKanzleiGatE_v, valueToMoveGlyph)
    return KurrentKanzleiGatE_v

    
    
    

    
def drawKurrentKanzleiGatE_w(x, y, version="isol"):
    
    KurrentKanzleiGatE_w = BezierPath()
    
    # Raute Positionierung Verbindungsstrich A (weil weiter innen anfängt)
    Raute_a_forA, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6, y-3.5)

    # Raute Positionierung Verbindungsstrich H (weil manuell hingepflanzt – temporär)
    Raute_a_forH, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y-3.25)
    
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1.5)
    
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forA, "A", 3)
        KurrentKanzleiGatE_w += constroke       

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forH, "H", 3.1)
        KurrentKanzleiGatE_w += constroke 

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forA, "A", 3)
        KurrentKanzleiGatE_w += constroke        

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1.5)
        constroke = drawConstroke(*Raute_a_forH, "H", 3.1)
        KurrentKanzleiGatE_w += constroke
        
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1.5)


    downstroke_mid = drawSchriftzug9(x+3.5, y, "b-2", instrokeLen=0, outstrokeLen=1.5)
    bend_right = drawSchriftzug26(x+6.5, y)
        
    KurrentKanzleiGatE_w += downstroke_left + downstroke_mid + bend_right
    trans_scale(KurrentKanzleiGatE_w, valueToMoveGlyph)
    return KurrentKanzleiGatE_w
    
    
    
    
    
    
    
    
def drawKurrentKanzleiGatE_x(x, y, version="isol"):       

    KurrentKanzleiGatE_x = BezierPath()

    if version == "isol":
        glyph_r = drawKurrentKanzleiGatE_r(x, y, version="isol")

    if version == "iniA":
        glyph_r = drawKurrentKanzleiGatE_r(x, y, version="iniA")

    if version == "iniH":
        glyph_r = drawKurrentKanzleiGatE_r(x, y, version="iniH")

    if version == "midA":
        glyph_r = drawKurrentKanzleiGatE_r(x, y, version="midA")

    if version == "midH":
        glyph_r = drawKurrentKanzleiGatE_r(x, y, version="midH")

    if version == "fina":
        glyph_r = drawKurrentKanzleiGatE_r(x, y, version="fina")

    tail = drawTail_x(x, y)
 
    trans_scale(tail, valueToMoveGlyph)
    drawPath(KurrentKanzleiGatE_x)
    KurrentKanzleiGatE_x += glyph_r + tail
    return KurrentKanzleiGatE_x
    
    
    



def drawKurrentKanzleiGatE_y(x, y, version="isol"):
    
    KurrentKanzleiGatE_y = BezierPath()
          
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")    #"fina"
   
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A")       

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1, outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H", 5.75) 

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A") 

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H", 5.75)
  
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0, outstrokeLen=1.5)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")    #"fina"

    pkt_Auslauf = bend_right.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)

    KurrentKanzleiGatE_y += downstroke_left + bend_right
    trans_scale(KurrentKanzleiGatE_y, valueToMoveGlyph)
 
    if version == "isol" or version == "fina":
        return KurrentKanzleiGatE_y, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    return KurrentKanzleiGatE_y
    
    





def drawKurrentKanzleiGatE_z(x,y, version="isol"):
    
    KurrentKanzleiGatE_z = BezierPath()
        
    if version == "isol":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="begin")
        Bogen_unten = drawSchriftzug27(x+0.5, y-3)
    
    if version == "iniA":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="begin")
        Bogen_unten = drawSchriftzug_z_unten(x-1.5, y-2, "loop_A", 7)

    if version == "iniH":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="begin")
        Bogen_unten = drawSchriftzug_z_unten(x-1.5, y-2, "loop_H", 6.45)   

    if version == "midA":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="mid")
        Bogen_unten = drawSchriftzug_z_unten(x-1.5, y-2, "loop_A", 7)        
        
    if version == "midH":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="mid")
        Bogen_unten = drawSchriftzug_z_unten(x-1.5, y-2, "loop_H", 6.45)       
   
    if version == "fina":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="mid")
        Bogen_unten = drawSchriftzug27(x+0.5, y-3)

    pkt_Auslauf = Bogen_unten.points[-1]   ###  9
    #text("pkt_Auslauf", pkt_Auslauf)

    KurrentKanzleiGatE_z += Bogen_oben + Bogen_unten
    trans_scale(KurrentKanzleiGatE_z, valueToMoveGlyph)

    if version == "isol" or version == "fina":
        return KurrentKanzleiGatE_z, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    return KurrentKanzleiGatE_z
    
    
def drawKurrentKanzleiGatE_z_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatE_z_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 6, HSL_size=2, HSL_start=20, clockwise=False, inward=True)
    #drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 4, HSL_size=2, HSL_start=22, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
          
    KurrentKanzleiGatE_z_thinStroke += Auslauf
    drawPath(KurrentKanzleiGatE_z_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatE_z_thinStroke) 
    trans_scale(KurrentKanzleiGatE_z_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_z_thinStroke
    
    
    
      


    
    
    
#############################
### ab hier Interpunktion ###
#############################
    
    

    
    
def drawKurrentKanzleiGatE_period(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurBtm(offset, offsetDir, x, baseline+0.5)
    period = drawGrundelementE(*Grund_a)
        
    KurrentKanzleiGatE_period = period
    trans_scale(KurrentKanzleiGatE_period, valueToMoveGlyph) 
    return KurrentKanzleiGatE_period
    
    
    
    
    
def drawKurrentKanzleiGatE_colon(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    colon_top = drawGrundelementE(*Grund_a)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurBtm(offset, offsetDir, x+0.4, baseline+0.5)
    colon_btm = drawGrundelementE(*Grund_a)  
        
    KurrentKanzleiGatE_colon = colon_top + colon_btm
    trans_scale(KurrentKanzleiGatE_colon, valueToMoveGlyph)
    return KurrentKanzleiGatE_colon
    
    
    
    
    
def drawKurrentKanzleiGatE_semicolon(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    semicolon_top = drawGrundelementE(*Grund_a)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, baseline-0.25) 
    semicolon_btm = drawSchriftzug_semicolon_btm(*Grund_a)  

    KurrentKanzleiGatE_semicolon = semicolon_top + semicolon_btm
    trans_scale(KurrentKanzleiGatE_semicolon, valueToMoveGlyph)
    return KurrentKanzleiGatE_semicolon
    
    
    
    

def drawKurrentKanzleiGatE_quoteright(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+3) 
    quoteright = drawSchriftzug_semicolon_btm(*Grund_a) 
    
    KurrentKanzleiGatE_quoteright = quoteright
    trans_scale(KurrentKanzleiGatE_quoteright, valueToMoveGlyph)
    return KurrentKanzleiGatE_quoteright
    
    

    
def drawKurrentKanzleiGatE_quotesingle(x, y):

    KurrentKanzleiGatE_quotesingle = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.5)
    fake = drawGrundelementA(*Grund_c, 0.001)
    pkt_fake = fake.points[-1]
    
    KurrentKanzleiGatE_quotesingle = fake
    drawPath(KurrentKanzleiGatE_quotesingle)
    trans_scale(KurrentKanzleiGatE_quotesingle, valueToMoveGlyph)
    return KurrentKanzleiGatE_quotesingle, collections.namedtuple('dummy', 'pkt_fake')(pkt_fake)
    

def drawKurrentKanzleiGatE_quotesingle_thinstroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatE_quotesingle_thinstroke = BezierPath()

    quotesingle = drawSchneckenzug(*pass_from_thick.pkt_fake, LOWER_B, 3, HSL_size=2, HSL_start=6, clockwise=True, inward=False)
    #KurrentKanzleiGatE_quotesingle_thinstroke.oval(pass_from_thick.pkt_fake[0]-modul_width*1.11, pass_from_thick.pkt_fake[1]-modul_height*0.55, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(pass_from_thick.pkt_fake[0]-modul_width*1.11, pass_from_thick.pkt_fake[1]-modul_height*0.55)    
    
    KurrentKanzleiGatE_quotesingle_thinstroke += quotesingle + Endpunkt
    drawPath(KurrentKanzleiGatE_quotesingle_thinstroke)
    trans_scale(KurrentKanzleiGatE_quotesingle_thinstroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_quotesingle_thinstroke
    
    
    
    
    
    
def drawKurrentKanzleiGatE_comma(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    comma = drawGrundelementG(*Grund_a, 2, "down") 

    KurrentKanzleiGatE_comma = comma
    trans_scale(KurrentKanzleiGatE_comma, valueToMoveGlyph)
    return KurrentKanzleiGatE_comma
    
    

    
def drawKurrentKanzleiGatE_endash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, baseline-0.5)
    endash = drawGrundelementB(*Grund_a, 2) 

    KurrentKanzleiGatE_endash = endash
    trans_scale(KurrentKanzleiGatE_endash, valueToMoveGlyph)
    return KurrentKanzleiGatE_endash
    
    
    
    
    
def drawKurrentKanzleiGatE_hyphen(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    hyphen_top = drawGrundelementH(*Grund_b, 2, "down") 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-2.5)
    hyphen_btm = drawGrundelementH(*Grund_b, 2, "down") 
    
    KurrentKanzleiGatE_hyphen = hyphen_top + hyphen_btm
    trans_scale(KurrentKanzleiGatE_hyphen, valueToMoveGlyph)
    return KurrentKanzleiGatE_hyphen
     
    
    
    
def drawKurrentKanzleiGatE_exclam(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    exclam = drawGrundelementF(*Grund_a, 6.75) 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurBtm(offset, offsetDir, x, baseline+0.5)
    period = drawGrundelementE(*Grund_a)

    KurrentKanzleiGatE_exclam = exclam + period
    trans_scale(KurrentKanzleiGatE_exclam, valueToMoveGlyph)
    return KurrentKanzleiGatE_exclam
    
    



def drawKurrentKanzleiGatE_question(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurBtm(offset, offsetDir, x, baseline+0.5)
    period = drawGrundelementE(*Grund_a)

    question = drawSchriftzug_question(x, y) 

    KurrentKanzleiGatE_question = question + period
    trans_scale(KurrentKanzleiGatE_question, valueToMoveGlyph)
    return KurrentKanzleiGatE_question
    
    
    
    
    
    
    
    
    
    

###################################
######     ab hier Zahlen    ######
###################################




def drawKurrentKanzleiGatE_zero(x, y):
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y)

    KurrentKanzleiGatE_zero = Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatE_zero, valueToMoveGlyph)    
    return KurrentKanzleiGatE_zero
    


def drawKurrentKanzleiGatE_one(x, y):
    x += 2
    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurTop(offset, offsetDir, x, y+1.5)
    stem = drawGrundelementF(*Grund_a, 4.5)
    Signatur_oben = drawGrundelementE(*Grund_a, instrokeLen=0.5)
    pkt_Ausstrich = Grund_a[0]+modul_width*1.25, Grund_a[1]-modul_height*0.25
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    #Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x-0.5, baseline+1)
    transition = drawSchneckenzug(*stem.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=12.8, clockwise=True, inward=True)
    pkt_Auslauf = transition.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KurrentKanzleiGatE_one = stem + Signatur_oben + transition
    drawPath(KurrentKanzleiGatE_one)
    trans_scale(KurrentKanzleiGatE_one, valueToMoveGlyph)    
    return KurrentKanzleiGatE_one, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Ausstrich')(pkt_Auslauf, pkt_Ausstrich)
    

def drawKurrentKanzleiGatE_one_thinStroke(x, y, *, pass_from_thick=None): 
    
    KurrentKanzleiGatE_one_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    #KurrentKanzleiGatE_one_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
        
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 8.5)

    KurrentKanzleiGatE_one_thinStroke += Auslauf + Zierstrich + Endpunkt
    drawPath(KurrentKanzleiGatE_one_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatE_one_thinStroke) 
    trans_scale(KurrentKanzleiGatE_one_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_one_thinStroke
    
        
    

    



def drawKurrentKanzleiGatE_two(x, y):
     
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+1, y+1.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+1.5)

    instroke = drawInstroke(*Raute_a, 1)
    Top = drawSchriftteil10(*Raute_a)
    outstroke = drawOutstroke(*Top.points[-1], 0.55)
    
    Stem = drawGrundelementG(*Grund_a, length=6, direction="down")
    outstroke2 = drawOutstroke(*Stem.points[-1], 0.8)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.5, y-3.75)
    Schwung_unten = drawSchriftteil11(*Grund_c)
    
    outstroke3 = drawOutstroke(*Schwung_unten.points[-1], 0.75)

    KurrentKanzleiGatE_two = instroke + Top + Stem + Schwung_unten + outstroke + outstroke2 + outstroke3
    trans_scale(KurrentKanzleiGatE_two, valueToMoveGlyph)
    return KurrentKanzleiGatE_two  
    
    
        
    
    


def drawKurrentKanzleiGatE_three(x, y):

    KurrentKanzleiGatE_three = BezierPath()     
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    
    instroke = drawInstroke(*Grund_c, 1)
    Three_Top = drawSchriftteil10(*Grund_c)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    KurrentKanzleiGatE_three.line(Three_Top.points[-1], Three_Schwung.points[0])
    ### Verbindungswinkel müsste eigentlich G sein, oder???
    con = drawGrundelementG(*Three_Top.points[-1], 2, "down")
    
    drawPath(KurrentKanzleiGatE_three)

    KurrentKanzleiGatE_three += instroke + Three_Top + Three_Schwung
    trans_scale(KurrentKanzleiGatE_three, valueToMoveGlyph)
    return KurrentKanzleiGatE_three, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawKurrentKanzleiGatE_three_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatE_three_thinStroke = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, pass_from_thick.pkt_Auslauf[0]/modul_width, pass_from_thick.pkt_Auslauf[1]/modul_height)  
    Auslauf = drawSchneckenzug(*Grund_c, LOWER_E, 6, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 7)])
    #KurrentKanzleiGatE_three_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.05, Auslauf.points[-1][1]-modul_height*0.8, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.05, Auslauf.points[-1][1]-modul_height*0.8)
        
    KurrentKanzleiGatE_three_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentKanzleiGatE_three_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatE_three_thinStroke) 
    trans_scale(KurrentKanzleiGatE_three_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_three_thinStroke 
    
    
    
    
    
    
    
def drawKurrentKanzleiGatE_four(x, y):
    
    #x+=2
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-2.75, y-0.25)

    stem = drawSchriftzug3_Figures(x, y, instrokeLen=0)
    stroke_down = drawGrundelementH(*stem.points[-2], 2.75, "down")
    stroke_hor = drawGrundelementB(*Grund_d, 4)

    KurrentKanzleiGatE_four = stem + stroke_down + stroke_hor
    trans_scale(KurrentKanzleiGatE_four, valueToMoveGlyph)
    return KurrentKanzleiGatE_four   
    
    
    
    
def drawKurrentKanzleiGatE_five(x, y):

    #x -= 3
    KurrentKanzleiGatE_five = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3, y+1.5)
    Five_Top = drawSchriftteil10(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2, y+1)
    instroke = drawInstroke(*Raute_b, 1)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    ### Verbindungswinkel fragwürdig >>> siehe Zahl 3
    KurrentKanzleiGatE_five.line(Raute_a, Three_Schwung.points[0])
    KurrentKanzleiGatE_five += Five_Top + instroke + Three_Schwung
    
    drawPath(KurrentKanzleiGatE_five)
    trans_scale(KurrentKanzleiGatE_five, valueToMoveGlyph)
    return KurrentKanzleiGatE_five, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    


    
    
def drawKurrentKanzleiGatE_six(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    pkt_Auslauf_top = Bogen_links.points[0]
    #text("pkt_Auslauf_top", pkt_Auslauf_top) 
    
    Bogen_rechts = drawSchriftzug_six_BogenRechts(x, y)
    pkt_Auslauf_inner = Bogen_rechts.points[0]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner)
    
    KurrentKanzleiGatE_six = Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatE_six, valueToMoveGlyph)
    return KurrentKanzleiGatE_six, collections.namedtuple('dummy', 'pkt_Auslauf_top pkt_Auslauf_inner')(pkt_Auslauf_top, pkt_Auslauf_inner)
    

def drawKurrentKanzleiGatE_six_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatE_six_thinStroke = BezierPath() 
    
    Auslauf_top = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_top, UPPER_E, 4, HSL_size=0, HSL_start=13, clockwise=True, inward=False)
    
    ### wegen blödem Absatz
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.6, y-0.25)
    Auslauf_inner = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=14, clockwise=False, inward=False)
        
    trans_thinStroke_down_left(Auslauf_inner) 
    #trans_thinStroke_down_left(Auslauf_inner) 
    trans_thinStroke_up_right(Auslauf_top)
    
    KurrentKanzleiGatE_six_thinStroke += Auslauf_top + Auslauf_inner 
    drawPath(KurrentKanzleiGatE_six_thinStroke)
    trans_scale(KurrentKanzleiGatE_six_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_six_thinStroke    
    
    
    
    
def drawKurrentKanzleiGatE_seven(x, y):

    KurrentKanzleiGatE_seven = BezierPath()     
     
    Seven_Top = drawSchriftzug_three_Top(x, y)
    Seven_Stem = drawSchriftzug_seven_Stem(x+1, y)
    
    drawPath(KurrentKanzleiGatE_seven)

    KurrentKanzleiGatE_seven = Seven_Top + Seven_Stem
    trans_scale(KurrentKanzleiGatE_seven, valueToMoveGlyph)
    return KurrentKanzleiGatE_seven        
            
            


def drawKurrentKanzleiGatE_eight(x, y):

    KurrentKanzleiGatE_eight = drawSchriftzug_eight(x, y)
    trans_scale(KurrentKanzleiGatE_eight, valueToMoveGlyph)
    return KurrentKanzleiGatE_eight
    
    
    
    
      


def drawKurrentKanzleiGatE_nine(x, y):
    
    Bogen_links = drawSchriftzug_nine_BogenLinks(x, y)
    pkt_Auslauf_inner = Bogen_links.points[-1]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 
    
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y, version="nine")
    pkt_Auslauf_btm = Bogen_rechts.points[-1]
    #text("pkt_Auslauf_btm", pkt_Auslauf_btm) 
    
    KurrentKanzleiGatE_nine = Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatE_nine, valueToMoveGlyph)
    return KurrentKanzleiGatE_nine, collections.namedtuple('dummy', 'pkt_Auslauf_btm pkt_Auslauf_inner')(pkt_Auslauf_btm, pkt_Auslauf_inner)
    


def drawKurrentKanzleiGatE_nine_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatE_nine_thinStroke = BezierPath() 
    
    Auslauf_btm = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_btm, LOWER_E, 4, HSL_size=0, HSL_start=14, clockwise=True, inward=False)

    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, LOWER_E, 4, HSL_size=2, HSL_start=10, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf_btm) 
    #trans_thinStroke_up_right(Auslauf_inner)
    
    KurrentKanzleiGatE_nine_thinStroke += Auslauf_btm + Auslauf_inner 
    drawPath(KurrentKanzleiGatE_nine_thinStroke)
    trans_scale(KurrentKanzleiGatE_nine_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatE_nine_thinStroke



    
    
    
    
    
    
    
    


def drawKurrentKanzleiGatE_thinstroke_Straight(x, y, *, pass_from_thick=None):     
    
    KurrentKanzleiGatE_thinstroke_Straight = BezierPath()
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
          
    KurrentKanzleiGatE_thinstroke_Straight +=  Zierstrich
    trans_thinStroke_down_left(KurrentKanzleiGatE_thinstroke_Straight) 
    trans_scale(KurrentKanzleiGatE_thinstroke_Straight, valueToMoveGlyph)
    return KurrentKanzleiGatE_thinstroke_Straight
    
    
    
def drawKurrentKanzleiGatE_empty(x, y, *, pass_from_thick=None):     
    
    KurrentKanzleiGatE_empty = BezierPath()
    KurrentKanzleiGatE_empty.line((-100, -100), (-100, -110))
    return KurrentKanzleiGatE_empty


 
    
 
# _______________________________________________________ 
   
margin_Str = 60     
margin_Rnd = 60   
marginStr = 60  
marginRnd = -6
marginConA = 8
marginConH = -31
marginFlush = 30

# ____________ ab hier in RF ____________________________
    
font = CurrentFont()

drawFunctions = {
    
    #  'a' : [ drawKurrentKanzleiGatE_a, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'a.isol' : [ drawKurrentKanzleiGatE_a, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'a.iniA' : [ drawKurrentKanzleiGatE_a, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'a.iniH' : [ drawKurrentKanzleiGatE_a, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'a.midA' : [ drawKurrentKanzleiGatE_a, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'a.midH' : [ drawKurrentKanzleiGatE_a, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'a.fina' : [ drawKurrentKanzleiGatE_a, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'b' : [ drawKurrentKanzleiGatE_b, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'b.isol' : [ drawKurrentKanzleiGatE_b, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'b.iniA' : [ drawKurrentKanzleiGatE_b, [temp_x, temp_y, "iniA"], marginFlush, 30 ],
    # 'b.iniH' : [ drawKurrentKanzleiGatE_b, [temp_x, temp_y, "iniH"], marginFlush, 22 ],
    # 'b.midA' : [ drawKurrentKanzleiGatE_b, [temp_x, temp_y, "midA"], marginFlush, 30 ],
    # 'b.midH' : [ drawKurrentKanzleiGatE_b, [temp_x, temp_y, "midH"], marginFlush, 22 ],
    # 'b.fina' : [ drawKurrentKanzleiGatE_b, [temp_x, temp_y, "fina"], marginFlush, margin_Str ], 
    
    # 'c' : [ drawKurrentKanzleiGatE_c, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'c.isol' : [ drawKurrentKanzleiGatE_c, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'c.iniA' : [ drawKurrentKanzleiGatE_c, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'c.iniH' : [ drawKurrentKanzleiGatE_c, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'c.midA' : [ drawKurrentKanzleiGatE_c, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'c.midH' : [ drawKurrentKanzleiGatE_c, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'c.fina' : [ drawKurrentKanzleiGatE_c, [temp_x, temp_y, "isol"], marginRnd, margin_Str ], 
    
    # 'd' : [ drawKurrentKanzleiGatE_d, [temp_x, temp_y], margin_Rnd, margin_Rnd ],
    # 'd.isol' : [ drawKurrentKanzleiGatE_d, [temp_x, temp_y, "isol"], margin_Rnd, margin_Rnd ],
    # 'd.iniA' : [ drawKurrentKanzleiGatE_d, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA+48 ],
    # 'd.iniH' : [ drawKurrentKanzleiGatE_d, [temp_x, temp_y, "iniH"], marginStr, marginConH+120 ],
    # 'd.midA' : [ drawKurrentKanzleiGatE_d, [temp_x, temp_y, "iniA"], marginRnd, marginConA+48 ],
    # 'd.midH' : [ drawKurrentKanzleiGatE_d, [temp_x, temp_y, "iniH"], marginRnd, marginConH+120 ],
    # 'd.fina' : [ drawKurrentKanzleiGatE_d, [temp_x, temp_y, "fina"], marginRnd, marginRnd],
    
    # 'e' : [ drawKurrentKanzleiGatE_e, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'e.isol' : [ drawKurrentKanzleiGatE_e, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'e.iniA' : [ drawKurrentKanzleiGatE_e, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'e.iniH' : [ drawKurrentKanzleiGatE_e, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'e.midA' : [ drawKurrentKanzleiGatE_e, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'e.midH' : [ drawKurrentKanzleiGatE_e, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'e.fina' : [ drawKurrentKanzleiGatE_e, [temp_x, temp_y, "isol"], marginRnd, margin_Str ],
    
    # 'f' : [ drawKurrentKanzleiGatE_f, [temp_x, temp_y], marginStr, marginStr ],
    # 'f.isol' : [ drawKurrentKanzleiGatE_f, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'f.iniA' : [ drawKurrentKanzleiGatE_f, [temp_x, temp_y, "iniA"], margin_Str, marginConA-55 ],
    # 'f.iniH' : [ drawKurrentKanzleiGatE_f, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'f.midA' : [ drawKurrentKanzleiGatE_f, [temp_x, temp_y, "iniA"], -30, marginConA-55 ],
    # 'f.midH' : [ drawKurrentKanzleiGatE_f, [temp_x, temp_y, "iniH"], -30, marginConH ],
    # 'f.fina' : [ drawKurrentKanzleiGatE_f, [temp_x, temp_y, "isol"], -30, marginStr ],
    
    # 'g' : [ drawKurrentKanzleiGatE_g, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'g.isol' : [ drawKurrentKanzleiGatE_g, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'g.iniA' : [ drawKurrentKanzleiGatE_g, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'g.iniH' : [ drawKurrentKanzleiGatE_g, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'g.midA' : [ drawKurrentKanzleiGatE_g, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'g.midH' : [ drawKurrentKanzleiGatE_g, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'g.fina' : [ drawKurrentKanzleiGatE_g, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'h' : [ drawKurrentKanzleiGatE_h, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'h.isol' : [ drawKurrentKanzleiGatE_h, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'h.iniA' : [ drawKurrentKanzleiGatE_h, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'h.iniH' : [ drawKurrentKanzleiGatE_h, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'h.midA' : [ drawKurrentKanzleiGatE_h, [temp_x, temp_y, "midA"], 12, marginConA],
    # 'h.midH' : [ drawKurrentKanzleiGatE_h, [temp_x, temp_y, "midH"], 12, marginConH ],
    # 'h.fina' : [ drawKurrentKanzleiGatE_h, [temp_x, temp_y, "fina"], 12, margin_Str ],
    
    # 'i' : [ drawKurrentKanzleiGatE_i, [temp_x, temp_y], margin_Str, margin_Str-90],
    # 'i.isol' : [ drawKurrentKanzleiGatE_i, [temp_x, temp_y, "isol"], margin_Str, margin_Str-90],
    # 'i.iniA' : [ drawKurrentKanzleiGatE_i, [temp_x, temp_y, "iniA"], margin_Str, marginConA],
    # 'i.iniH' : [ drawKurrentKanzleiGatE_i, [temp_x, temp_y, "iniH"], marginStr, marginConH ],
    # 'i.midA' : [ drawKurrentKanzleiGatE_i, [temp_x, temp_y, "midA"], 30, marginConA ],
    # 'i.midH' : [ drawKurrentKanzleiGatE_i, [temp_x, temp_y, "midH"], 30, marginConH ],
    # 'i.fina' : [ drawKurrentKanzleiGatE_i, [temp_x, temp_y, "fina"], 30, margin_Str-90],
    
    # 'j' : [ drawKurrentKanzleiGatE_j, [temp_x, temp_y], margin_Str-60, margin_Str-90],
    # 'j.isol' : [ drawKurrentKanzleiGatE_j, [temp_x, temp_y, "isol"], margin_Str, margin_Str],
    # 'j.iniA' : [ drawKurrentKanzleiGatE_j, [temp_x, temp_y, "iniA"], margin_Str-80, marginConA ],
    # 'j.iniH' : [ drawKurrentKanzleiGatE_j, [temp_x, temp_y, "iniH"], margin_Str-80, marginConH ],
    # 'j.midA' : [ drawKurrentKanzleiGatE_j, [temp_x, temp_y, "midA"], margin_Str-167+43, marginConA ],
    # 'j.midH' : [ drawKurrentKanzleiGatE_j, [temp_x, temp_y, "midH"], margin_Str-162+22, marginConH ],
    # 'j.fina' : [ drawKurrentKanzleiGatE_j, [temp_x, temp_y, "fina"], margin_Str-73, margin_Str-90],
    
    # 'k' : [ drawKurrentKanzleiGatE_k, [temp_x, temp_y], margin_Str, margin_Str-120 ],
    # 'k.isol' : [ drawKurrentKanzleiGatE_k, [temp_x, temp_y, "isol"], margin_Str, margin_Str-120 ],
    # 'k.iniA' : [ drawKurrentKanzleiGatE_k, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'k.iniH' : [ drawKurrentKanzleiGatE_k, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'k.midA' : [ drawKurrentKanzleiGatE_k, [temp_x, temp_y, "midA"], margin_Str-90, marginConA ],
    # 'k.midH' : [ drawKurrentKanzleiGatE_k, [temp_x, temp_y, "midH"], margin_Str-90, marginConH ],
    # 'k.fina' : [ drawKurrentKanzleiGatE_k, [temp_x, temp_y, "fina"], margin_Str-90, margin_Str-100 ],
    
    # 'l' : [ drawKurrentKanzleiGatE_l, [temp_x, temp_y], margin_Str, margin_Str-20 ],
    # 'l.isol' : [ drawKurrentKanzleiGatE_l, [temp_x, temp_y, "isol"], margin_Str, margin_Str-20 ],
    # 'l.iniA' : [ drawKurrentKanzleiGatE_l, [temp_x, temp_y, "iniA"], margin_Str-20, marginConA ],
    # 'l.iniH' : [ drawKurrentKanzleiGatE_l, [temp_x, temp_y, "iniH"], margin_Str-20, marginConH ],
    # 'l.midA' : [ drawKurrentKanzleiGatE_l, [temp_x, temp_y, "midA"], margin_Str-30, marginConA ],
    # 'l.midH' : [ drawKurrentKanzleiGatE_l, [temp_x, temp_y, "midH"], margin_Str-30, marginConH ],
    # 'l.fina' : [ drawKurrentKanzleiGatE_l, [temp_x, temp_y, "fina"], margin_Str-30, margin_Str-20  ],
    
    # 'm' : [ drawKurrentKanzleiGatE_m, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'm.isol' : [ drawKurrentKanzleiGatE_m, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'm.iniA' : [ drawKurrentKanzleiGatE_m, [temp_x, temp_y, "iniA"], marginStr, marginConA ],
    # 'm.iniH' : [ drawKurrentKanzleiGatE_m, [temp_x, temp_y, "iniH"], marginStr, marginConH ],
    # 'm.midA' : [ drawKurrentKanzleiGatE_m, [temp_x, temp_y, "midA"], 13, marginConA ],
    # 'm.midH' : [ drawKurrentKanzleiGatE_m, [temp_x, temp_y, "midH"], 13, marginConH ],
    # 'm.fina' : [ drawKurrentKanzleiGatE_m, [temp_x, temp_y, "fina"], 13, margin_Str ],
    
    # 'n' : [ drawKurrentKanzleiGatE_n, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'n.isol' : [ drawKurrentKanzleiGatE_n, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'n.iniA' : [ drawKurrentKanzleiGatE_n, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'n.iniH' : [ drawKurrentKanzleiGatE_n, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'n.midA' : [ drawKurrentKanzleiGatE_n, [temp_x, temp_y, "midA"], 13, marginConA ],
    # 'n.midH' : [ drawKurrentKanzleiGatE_n, [temp_x, temp_y, "midH"], 13, marginConH ],
    # 'n.fina' : [ drawKurrentKanzleiGatE_n, [temp_x, temp_y, "fina"], 13, margin_Str ],

    # 'o' : [ drawKurrentKanzleiGatE_o, [temp_x, temp_y], margin_Rnd, margin_Rnd ],
    # 'o.isol' : [ drawKurrentKanzleiGatE_o, [temp_x, temp_y, "isol"], margin_Rnd, margin_Rnd ],
    # 'o.iniA' : [ drawKurrentKanzleiGatE_o, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA+40 ],
    # 'o.iniH' : [ drawKurrentKanzleiGatE_o, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'o.midA' : [ drawKurrentKanzleiGatE_o, [temp_x, temp_y, "midA"], marginRnd, marginConA+40 ],
    # 'o.midH' : [ drawKurrentKanzleiGatE_o, [temp_x, temp_y, "midH"], marginRnd, marginConH ],
    # 'o.fina' : [ drawKurrentKanzleiGatE_o, [temp_x, temp_y, "fina"], marginRnd, margin_Rnd ],
        
    # 'p' : [ drawKurrentKanzleiGatE_p, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'p.isol' : [ drawKurrentKanzleiGatE_p, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'p.iniA' : [ drawKurrentKanzleiGatE_p, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'p.iniH' : [ drawKurrentKanzleiGatE_p, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'p.midA' : [ drawKurrentKanzleiGatE_p, [temp_x, temp_y, "midA"], 5, marginConA ],
    # 'p.midH' : [ drawKurrentKanzleiGatE_p, [temp_x, temp_y, "midH"], 5, marginConH ],
    # 'p.fina' : [ drawKurrentKanzleiGatE_p, [temp_x, temp_y, "fina"], 5, margin_Rnd ],
    
    # 'q' : [ drawKurrentKanzleiGatE_q, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'q.isol' : [ drawKurrentKanzleiGatE_q, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'q.iniA' : [ drawKurrentKanzleiGatE_q, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'q.iniH' : [ drawKurrentKanzleiGatE_q, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'q.midA' : [ drawKurrentKanzleiGatE_q, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'q.midH' : [ drawKurrentKanzleiGatE_q, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'q.fina' : [ drawKurrentKanzleiGatE_q, [temp_x, temp_y, "isol"], marginRnd, margin_Str ],
    
    # 'r' : [ drawKurrentKanzleiGatE_r, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'r.isol' : [ drawKurrentKanzleiGatE_r, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'r.iniA' : [ drawKurrentKanzleiGatE_r, [temp_x, temp_y, "iniA"], margin_Str, marginConA-6 ],
    # 'r.iniH' : [ drawKurrentKanzleiGatE_r, [temp_x, temp_y, "iniA"], margin_Str, marginConA-6 ],  ### hier 2x iniA!!!
    # 'r.midA' : [ drawKurrentKanzleiGatE_r, [temp_x, temp_y, "midA"], 18, marginConA-5 ],
    # 'r.midH' : [ drawKurrentKanzleiGatE_r, [temp_x, temp_y, "midA"], 18, marginConA-5 ],  ### hier 2x midA!!!
    # 'r.fina' : [ drawKurrentKanzleiGatE_r, [temp_x, temp_y, "fina"], 18, margin_Str ],

    # 's' : [ drawKurrentKanzleiGatE_s, [temp_x, temp_y], margin_Str-3, margin_Str ],
    # 's.isol' : [ drawKurrentKanzleiGatE_s, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],    
    # 's.iniA' : [ drawKurrentKanzleiGatE_s, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],    
    # 's.iniH' : [ drawKurrentKanzleiGatE_s, [temp_x, temp_y, "iniH"], margin_Str, marginConH+20 ],    
    # 's.midA' : [ drawKurrentKanzleiGatE_s, [temp_x, temp_y, "fina"], margin_Str-66, margin_Str ],    
    # 's.midH' : [ drawKurrentKanzleiGatE_s, [temp_x, temp_y, "midH"], margin_Str-66, marginConH+20 ],    
    # 's.fina' : [ drawKurrentKanzleiGatE_s, [temp_x, temp_y, "fina"], -7, margin_Str-3 ],

    #  'longs' : [ drawKurrentKanzleiGatE_longs, [temp_x, temp_y], margin_Str, margin_Str-50 ],
    # 'longs.isol' : [ drawKurrentKanzleiGatE_longs, [temp_x, temp_y, "isol"], margin_Str, margin_Str-50 ],
    # 'longs.iniA' : [ drawKurrentKanzleiGatE_longs, [temp_x, temp_y, "iniA"], margin_Str, marginStr-107],
    # 'longs.iniH' : [ drawKurrentKanzleiGatE_longs, [temp_x, temp_y, "iniH"], margin_Str, marginStr-91 ],
    # 'longs.midA' : [ drawKurrentKanzleiGatE_longs, [temp_x, temp_y, "midA"], marginFlush, marginStr-107 ],
    # 'longs.midH' : [ drawKurrentKanzleiGatE_longs, [temp_x, temp_y, "midH"], marginFlush, marginStr-89 ],    
    # 'longs.fina' : [ drawKurrentKanzleiGatE_longs, [temp_x, temp_y, "fina"], marginFlush, marginStr-89 ],    

    # 'germandbls' : [ drawKurrentKanzleiGatE_germandbls, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'germandbls.isol' : [ drawKurrentKanzleiGatE_germandbls, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'germandbls.iniA' : [ drawKurrentKanzleiGatE_germandbls, [temp_x, temp_y, "iniA"], margin_Str, marginStr+30],
    # 'germandbls.iniH' : [ drawKurrentKanzleiGatE_germandbls, [temp_x, temp_y, "iniH"], margin_Str, marginStr-69 ],
    # 'germandbls.midA' : [ drawKurrentKanzleiGatE_germandbls, [temp_x, temp_y, "midA"], marginFlush, marginStr+30 ],
    # 'germandbls.midH' : [ drawKurrentKanzleiGatE_germandbls, [temp_x, temp_y, "midH"], marginFlush, marginStr-69 ],    
    # 'germandbls.fina' : [ drawKurrentKanzleiGatE_germandbls, [temp_x, temp_y, "fina"], marginFlush, marginStr ],    

    # 't' : [ drawKurrentKanzleiGatE_t, [temp_x, temp_y], margin_Str, margin_Str ],
    # 't.isol' : [ drawKurrentKanzleiGatE_t, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 't.iniA' : [ drawKurrentKanzleiGatE_t, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 't.iniH' : [ drawKurrentKanzleiGatE_t, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 't.midA' : [ drawKurrentKanzleiGatE_t, [temp_x, temp_y, "iniA"], marginStr-89-15, marginConA ],
    # 't.midH' : [ drawKurrentKanzleiGatE_t, [temp_x, temp_y, "iniH"], marginStr-89-15, marginConH ],
    # 't.fina' : [ drawKurrentKanzleiGatE_t, [temp_x, temp_y, "fina"], marginStr-89-15, margin_Str ],
    
    # 'u' : [ drawKurrentKanzleiGatE_u, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'u.isol' : [ drawKurrentKanzleiGatE_u, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'u.iniA' : [ drawKurrentKanzleiGatE_u, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'u.iniH' : [ drawKurrentKanzleiGatE_u, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'u.midA' : [ drawKurrentKanzleiGatE_u, [temp_x, temp_y, "midA"], marginFlush, marginConA ],
    # 'u.midH' : [ drawKurrentKanzleiGatE_u, [temp_x, temp_y, "midH"], marginFlush, marginConH ],
    # 'u.fina' : [ drawKurrentKanzleiGatE_u, [temp_x, temp_y, "fina"], marginFlush, margin_Str ],
    
    # 'v' : [ drawKurrentKanzleiGatE_v, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'v.isol' : [ drawKurrentKanzleiGatE_v, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'v.iniA' : [ drawKurrentKanzleiGatE_v, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'v.iniH' : [ drawKurrentKanzleiGatE_v, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'v.midA' : [ drawKurrentKanzleiGatE_v, [temp_x, temp_y, "midA"], 5, marginConA ],
    # 'v.midH' : [ drawKurrentKanzleiGatE_v, [temp_x, temp_y, "midH"], 7, marginConH ],
    # 'v.fina' : [ drawKurrentKanzleiGatE_v, [temp_x, temp_y, "fina"], 7, margin_Rnd ],
    
    # 'w' : [ drawKurrentKanzleiGatE_w, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'w.isol' : [ drawKurrentKanzleiGatE_w, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'w.iniA' : [ drawKurrentKanzleiGatE_w, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'w.iniH' : [ drawKurrentKanzleiGatE_w, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'w.midA' : [ drawKurrentKanzleiGatE_w, [temp_x, temp_y, "midA"], 13, marginConA ],
    # 'w.midH' : [ drawKurrentKanzleiGatE_w, [temp_x, temp_y, "midH"], 13, marginConH ],
    # 'w.fina' : [ drawKurrentKanzleiGatE_w, [temp_x, temp_y, "fina"], 13, margin_Rnd ],
    
    # 'x' : [ drawKurrentKanzleiGatE_x, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'x.isol' : [ drawKurrentKanzleiGatE_x, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'x.iniA' : [ drawKurrentKanzleiGatE_x, [temp_x, temp_y, "iniA"], margin_Str-139, marginConA-10 ],
    # 'x.iniH' : [ drawKurrentKanzleiGatE_x, [temp_x, temp_y, "iniA"], margin_Str-139, marginConA-10 ],  ### hier 2x iniA!!!
    # 'x.midA' : [ drawKurrentKanzleiGatE_x, [temp_x, temp_y, "midA"], marginStr-181, marginConA-5 ],
    # 'x.midH' : [ drawKurrentKanzleiGatE_x, [temp_x, temp_y, "midA"], marginStr-181, marginConA-5 ],  ### hier 2x midA!!!
    # 'x.fina' : [ drawKurrentKanzleiGatE_x, [temp_x, temp_y, "fina"], marginStr-181, margin_Str ],
    
    # 'y' : [ drawKurrentKanzleiGatE_y, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'y.isol' : [ drawKurrentKanzleiGatE_y, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'y.iniA' : [ drawKurrentKanzleiGatE_y, [temp_x, temp_y, "iniA"], margin_Str, marginConA+60 ],
    # 'y.iniH' : [ drawKurrentKanzleiGatE_y, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'y.midA' : [ drawKurrentKanzleiGatE_y, [temp_x, temp_y, "midA"], 12, marginConA+60],
    # 'y.midH' : [ drawKurrentKanzleiGatE_y, [temp_x, temp_y, "midH"], 12, marginConH ],
    # 'y.fina' : [ drawKurrentKanzleiGatE_y, [temp_x, temp_y, "fina"], 12, margin_Str ],
    
    # 'z' : [ drawKurrentKanzleiGatE_z, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'z.isol' : [ drawKurrentKanzleiGatE_z, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'z.iniA' : [ drawKurrentKanzleiGatE_z, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'z.iniH' : [ drawKurrentKanzleiGatE_z, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'z.midA' : [ drawKurrentKanzleiGatE_z, [temp_x, temp_y, "midA"], marginStr-213+36, marginConA ],
    # 'z.midH' : [ drawKurrentKanzleiGatE_z, [temp_x, temp_y, "midH"], marginStr-143-38, marginConH ],
    # 'z.fina' : [ drawKurrentKanzleiGatE_z, [temp_x, temp_y, "fina"], marginStr-101, margin_Str ],





    # ####### Dieresis

    # 'adieresis' : [ drawKurrentKanzleiGatE_adieresis, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'adieresis.isol' : [ drawKurrentKanzleiGatE_adieresis, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'adieresis.iniA' : [ drawKurrentKanzleiGatE_adieresis, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'adieresis.iniH' : [ drawKurrentKanzleiGatE_adieresis, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'adieresis.midA' : [ drawKurrentKanzleiGatE_adieresis, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'adieresis.midH' : [ drawKurrentKanzleiGatE_adieresis, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'adieresis.fina' : [ drawKurrentKanzleiGatE_adieresis, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'odieresis' : [ drawKurrentKanzleiGatE_odieresis, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'odieresis.isol' : [ drawKurrentKanzleiGatE_odieresis, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'odieresis.iniA' : [ drawKurrentKanzleiGatE_odieresis, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'odieresis.iniH' : [ drawKurrentKanzleiGatE_odieresis, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'odieresis.midA' : [ drawKurrentKanzleiGatE_odieresis, [temp_x, temp_y, "midA"], marginRnd, marginConA ],
    # 'odieresis.midH' : [ drawKurrentKanzleiGatE_odieresis, [temp_x, temp_y, "midH"], marginRnd, marginConH ],
    # 'odieresis.fina' : [ drawKurrentKanzleiGatE_odieresis, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'udieresis' : [ drawKurrentKanzleiGatE_udieresis, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'udieresis.isol' : [ drawKurrentKanzleiGatE_udieresis, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'udieresis.iniA' : [ drawKurrentKanzleiGatE_udieresis, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'udieresis.iniH' : [ drawKurrentKanzleiGatE_udieresis, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'udieresis.midA' : [ drawKurrentKanzleiGatE_udieresis, [temp_x, temp_y, "midA"], marginFlush, marginConA ],
    # 'udieresis.midH' : [ drawKurrentKanzleiGatE_udieresis, [temp_x, temp_y, "midH"], marginFlush, marginConH ],
    # 'udieresis.fina' : [ drawKurrentKanzleiGatE_udieresis, [temp_x, temp_y, "fina"], marginFlush, margin_Str ],
    
    
    
    
    
    # ######## Ligatures
    
    # 'r_c' : [ drawKurrentKanzleiGatE_rc, [temp_x, temp_y], marginStr, marginStr-60 ],

    #  'longs_longs' : [ drawKurrentKanzleiGatE_longs_longs, [temp_x, temp_y], marginStr-30, marginStr-200],
    #  'longs_longs.isol' : [ drawKurrentKanzleiGatE_longs_longs, [temp_x, temp_y, "isol"], marginStr-30, marginStr-200],
    #  'longs_longs.iniA' : [ drawKurrentKanzleiGatE_longs_longs, [temp_x, temp_y, "iniA"], marginStr-30, marginStr-110],
    #  'longs_longs.iniH' : [ drawKurrentKanzleiGatE_longs_longs, [temp_x, temp_y, "iniH"], marginStr-30, marginStr-90],
    #  'longs_longs.midA' : [ drawKurrentKanzleiGatE_longs_longs, [temp_x, temp_y, "midA"], 30, marginStr-110],
    #  'longs_longs.midH' : [ drawKurrentKanzleiGatE_longs_longs, [temp_x, temp_y, "midH"], 30, marginStr-90],

    #  'f_f' : [ drawKurrentKanzleiGatE_f_f, [temp_x, temp_y], marginStr, marginStr-120 ],
    #  'f_f.isol' : [ drawKurrentKanzleiGatE_f_f, [temp_x, temp_y, "isol"], marginStr-30, marginStr-120],
    #  'f_f.iniA' : [ drawKurrentKanzleiGatE_f_f, [temp_x, temp_y, "iniA"], marginStr-30, marginStr-110],
    #  'f_f.iniH' : [ drawKurrentKanzleiGatE_f_f, [temp_x, temp_y, "iniH"], marginStr-30, marginStr-90],
    #  'f_f.midA' : [ drawKurrentKanzleiGatE_f_f, [temp_x, temp_y, "midA"], marginStr-60, marginStr-110],
    #  'f_f.midH' : [ drawKurrentKanzleiGatE_f_f, [temp_x, temp_y, "midH"], marginStr-60, marginStr-90],
    #  'f_f.fina' : [ drawKurrentKanzleiGatE_f_f, [temp_x, temp_y, "fina"], marginStr-30, marginStr-130],
    
    #  'longs_t' : [ drawKurrentKanzleiGatE_longs_t, [temp_x, temp_y], marginStr, marginStr-50 ],
    #  'longs_t.isol' : [ drawKurrentKanzleiGatE_longs_t, [temp_x, temp_y, "isol"], marginStr-30, marginStr-50],
    #  'longs_t.iniA' : [ drawKurrentKanzleiGatE_longs_t, [temp_x, temp_y, "iniA"], marginStr-30, marginConA],
    #  'longs_t.iniH' : [ drawKurrentKanzleiGatE_longs_t, [temp_x, temp_y, "iniH"], marginStr-130, marginConH],
    #  'longs_t.midA' : [ drawKurrentKanzleiGatE_longs_t, [temp_x, temp_y, "midA"], marginStr-30, marginConA],
    #  'longs_t.midH' : [ drawKurrentKanzleiGatE_longs_t, [temp_x, temp_y, "midH"], marginStr-30, marginConH],
    #  'longs_t.fina' : [ drawKurrentKanzleiGatE_longs_t, [temp_x, temp_y, "fina"], marginStr-30, marginStr-50],

    #  'f_f_t' : [ drawKurrentKanzleiGatE_f_f_t, [temp_x, temp_y], marginStr, marginStr ],
    #  'f_f_t.isol' : [ drawKurrentKanzleiGatE_f_f_t, [temp_x, temp_y, "isol"], marginStr-30, marginStr ],
    #  'f_f_t.iniA' : [ drawKurrentKanzleiGatE_f_f_t, [temp_x, temp_y, "iniA"], marginStr-30, marginConA],
    #  'f_f_t.iniH' : [ drawKurrentKanzleiGatE_f_f_t, [temp_x, temp_y, "iniH"], marginStr-30, marginConH],
    #  'f_f_t.midA' : [ drawKurrentKanzleiGatE_f_f_t, [temp_x, temp_y, "midA"], marginStr-60, marginConA],
    #  'f_f_t.midH' : [ drawKurrentKanzleiGatE_f_f_t, [temp_x, temp_y, "midH"], marginStr-60, marginConH],
    #  'f_f_t.fina' : [ drawKurrentKanzleiGatE_f_f_t, [temp_x, temp_y, "fina"], marginStr-60, marginStr],

    
    # ###### Interpunction
    
    # 'period' : [ drawKurrentKanzleiGatE_period, [temp_x, temp_y], marginStr, marginStr ],
    # 'colon' : [ drawKurrentKanzleiGatE_colon, [temp_x, temp_y], marginStr, marginStr ],
    # 'semicolon' : [ drawKurrentKanzleiGatE_semicolon, [temp_x, temp_y], marginStr, marginStr ],
    # 'quoteright' : [ drawKurrentKanzleiGatE_quoteright, [temp_x, temp_y], marginStr, marginStr ],
    # 'quotesingle' : [ drawKurrentKanzleiGatE_quotesingle, [temp_x, temp_y], marginStr, marginStr ],
    # 'comma' : [ drawKurrentKanzleiGatE_comma, [temp_x, temp_y], marginStr-30, marginStr ],
    # 'endash' : [ drawKurrentKanzleiGatE_endash, [temp_x, temp_y], marginStr, marginStr ],
    # 'hyphen' : [ drawKurrentKanzleiGatE_hyphen, [temp_x, temp_y], marginStr, marginStr ],
    # 'exclam' : [ drawKurrentKanzleiGatE_exclam, [temp_x, temp_y], marginStr, marginStr ],
    # 'question' : [ drawKurrentKanzleiGatE_question, [temp_x, temp_y], marginStr, marginStr ],



    # ###### Numbers
    
    # 'zero' : [ drawKurrentKanzleiGatE_zero, [temp_x, temp_y], marginStr, marginStr ],
    # 'one' : [ drawKurrentKanzleiGatE_one, [temp_x, temp_y], marginStr+110, marginStr ],
    # 'two' : [ drawKurrentKanzleiGatE_two, [temp_x, temp_y], marginStr, marginStr ],
    # 'three' : [ drawKurrentKanzleiGatE_three, [temp_x, temp_y], marginStr+15, marginStr ],
    # 'four' : [ drawKurrentKanzleiGatE_four, [temp_x, temp_y], marginStr-30, marginStr ],
    # 'five' : [ drawKurrentKanzleiGatE_five, [temp_x, temp_y], marginStr+45, marginStr-90 ],
    # 'six' : [ drawKurrentKanzleiGatE_six, [temp_x, temp_y], marginStr, marginStr ],
    # 'seven' : [ drawKurrentKanzleiGatE_seven, [temp_x, temp_y], marginStr-30, marginStr-60 ],
    # 'eight' : [ drawKurrentKanzleiGatE_eight, [temp_x, temp_y], marginStr, marginStr ],    
    # 'nine' : [ drawKurrentKanzleiGatE_nine, [temp_x, temp_y], marginStr, marginStr ],    

    


    # # # ### Uppercase
    
    # # #  #   'percent' : [ drawKurrentKanzleiGatE_helperUC, [temp_x, temp_y], marginStr, marginStr ],

    # # # # # # 'C' : [ copy from GatC ],
    # # # # # # 'D' : [ copy from GatC ],
    # # # # # # 'G' : [ copy from GatC ],
    # # # # # # 'H' : [ copy from GatC ],
    # # # # # # 'W' : [ copy from GatC ],
    
    
    # # # # 'A' : [ drawKurrentKanzleiGatE_A, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'B' : [ drawKurrentKanzleiGatE_B, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'C' : [ drawKurrentKanzleiGatE_C, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'D' : [ drawKurrentKanzleiGatE_D, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'E' : [ drawKurrentKanzleiGatE_E, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'F' : [ drawKurrentKanzleiGatE_F, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'G' : [ drawKurrentKanzleiGatE_G, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'H' : [ drawKurrentKanzleiGatE_H, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'I' : [ drawKurrentKanzleiGatE_I, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'J' : [ drawKurrentKanzleiGatE_J, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'K' : [ drawKurrentKanzleiGatE_K, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'L' : [ drawKurrentKanzleiGatE_L, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'M' : [ drawKurrentKanzleiGatE_M, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'N' : [ drawKurrentKanzleiGatE_N, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'O' : [ drawKurrentKanzleiGatE_O, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'P' : [ drawKurrentKanzleiGatE_P, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'Q' : [ drawKurrentKanzleiGatE_Q, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'R' : [ drawKurrentKanzleiGatE_R, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'S' : [ drawKurrentKanzleiGatE_S, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'T' : [ drawKurrentKanzleiGatE_T, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'U' : [ drawKurrentKanzleiGatE_U, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'V' : [ drawKurrentKanzleiGatE_V, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'W' : [ drawKurrentKanzleiGatE_W, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'X' : [ drawKurrentKanzleiGatE_X, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'Y' : [ drawKurrentKanzleiGatE_Y, [temp_x, temp_y], marginStr, marginStr ],
    # # # # 'Z' : [ drawKurrentKanzleiGatE_Z, [temp_x, temp_y], marginStr, marginStr ],
    
    }
    
    
    

drawFunctions_thinStroke = {

     'f' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f.isol' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f.iniA' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f.iniH' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f.midA' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f.midH' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f.fina' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],

     'f_f' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.isol' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.iniA' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.iniH' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.midA' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.midH' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.fina' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
     
      'g' : [ drawKurrentKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],
      'g.isol' : [ drawKurrentKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],
      'g.fina' : [ drawKurrentKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],
      
      'h' : [ drawKurrentKanzleiGatE_h_thinStroke, [temp_x, temp_y] ],
      'h.isol' : [ drawKurrentKanzleiGatE_h_thinStroke, [temp_x, temp_y] ],
      'h.fina' : [ drawKurrentKanzleiGatE_h_thinStroke, [temp_x, temp_y] ],

      'j' : [ drawKurrentKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],
      'j.isol' : [ drawKurrentKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],
      'j.fina' : [ drawKurrentKanzleiGatE_g_thinStroke, [temp_x, temp_y] ],

      'p' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
      'q' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],

      'y' : [ drawKurrentKanzleiGatE_h_thinStroke, [temp_x, temp_y] ],
      'y.isol' : [ drawKurrentKanzleiGatE_h_thinStroke, [temp_x, temp_y] ],
      'y.fina' : [ drawKurrentKanzleiGatE_h_thinStroke, [temp_x, temp_y] ],

      'z' : [ drawKurrentKanzleiGatE_z_thinStroke, [temp_x, temp_y] ],
      'z.isol' : [ drawKurrentKanzleiGatE_z_thinStroke, [temp_x, temp_y] ],
      'z.fina' : [ drawKurrentKanzleiGatE_z_thinStroke, [temp_x, temp_y] ],

      'longs' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],

      'germandbls' : [ drawKurrentKanzleiGatE_germandbls_thinStroke, [temp_x, temp_y] ],

      'longs_longs' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],   
    
      'longs_t' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
         
      'f_f_t' : [ drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y] ],
    
      'quotesingle' : [ drawKurrentKanzleiGatE_quotesingle_thinstroke, [temp_x, temp_y] ],  



      'one' : [ drawKurrentKanzleiGatE_one_thinStroke, [temp_x, temp_y] ],
      'three' : [ drawKurrentKanzleiGatE_three_thinStroke, [temp_x, temp_y] ],
      'five' : [ drawKurrentKanzleiGatE_three_thinStroke, [temp_x, temp_y] ],
      'six' : [ drawKurrentKanzleiGatE_six_thinStroke, [temp_x, temp_y] ],
      'nine' : [ drawKurrentKanzleiGatE_nine_thinStroke, [temp_x, temp_y] ],


    }
    
    


for variant in ['isol', 'fina','iniA', 'iniH', 'midA', 'midH']: 
	drawFunctions_thinStroke['p.' + variant] = [drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y]]

for variant in ['isol', 'fina','iniA', 'iniH', 'midA', 'midH']: 
	drawFunctions_thinStroke['q.' + variant] = [drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y]]

for variant in ['isol', 'iniA', 'iniH', 'midA', 'midH', 'fina']: 
	drawFunctions_thinStroke['germandbls.' + variant] = [drawKurrentKanzleiGatE_germandbls_thinStroke, [temp_x, temp_y]]

for variant in ['isol', 'iniA', 'iniH', 'midA', 'midH', 'fina']: 
    drawFunctions_thinStroke['longs.' + variant] = [drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y]]

for variant in ['isol', 'iniA', 'iniH', 'midA', 'midH']: 
	drawFunctions_thinStroke['longs_longs.' + variant] = [drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y]]
     
for variant in ['isol','iniA', 'iniH', 'midA', 'midH' , 'fina']: 
	drawFunctions_thinStroke['longs_t.' + variant] = [drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y]]   
     
for variant in ['isol','iniA', 'iniH', 'midA', 'midH' , 'fina']: 
	drawFunctions_thinStroke['f_f_t.' + variant] = [drawKurrentKanzleiGatE_thinstroke_Straight, [temp_x, temp_y]]   
  
     
     
     
     
     
     
    
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	

    if key in drawFunctions_thinStroke:

        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    #print("margin left:", glyph.leftMargin,"_", "right:", glyph.rightMargin)
    #print("supposed to be:", drawFunctions[key][2],"_", drawFunctions[key][3])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_before = margin_ts - margin_fg
    #print("difference before:", difference_before)
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_mid = margin_ts - margin_fg
    #print("difference mid:", difference_mid)

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid
    #print("move thinStroke by:", move_thinStroke)
    

    glyph.copyLayerToLayer('foreground', 'background')
   
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): 
        margin_ts = 0 
        #print("(no thin line)")   
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_final = margin_ts - margin_fg
    #print("difference before:", difference_before, "=== difference final:", difference_final , "?")
    
    foreground.clear()

    
    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    #print("drawn with:  __Angle:", a, "__Width:", w)

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 

        #print("drawn with:  __Angle:", a, "__Width:", w)

        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    

    
    
    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)    

    
    
    
    

