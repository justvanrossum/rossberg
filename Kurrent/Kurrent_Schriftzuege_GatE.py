import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatE

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatE)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatE import *
from version_3.creation.arc_path import ArcPath as BezierPath

import math as m




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 21
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)




# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 1.5

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)


# temporary to check height of numbers
line((1*modul_width, 12.5*modul_height), (12*modul_width, 12.5*modul_height))





# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 7


#Helper
#Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, temp_x, temp_y)
#Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, temp_x, temp_y-0.25)




def drawSchriftzug1(x,y, version="a", instrokeLen=1, outstrokeLen=0):
    
    Schriftzug1 = BezierPath() 
     
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    downstroke = drawGrundelementF(*Grund_a, 1.5)

    if version == "a":
        stroke = drawInstroke(*Grund_a, instrokeLen)
        
    if version == "b":
        stroke = drawOutstroke(*downstroke.points[1], outstrokeLen)
    
    
    Schriftzug1 += stroke + downstroke

    drawPath(Schriftzug1)
    
    return Schriftzug1
   
#drawSchriftzug1 (temp_x, temp_y)
#drawSchriftzug1 (temp_x, temp_y, "a", 2, 0)
#drawSchriftzug1 (temp_x, temp_y, "b", 0, 2)

    
    


def drawSchriftzug2(x,y, version="a", style="gerade", instrokeLen=1, outstrokeLen=1):
    
    Schriftzug2 = BezierPath() 
     
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    


    if version == "a":
        instroke = drawInstroke(*Grund_a, instrokeLen)
        
        if style == "gerade":
            downstroke = drawGrundelementF(*Grund_a, 1.5)
        if style == "gebogen":
            downstroke = drawGrundelementF(*Grund_a, 1.5)
            print("There is no gebogen, 2a is straigt!")
        
        outstroke = drawOutstroke(*downstroke.points[1], outstrokeLen)
        
        
    if version == "b":
        
        if style == "gerade":
            instroke = drawInstroke(*Grund_a, instrokeLen, "down")
            downstroke = drawGrundelementF(*Grund_a, 1.5)

        if style == "gebogen":
            instroke = drawSchriftteil5(*Grund_b, "fakeGatC")    ### geht das wirklch so???
            downstroke = drawGrundelementF(*instroke.points[-1], 0.735)
        
        outstroke = drawOutstroke(Grund_d[0], Grund_d[1]-modul_height/2, outstrokeLen)

        
    if version == "c":
        instroke = drawInstroke(*Grund_a, instrokeLen)

        if style == "gerade":
            downstroke = drawGrundelementF(*Grund_a, 1.5)
            outstroke = drawOutstroke(*downstroke.points[-1], outstrokeLen, "down")

        if style == "gebogen":
            downstroke = drawGrundelementF(*Grund_a, 0.735)
            pos_point_help = Grund_a[0]-modul_width,  Grund_a[1]- modul_height*2
            outstroke = drawSchriftteil6(*pos_point_help, "fakeGatC")     # fragwürdig.
        
        
            
    Schriftzug2 = instroke + downstroke + outstroke 
        
    drawPath(Schriftzug2)
    
    return Schriftzug2


#drawSchriftzug2(temp_x, temp_y, "c", "gerade", instrokeLen=1)
#drawSchriftzug2(temp_x, temp_y, "a", "gebogen")   # outstrokeLen=3
#drawSchriftzug2(temp_x+4, temp_y, "b", "gebogen", outstrokeLen=2) 
#drawSchriftzug2(temp_x+2, temp_y, "a", instrokeLen=2, outstrokeLen=3)





  
  
def drawSchriftzugStern1(x,y):
    
    SchriftzugStern1 = BezierPath() 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    # Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    

    Signatur_oben = drawGrundelementE(*Grund_a, "unten")
    
    downstroke = drawGrundelementF(*Grund_a, 1.5)

    Signatur_unten = drawGrundelementE(*downstroke.points[1])        
    
    SchriftzugStern1 = Signatur_oben + downstroke + Signatur_unten
    
    drawPath(SchriftzugStern1)
    
    return SchriftzugStern1
    

#drawSchriftzugStern1(temp_x, temp_y)   






def drawSchriftzugStern2(x,y):
    
    SchriftzugStern2 = BezierPath() 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    Signatur_oben = drawSchriftteil2(*Grund_a)
    
    downstroke = drawGrundelementF(*Grund_a, 1.5)

    Signatur_unten = drawSchriftteil1(*downstroke.points[1]) 

               
    SchriftzugStern2 = Signatur_oben + downstroke + Signatur_unten
    
    drawPath(SchriftzugStern2)
    
    return SchriftzugStern2
    


#drawSchriftzugStern2(temp_x, temp_y)






### Das Folgende ist sehr fragwürdig. Müsste man nochmal anpassen, falls wirklich benutzt.
def drawSchriftzugStern3(x, y , Sig_oben=True, Sig_unten=True):
    
    SchriftzugStern3 = BezierPath() 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    Grund_a_btm, Grund_b_btm, Grund_c_btm, Grund_d_btm = drawGrundelOrient(A1, A2, offset, x-1, y-1)

    if Sig_oben == True:
        Signatur_oben = drawSchriftteil10(Grund_a[0]-offset[0]+1.5, Grund_a[1]+offset[1])
        SchriftzugStern3 += Signatur_oben
  
        
    downstroke_top = drawSchriftteil5(*Grund_b, version="fakeGatC")
    con_start = downstroke_top.points[-1]   # Punkt festhalten für Verbindung später
    
    downstroke_btm = drawSchriftteil6(Grund_d[0]-modul_width, Grund_d[1]-modul_height, version="fakeGatC")
    con_end = downstroke_btm.points[-1]      # Punkt festhalten für Verbindung später
    
    SchriftzugStern3.line(con_start, con_end)

    if Sig_unten == True:
        Signatur_unten = drawSchriftteil9(Grund_d[0]+modul_width*0.7, Grund_d[1]-modul_height) 
        SchriftzugStern3 += Signatur_unten
        
        
    SchriftzugStern3 += downstroke_top + downstroke_btm
    
    drawPath(SchriftzugStern3)
    
    return SchriftzugStern3
    


#drawSchriftzugStern3(temp_x, temp_y, Sig_oben=True, Sig_unten=True)



def drawSchriftzug3_loopRight(x, y, instrokeLen=0.1):
    
    drawSchriftzug3_loopRight = BezierPath() 
    
    # Helper for positioning
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.5, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.25, y+0.125)

    
    # Windung von Raute_b nach rechts
    HSL_size = 0.5
    HSL_start = 1.55
    
    E3, E4 = line_E_vonD_o_gr_s(Raute_b, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E3, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)
    
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_13, angle_14))
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_14, angle_15))
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_15, angle_16))
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_0, angle_1))
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_1, angle_2))
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_2, angle_3))
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_3, angle_4))
    drawSchriftzug3_loopRight.arc(*drawKreisSeg(E3, HSL_start, angle_4, angle_5))



    instroke = drawOutstroke(*E2, instrokeLen, "down")


    drawSchriftzug3_loopRight += instroke       
    
    drawPath(drawSchriftzug3_loopRight)
    
    return drawSchriftzug3_loopRight
    
#drawSchriftzug3_loopRight(temp_x, temp_y)







def drawSchriftzug3_loopLeft(x, y, outstrokeLen=0.25):
    
    Schriftzug3_loopLeft = BezierPath() 
    
    # Helper for positioning
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.5, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.25, y+0.125)
    

    # Wendepunkt ab Raute_b nach links
    HSL_size = 0.5
    HSL_start = 1.35
        
    E1, E2 = line_E_vonF_o_gr_s(Raute_b, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*8)
    Schriftzug3_loopLeft.arc(*drawKreisSeg(F1, HSL_start+HSL_size*7, angle_12, angle_13))



    outstroke = drawOutstroke(*E2, outstrokeLen)
        
        
        
    Schriftzug3_loopLeft += outstroke       
    
    drawPath(Schriftzug3_loopLeft)
    
    return Schriftzug3_loopLeft
    

#drawSchriftzug3_loopLeft(temp_x, temp_y)











def drawSchriftzug3(x, y, outstrokeLen=1):
    
    Schriftzug3 = BezierPath() 
    
    # Helper for positioning
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.5, y)
    
    
    # Windung von Raute_b nach rechts
    loopRight = drawSchriftzug3_loopRight(x, y)

    # Wendepunkt ab Raute_b nach links
    loopLeft = drawSchriftzug3_loopLeft(x, y, 0.75)

    
    downstroke = drawSchriftzug2(x, y, "b", "gebogen")
    
    Schriftzug3 += loopRight + loopLeft + downstroke      
    
    drawPath(Schriftzug3)
    
    return Schriftzug3
    

#drawSchriftzug3(temp_x, temp_y, outstrokeLen=0)     ### Warum funktioniert hier die outstrokeLen nicht???      

#drawSchriftzug2(temp_x, temp_y, "b", "gebogen", outstrokeLen)
#drawSchriftzug2(temp_x, temp_y, "b", "gebogen", outstrokeLen=1.5)




#temp_y = 2




def drawSchriftzug4(x, y, version="gebogen", outstrokeLen=0.1):
    
    Schriftzug4 = BezierPath() 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.2, y-1.65)
       
    if version == "gerade":
        downstroke = drawSchriftzug2(x, y, "c", "gerade", 1)
        spitze = drawInstroke(*Raute_a, 0, "down")
        
    else:
        downstroke = drawSchriftzug2(x, y, "c", "gebogen")
        spitze = drawInstroke(Raute_a[0]-offset[0], Raute_a[1]-offset[1], 0.7, "down")
            
    # Wendepunkt: Raute_a    
    nach_rechts = drawSchneckenzug(*Raute_d, LOWER_E, 8, HSL_size=0.5, HSL_start=1.55, clockwise=True, inward=False)

        
    if version == "1.5":    ### für r und x angelegt, Zahl gibt Abstand zur Singatur an.
       Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.4, y-1.55)
       drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.2, y-1.4)
       nach_rechts = drawSchneckenzug(*Raute_d, LOWER_E, 8, HSL_size=0.84, HSL_start=0.5, clockwise=True, inward=False, HSL_size_multipliers=[ 1+.1 * i      for  i in range(0, 9)])
       Zusatz = drawOutstroke(*spitze.points[-1], 0.2)
       Schriftzug4 += Zusatz
    
    nach_links = drawSchneckenzug(*Raute_d, LOWER_E, 8, HSL_size=0.5, HSL_start=1.35, clockwise=False, inward=False)
       
    outstroke = drawOutstroke(*nach_rechts.points[-1], outstrokeLen)
    
    Schriftzug4 += downstroke + outstroke + nach_links + nach_rechts + spitze
    drawPath(Schriftzug4)
    return Schriftzug4
    
    
#drawSchriftzug4(temp_x, temp_y, version="gerade")
#drawSchriftzug4(temp_x, temp_y, version="gebogen")
#drawSchriftzug4(temp_x, temp_y, version="1.5")    ### für r und x angelegt, Zahl gibt Abstand zur Singatur an.

    
 
 
 

    
    
def drawSchriftzug5(x, y, height="1.5", instrokeLen=1, outstrokeLen=1):
    
    Schriftzug5 = BezierPath()
 
    #Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)   ### Einblenden um Pos zu checken

    if height == "1.25":    
        HSL_size = 1
        HSL_start = 10
    
    if height == "1":    
        HSL_size = 1
        HSL_start = 8
        
    else:        ### height == "1.5" 
        HSL_size = 1
        HSL_start = 12
        
        
    A4 = x, y            ### Switchen um Pos zu checken
    #A4 = Grund_d          ### Switchen um Pos zu checken
    A3 = A4[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),  A4[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    #polygon(A3, A4)
    
    B1 = A3
    B2 = B1[0]- part*HSL_start,     B1[1] 
    #polygon(B1, B2)
    Schriftzug5.arc(*drawKreisSeg(A3, HSL_start, angle_9, angle_8, True))
    
    C1 = A3
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),  C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    #polygon(C1, C2)
    Schriftzug5.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_7, True))
    
    
    instroke = drawInstroke(*C2, instrokeLen, "down")
    
    outstroke = drawOutstroke(*A4, outstrokeLen)
    
    Schriftzug5 += instroke + outstroke
    drawPath(Schriftzug5)
    return Schriftzug5
    
    
#drawSchriftzug5(temp_x, temp_y)     



'''nochmal getestet weil die Form so schrecklich'''
# # # # def drawSchriftzug5(x, y, height="1.5", instrokeLen=1, outstrokeLen=1):
    
# # # #     Schriftzug5 = BezierPath()
 
# # # #     Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)   ### Einblenden um Pos zu checken


# # # #     if height == "1.25":    
# # # #         HSL_size = 1
# # # #         HSL_start = 10
    
# # # #     if height == "1":    
# # # #         HSL_size = 1
# # # #         HSL_start = 8
        
# # # #     else:        ### height == "1.5" 
# # # #         HSL_size = 1
# # # #         HSL_start = 12
    
# # # #     #bogen = drawSchneckenzug(*Grund_d, LOWER_A, 2, HSL_size=2, HSL_start=11, clockwise=True, inward=False)
# # # #     #bogen = drawSchneckenzug(*Grund_d, LOWER_H, 5, HSL_size=1.5, HSL_start=11, clockwise=True, inward=True)

# # # #     Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.5)   ### Einblenden um Pos zu checken
# # # #     bogen1 = drawSchneckenzug(*Grund_d, UPPER_E, 2, HSL_size=0.5, HSL_start=4, clockwise=False, inward=True)
# # # #     bogen2 = drawSchneckenzug(*bogen1.points[-1], UPPER_C, 2, HSL_size=9.25, HSL_start=6, clockwise=False, inward=False)

# # # #     #A4 = x, y            ### Switchen um Pos zu checken
# # # #     A4 = Grund_d          ### Switchen um Pos zu checken
# # # #     A3 = A4[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),  A4[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
# # # #     #polygon(A3, A4)
    
# # # #     B1 = A3
# # # #     B2 = B1[0]- part*HSL_start,     B1[1] 
# # # #     #polygon(B1, B2)
# # # #     Schriftzug5.arc(*drawKreisSeg(A3, HSL_start, angle_9, angle_8, True))
    
# # # #     C1 = A3
# # # #     C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),  C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
# # # #     #polygon(C1, C2)
# # # #     Schriftzug5.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_7, True))
    
    
# # # #     instroke = drawInstroke(*C2, instrokeLen, "down")
    
# # # #     outstroke = drawOutstroke(*A4, outstrokeLen)
    
# # # #     Schriftzug5 += instroke + outstroke + bogen1 + bogen2
# # # #     drawPath(Schriftzug5)
# # # #     return Schriftzug5
    
    
# # # # drawSchriftzug5(temp_x, temp_y)  






def drawSchriftzug6(x, y, version="standard", instrokeLen= 1, outstrokeLen=1):
    
    Schriftzug6 = BezierPath()
 
    # Help for orientation only 
    Raute_a, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-0.5)  

    if version == "standard":
        # values for following HSL made up by me
        bend= drawSchneckenzug(*Raute_d, LOWER_A, 4, HSL_size=0, HSL_start=8, clockwise=True, inward=False)

    if version == "1":
        bend= drawSchneckenzug(*Raute_d, LOWER_A, 4, HSL_size=0, HSL_start=6.4, clockwise=True, inward=False)
 
    if version == "2":
        bend= drawSchneckenzug(*Raute_d, LOWER_A, 4, HSL_size=2, HSL_start=13.225, clockwise=True, inward=True)


    instroke = drawInstroke(*bend.points[-1], instrokeLen, "down")  
    outstroke = drawOutstroke(*bend.points[0], outstrokeLen)     
    
    Schriftzug6 += instroke + outstroke + bend
    drawPath(Schriftzug6)
    return Schriftzug6
    
    
# drawSchriftzug6(temp_x,temp_y, version="1")
# drawSchriftzug6(temp_x,temp_y, version="standard")
# drawSchriftzug6(temp_x,temp_y, version="2")



    

    
    



def drawSchriftzug7(x, y, instrokeLen= 1, outstrokeLen=1):
    
    Schriftzug7 = BezierPath()
    
    # Help for orientation only    
    #drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    # values for following HSL made up by me  # ???
    HSL_size = 0
    HSL_start = 8
        
    A1, A2 = line_A_vonH_o_gr_s((x,y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug7.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True))
    
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug7.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug7.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug7.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))
    
    
    instroke = drawInstroke(*A2, instrokeLen)
    
    outstroke = drawOutstroke(*E2, outstrokeLen, "down")
    
    Schriftzug7 += instroke + outstroke
        

    drawPath(Schriftzug7)
    
    return Schriftzug7

# temp_x = 14
# temp_y = 20


# drawSchriftzug7(temp_x, temp_y)







def drawSchriftzug8(x,y):
    
    Schriftzug8 = BezierPath()
    
    # draw Modul + Raute
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+6)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1)

    HSL_size = 0
    HSL_start = 96
    
    E1, E2 = line_E_vonF_u_gr_s(Grund_d, *angles, part, HSL_size, HSL_start)
    
    D1, D2 = line_D_vonE_u_gr(E1, *angles, part, HSL_size, HSL_start)
    Schriftzug8.arc(*drawKreisSeg(D1, HSL_start, angle_13, angle_14))

    C1, C2 = line_C_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start)
    Schriftzug8.arc(*drawKreisSeg(C1, HSL_start, angle_14, angle_15))
   
    C4 = Grund_d
    C3 = C4[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      C4[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C3, C4)

    D3, D4 = line_D_vonC_o_kl(C3, *angles, part, HSL_size, HSL_start)
    Schriftzug8.arc(*drawKreisSeg(D3, HSL_start, angle_7, angle_6, True))

    E3, E4 = line_E_vonD_o_gr(D3, *angles, part, HSL_size, HSL_start)
    Schriftzug8.arc(*drawKreisSeg(E3, HSL_start, angle_6, angle_5, True))
    
    solid = drawSchriftzug2(x, y, "a")
    
    Schriftzug8 += solid
    
    drawPath(Schriftzug8)
    
    return Schriftzug8

#drawSchriftzug2(temp_x, temp_y, "a")
#drawSchriftzug8(temp_x, temp_y)






def drawSchriftzug9(x, y, version="standard", instrokeLen=0, outstrokeLen=2):
    
    Schriftzug9 = BezierPath()
    
    # draw Modul + Raute _ rechts
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-2.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-4.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-5.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-6.5)  
    
    # draw Modul + Raute _ links
    drawGrundelOrient(A1, A2, offset, x, y-6.5)  

    solid = drawSchriftzug1(x, y, "a", instrokeLen)
    
    
    if version == "beginner":
        
        # Kurve oben
        HSL_size = 6.45*8
        HSL_start = 20.5*8
   
        E1, E2 = line_E_vonF_o_kl_s(solid.points[-1], *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))

        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        Schriftzug9.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        Schriftzug9.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))

        # Kurve unten
        HSL_start = 11.5*8
    
        C2 = solid.points[-1]
        C1 = C2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      C2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start) 
        polygon(C1, C2)
    
        E1 = C1
        E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start) ,   E1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
        polygon(E1, E2)
    
        Schriftzug9.arc(*drawKreisSeg(C1, HSL_start, angle_13, angle_15))
        
        
        
    if version == "standard":

        # Kurve unter baseline - nach unten
        HSL_size =  8* 6.5
        HSL_start = 8* 20
        
        C3, C4 = line_C_vonB_u_kl_s(solid.points[-1], *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size)
        Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_15, angle_14, True))

        E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        Schriftzug9.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_14, angle_13, True))

        F1, F2 = line_F_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        Schriftzug9.arc(*drawKreisSeg(F1, HSL_start-HSL_size*3, angle_13, angle_12, True))

    
        # Kurve links nach oben – erstmal nur konstruieren
        # HSL_size =  8* 6.5
        # HSL_start = 8* 20
   
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)

        E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    
    
        # ### Wendung unten
        CP = F2[0]-1.6, F2[1]+0.1
        # #text("CP", CP) 
        Schriftzug9.moveTo(F2)
        Schriftzug9.curveTo(CP, B2)
    
    
        # # Kurve links nach oben – Linie ziehen
        Schriftzug9.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_7, True))
        Schriftzug9.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
        Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))

    
        
        outstroke = drawOutstroke(*E2, outstrokeLen)
        Schriftzug9 += outstroke



    if version == "end":
    
        Kurve_nachUnten = drawSchneckenzug(*solid.points[-1], LOWER_C, 2, HSL_size=40, HSL_start=140, clockwise=True, inward=True)
        
        Schriftzug9 += Kurve_nachUnten

    Schriftzug9 += solid
    drawPath(Schriftzug9)
    return Schriftzug9
    
    
#drawSchriftzug9(temp_x+5, temp_y)




 


def drawSchriftzug10(x, y, instrokeLen=0, outstrokeLen=1):
    
    Schriftzug10 = BezierPath()
    
    # draw Modul + Raute _ links
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+6)
    Grund_a_later, Grund_b_later, Grund_c_later, Grund_d_later = drawGrundelOrient(A1, A2, offset, x, y+1)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    


    # draw Modul + Raute _ rechts
    drawGrundelOrient(A1, A2, offset, x+6, y+6)
    drawGrundelOrient(A1, A2, offset, x+5, y+5.5)


    # Kurve links nach oben – wird erst größer
    HSL_size =  4
    HSL_start = 12
        
    A3, A4 = line_A_vonB_u_gr_s(Grund_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)    
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)

    Schriftzug10.arc(*drawKreisSeg(A3, HSL_start, angle_9, angle_8, True))
    Schriftzug10.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))


    #Übergang, gleiche Kurve - wird jetzt kleiner
    HSL_size =  28     
    HSL_start = 123.5

    C3, C4 = line_C_vonB_o_kl_s(C2, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonC_o_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size) 
    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)

    Schriftzug10.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_7, angle_6, True))
    Schriftzug10.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_6, angle_5, True))
   

    # Kurve nach unten
    HSL_size =  8* 6.5
    HSL_start = 8* 20
    
    E3, E4 = line_E_vonF_u_kl_s(Grund_d_later, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_u_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
       
    # ### Wendung oben
    CP = E2[0]+2.5, E2[1]+0.5
    #text("CP", CP) 
    Schriftzug10.moveTo(E2)
    Schriftzug10.curveTo(CP, B2)

    # # Kurve nach unten Linie ziehen
    Schriftzug10.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_16, angle_15, True))
    Schriftzug10.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_15, angle_14, True))
    Schriftzug10.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_14, angle_13, True))

    instroke = drawInstroke(*Grund_d_later, instrokeLen)
    outstroke = drawOutstroke(*A4, outstrokeLen)
    
    Schriftzug10 += instroke + outstroke    
    drawPath(Schriftzug10)    
    return Schriftzug10

#drawSchriftzug10(temp_x, temp_y, outstrokeLen=2)








def drawSchriftzug11(x, y, instrokeLen=0, outstrokeLen=1):
    
    Schriftzug11 = BezierPath()
    
    # draw Modul + Raute Block links
    drawGrundelOrient(A1, A2, offset, x, y-1.5)
    drawGrundelOrient(A1, A2, offset, x, y-2.5)
    drawGrundelOrient(A1, A2, offset, x, y-3.5)
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x, y-5.5)
    drawGrundelOrient(A1, A2, offset, x, y-6.5)
    
    # draw Modul + Raute Block rechts
    drawGrundelOrient(A1, A2, offset, x+6, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+6, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+6, y-3.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y)

    instroke = drawInstroke(*Grund_a, instrokeLen)

    #solid part x-height
    HSL_size =  8
    HSL_start = 12
            
    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)    
    Schriftzug11.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug11.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))



    # Kurve unter baseline - nach unten
    HSL_size =  48
    HSL_start = 58 + HSL_size*2
    
    C3, C4 = line_C_vonB_u_kl_s(C2, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug11.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_15, angle_14, True))

    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug11.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_14, angle_13, True))
    

    # Kurve links nach oben – erstmal nur konstruieren
    # HSL_size =  8* 6.5
    # HSL_start = 8* 20
   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y-1.5)

    E3, E4 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)



    ### Wendung unten
    CP = E2[0]-3.2, E2[1]-0.75
    #text("CP", CP) 
    Schriftzug11.moveTo(E2)
    Schriftzug11.curveTo(CP, B2)


    # Kurve links nach oben – Linie ziehen
    Schriftzug11.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_7, True))
    Schriftzug11.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
    Schriftzug11.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))

        
    outstroke = drawOutstroke(*E4, outstrokeLen)
    
    Schriftzug11 += instroke + outstroke 
    
    drawPath(Schriftzug11)
    
    return Schriftzug11


#drawSchriftzug11(temp_x, temp_y, outstrokeLen=3)







def drawSchriftzug12(x, y, version="a", outstrokeLen=1):
    
    Schriftzug12 = BezierPath()
    
    # draw Modul + Raute Block links
    drawGrundelOrient(A1, A2, offset, x, y-1.5)
    drawGrundelOrient(A1, A2, offset, x, y-2.5)
    drawGrundelOrient(A1, A2, offset, x, y-3.5)
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x, y-5.5)
    drawGrundelOrient(A1, A2, offset, x, y-6.5)
    
    
    if version == "a":
        
        # draw Modul + Raute Block rechts
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y)



        #solid part x-height
        HSL_size =  4
        HSL_start = 18
            
        A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
        B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)    
        Schriftzug12.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_0, True))

        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug12.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    

        # Kurve unter baseline - nach unten
        HSL_size =  8* 5.1
        HSL_start = 8* 16
        
        C3, C4 = line_C_vonB_u_kl_s(C2, *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size)
        Schriftzug12.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_15, angle_14, True))

        E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        Schriftzug12.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_14, angle_13, True))

        F1, F2 = line_F_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        Schriftzug12.arc(*drawKreisSeg(F1, HSL_start-HSL_size*3, angle_13, angle_12, True))

    
        # Kurve links nach oben – erstmal nur konstruieren
        # HSL_size =  
        # HSL_start =
   
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+5.5, y-2.25)

        E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    
    
        ### Wendung unten
        CP = F2[0]-2.9, F2[1]-0.2
        #text("CP", CP) 
        Schriftzug12.moveTo(F2)
        Schriftzug12.curveTo(CP, B2)
    
    
        # Kurve links nach oben – Linie ziehen
        Schriftzug12.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_7, True))
        Schriftzug12.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
        Schriftzug12.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))

        outstroke = drawOutstroke(*E2, outstrokeLen)
        Schriftzug12 += outstroke 


    
    if version == "b":
        
        # draw Modul + Raute Block rechts
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y-0.5)



        #solid part x-height
        HSL_size =  1
        HSL_start = 18
            
        A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
        B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)    
        Schriftzug12.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_0, True))

        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug12.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    

        # Kurve unter baseline - nach unten
        HSL_size =  8* 4.25
        HSL_start = 8* 14
        
        C3, C4 = line_C_vonB_u_kl_s(C2, *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size)
        Schriftzug12.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_15, angle_14, True))

        E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        Schriftzug12.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_14, angle_13, True))

        F1, F2 = line_F_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        Schriftzug12.arc(*drawKreisSeg(F1, HSL_start-HSL_size*3, angle_13, angle_12, True))

    
        # Kurve links nach oben – erstmal nur konstruieren
        HSL_size =  8* 4.25
        HSL_start = 8* 14
   
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+5.5, y-2.25)

        E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    
    
        ### Wendung unten
        CP = F2[0]-2.2, F2[1]       
        #text("CP", CP) 
        Schriftzug12.moveTo(F2)
        Schriftzug12.curveTo(CP, B2)
    
    
        # Kurve links nach oben – Linie ziehen
        Schriftzug12.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_7, True))
        Schriftzug12.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
        Schriftzug12.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))

        outstroke = drawOutstroke(*E2, outstrokeLen)
        Schriftzug12 += outstroke 
    


    if version == "end":
        
        #x +=3 
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y)
        instroke = drawGrundelementE(*Grund_a, 0.25)
        solid_xheight = drawSchneckenzug(*instroke.points[0], UPPER_A, 2, HSL_size=4, HSL_start=16, clockwise=True, inward=False)

        continue_down_right = drawSchneckenzug(*solid_xheight.points[-1], LOWER_C, 2, HSL_size=20, HSL_start=84, clockwise=True, inward=True)
        
        Schriftzug12 += solid_xheight + continue_down_right
    
     

    
    drawPath(Schriftzug12)
    
    return Schriftzug12


# drawSchriftzug12(temp_x, temp_y, version="a", outstrokeLen=3)
#drawSchriftzug12(temp_x, temp_y, version="b", outstrokeLen=3)
# drawSchriftzug12(temp_x, temp_y, version="end")








def drawSchriftzug13(x, y, instrokeLen=1, outstrokeLen=1):
    
    Schriftzug13 = BezierPath()
    
    # draw Modul + Raute (top two)
    Grund_a_right, Grund_b_right, Grund_c_right, Grund_d_right = drawGrundelOrient(A1, A2, offset, x+6, y+6)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3)
    
    # Hilfslinie
    save()
    stroke(0, 1, 0.5)
    polygon(Grund_b, Grund_a_right)
    restore()
        
    # draw Modul + Raute (lower two)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1)



    instroke = drawInstroke(*Grund_c, instrokeLen)


    Schnittpunkt = (x+2.25)*modul_width, (y+4)*modul_height
    #text("Schnittpunkt", Schnittpunkt)
    
    
    # Kurve links nach unten
    HSL_size =  8* 0
    HSL_start = 8* 4.5
        
    C3, C4 = line_C_vonB_o_kl_s(Schnittpunkt, *angles, part, HSL_size, HSL_start)

    D3, D4 = line_D_vonC_o_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug13.arc(*drawKreisSeg(D3, HSL_start-HSL_size, angle_7, angle_6, True))

    E3, E4 = line_E_vonD_o_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug13.arc(*drawKreisSeg(E3, HSL_start-HSL_size*2, angle_6, angle_5, True))
    
    # F1, F2 = line_F_vonE_o_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    # Schriftzug13.arc(*drawKreisSeg(F1, HSL_start-HSL_size*3, angle_5, angle_4, True))
    
    
    
    # Kurve rechts nach oben
    HSL_size =  8* 5
    HSL_start = 8* 16.6
    
    E1, E2 = line_E_vonF_u_kl_s(Grund_c, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    
    ### Wendung oben
    CP = E4[0]+2, E4[1]+1
    #text("CP", CP) 
    Schriftzug13.moveTo(E4)
    Schriftzug13.curveTo(CP, B2)


    # Kurve nach unten Linie ziehen
    Schriftzug13.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_16, angle_15, True))
    Schriftzug13.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_15, angle_14, True))
    Schriftzug13.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_14, angle_13, True))



    #solid part x-height
    stroke_straight = drawGrundelementG(*Schnittpunkt, length=5.1, direction="down")
    
    outstroke = drawOutstroke(*stroke_straight.points[1], outstrokeLen)
    
    Schriftzug13 += stroke_straight + outstroke + instroke
    drawPath(Schriftzug13)
    return Schriftzug13


#drawSchriftzug13(temp_x, temp_y, outstrokeLen=2)


#temp_x=5




def drawSchriftzug_k_start(x, y, instrokeLen=1, outstrokeLen=1):
    
    Schriftzug_k_start = BezierPath()
    
    # draw Modul + Raute (top two)
    Grund_a_right, Grund_b_right, Grund_c_right, Grund_d_right = drawGrundelOrient(A1, A2, offset, x+6, y+6)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3)
    
    # Hilfslinie
    save()
    stroke(0, 1, 0.5)
    polygon(Grund_b, Grund_a_right)
    restore()
        
    # draw Modul + Raute (lower two)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1)
    
    
    
    # ### Wendung oben
    # CP = E4[0]+2, E4[1]+1
    # #text("CP", CP) 
    # Schriftzug_k_start.moveTo(E4)
    # Schriftzug_k_start.curveTo(CP, B2)

    Schnittpunkt = (x+2.25)*modul_width, (y+4)*modul_height
    #text("Schnittpunkt", Schnittpunkt)



    #solid part x-height
    stroke_straight = drawGrundelementG(*Schnittpunkt, length=5.1, direction="down")
    
    curve_up = drawSchneckenzug(*Schnittpunkt, UPPER_C, 3, HSL_size=10, HSL_start=50, clockwise=True, inward=True)
    Wendung = drawSchneckenzug(*curve_up.points[-1], UPPER_F, 5, HSL_size=1, HSL_start=5, clockwise=True, inward=False)
    Out = drawSchneckenzug(*Wendung.points[-1], LOWER_C, 2, HSL_size=1, HSL_start=24, clockwise=True, inward=False)


    outstroke = drawOutstroke(*stroke_straight.points[1], outstrokeLen)
    
    Schriftzug_k_start += stroke_straight + curve_up + Wendung + Out + outstroke 
    
    drawPath(Schriftzug_k_start)
    
    return Schriftzug_k_start


#drawSchriftzug_k_start(temp_x, temp_y, outstrokeLen=2)








def drawSchriftzug14(x, y, instroke="btm", instrokeLen=1, outstrokeLen=1):
    
    Schriftzug14 = BezierPath()
    
    # draw Modul + Raute (top ones)
    Grund_a_left, Grund_b_left, Grund_c_left, Grund_d_left = drawGrundelOrient(A1, A2, offset, x-3, y+0.5)
    Grund_a_right, Grund_b_right, Grund_c_right, Grund_d_right = drawGrundelOrient(A1, A2, offset, x+3, y+4.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3)
    
    # Hilfslinie
    save()
    stroke(0, 1, 0.5)
    polygon(Grund_b, Grund_a_right)
    restore()
        
    # draw Modul + Raute (lower two)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1)

    
    if instroke == "btm":
    
        # Kurve nach oben gerades Stück am Anfang
        instroke = drawInstroke(*Grund_c_left, instrokeLen)

        # Kurve rechts nach oben
        HSL_size =  8* 6.125
        HSL_start = 8* 18.7
    
        E1, E2 = line_E_vonF_u_kl_s(Grund_c_left, *angles, part, HSL_size, HSL_start)
    
        D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        Schriftzug14.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))

        C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        Schriftzug14.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))

    
    if instroke == "top":

        instroke = drawInstroke(*Grund_a_right, instrokeLen)
        
          

    #solid part x-height
    stroke_straight = drawGrundelementG(*Grund_a_right, length=6, direction="down")
    outstroke = drawOutstroke(*stroke_straight.points[1], outstrokeLen)
    
    
    
    Schriftzug14 += instroke + stroke_straight + outstroke 
    
    drawPath(Schriftzug14)
    
    return Schriftzug14


#drawSchriftzug14(temp_x, temp_y, instroke = "top",  instrokeLen=2, outstrokeLen=1)
#drawSchriftzug14(temp_x, temp_y, instroke = "btm",  instrokeLen=1, outstrokeLen=1)







def drawSchriftzug15(x, y, outstrokeLen=0.5):
    
    Schriftzug15 = BezierPath()
    
    # draw Modul + Raute LINKS
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    # draw Modul + Raute RECHTS
    Grund_a_right, Grund_b_right, Grund_c_right, Grund_d_right = drawGrundelOrient(A1, A2, offset, x+6, y+6)
    drawGrundelOrient(A1, A2, offset, x+6, y+5)
    drawGrundelOrient(A1, A2, offset, x+6, y+4)
    drawGrundelOrient(A1, A2, offset, x+5, y+2.5)

    
        
    # Instroke gerades Stück am Anfang
    #instroke = drawInstroke(*Grund_a, 0.5, "down")
    
    # Bauch links unten
    body = drawSchriftzug6(x, y, version="2", instrokeLen=0.25, outstrokeLen=3)

    drawGrundelOrient(A1, A2, offset, x+3, y)



    # Kurve rechts nach oben
    HSL_size =  8
    HSL_start = 48

    E1, E2 = line_E_vonF_u_kl_s(body.points[2], *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug15.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))

    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug15.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))

    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug15.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))
        

    
    # Übergang 3 Uhr
    HSL_size =  1.8
    HSL_start = 13
    
    B3, B4 = line_B_vonC_u_kl_s(B2, *angles, part, HSL_size, HSL_start)
    
    A3, A4 = line_A_vonB_o_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug15.arc(*drawKreisSeg(A3, HSL_start-HSL_size, angle_0, angle_1))

    H1, H2 = line_H_vonA_o_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug15.arc(*drawKreisSeg(H1, HSL_start-HSL_size*2, angle_1, angle_2))

    G1, G2 = line_G_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug15.arc(*drawKreisSeg(G1, HSL_start-HSL_size*3, angle_2, angle_3))
      
    F1, F2 = line_F_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    Schriftzug15.arc(*drawKreisSeg(F1, HSL_start-HSL_size*4, angle_3, angle_4))  

    E3, E4 = line_E_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*5)
    Schriftzug15.arc(*drawKreisSeg(E3, HSL_start-HSL_size*5, angle_4, angle_5))  




    
    # Übergang 11 Uhr
    HSL_size =  1.35
    HSL_start = 4
    
    
    E5, E6 = line_E_vonF_o_gr_s(E4, *angles, part, HSL_size, HSL_start)

    D3, D4 = line_D_vonE_o_gr(E5, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug15.arc(*drawKreisSeg(E5, HSL_start, angle_5, angle_6))  

    C3, C4 = line_C_vonD_o_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug15.arc(*drawKreisSeg(D3, HSL_start+HSL_size, angle_6, angle_7))  

    B3, B4 = line_B_vonC_o_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug15.arc(*drawKreisSeg(C3, HSL_start+HSL_size*2, angle_7, angle_8))  

    A7, A8 = line_A_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug15.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_8, angle_9))  

    H1, H2 = line_H_vonA_u_gr(A7, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug15.arc(*drawKreisSeg(A7, HSL_start+HSL_size*4, angle_9, angle_10))  

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug15.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))  

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    Schriftzug15.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))  

    E3, E4 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*8)
    Schriftzug15.arc(*drawKreisSeg(F1, HSL_start+HSL_size*7, angle_12, angle_13))  


    outstroke = drawOutstroke(*E4, outstrokeLen)
    
    Schriftzug15 += body + outstroke 
    drawPath(Schriftzug15)
    return Schriftzug15


#drawSchriftzug15(temp_x, temp_y, outstrokeLen=1) 








def drawSchriftzug16(x, y, instrokeLen=1):  ### Diese Kurve stimmt noch gar nicht.
    
    Schriftzug16 = BezierPath()
    
    # draw Modul + Raute LINKS
    drawGrundelOrient(A1, A2, offset, x, y+2.5)
    drawGrundelOrient(A1, A2, offset, x, y+1.5)
    drawGrundelOrient(A1, A2, offset, x, y+0.5)
    drawGrundelOrient(A1, A2, offset, x, y-0.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y)

    # draw Modul + Raute RECHTS
    Grund_a_right, Grund_b_right, Grund_c_right, Grund_d_right = drawGrundelOrient(A1, A2, offset, x+4, y+4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y+3.5)
    drawGrundelOrient(A1, A2, offset, x+4, y+2.5)
    drawGrundelOrient(A1, A2, offset, x+4, y+1.5)

    # Start innen wie man es auch richtig schreiben würde
    bend_bottom_right = drawSchneckenzug(*Grund_a, UPPER_E, 9, HSL_size=0.5, HSL_start=9, clockwise=True, inward=True)
    bend_bottom_left = drawSchneckenzug(*bend_bottom_right.points[-1], LOWER_F, 7, HSL_size=2, HSL_start=5.4, clockwise=True, inward=False)

    conStroke = drawOutstroke(*bend_bottom_left.points[-1], 1.6)

    stretchy_part = drawSchneckenzug(*conStroke.points[0], LOWER_E, 2, HSL_size=16.25, HSL_start=60, clockwise=False, inward=True)
 

    hook = drawSchriftteil10(*Grund_a_right)
    instroke = drawInstroke(*Grund_a, instrokeLen)

    Schriftzug16 += bend_bottom_right + bend_bottom_left + conStroke + stretchy_part + hook
    
    drawPath(Schriftzug16)
    
    return Schriftzug16


#drawSchriftzug16(temp_x, temp_y) 



def drawSchriftzug16(x, y, instrokeLen=1):
    
    Schriftzug16 = BezierPath()
    
    # draw Modul + Raute LINKS
    drawGrundelOrient(A1, A2, offset, x, y+2.5)
    drawGrundelOrient(A1, A2, offset, x, y+1.5)
    drawGrundelOrient(A1, A2, offset, x, y+0.5)
    drawGrundelOrient(A1, A2, offset, x, y-0.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y)

    # draw Modul + Raute RECHTS
    Grund_a_right, Grund_b_right, Grund_c_right, Grund_d_right = drawGrundelOrient(A1, A2, offset, x+4, y+4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y+3.5)
    drawGrundelOrient(A1, A2, offset, x+4, y+2.5)
    drawGrundelOrient(A1, A2, offset, x+4, y+1.5)

    # Start innen wie man es auch richtig schreiben würde
    instroke = drawInstroke(*Grund_a, instrokeLen)
    extra = drawOutstroke(*Grund_a, 0.6)
    bend_bottom_right = drawSchneckenzug(*extra.points[0], UPPER_E, 5, HSL_size=0.6, HSL_start=6.25, clockwise=True, inward=False)
    bend_bottom_btm = drawSchneckenzug(*bend_bottom_right.points[-1], LOWER_B, 8, HSL_size=0.38, HSL_start=6, clockwise=True, inward=False)
    bend_bottom_left = drawSchneckenzug(*bend_bottom_btm.points[-1], UPPER_B, 3, HSL_size=8, HSL_start=13.25, clockwise=True, inward=False)
    conStroke = drawOutstroke(*bend_bottom_left.points[-1], 1)
    stretchy_part = drawSchneckenzug(*conStroke.points[0], LOWER_E, 2, HSL_size=12, HSL_start=52.5, clockwise=False, inward=True)
    hook = drawSchriftteil10(*Grund_a_right)

    Schriftzug16 += instroke + extra + bend_bottom_right + bend_bottom_btm + bend_bottom_left + conStroke + stretchy_part + hook
    drawPath(Schriftzug16)
    return Schriftzug16


#drawSchriftzug16(temp_x, temp_y) 










def drawSchriftzug17(x,y, version="b", base=False):
    
    Schriftzug17 = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    
    if base == True:
        base = drawSchriftzug1(x, y, "a")
        Schriftzug17 += base
    else:
        pass
    
    downstroke = drawGrundelementH(*Grund_a, 6, "down")
    upstroke = drawGrundelementH(*Grund_a, 4.6, "up")

    # draw Modul + Raute
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y+h)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+8.8, y+6)
    instroke = drawGrundelementE(*Raute_d, 0.125, "unten")
    Signatur_rechts = drawSchneckenzug(*instroke.points[-1], UPPER_A, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)

    # Übergang Signatur zu downstroke
    Signatur_links = drawSchneckenzug(*Signatur_rechts.points[-1], UPPER_E, 1, HSL_size=0, HSL_start=98, clockwise=False, inward=False)
    
    
    Schriftzug17 += downstroke + upstroke + Signatur_rechts + Signatur_links
    drawPath(Schriftzug17)
    return Schriftzug17

temp_x = 6
#drawSchriftzug17(temp_x, temp_y)




 
   
''' Schriftzug22 – Naminig to be considered'''

# # def drawSchriftzug22(x,y):
    
# #     Schriftzug22 = BezierPath()
    
# #     # draw Modul + Raute
# #     Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
# #     drawGrundelOrient(A1, A2, offset, x+9, y+3)

# #     base = drawSchriftzug1(x, y, "a")
# #     downstroke = drawGrundelementH(*Grund_a, 6, "down")
# #     upstroke = drawGrundelementH(*Grund_a, 4.3, "up")

# #     # draw Modul + Raute
# #     helper = (0.5, 1.5, 2.5, 3.5, 4.5)
# #     for h in helper:
# #         Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y+h)


    
# #     # Signatur
# #     Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+9.5, y+5.5)
    
# #     HSL_size = 4
# #     HSL_start = 21.5     # 22.65 war es vorher

# #     G1, G2 = line_G_vonH_o_gr_s(Raute_c, *angles, part, HSL_size, HSL_start)

# #     F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
# #     Schriftzug22.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_4))
    
# #     E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
# #     Schriftzug22.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_5))
    
    
    
# #     # Übergang Signatur zu downstroke
# #     HSL_size = 0
# #     HSL_start = 96
        
# #     E3, E4 = line_E_vonF_o_gr_s(E2, *angles, part, HSL_size, HSL_start)

# #     D1, D2 = line_D_vonE_o_gr(E3, *angles, part, HSL_size, HSL_start)
# #     Schriftzug22.arc(*drawKreisSeg(D1, HSL_start, angle_5, angle_6))
    
    
# #     ### Ab hier Schluss-s
# #     Schriftzug22_s = BezierPath()
# #     #Schriftzug22.moveTo(Raute_c)

# #     connection = drawOutstroke(*Raute_c, 0.5, "down")


# #     # s oben
# #     HSL_size = 4
# #     HSL_start = 16.75
        
# #     E5, E6 = line_E_vonF_o_gr_s(connection.points[0], *angles, part, HSL_size, HSL_start)
    
# #     D3, D4 = line_D_vonE_o_gr(E5, *angles, part, HSL_size, HSL_start+HSL_size)
# #     Schriftzug22_s.arc(*drawKreisSeg(E5, HSL_start, angle_5, angle_6))
    
# #     C3, C4 = line_C_vonD_o_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*2)
# #     Schriftzug22_s.arc(*drawKreisSeg(D3, HSL_start+HSL_size, angle_6, angle_7))  
    
# #     B3, B4 = line_B_vonC_o_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*3)
# #     Schriftzug22_s.arc(*drawKreisSeg(C3, HSL_start+HSL_size*2, angle_7, angle_8))  
 
 
# #     # s mitte
# #     HSL_size = 8
# #     HSL_start = 28
        
# #     B5, B6 = line_B_vonA_u_gr_s(B4, *angles, part, HSL_size, HSL_start)
# #     Schriftzug22_s.arc(*drawKreisSeg(B5, HSL_start, angle_16, angle_15, True))  

# #     C5, C6 = line_C_vonB_u_gr(B5, *angles, part, HSL_size, HSL_start+HSL_size)


# #     # s unten
# #     HSL_size = 6
# #     HSL_start = 38
        
# #     C7, C8 = line_C_vonB_u_gr_s(C6, *angles, part, HSL_size, HSL_start)

# #     D7, D8 = line_D_vonC_u_kl(C7, *angles, part, HSL_size, HSL_start-HSL_size)
# #     Schriftzug22_s.arc(*drawKreisSeg(D7, HSL_start-HSL_size, angle_15, angle_14, True)) 
  
# #     E7, E8 = line_E_vonD_u_kl(D7, *angles, part, HSL_size, HSL_start-HSL_size*2)
# #     Schriftzug22_s.arc(*drawKreisSeg(E7, HSL_start-HSL_size*2, angle_14, angle_13, True)) 
    
    
# #     Schriftzug22 += base + downstroke + upstroke + connection + Schriftzug22_s    
# #     drawPath(Schriftzug22)
# #     return Schriftzug22
    
    
# # drawSchriftzug22(temp_x, temp_y)
    
   
### Schriftzug22 (Naminig to be considered)
### das ist das lange s  

def drawSchriftzug22(x,y):
    
    Schriftzug22 = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    
    base = drawSchriftzug1(x, y, "a")
    downstroke = drawGrundelementH(*Grund_a, 6, "down")
    upstroke = drawGrundelementH(*Grund_a, 5, "up")

    # draw Modul + Raute
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y+h)
   
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+10.5, y+6.125)


    Signatur_rechts = drawSchneckenzug(*Raute_d, UPPER_A, 4, HSL_size=6.2, HSL_start=4, clockwise=False, inward=False)

    # Übergang Signatur zu downstroke
    Signatur_links = drawSchneckenzug(*Signatur_rechts.points[-1], UPPER_E, 1, HSL_size=0, HSL_start=83, clockwise=False, inward=False)
    
    Signatur_rundung = drawSchneckenzug(*Raute_d, UPPER_A, 3, HSL_size=0.5, HSL_start=3, clockwise=True, inward=False)

    
    ### Ab hier Schluss-s
    connection = drawOutstroke(*Signatur_rundung.points[-1], 1, "down")

    s_oben = drawSchneckenzug(*connection.points[0], UPPER_E, 3, HSL_size=4, HSL_start=10, clockwise=False, inward=False)
    Einsatz = drawGrundelementF(*s_oben.points[-1], 0.5)

    s_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=6, HSL_start=36, clockwise=True, inward=True)


    Schriftzug22 += downstroke + upstroke + Signatur_rechts + Signatur_links + Signatur_rundung
    Schriftzug22 += base + connection + s_oben + Einsatz + s_unten

    drawPath(Schriftzug22)  
    return Schriftzug22
        
#drawSchriftzug22(temp_x, temp_y)
    


    
# # # # # # # def drawSchriftzug22_thinStroke(x, y, version="normal"):
    
# # # # # # #     Schriftzug22_thinStroke = BezierPath()
    
# # # # # # #     Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)


# # # # # # #     # s unten angesetzt für dünne Linie
    
# # # # # # #     ### Dieser Punkt kommt von E8, siehe Funktion darüber
# # # # # # #     refPoint_x = 50.92830093289801            
# # # # # # #     refPoint_y = 37.60822938718428
# # # # # # #     refPoint = refPoint_x, refPoint_y
# # # # # # #     refPoint_halfoffset = refPoint_x -offset[0]/2, refPoint_y -offset[1]/2

        
# # # # # # #     if version == "normal":
        
# # # # # # #         HSL_size = 4
# # # # # # #         HSL_start = 30
        
            
# # # # # # #         E7, E8 = line_E_vonD_u_kl_s(refPoint, *angles, part, HSL_size, HSL_start-HSL_size*2)

# # # # # # #         F7, F8 = line_F_vonE_u_kl(E7, *angles, part, HSL_size, HSL_start-HSL_size*3)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(F7, HSL_start-HSL_size*3, angle_13, angle_12, True)) 

# # # # # # #         G7, G8 = line_G_vonF_u_kl(F7, *angles, part, HSL_size, HSL_start-HSL_size*4)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(G7, HSL_start-HSL_size*4, angle_12, angle_11, True)) 

# # # # # # #         H7, H8 = line_H_vonG_u_kl(G7, *angles, part, HSL_size, HSL_start-HSL_size*5)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(H7, HSL_start-HSL_size*5, angle_11, angle_10, True)) 

# # # # # # #         A7, A8 = line_A_vonH_u_kl(H7, *angles, part, HSL_size, HSL_start-HSL_size*6)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(A7, HSL_start-HSL_size*6, angle_10, angle_9, True))  
    
# # # # # # #         B7, B8 = line_B_vonA_o_kl(A7, *angles, part, HSL_size, HSL_start-HSL_size*7)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(B7, HSL_start-HSL_size*7, angle_9, angle_8, True))  
    

    
    
# # # # # # #     if version == "kleiner":
        
# # # # # # #         HSL_size = 6
# # # # # # #         HSL_start = 27.5
        
# # # # # # #         E7, E8 = line_E_vonD_u_kl_s(refPoint, *angles, part, HSL_size, HSL_start)

# # # # # # #         ###
# # # # # # #         HSL_size = 5.5
# # # # # # #         HSL_start = 27.5 - 5.5
        
# # # # # # #         F7, F8 = line_F_vonE_u_kl(E7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(F7, HSL_start, angle_13, angle_12, True)) 


# # # # # # #         ###
# # # # # # #         HSL_size = 5
# # # # # # #         HSL_start = 27.5 - 5.5 - 5
        
# # # # # # #         G7, G8 = line_G_vonF_u_kl(F7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(G7, HSL_start, angle_12, angle_11, True)) 

# # # # # # #         ###
# # # # # # #         HSL_size = 4.5
# # # # # # #         HSL_start = 27.5 - 5.5 - 5 - 4.5
        
# # # # # # #         H7, H8 = line_H_vonG_u_kl(G7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(H7, HSL_start, angle_11, angle_10, True)) 

# # # # # # #         ###
# # # # # # #         HSL_size = 4
# # # # # # #         HSL_start = 27.5 - 5.5 - 5 - 4.5 - 4
        
# # # # # # #         A7, A8 = line_A_vonH_u_kl(H7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(A7, HSL_start, angle_10, angle_9, True))  


# # # # # # #     if version == "kleiner2":
        
# # # # # # #         HSL_size = 6
# # # # # # #         HSL_start = 27.5
 
    
# # # # # # #         E7, E8 = line_E_vonD_u_kl_s(refPoint_halfoffset, *angles, part, HSL_size, HSL_start)

# # # # # # #         ###
# # # # # # #         HSL_size = 5
# # # # # # #         HSL_start = 27.5 - 5
        
# # # # # # #         F7, F8 = line_F_vonE_u_kl(E7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(F7, HSL_start, angle_13, angle_12, True)) 


# # # # # # #         ###
# # # # # # #         HSL_size = 4
# # # # # # #         HSL_start = 27.5 - 5 - 4
        
# # # # # # #         G7, G8 = line_G_vonF_u_kl(F7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(G7, HSL_start, angle_12, angle_11, True)) 

# # # # # # #         ###
# # # # # # #         HSL_size = 3
# # # # # # #         HSL_start = 27.5 - 5 - 4 - 3
        
# # # # # # #         H7, H8 = line_H_vonG_u_kl(G7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(H7, HSL_start, angle_11, angle_10, True)) 

# # # # # # #         ###
# # # # # # #         HSL_size = 2
# # # # # # #         HSL_start = 27.5 - 5 - 4 - 3 - 2
        
# # # # # # #         A7, A8 = line_A_vonH_u_kl(H7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(A7, HSL_start, angle_10, angle_9, True)) 
        
        
# # # # # # #     if version == "grösser":
        
# # # # # # #         value = 30
# # # # # # #         HSL_size = 6
# # # # # # #         HSL_start = value
 
    
# # # # # # #         E7, E8 = line_E_vonD_u_kl_s(refPoint_halfoffset, *angles, part, HSL_size, HSL_start)

# # # # # # #         ###
# # # # # # #         HSL_size = 6.5
# # # # # # #         HSL_start = value - 6.5
        
# # # # # # #         F7, F8 = line_F_vonE_u_kl(E7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(F7, HSL_start, angle_13, angle_12, True)) 


# # # # # # #         ###
# # # # # # #         HSL_size = 7
# # # # # # #         HSL_start = value - 6.5 - 7
        
# # # # # # #         G7, G8 = line_G_vonF_u_kl(F7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(G7, HSL_start, angle_12, angle_11, True)) 

# # # # # # #         ###
# # # # # # #         HSL_size = 7.5
# # # # # # #         HSL_start = value - 6.5  - 7 - 7.5
        
# # # # # # #         H7, H8 = line_H_vonG_u_kl(G7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(H7, HSL_start, angle_11, angle_10, True)) 

# # # # # # #         ###
# # # # # # #         HSL_size = 8
# # # # # # #         HSL_start = value - 6.5  - 7 - 7.5 - 8
        
# # # # # # #         A7, A8 = line_A_vonH_u_kl(H7, *angles, part, HSL_size, HSL_start)
# # # # # # #         Schriftzug22_thinStroke.arc(*drawKreisSeg(A7, HSL_start, angle_10, angle_9, True))      
    
# # # # # # #     drawPath(Schriftzug22_thinStroke)
    
# # # # # # #     return Schriftzug22_thinStroke
    
    
#drawSchriftzug22_thinStroke(temp_x, temp_y, version = "normal")  
#drawSchriftzug22_thinStroke(temp_x, temp_y, version = "kleiner")
#drawSchriftzug22_thinStroke(temp_x, temp_y, version = "kleiner2")
#drawSchriftzug22_thinStroke(temp_x, temp_y, version = "grösser")



def drawSchriftzug25(x, y, version="a", outstrokeLen=0):
    
    Schriftzug25 = BezierPath()

    ### helpint elements
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y+h)
        

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    base = drawSchriftzug1(x, y, "a")   ### Warum zeichnet er das nicht in RF???
    downstroke = drawGrundelementH(*Grund_a, 6, "down")
    upstroke = drawGrundelementH(*Grund_a, 4.5, "up")



    continue_up_left = drawSchneckenzug(*upstroke.points[-1], UPPER_D, 1, HSL_size=0, HSL_start=98, clockwise=True, inward=False)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+9, y+6)

    
    if version == "a":
        
        Querstrich = drawSchriftzug2(x+2, y, "b", "gebogen", outstrokeLen=outstrokeLen)
        Einsatz = drawOutstroke(*Querstrich.points[0], 1)
        continue_down_right = drawSchneckenzug(*Einsatz.points[0], LOWER_E, 2, HSL_size=99, HSL_start=220, clockwise=False, inward=True)

    
        ### Wendung oben
        CP = continue_up_left.points[-1][0]+7.25, continue_up_left.points[-1][1]+2.75
        #text("CP", CP) 
        Schriftzug25.moveTo(continue_up_left.points[-1])
        Schriftzug25.curveTo(CP, continue_down_right.points[-1])
    
    if version == "b":
        
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.4, y-1.5)
        Querstrich = drawSchriftteil12(*Grund_b, outstrokeLen=outstrokeLen)
        
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y-0.9)
        instroke = drawInstroke(*Grund_a, 2.5, "down")
        continue_down_right = drawSchneckenzug(*instroke.points[-1], LOWER_E, 2, HSL_size=90, HSL_start=215, clockwise=False, inward=True)
        
        ### Wendung oben
        CP = continue_up_left.points[-1][0]+9, continue_up_left.points[-1][1]+4
        #text("CP", CP) 
        Schriftzug25.moveTo(continue_up_left.points[-1])
        Schriftzug25.curveTo(CP, continue_down_right.points[-1])
        Schriftzug25 += instroke
    

    Schriftzug25 += base + downstroke + upstroke + continue_up_left + continue_down_right + Querstrich
    drawPath(Schriftzug25)
    return Schriftzug25
    
#drawSchriftzug25(temp_x, temp_y, outstrokeLen=1)
#drawSchriftzug25(temp_x, temp_y, version="b", outstrokeLen=1)





   


def drawSchriftzug_b_hook(x, y, version="1"):
    
    Schriftzug_b_hook = BezierPath()

    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)


    if version == "1":
        
        drawRauteOrientKurBtm(offset, offsetDir, x+1, y-0.5)
        
        HSL_size = 1.2
        HSL_start = 1.55
        
        E1, E2 = line_E_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
        
        D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug_b_hook.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

        C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug_b_hook.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

        B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug_b_hook.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

        A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug_b_hook.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

        H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug_b_hook.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

        G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug_b_hook.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

        F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug_b_hook.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))

        x = F1[1]-F2[1]
        y = 6.75          # Höhenunterschied / Versatz nach oben. 
                            # Diesen Wert verstellen um Kurve zu beeinflussen.
   
        HSL_start = x+y

    
        E1 = F1[0],            F1[1] +(y)
        E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (HSL_start) , E1[1] - m.cos(m.radians(angle_seg_1)) * (HSL_start)
        #polygon(E1, E2)    # mit Absicht ausgeblendet weil irritierend
    
        Schriftzug_b_hook.arc(*drawKreisSeg(E1, HSL_start/part, angle_12, angle_13))
    
    
    if version == "0.5":
    
        drawRauteOrientKurBtm(offset, offsetDir, x+0.5, y-0.25)

        HSL_size = 0.94
        HSL_start = 1
        
        E1, E2 = line_E_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
        
        D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug_b_hook.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

        C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug_b_hook.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

        B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug_b_hook.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

        A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug_b_hook.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

        H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug_b_hook.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

        G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug_b_hook.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

        F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug_b_hook.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))

        x = F1[1]-F2[1]
        y = 4.7          # Höhenunterschied / Versatz nach oben. 
                            # Diesen Wert verstellen um Kurve zu beeinflussen.
   
        HSL_start = x+y

    
        E1 = F1[0],            F1[1] +(y)
        E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (HSL_start) , E1[1] - m.cos(m.radians(angle_seg_1)) * (HSL_start)
        #polygon(E1, E2)    # mit Absicht ausgeblendet weil irritierend
        
        Schriftzug_b_hook.arc(*drawKreisSeg(E1, HSL_start/part, angle_12, angle_13))


    if version == "1.5":
        drawRauteOrientKurBtm(offset, offsetDir, x+1, y-1)
        hook = drawSchneckenzug(*Raute_a, UPPER_E, 8, HSL_size=1.585, HSL_start=2.135, clockwise=False, inward=False)
        Schriftzug_b_hook += hook

    
    drawPath(Schriftzug_b_hook)
    return Schriftzug_b_hook 
 
   
# drawSchriftzug_b_hook(temp_x, temp_y, version="0.5")
# drawSchriftzug_b_hook(temp_x, temp_y, version="1")
# drawSchriftzug_b_hook(temp_x, temp_y, version="1.5")





def drawSchriftzug_u_hook(x, y):
    
    Schriftzug_u_hook = BezierPath() 
    
    # Helper for positioning
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y+1)
    
    
    left = drawSchneckenzug(*Raute_b, UPPER_E, 8, HSL_size=0.19, HSL_start=0.1, clockwise=False, inward=False, HSL_size_multipliers=[ 1+2 * i      for  i in range(0, 9)])
    
    constroke = drawOutstroke(*left.points[-1], 1)
    
    right = drawSchneckenzug(*constroke.points[0], LOWER_E, 2, HSL_size=2, HSL_start=16, clockwise=False, inward=True)
    right2 = drawSchneckenzug(*right.points[-1], LOWER_C, 6, HSL_size=0.8, HSL_start=5.5, clockwise=False, inward=True)

        
    Schriftzug_u_hook += left + constroke + right + right2
    
    drawPath(Schriftzug_u_hook)
    
    return Schriftzug_u_hook
    

#drawSchriftzug_u_hook(temp_x, temp_y)





def drawSchriftzug_x_hook(x, y):
    
    Schriftzug_x_hook = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-1.75) 
    hook = drawSchneckenzug(*Raute_a, UPPER_E, 16, HSL_size=1.5, HSL_start=26, clockwise=False, inward=True)
    
    Schriftzug_x_hook += hook       
    drawPath(Schriftzug_x_hook)
    return Schriftzug_x_hook
    
    
#drawSchriftzug_x_hook(temp_x, temp_y)






def drawDieresis(x, y):
    
    Dieresis = BezierPath() 
    
    drawGrundelOrient(A1, A2, offset, x, y)
    drawGrundelOrient(A1, A2, offset, x+3, y)
    drawGrundelOrient(A1, A2, offset, x+1, y+1.5)
    drawGrundelOrient(A1, A2, offset, x+2, y+2)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.5, y+2.5)
    Raute_a, Raute_b, Raute_c, Raute_d_rechts = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+2.5)

    stroke_left = drawGrundelementH(*Raute_d, 2)
    stroke_right = drawGrundelementH(*Raute_d_rechts, 2)

    Dieresis += stroke_left + stroke_right  
    drawPath(Dieresis)
    return Dieresis
    
    
#drawDieresis(temp_x, temp_y)

    
    










def drawSchriftzug23_KurrentKanzlei(x, y):   ### angelegt für b
    
    Schriftzug23_KurrentKanzlei = BezierPath()

    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
 

    HSL_size = 0.1
    HSL_start = 2.83
    
    E1, E2 = line_E_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*8)
    Schriftzug23_KurrentKanzlei.arc(*drawKreisSeg(F1, HSL_start+HSL_size*7, angle_12, angle_13))
    
    
    drawPath(Schriftzug23_KurrentKanzlei)
    
    return Schriftzug23_KurrentKanzlei 
 
   
#drawSchriftzug23_KurrentKanzlei(temp_x, temp_y)





'''Keine Ahnung wo das herkommt, scheint aber falsch/zu wenig gebogen'''
# # # def drawSchriftzug_semicolon_btm(x, y):
    
# # #     Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y-1.5)

# # #     Schriftzug_semicolon_btm = BezierPath()
    
# # #     HSL_size = 6
# # #     HSL_start = 18
    
# # #     H1, H2 = line_H_vonG_o_kl_s(Raute_a, *angles, part, HSL_size, HSL_start)

# # #     A1, A2 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size)
# # #     Schriftzug_semicolon_btm.arc(*drawKreisSeg(A1, HSL_start-HSL_size, angle_2, angle_1, True))

# # #     B1, B2 = line_B_vonA_u_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*2)    
# # #     Schriftzug_semicolon_btm.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_1, angle_0, True))
    
# # #     HSL_start = 3.5

# # #     B3, B4 = line_B_vonA_u_gr_s(B2, *angles, part, HSL_size, HSL_start)    

# # #     C1, C2 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size)
# # #     Schriftzug_semicolon_btm.arc(*drawKreisSeg(B3, HSL_start, angle_16, angle_15, True))
    
# # #     D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
# # #     Schriftzug_semicolon_btm.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_15, angle_14, True))
    
# # #     E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
# # #     Schriftzug_semicolon_btm.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_14, angle_13, True))
    
    
    
# # #     drawPath(Schriftzug_semicolon_btm)
        
# # #     return Schriftzug_semicolon_btm
    
    
# # # drawSchriftzug_semicolon_btm(temp_x, temp_y) 


def drawSchriftzug_semicolon_btm(x, y):
    
    Schriftzug_semicolon_btm = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y-1.5)
    part1 = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=0, HSL_start=4.5, clockwise=True, inward=False)
    part2 = drawSchneckenzug(*part1.points[-1], UPPER_A, 4, HSL_size=2, HSL_start=4.5, clockwise=True, inward=False)
    
    Schriftzug_semicolon_btm = part1 + part2
    drawPath(Schriftzug_semicolon_btm)   
    return Schriftzug_semicolon_btm
    
    
#drawSchriftzug_semicolon_btm(temp_x, temp_y)




def drawSchriftzug_question(x, y):
    
    Schriftzug_question = BezierPath()
    
    drawGrundelOrient(A1, A2, offset, x+1, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1, y+1.5)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2.5)
    drawGrundelOrient(A1, A2, offset, x+1, y+3.5)
    

    # Windung Mittelteil
    HSL_size = 18
    HSL_start = 60
    
    E3, E4 = line_E_vonF_u_kl_s(Grund_b, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_u_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)



    # Windung oben klein
    HSL_size_2 = 0.5
    HSL_start_2 = 6

    C3, C4 = line_C_vonD_u_kl_s(C2, *angles, part, HSL_size_2, HSL_start_2)
    B3, B4 = line_B_vonC_u_kl(C3, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2)
    A5, A6 = line_A_vonB_o_kl(B3, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2*2)
    H3, H4 = line_H_vonA_o_kl(A5, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2*3)
    G3, G4 = line_G_vonH_o_kl(H3, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2*4)
    F3, F4 = line_F_vonG_o_kl(G3, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2*5)
    E5, E6 = line_E_vonF_o_kl(F3, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2*6)
    
    
    
    # Linie Windung oben klein >>> START
    Schriftzug_question.arc(*drawKreisSeg(E5, HSL_start_2-HSL_size_2*6, angle_5, angle_4, True))
    Schriftzug_question.arc(*drawKreisSeg(F3, HSL_start_2-HSL_size_2*5, angle_4, angle_3, True))
    Schriftzug_question.arc(*drawKreisSeg(G3, HSL_start_2-HSL_size_2*4, angle_3, angle_2, True))
    Schriftzug_question.arc(*drawKreisSeg(H3, HSL_start_2-HSL_size_2*3, angle_2, angle_1, True))
    Schriftzug_question.arc(*drawKreisSeg(A5, HSL_start_2-HSL_size_2*2, angle_1, angle_0, True))
    Schriftzug_question.arc(*drawKreisSeg(B3, HSL_start_2-HSL_size_2, angle_16, angle_15, True))

    # Linie Windung oben mitte >>> MITTELTEIL
    Schriftzug_question.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_15, angle_14, True))
    Schriftzug_question.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_14, angle_13, True))


    
    
    # Windung unterer Teil
    HSL_size = 1
    HSL_start = 14
        
    E1, E2 = line_E_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*8)

    # Linie Windung unten >>> UNTEN - ENDE
    Schriftzug_question.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug_question.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug_question.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug_question.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))
    Schriftzug_question.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_9, angle_10))
    Schriftzug_question.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_10, angle_11))
    Schriftzug_question.arc(*drawKreisSeg(F1, HSL_start-HSL_size*7, angle_11, angle_12))
    Schriftzug_question.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_12, angle_13))
    
    
    
    drawPath(Schriftzug_question)
        
    return Schriftzug_question
    
    
    
#drawSchriftzug_question(temp_x, temp_y)
    
    








########################################################################################################################

########   ab hier Zahlen         

########################################################################################################################








def drawSchriftzug_four(x, y, instrokeLen=0, outstrokeLen=0.5):
        
    Schriftzug_four = BezierPath()   
    
    x +=6
     
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    instroke = drawInstroke(*Grund_a, instrokeLen)
    stem = drawGrundelementG(*Grund_a, 5.175, "down")
    
    # BOTTOM
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.5, y-0.5)

    HSL_size = 0.4
    HSL_start = 5
    
    C1, C2 = line_C_vonD_o_kl_s(stem.points[-1], *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*5)
    E3, E4 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    
    Schriftzug_four.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_7, angle_8))
    Schriftzug_four.arc(*drawKreisSeg(A3, HSL_start-HSL_size*2, angle_8, angle_9))
    Schriftzug_four.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_9, angle_10))
    Schriftzug_four.arc(*drawKreisSeg(G1, HSL_start-HSL_size*4, angle_10, angle_11))
    Schriftzug_four.arc(*drawKreisSeg(F1, HSL_start-HSL_size*5, angle_11, angle_12))
    Schriftzug_four.arc(*drawKreisSeg(E3, HSL_start-HSL_size*6, angle_12, angle_13))
    


    Schriftzug_four += stem + instroke

    drawPath(Schriftzug_four)
    return Schriftzug_four
    

#drawSchriftzug_four(temp_x, temp_y)    

    
    
    
    



def drawSchriftzug_four_left(x, y, instrokeLen=0, outstrokeLen=0.5):
        
    Schriftzug_four_left = BezierPath()   
    
    x +=6

    
    # LEFT STROKE
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-2, y+3.5)

    HSL_size = 53
    HSL_start = 140
    
    C5, C6 = line_C_vonB_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    D3, D4 = line_D_vonC_u_kl(C5, *angles, part, HSL_size, HSL_start-HSL_size)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    Schriftzug_four_left.arc(*drawKreisSeg(D3, HSL_start-HSL_size, angle_15, angle_14, True))
    Schriftzug_four_left.arc(*drawKreisSeg(E3, HSL_start-HSL_size*2, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E4, 5.5)
    
    Schriftzug_four_left += outstroke
    
    drawPath(Schriftzug_four_left)
    return Schriftzug_four_left
    

#drawSchriftzug_four_left(temp_x, temp_y)  





    
    
    
    
def drawSchriftzug_two_Top(x, y, instrokeLen=1, outstrokeLen=1):
        
    Schriftzug_two_Top = BezierPath()  
    
    #x+=4
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4)

    loop1 = drawSchriftzug3_loopRight(x, y+3.75, 0.5)
    loop2 = drawSchriftzug3_loopLeft(x, y+3.75, 0.75)


        
    Schriftzug_two_Top += loop1 + loop2

    drawPath(Schriftzug_two_Top)
    return Schriftzug_two_Top
    
    
#drawSchriftzug_two_Top(temp_x, temp_y)

    
    
    
    
    
def drawSchriftzug_two_Stem(x, y):
        
    Schriftzug_two_Stem = BezierPath()  
    
    #x+=4
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+4.25)

    
    stem = drawGrundelementG(*Grund_a, 3.65, "down")

    HSL_size = 2
    HSL_start = 24
        
    C1, C2 = line_C_vonB_u_gr_s(stem.points[-1], *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)

    Schriftzug_two_Stem.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_14, True))
    Schriftzug_two_Stem.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, 0.1, "down")
        
    Schriftzug_two_Stem += stem + outstroke

    drawPath(Schriftzug_two_Stem)
    return Schriftzug_two_Stem
    
    
#drawSchriftzug_two_Stem(temp_x, temp_y)






def drawSchriftzug_two_Schwung(x, y, instrokeLen=0):
    
    Schriftzug_two_Schwung = BezierPath()  

    #x+=4 

     
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-2.5, y-1)

    instroke = drawInstroke(*Grund_a, instrokeLen, "down")


    ### Teil 1 – links
    HSL_size = 0.25
    HSL_start = 3
    
    E1, E2 = line_E_vonD_o_gr_s(instroke.points[-1], *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_two_Schwung.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    ### ganz komisch warum hier nicht *3 funktioniert
    Schriftzug_two_Schwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_2, angle_1, True))



    ### Teil 2 – rechts
    HSL_size = 0.25
    HSL_start = 3
        
    A5, A6 = line_A_vonB_u_gr_s(A4, *angles, part, HSL_size, HSL_start)

    H3, H4 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_two_Schwung.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))

    G3, G4 = line_G_vonH_u_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_two_Schwung.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug_two_Schwung.arc(*drawKreisSeg(G3, HSL_start+HSL_size*2, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug_two_Schwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))

    outstroke = drawOutstroke(*E2, 1)
        
    Schriftzug_two_Schwung += instroke + outstroke

    drawPath(Schriftzug_two_Schwung)
    return Schriftzug_two_Schwung
    
    
#drawSchriftzug_two_Schwung(temp_x, temp_y)












def drawSchriftzug_three_Bogen(x, y):
        
    Schriftzug_three_Bogen = BezierPath()   
    
    #x +=2.5    

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2.2)

    instroke = drawInstroke(*Grund_a, 0.5, "down")

    # TOP
    HSL_size = 0.5
    HSL_start = 3.5
        
    E1, E2 = line_E_vonD_o_gr_s(instroke.points[-1], *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)

    Schriftzug_three_Bogen.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))


    # MIDDLE
    HSL_size = 24
    HSL_start = 78
    
    C3, C4 = line_C_vonB_u_kl_s(C2, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    Schriftzug_three_Bogen.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_15, angle_14, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(E3, HSL_start-HSL_size*2, angle_14, angle_13, True))
    
       
    Schriftzug_three_Bogen += instroke
    drawPath(Schriftzug_three_Bogen)
    return Schriftzug_three_Bogen

#drawSchriftzug_three_Bogen(temp_x, temp_y)






def drawSchriftzug_three_Top(x, y, instrokeLen=1):
        
    Schriftzug_three_Top = BezierPath()   
    
    #x +=6   
    hor = -1.25
    ver = 3.88
    
    loop1 = drawSchriftzug3_loopRight(x+hor, y+ver, 0.5)
    loop2 = drawSchriftzug3_loopLeft(x+hor, y+ver, 1)

    #connection = drawGrundelementG(*Grund_b, 2.1, "down")

    Schriftzug_three_Top += loop1 + loop2
    drawPath(Schriftzug_three_Top)
    return Schriftzug_three_Top

#drawSchriftzug_three_Top(temp_x, temp_y)







def drawSchriftzug_five_Top(x, y):
        
    Schriftzug_five_Top = BezierPath()   
        
    HSL_size = 1
    HSL_start = 4

    ### Positionierung hier relativ (also x, y), damit ich das Teil direkt ansetzen kann       
    H1, H2 = line_H_vonA_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)

    G1, G2 = line_G_vonF_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_five_Top.arc(*drawKreisSeg(H1, HSL_start, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_five_Top.arc(*drawKreisSeg(G1, HSL_start+HSL_size, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*3)     
    Schriftzug_five_Top.arc(*drawKreisSeg(F1, HSL_start+HSL_size*2, angle_12, angle_13))

    #outstroke = drawOutstroke(*E2, 0.5)
    
    #Schriftzug_five_Top += outstroke 
    drawPath(Schriftzug_five_Top)
    return Schriftzug_five_Top

#drawSchriftzug_five_Top(temp_x, temp_y)






def drawSchriftzug_seven_Stem(x, y):
        
    Schriftzug_seven_Stem = BezierPath()   

    #x+=6
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    HSL_size = 46
    HSL_start = 135.8
        
    D1, D2 = line_D_vonE_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)

    Schriftzug_seven_Stem.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_6, angle_7))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_7, angle_8))


    drawPath(Schriftzug_seven_Stem)
    return Schriftzug_seven_Stem

#drawSchriftzug_seven_Stem(temp_x, temp_y)




# def drawSchriftzug_seven_Top(x, y, instrokeLen=1):
        
#     Schriftzug_seven_Top = BezierPath()   
    
#     #x +=3  
      
#     Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
#     instroke = drawInstroke(*Grund_a, instrokeLen)


#     HSL_size = 2
#     HSL_start = 32
        
#     G1, G2 = line_G_vonH_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
#     F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
#     E1, E2 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
#     Schriftzug_seven_Top.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_11, angle_12))
#     Schriftzug_seven_Top.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_12, angle_13))
    
#     outstroke = drawOutstroke(*E2, 0.5)

#     Schriftzug_seven_Top += instroke + outstroke
   
#     drawPath(Schriftzug_seven_Top)
#     return Schriftzug_seven_Top

# drawSchriftzug_seven_Top(temp_x, temp_y)







def drawSchriftzug_zero_BogenLinks(x, y):
    
    #x += 5    
    Schriftzug_zero_BogenLinks = BezierPath()   
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    
    # Flanke links
    HSL_size = 10
    HSL_start = 58.75

    E1, E2 = line_E_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)

    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))


    # Rundung unten
    HSL_size = 0
    HSL_start = 9

    B3, B4 = line_B_vonC_o_kl_s(B2, *angles, part, HSL_size, HSL_start)
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(B3, HSL_start, angle_8, angle_13))

    drawPath(Schriftzug_zero_BogenLinks)
    return Schriftzug_zero_BogenLinks

#drawSchriftzug_zero_BogenLinks(temp_x, temp_y)





def drawSchriftzug_zero_BogenRechts(x, y):
     
    #x += 5   
    Schriftzug_zero_BogenRechts = BezierPath()   
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    
    # Rundung oben
    HSL_size = 0
    HSL_start = 9


    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonC_u_kl(E1, *angles, part, HSL_size, HSL_start)
    
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_0, True))

    # Flanke rechts
    HSL_size = 10
    HSL_start = 28.8
    
    B3, B4 = line_B_vonC_u_gr_s(B2, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonB_u_gr  (B3, *angles, part, HSL_size, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_gr  (C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    E3, E4 = line_E_vonD_u_gr  (D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(B3, HSL_start, angle_16, angle_15, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_15, angle_14, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_14, angle_13, True))


    drawPath(Schriftzug_zero_BogenRechts)
    return Schriftzug_zero_BogenRechts

#drawSchriftzug_zero_BogenRechts(temp_x, temp_y)




def drawSchriftzug_six_BogenRechts(x, y):
        
    Schriftzug_six_BogenRechts = BezierPath()   
        
    #x +=4    

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.5, y+2.25)

    # TOP
    HSL_size = 0.25
    HSL_start = 6.4
        
    E1, E2 = line_E_vonD_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)

    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))


    # MIDDLE
    HSL_size = 4
    HSL_start = 42
    
    C3, C4 = line_C_vonB_u_kl_s(C2, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_15, angle_14, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(E3, HSL_start-HSL_size*2, angle_14, angle_13, True))
    

    drawPath(Schriftzug_six_BogenRechts)
    return Schriftzug_six_BogenRechts

#drawSchriftzug_six_BogenRechts(temp_x, temp_y)



def drawSchriftzug_six_full(x, y):
    Schriftzug_six_full = BezierPath()    
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    left = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=10, HSL_start=58.75, clockwise=False, inward=True)
    btm = drawSchneckenzug(*left.points[-1], UPPER_B, 5, HSL_size=0, HSL_start=9, clockwise=False, inward=True)
    Einsatz = drawOutstroke(*btm.points[-1], 0.25)
    middle = drawSchneckenzug(*Einsatz.points[0], LOWER_E, 2, HSL_size=0, HSL_start=28, clockwise=False, inward=False)
    top = drawSchneckenzug(*middle.points[-1], LOWER_C, 6, HSL_size=1, HSL_start=11, clockwise=False, inward=True)

    
    Schriftzug_six_full = left + btm + middle + top + Einsatz
    drawPath(Schriftzug_six_full)
    return Schriftzug_six_full

#drawSchriftzug_six_full(temp_x, temp_y)



def drawSchriftzug_nine_full(x, y):

    Schriftzug_nine_full = BezierPath()   
          
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    top = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=0, HSL_start=9, clockwise=True, inward=True)
    right = drawSchneckenzug(*top.points[-1], LOWER_B, 3, HSL_size=10, HSL_start=28, clockwise=True, inward=False)
    Einsatz = drawOutstroke(*top.points[0], 0.25, "down")
    left = drawSchneckenzug(*Einsatz.points[0], UPPER_E, 2, HSL_size=0, HSL_start=28, clockwise=False, inward=False)
    middle = drawSchneckenzug(*left.points[-1], UPPER_C, 6, HSL_size=0.5, HSL_start=10.5, clockwise=False, inward=True)

    Schriftzug_nine_full = top + right + left + middle + Einsatz
    drawPath(Schriftzug_nine_full)
    return Schriftzug_nine_full

#drawSchriftzug_nine_full(temp_x, temp_y)





def drawSchriftzug_nine_BogenLinks(x, y):
          
    #x +=5
  
    Schriftzug_nine_BogenLinks = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    
    
    # Flanke links
    HSL_size = 8
    HSL_start = 41

    E1, E2 = line_E_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)

    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))


    # Rundung unten
    HSL_size = 0.25
    HSL_start = 6.4

    B3, B4 = line_B_vonC_o_kl_s(B2, *angles, part, HSL_size, HSL_start)
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(B3, HSL_start, angle_8, angle_13))


    drawPath(Schriftzug_nine_BogenLinks)
    return Schriftzug_nine_BogenLinks

#drawSchriftzug_nine_BogenLinks(temp_x, temp_y)









def drawSchriftzug_eight(x, y):        
    x += 2
    Schriftzug_eight = BezierPath()   
 
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4)
    
    oben_nach_links = drawSchneckenzug(*Grund_a, UPPER_A, 8, HSL_size=1, HSL_start=6.5, clockwise=False, inward=False)
    straight_middle = drawGrundelementE(*oben_nach_links.points[-1], length=1.15)
    unten_rechts = drawSchneckenzug(*straight_middle.points[-3], UPPER_A, 4, HSL_size=0.75, HSL_start=13, clockwise=True, inward=False)

    oben_nach_rechts = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=1.1, HSL_start=8, clockwise=True, inward=False)
    straight_middle2 = drawGrundelementA(*oben_nach_rechts.points[-1], 2.475, "down")
    unten_links = drawSchneckenzug(*unten_rechts.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=5.25, clockwise=True, inward=False)

    Schriftzug_eight = oben_nach_links + straight_middle + unten_rechts + oben_nach_rechts + straight_middle2 + unten_links
    drawPath(Schriftzug_eight)

    return Schriftzug_eight

#drawSchriftzug_eight(temp_x, temp_y)







def drawKurrentGatE_helper(x, y):
    
    KurrentGatE_helper = BezierPath()     
 
    #x -= 2 
    save()
    stroke(1,0,1)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-0.5)
    # drawGrundelOrient(A1, A2, offset, x, y+1.5)
    # drawGrundelOrient(A1, A2, offset, x, y+2.5)
    # drawGrundelOrient(A1, A2, offset, x, y+3.5)
    #drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    Grund_a_top, Grund_b_top, Grund_c, Grund_d_top = drawGrundelOrient(A1, A2, offset, x+3, y+4.5)
    drawGrundelementG(*Grund_b_top, 6, "down")

    Grund_a_2, Grund_b_2, Grund_c, Grund_d_2 = drawGrundelOrient(A1, A2, offset, x+6, y+4.5)

    drawGrundelementG(*Grund_a_2, 6, "down")
    drawGrundelementG(*Grund_b_2, 6, "down")

    line(Grund_a_top, Grund_d)

    restore()
    
    return KurrentGatE_helper


#drawKurrentGatE_helper(temp_x, temp_y)








    
def drawSchriftzug_longs_longs(x, y):
    
    Schriftzug_longs_longs = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    
    base = drawSchriftzug1(x, y, "a")
    downstroke = drawGrundelementH(*Grund_a, 6, "down")
    upstroke = drawGrundelementH(*Grund_a, 3.7, "up")

    # draw Modul + Raute
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y+h)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+8.25, y+6.25)
    #Raute_a, Raute_b, Raute_c, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+10.5, y+4.5)
    Signatur_rechts = drawSchneckenzug(*Raute_d, UPPER_E, 7, HSL_size=1, HSL_start=15.69, clockwise=True, inward=True)
    Signatur_links = drawSchneckenzug(*Raute_d, UPPER_E, 1, HSL_size=0, HSL_start=132.5, clockwise=False, inward=False)
    
    outstroke = drawGrundelementH(*Signatur_rechts.points[-1], 1, "down")
    
    Schriftzug_longs_longs += base + downstroke + upstroke + Signatur_rechts + Signatur_links + outstroke
    drawPath(Schriftzug_longs_longs)

    return Schriftzug_longs_longs
    
#drawSchriftzug_longs_longs(temp_x, temp_y)
    
    
    
    
    
    
def drawSchriftzug_longs_t(x, y, base=True):
    
    Schriftzug_longs_t = BezierPath()
    
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    
    if base == True:
        base = drawSchriftzug1(x, y, "a")
        Schriftzug_longs_t += base
    
    downstroke = drawGrundelementH(*Grund_a, 6, "down")
    upstroke = drawGrundelementH(*Grund_a, 3.5, "up")

    # draw Modul + Raute
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y+h)
   
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+10.6, y+6)
    Raute_a, Raute_b, Raute_c, _= drawRauteOrientKurTop(offset, offsetDir, x+8.8, y+6)
    instroke = drawGrundelementE(*Raute_d, 0.125, "unten")
    Signatur_rechts = drawSchneckenzug(*instroke.points[-1], UPPER_A, 4, HSL_size=1, HSL_start=9, clockwise=False, inward=False)

    # Übergang Signatur zu downstroke
    Signatur_links = drawSchneckenzug(*Signatur_rechts.points[-1], UPPER_E, 1, HSL_size=0, HSL_start=152, clockwise=False, inward=False)
    
    Schriftzug_longs_t += downstroke + upstroke + Signatur_rechts + Signatur_links + instroke

    
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    
    Einsatz = drawOutstroke(*Raute_d, 1.125, "down")  
    transition = drawSchneckenzug(*Einsatz.points[0], UPPER_E, 2, HSL_size=2, HSL_start=28, clockwise=False, inward=True)
    stroke = drawGrundelementG(*transition.points[-1], 4.57, "down")
    Querstrich = drawGrundelementC(*stroke.points[-1], 1, "unten")
    outstroke = drawOutstroke(*stroke.points[-1], 1)
    
    Schriftzug_longs_t += Einsatz + transition + stroke + Querstrich + outstroke
    drawPath(Schriftzug_longs_t)

    return Schriftzug_longs_t
    
#drawSchriftzug_longs_t(temp_x, temp_y)





def drawSchriftzug_z_top(x, y):
    
    Schriftzug_z_top = BezierPath()
    #Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+0.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+2.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y+4)

    Schwung_top = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)
    Schwung_btm = drawSchneckenzug(*Schwung_top.points[-1], UPPER_A, 4, HSL_size=1, HSL_start=9, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Schwung_btm.points[-1], 2.25, "down")
    
    Schriftzug_z_top += Schwung_top + Schwung_btm + outstroke
    drawPath(Schriftzug_z_top)

    return Schriftzug_z_top
    
#drawSchriftzug_z_top(temp_x, temp_y)






def drawSchriftzug_etc(x, y):
    
    Schriftzug_etc = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-0.5) 
    top_left = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=1, HSL_start=11, clockwise=True, inward=True)
    top_right = drawSchneckenzug(*top_left.points[-1], LOWER_A, 4, HSL_size=2, HSL_start=8, clockwise=False, inward=False)
    turn = drawSchneckenzug(*top_right.points[-1], LOWER_E, 6, HSL_size=0, HSL_start=5, clockwise=False, inward=True)
    transition = drawSchneckenzug(*turn.points[-1], UPPER_G, 2, HSL_size=6, HSL_start=6, clockwise=False, inward=False)
    straight = drawOutstroke(*transition.points[-1], 3, "down")
    
    hook1 = drawSchneckenzug(*straight.points[0], UPPER_E, 8, HSL_size=1.5, HSL_start=26, clockwise=False, inward=True)
    hook2 = drawSchneckenzug(*hook1.points[-1], LOWER_E, 8, HSL_size=2, HSL_start=20, clockwise=False, inward=True)

    Schriftzug_etc += top_left + top_right + turn + transition + straight + hook1 + hook2
    drawPath(Schriftzug_etc)
    return Schriftzug_etc
     
#drawSchriftzug_etc(temp_x, temp_y)

