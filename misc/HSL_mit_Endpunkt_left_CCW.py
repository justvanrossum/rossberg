import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *


import math as m
from drawBot import *



"""
12. Mai 2020
Theorie S. 25

Wenn am Ende eines Schneckenbogens, welcher mit 4. Bogenmaas und 4. Part in dem Punkte h ander Richtlinie E schließt, der Schlußbogen mit 4. Bogenmaas und 3 3/4 Part an eben dieser Richtlinie Ein dem Punkte i angefangen, auf ieder Richtlinie iedes mal 1/4 Part weniger genommen und damit bis zu demBuchstaben x fortgeschritten wird; so kommt man zuletzt auf die Richtlinie D und in das Centrum des Constru-ctions-Vielecks. Da nun nach dieser fortschreitenden Verminderung bis an den Punkt x von den 3 3/4 Partetwas nicht weiter übrig und also zu der letzten Richtlinie E nichts weiter fortzutragen ist, aber doch noch6. Part übrig geblieben sind; so ist zu ersehen, daß mit dem Radio von 6. Part, nunmehro in dem Centrodes Constructions-Vielecks fortzuziehen und damit der Endpunkt zu formieren sey.

"""





temp_x = 8 #* modul_width
temp_y = 9 #* modul_height



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 16
page_height = 20

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(1)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)






# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)








def drawHSL_left_4_Endpunkt(x, y):
    
    HSL_left_4_Endpunkt = BezierPath()



    #Block 1
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25)
    drawGrundelOrient(A1, A2, offset, x, y-0.75)
    drawGrundelOrient(A1, A2, offset, x, y-1.75)
    drawGrundelOrient(A1, A2, offset, x, y-2.75)
    
    Linie_oben_1 = drawInstroke(*Grund_a,1)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-2.75)
    Linie_oben_1 = drawInstroke(*Grund_d,1)


    #Block 2
    drawGrundelOrient(A1, A2, offset, x, y+1.25)
    drawGrundelOrient(A1, A2, offset, x, y+2.25)
    drawGrundelOrient(A1, A2, offset, x, y+3.25)
    Grund_a, Grund_b, Grund_c, Grund_d  = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    
    Linie_oben_1 = drawInstroke(*Grund_a, 1)
    
    
    #Block 3
    drawGrundelOrient(A1, A2, offset, x, y+5.25)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+8)
    drawGrundelOrient(A1, A2, offset, x, y+6.25)
    drawGrundelOrient(A1, A2, offset, x, y+7.25)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+8.25)
    
    Linie_oben_1 = drawInstroke(*Grund_a, 6)
    Linie_oben_2 = drawOutstroke(*Grund_a, 4)
    
    
    #Block 0
    drawGrundelOrient(A1, A2, offset, x, y-3.75)
    drawGrundelOrient(A1, A2, offset, x, y-4.75)
    drawGrundelOrient(A1, A2, offset, x, y-5.75)
    Grund_a, Grund_b, Grund_c, Grund_d  = drawGrundelOrient(A1, A2, offset, x, y-6.75)
    
    Linie_oben_1 = drawInstroke(*Grund_d, 2)
    Linie_oben_2 = drawOutstroke(*Grund_d, 8)
    
    
    
    
    
    
    HSL_size = 4
    HSL_start = 70 
        
    E1, E2 = line_E_vonF_o_kl_s(Raute_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*8)


    HSL_left_4_Endpunkt.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_9, angle_10))
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_10, angle_11))
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(F1, HSL_start-HSL_size*7, angle_11, angle_12))
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_12, angle_13))




    ### ab jetzt kleiner
    print(calcDistance(*E3, *E4)/part)
    last_value = HSL_start-HSL_size*8   ## >>> 38
    print(last_value)
    # steps = 0.25



    ###
    HSL_size = 4
    HSL_start = 34       # entspricht: 38 - HSL-size
    
    D3, D4 = line_D_vonE_u_kl(E3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(D3, HSL_start, angle_13, angle_14))


    ###
    HSL_size = 3.75
    HSL_start = 34 - 3.75

    C3, C4 = line_C_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(C3, HSL_start, angle_14, angle_15))
    
    
    ###
    HSL_size = 3.5
    HSL_start = 34 - 3.75 - 3.5

    B3, B4 = line_B_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(B3, HSL_start, angle_15, angle_16))
    

    ###
    HSL_size = 3.25
    HSL_start = 34 - 3.75 - 3.5 - 3.25

    A5, A6 = line_A_vonB_o_kl(B3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(A5, HSL_start, angle_0, angle_1))
    
    
    ###
    HSL_size = 3
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3

    H3, H4 = line_H_vonA_o_kl(A5, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(H3, HSL_start, angle_1, angle_2))
    
    
    ###
    HSL_size = 2.75
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75

    G3, G4 = line_G_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(G3, HSL_start, angle_2, angle_3))    


    ###
    HSL_size = 2.5
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5

    F3, F4 = line_F_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(F3, HSL_start, angle_3, angle_4))


    ###
    HSL_size = 2.25
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25

    E5, E6 = line_E_vonF_o_kl(F3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(E5, HSL_start, angle_4, angle_5))


    ###
    HSL_size = 2
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2

    D3, D4 = line_D_vonE_o_kl(E5, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(D3, HSL_start, angle_5, angle_6))


    ###
    HSL_size = 1.75
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2 - 1.75

    C3, C4 = line_C_vonD_o_kl(D3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(C3, HSL_start, angle_6, angle_7))


    ###
    HSL_size = 1.5
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2 - 1.75 - 1.5

    B3, B4 = line_B_vonC_o_kl(C3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(B3, HSL_start, angle_7, angle_8))


    ###
    HSL_size = 1.25
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2 - 1.75 - 1.5 - 1.25

    A7, A8 = line_A_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(A7, HSL_start, angle_8, angle_9))
    
    
    ###
    HSL_size = 1
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2 - 1.75 - 1.5 - 1.25 - 1

    H3, H4 = line_H_vonA_u_kl(A7, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(H3, HSL_start, angle_9, angle_10))
    
    
    ###
    HSL_size = 0.75
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2 - 1.75 - 1.5 - 1.25 - 1 - 0.75

    G3, G4 = line_G_vonH_u_kl(H3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(G3, HSL_start, angle_10, angle_11))
    
    
    ###
    HSL_size = 0.5
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2 - 1.75 - 1.5 - 1.25 - 1 - 0.75 - 0.5

    F3, F4 = line_F_vonG_u_kl(G3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(F3, HSL_start, angle_11, angle_12))
    
    
    ###
    HSL_size = 0.25
    HSL_start = 34 - 3.75 - 3.5 - 3.25 - 3 - 2.75 - 2.5 - 2.25 - 2 - 1.75 - 1.5 - 1.25 - 1 - 0.75 - 0.5 - 0.25

    E7, E8 = line_E_vonF_u_kl(F3, *angles, part, HSL_size, HSL_start)
    HSL_left_4_Endpunkt.arc(*drawKreisSeg(E7, HSL_start, angle_12, angle_13))

    
    ### ab hier Endpunkt
    radius_of_Endpunkt = distance(E7, E8)
    dia_of_Endpunkt = radius_of_Endpunkt*2
    center_of_Endpunkt = E7[0] - radius_of_Endpunkt, E7[1] - radius_of_Endpunkt
    
    #HSL_left_4_Endpunkt.oval(*center_of_Endpunkt, dia_of_Endpunkt, dia_of_Endpunkt)
    
    # Kreis mit arc erzeugt weil ich das Oval nicht unter Kontrolle hatte
    HSL_left_4_Endpunkt.arc(E7, radius_of_Endpunkt, 300, 299.99, False)
    
    #text("E7", E7)
    #text("E8", E8)


    
    drawPath(HSL_left_4_Endpunkt)
        
    return HSL_left_4_Endpunkt
    


drawHSL_left_4_Endpunkt(temp_x, temp_y)



