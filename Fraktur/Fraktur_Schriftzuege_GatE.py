import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente
import version_3.creation.Halbboegen_GatE
import version_3.creation.Schriftteile_GatE

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Halbboegen_GatE)
importlib.reload(version_3.creation.Schriftteile_GatE)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.Halbboegen_GatE import *
from version_3.creation.Schriftteile_GatE import *
from version_3.creation.arc_path import ArcPath as BezierPath

import math as m




temp_x = 3
temp_y = 9


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 16
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)







# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)



stroke(.1)
strokeWidth(.1)






def drawSchriftzug1(x,y, instroke=True, outstroke=True):
    
    Schriftzug1 = BezierPath()  
    pos = x *modul_width, (y+0.75) *modul_height
        
    if instroke == True:
        instroke_start = pos[0] - offset[0]/2,  pos[1] - offset[1]/2
        Schriftzug1.polygon(instroke_start, pos)

    downstroke_end = pos[0], pos[1] - (x_height)*modul_width
    Schriftzug1.polygon(pos, downstroke_end)

    if outstroke == True:
        polygon(downstroke_end, (downstroke_end[0] + offset[0]/2,    downstroke_end[1] + offset[1]/2))
    
    drawPath(Schriftzug1)
    
    return Schriftzug1
   
#drawSchriftzug1 (temp_x, temp_y, instroke=True, outstroke=True)

   
    




def drawSchriftzug2(x, y, instrokeLen=1, AuslaufKehlung=False, ligatureLen=3):
    
    Schriftzug2 = BezierPath()  
    
    # draw Modul + Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    
    pos_stroke = drawPosStroke(x, y)
    
    # draw Modul + Raute - top
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+ligatureLen)    

    instroke_top = drawInstroke(*Raute_d, instrokeLen, "down")
    
    Rundung_oben = drawSchriftteil5(*instroke_top.points[0])
    
    downstroke_start = Rundung_oben.points[-1]
    downstroke_end = downstroke_start[0],          (baseline-0.25)*modul_height

    if AuslaufKehlung == True:
        AuslaufKehlung = drawAuslaufKehlung(x, baseline-0.75)
    
    Schriftzug2 += pos_stroke + instroke_top + Rundung_oben
    Schriftzug2.polygon(downstroke_start, downstroke_end)

    drawPath(Schriftzug2)
    
    return Schriftzug2
    
    
#drawSchriftzug2(temp_x, temp_y, AuslaufKehlung=False, ligatureLen=3)
#drawSchriftzug2(temp_x+3, temp_y, AuslaufKehlung=False)
  
  

  
  
  
def drawSchriftzug_longs_longs_con(x, y):
    
    Schriftzug_longs_longs_con = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1, y+3) 
    
    connection_left = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=1, HSL_start=27.675, clockwise=True, inward=False)
    
    connection_transition = drawSchneckenzug(*connection_left.points[-1], UPPER_G, 3, HSL_size=2, HSL_start=12.25, clockwise=True, inward=True)

    outstroke = drawGrundelementF(*connection_transition.points[-1], 1)
        
    Schriftzug_longs_longs_con += connection_left + connection_transition + outstroke
    drawPath(Schriftzug_longs_longs_con)
    return Schriftzug_longs_longs_con
     
#drawSchriftzug_longs_longs_con(temp_x, temp_y)



def drawSchriftzug_f_f_con(x, y):
    
    Schriftzug_f_f_con = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1, y+5)  
    connection_left = drawSchneckenzug(*Raute_d, UPPER_E, 6, HSL_size=1.5, HSL_start=3, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*connection_left.points[-1], 0.8125)
    connection_transition = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 3, HSL_size=1, HSL_start=9, clockwise=True, inward=True)
    outstroke = drawGrundelementF(*connection_transition.points[-1], 1.5)

    Schriftzug_f_f_con += connection_left + connection_transition + outstroke + Einsatz
    drawPath(Schriftzug_f_f_con)
    return Schriftzug_f_f_con
    
#drawSchriftzug_f_f_con(temp_x, temp_y)
    
    

def drawSchriftzug_f_t_con(x, y):
    
    Schriftzug_f_t_con = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.5, y+2.75)  
    links = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=1, HSL_start=5, clockwise=True, inward=False)
    rechts = drawSchneckenzug(*links.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=8.15, clockwise=False, inward=True)
    
    Schriftzug_f_t_con += links + rechts
    drawPath(Schriftzug_f_t_con)
    return Schriftzug_f_t_con
    
#drawSchriftzug_f_t_con(temp_x, temp_y)



  




def drawSchriftzug3(x,y, top="standard", bottom="standard", instrokeLen=0.5, outstrokeLen=0.5, ascenderLen=2.5):
    
    Schriftzug3 = BezierPath()   
    
        
    if top == "ascender":
    
        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+ascenderLen)    

        pos_stroke = drawPosStroke(x, y)
        instroke = drawGrundelementA(*Raute_d)

        # 1st bend
        Schriftteil5 = drawSchriftteil5(*Raute_d)
        stroke_start = Schriftteil5.points[-1]
        
        Schriftzug3_top = pos_stroke + instroke + Schriftteil5
        
    else:
        
        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y) 

        Rundung_oben = drawSchriftteil3(*Raute_a)
        stroke_start = Rundung_oben.points[-1]
        
        instroke = drawInstroke(*Raute_a, instrokeLen)

        Schriftzug3_top = Rundung_oben + instroke


    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)    
            
    
    if bottom == "gerade":    # für g und j
        stroke_end = stroke_start[0],    Raute_a[1]+modul_height*0.25
        Schriftzug3_bottom = Schriftzug3_top
        
    elif bottom == "Kehlung":
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, baseline+0.25)
        stroke_end = Grund_d
        Schriftzug3_bottom = Schriftzug3_top
    
    else:
        
        Rundung_unten = drawSchriftteil4(*Raute_d)
        stroke_end = Rundung_unten.points[-1]
        outstroke = drawOutstroke(*Raute_d, outstrokeLen)
        Schriftzug3_bottom = Schriftzug3_top + Rundung_unten + outstroke

    
    Schriftzug3 = Schriftzug3_top + Schriftzug3_bottom

    Schriftzug3.polygon(stroke_start, stroke_end)
    
    drawPath(Schriftzug3)
    return Schriftzug3
    

#drawSchriftzug3(temp_x, temp_y, "standard", instrokeLen=1, outstrokeLen=1)  #, instrokeLen=0.5, outstrokeLen=0.5
#drawSchriftzug3(temp_x, temp_y, bottom="gerade", instrokeLen=1)    
#drawSchriftzug3(temp_x, temp_y, bottom="Kehlung", instrokeLen=1.5)    
#drawSchriftzug3(temp_x, temp_y, top="ascender", ascenderLen=2)    







def drawSchriftzug4(x, y, bottom="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug4 = BezierPath() 
    
    
    if bottom == "Schwung":             # für g und j  
    
        main_stroke = drawSchriftzug3(x, y, instrokeLen=instrokeLen, bottom="gerade")  

        # Übergang zu Schwung unten
        HSL_size = 2
        HSL_start = 14

        B1, B2 = line_B_vonC_o_gr_s(main_stroke.points[-2], *angles, part, HSL_size, HSL_start)
        A1, A2 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug4.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))
        
        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y- x_height+1)  
    
        ### Übergang andere Richtung
        HSL_size = 2 
        HSL_start = 12
    
        A3, A4 = line_A_vonH_o_gr_s(A2, *angles, part, HSL_size, HSL_start)

        B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug4.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_0, True))
    
        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug4.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
        D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug4.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    
        E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug4.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))   
                
         
    
    if bottom == "Kehlung":
        main_stroke = drawSchriftzug3(x, y, instrokeLen=instrokeLen, bottom="Kehlung")
    
    
    if bottom == "standard":      
        main_stroke = drawSchriftzug3(x, y, "standard", instrokeLen=instrokeLen, outstrokeLen=outstrokeLen)



    # Raute zur Positionierung der beiden Serifen
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)  
           
    Serife_nach_rechts = drawSchriftteil2(*Raute_a)
    Serife_nach_unten = drawSchriftteil5(*Raute_d)
    
    
    Schriftzug4 += main_stroke + Serife_nach_rechts + Serife_nach_unten
    drawPath(Schriftzug4)
    return Schriftzug4
    
    

#drawSchriftzug4(temp_x, temp_y, "standard")
#drawSchriftzug4(temp_x+2, temp_y, "Schwung", 1)
#drawSchriftzug4(temp_x, temp_y, "Kehlung", instrokeLen=1)

    
    
    
    
    
    
    
    
    
    
def drawSchriftzug5(x, y, top="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug5 = BezierPath()
    
    # Fußteil
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)
    Serife_nach_links = drawSchriftteil6(*Raute_a)
    Serife_nach_unten = drawSchriftteil1(*Raute_a)
    
    

    if top == "ascender":

        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+3)    

        pos_stroke = drawPosStroke(x, y)
        instroke = drawInstroke(*Raute_d, 1, "down")
        bend_top = drawSchriftteil5(*Raute_d)
        
        stroke_start = bend_top.points[-1]    ### keep as 1st reference
        
        # Raute bestimmt Höhe der Oberlänge – Wert eventuell noch anpassen!s
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)  

        bend_bottom = drawSchriftteil4(*Raute_d)
        stroke_end = bend_bottom.points[-1]    ### keep as 2nd reference

        Schriftzug5.line(stroke_start, stroke_end)        ### Hauptlinie nach unten


        outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
        Schriftzug5 += pos_stroke + instroke + bend_top + bend_bottom + Serife_nach_links + Serife_nach_unten + outstroke


    else:        # >>> if top == "standard":
            
        main_stroke = drawSchriftzug3(x, y, instrokeLen=instrokeLen, outstrokeLen=outstrokeLen)
        
        Schriftzug5 += main_stroke + Serife_nach_links + Serife_nach_unten


        
    drawPath(Schriftzug5)

    return Schriftzug5
    
    
#drawSchriftzug5(temp_x, temp_y, top="standard", outstrokeLen=0.5)      
# drawSchriftzug5(temp_x, temp_y, top="ascender", outstrokeLen=0.5)  





def drawSchriftzug6(x,y, Einsatz="standard", instrokeLen=1, outstrokeLen=1):
    
    Schriftzug6 = BezierPath()
    
    # Bei dem Hauptbestandtheile E ist der obere Bogen ebenfalls an der Seite des Hauptbestandtheils dc angefangen, 
    # und mit einem Radio von 3 1/2 Bogenmaas von der Richtlinie C bis zur Richtlinie B, und der untere mit dem obigen Maase 
    # von der Richtlinie B bis zur Hauptlinie formiret.
    # 3 1/2 BM = 28 part

    if Einsatz == "for o":  
        y = y-0.5            ### <<< Wert noch genau bestimmen, wenn o festgelegt wird!
    
    # draw Modul + Raute    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    
    instroke = drawInstroke(*Raute_d, instrokeLen, "down")

        
    HSL_size = 0
    HSL_start = 28

    C1, C2 = line_C_vonD_o_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size)

    Schriftzug6.arc(*drawKreisSeg(B1, HSL_start, angle_7, angle_8))
    

    # Das Ermangelnde aber durch 4 1/2 Modul oder 1 1/2 Grundbestandtheil ergänzet.

    # Raute am Fuß
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)  
    
    A1, A2 = line_A_vonH_u_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size)
    
    Schriftzug6.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))


    outstroke = drawOutstroke(*Raute_d, outstrokeLen)  
    
    Schriftzug6 = Schriftzug6 + instroke + outstroke
    drawPath(Schriftzug6)
    return Schriftzug6


# drawSchriftzug6(temp_x,temp_y)        
# drawSchriftzug6(temp_x,temp_y, Einsatz="for o", instrokeLen=1)        








def drawSchriftzug7(x, y, instrokeLen=2, outstrokeLen=2):
    
    Schriftzug7 = BezierPath()
    
    # E ist auch, außer einem Bogenmaase längern An- und Ausstrichslinien und daß seine Bögen 
    # mit einem Radio von 3. Bogenmaas formiret sind, völlig wie voriger Schrift-Zug Fig. VI. Tab. 10. construiret. 
    # 3 BM = 24 part
    
    # draw Modul + Raute    
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-0.5)  
    
    instroke = drawInstroke(*Raute_d, instrokeLen, "down")

                
    HSL_size = 0
    HSL_start = 24
  
    C1, C2 = line_C_vonD_o_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size)  
    
    Schriftzug7.arc(*drawKreisSeg(B1, HSL_start, angle_7, angle_8))

    # Raute am Fuß
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)  
    
    A1, A2 = line_A_vonH_u_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size)
    
    Schriftzug7.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))


    outstroke = drawOutstroke(*Raute_d, outstrokeLen)    

    Schriftzug7 += instroke + outstroke
    drawPath(Schriftzug7)
    return Schriftzug7


#drawSchriftzug7(temp_x, temp_y)






def drawSchriftzug8(x,y, Einsatz="standard", instrokeLen=1, outstrokeLen=1.4):
    
    Schriftzug8 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-3, y-1.5)
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-3, baseline)
    
    instroke = drawInstroke(*Raute_a, instrokeLen)

    
    # Ebenso ist bei dem Hauptbestandtheile E von der Seite ab der obere Bogen  
    # mit einem Radio von 3. Bogenmaas von der Hauptlinie bis zur Richtlinie B formiret; 
 
    # Der Einsatz zum völligen Innhalte der 4 1/2 Bestandtheile, ist 2 2/3 Modul oder 5/6 Grundbestandtheil. 
    # 24 part bis B, dann 4. HSL 16 part

    HSL_size = 3
    HSL_start = 24
    
    A1, A2 = line_A_vonH_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)

    Schriftzug8.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True))  
    
    # der untere aber von der Richtlinie B mit einem Radio von 3. Bogenmaas angefangen
    # und nach der Endspitze eines Constructions-Bogens der  
    # vierten Hauptschneckenlinie am Ende mit 2. Bogenmaas construiret.
    
    HSL_size = 4    
    HSL_start = 24        
    
    if Einsatz == "standard":
        # Diesen beiden Bögen darf nur, zu Ergänzung des Schrift-Zugs 3 2/3 Modul eingesetzt werden. 
        B2 = B2[0], B2[1] - modul_height*(5/6)-1
        
    if Einsatz == "for o":
        B2 = B2[0], B2[1] - modul_height*((5/6)+0.25)+0.5
        
    if Einsatz == "1":
        B2 = B2[0], B2[1] - modul_height*0.6
        
    HSL_start = 27
    
    B3, B4 = line_B_vonA_u_kl_s(B2, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)

    Schriftzug8.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_16, angle_15, True))  
    Schriftzug8.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_15, angle_14, True))  
    Schriftzug8.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_14, angle_13, True))  
    
    outstroke = drawOutstroke(*E2, outstrokeLen, "down")
    
    Schriftzug8 += instroke + outstroke
    drawPath(Schriftzug8)
    return Schriftzug8


#drawSchriftzug8(temp_x+2, temp_y)      
#drawSchriftzug8(temp_x+2, temp_y, Einsatz = "for o")      






def drawSchriftzug9(x, y, instrokeLen=2, outstrokeLen=0):
    
    Schriftzug9 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    
    instroke = drawInstroke(*Raute_a, instrokeLen)


    # Bei dem Hauptbestandtheile E ist der obere Bogen mit einem Radio von 3 1/2 Bogenmaas von der Hauptlinie 
    # bis zur Richtlinie B formiret; 

    HSL_size = 3
    HSL_start = 28
    
    A1, A2 = line_A_vonH_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    
    Schriftzug9.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True)) 

    # Der Einsatz dieses Schrift-Zugs ist 1 1/4 Grundbestandtheil. -- sagt es ... aber es sind 1.75 gezeichnet!

    Einsatz = 1.75 * modul_height
    B2 = B2[0], B2[1] - Einsatz
    
    # der untere aber ist mit einem Radio von 3 1/2 Bogenmaas bei der Richtlinie B angefangen 
    # und mit der Endspitze eines Constructions-Bogens der vierten Hauptschneckenlinie am Ende durch 2 1/2 Bogenmaas construiert. 
    
    HSL_size = 4
    HSL_start = 32
    
    B3, B4 = line_B_vonA_u_kl_s(B2, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)

    Schriftzug9.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_16, angle_15, True))  
    Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_15, angle_14, True))  
    Schriftzug9.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_14, angle_13, True))  


    Schriftzug9 += instroke
    
    drawPath(Schriftzug9)
    return Schriftzug9
    
    
    
#drawSchriftzug9(temp_x+2, temp_y)
 
 
 
 
 
 
 
def drawSchriftzug_x_hook(x, y):
    
    Schriftzug_x_hook = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y) 


    HSL_size = 1
    HSL_start = 18

    E1, E2 = line_E_vonF_o_kl_s(Raute_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*8)
    D3, D4 = line_D_vonE_u_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size*9)
    C3, C4 = line_C_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*10)
    B3, B4 = line_B_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*11)
    A5, A6 = line_A_vonB_o_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*12)
    H3, H4 = line_H_vonA_o_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*13)
    G3, G4 = line_G_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*14)
    F3, F4 = line_F_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size*15)

    Schriftzug_x_hook.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug_x_hook.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug_x_hook.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug_x_hook.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))
    Schriftzug_x_hook.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_9, angle_10))
    Schriftzug_x_hook.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_10, angle_11))
    Schriftzug_x_hook.arc(*drawKreisSeg(F1, HSL_start-HSL_size*7, angle_11, angle_12))
    Schriftzug_x_hook.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_12, angle_13))
    Schriftzug_x_hook.arc(*drawKreisSeg(D3, HSL_start-HSL_size*9, angle_13, angle_14))
    Schriftzug_x_hook.arc(*drawKreisSeg(C3, HSL_start-HSL_size*10, angle_14, angle_15))
    Schriftzug_x_hook.arc(*drawKreisSeg(B3, HSL_start-HSL_size*11, angle_15, angle_16))
    Schriftzug_x_hook.arc(*drawKreisSeg(A5, HSL_start-HSL_size*12, angle_0, angle_1))
    Schriftzug_x_hook.arc(*drawKreisSeg(H3, HSL_start-HSL_size*13, angle_1, angle_2))
    Schriftzug_x_hook.arc(*drawKreisSeg(G3, HSL_start-HSL_size*14, angle_2, angle_3))
    Schriftzug_x_hook.arc(*drawKreisSeg(F3, HSL_start-HSL_size*15, angle_3, angle_4))
    Schriftzug_x_hook.arc(*drawKreisSeg(E1, HSL_start-HSL_size*16, angle_4, angle_5))

    
    drawPath(Schriftzug_x_hook) 
    return Schriftzug_x_hook
    
    
    
#drawSchriftzug_x_hook(temp_x, temp_y)






 
 
def drawDieresis(x, y):
    
    Dieresis = BezierPath() 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2) 
    links = drawSchriftteil8(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y+2)
    rechts = drawSchriftteil8(*Raute_a)

    Dieresis = links + rechts
    drawPath(Dieresis) 
    return Dieresis
    
    
#drawDieresis(temp_x, temp_y)




def drawSchriftzug_semicolon_btm(x, y):
    
    Schriftzug_semicolon_btm = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y-4.5)
    part1 = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=0, HSL_start=4.5, clockwise=True, inward=False)
    part2 = drawSchneckenzug(*part1.points[-1], UPPER_A, 4, HSL_size=2, HSL_start=4.5, clockwise=True, inward=False)
    
    Schriftzug_semicolon_btm = part1 + part2
    drawPath(Schriftzug_semicolon_btm)   
    return Schriftzug_semicolon_btm
    
    
#drawSchriftzug_semicolon_btm(temp_x, temp_y) 








def drawSchriftzug_question(x, y):
    
    Schriftzug_question = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25)
    drawGrundelOrient(A1, A2, offset, x-0.5, y+3)
    drawGrundelOrient(A1, A2, offset, x, y-3.75)
    
    top = drawSchneckenzug(*Grund_a, LOWER_E, 8, HSL_size=1.125, HSL_start=14.85, clockwise=False, inward=True)
    btm = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=1.125, HSL_start=18, clockwise=False, inward=True)

    Schriftzug_question += top + btm
    drawPath(Schriftzug_question)    
    return Schriftzug_question
    
#drawSchriftzug_question(temp_x, temp_y)














def drawSchriftzug3_Figures(x, y, instrokeLen=0.5, outstrokeLen=0.5):
    
    ### dieser Schriftzug ist wie 3, nur dass er 8 hoch ist, so hoch wie Zahlen/Ziffern
    
    Schriftzug3_fullHeight = BezierPath()   
        
        
    # TOP
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2) 

    Rundung_oben = drawSchriftteil3(*Raute_a)
    stroke_start = Rundung_oben.points[-1]

    instroke = drawInstroke(*Raute_a, instrokeLen)



    # BOTTOM 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)    
        
    Rundung_unten = drawSchriftteil4(*Raute_d)
    stroke_end = Rundung_unten.points[-1]
    
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
    
    Schriftzug3_Figures = instroke + Rundung_oben + Rundung_unten + outstroke

    Schriftzug3_Figures.line(stroke_start, stroke_end)

    drawPath(Schriftzug3_Figures)
    return Schriftzug3_Figures
    
    
#drawSchriftzug3_Figures(temp_x, temp_y)
    
    
    
    
    
    
def drawSchriftzug_two_Top(x, y, instrokeLen=1, outstrokeLen=2.15):
        
    Schriftzug_two_Top = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x+1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x-1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-4.5)


    instroke = drawInstroke(*Grund_a, instrokeLen)


    HSL_size = 0
    HSL_start = 24
        
    G1, G2 = line_G_vonF_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_gr(G1, *angles, part, HSL_size, HSL_start)

    Schriftzug_two_Top.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_13))

        
    Schriftzug_two_Top += instroke

    drawPath(Schriftzug_two_Top)
    return Schriftzug_two_Top
    
    
#drawSchriftzug_two_Top(temp_x, temp_y)







def drawSchriftzug_two_Schwung(x, y, instrokeLen=1):
    
    Schriftzug_two_Schwung = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-4.5)

    instroke = drawInstroke(*Grund_d, instrokeLen, "down")


    ### Teil 1 – links
    HSL_size = 0
    HSL_start = 7
        
    E1, E2 = line_E_vonD_o_gr_s(instroke.points[-1], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, HSL_size, HSL_start)

    Schriftzug_two_Schwung.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_2, True))



    # ### Teil 2 – rechts
    HSL_size = 0
    HSL_start = 11.5
        
    H3, H4 = line_H_vonA_u_gr_s(H2, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_gr(H3, *angles, part, HSL_size, HSL_start)
    
    Schriftzug_two_Schwung.arc(*drawKreisSeg(H3, HSL_start, angle_10, angle_13))

        
    Schriftzug_two_Schwung += instroke

    drawPath(Schriftzug_two_Schwung)
    return Schriftzug_two_Schwung
    
    
#drawSchriftzug_two_Schwung(temp_x, temp_y)







def drawSchriftzug_three_Bogen(x, y):
        
    Schriftzug_three_Bogen = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.625, y-0.75)
    

    HSL_size = 2
    HSL_start = 9.4
        
    E1, E2 = line_E_vonD_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    E3, E4 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)

    Schriftzug_three_Bogen.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_15, angle_14, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_14, angle_13, True))


    drawPath(Schriftzug_three_Bogen)
    return Schriftzug_three_Bogen

#drawSchriftzug_three_Bogen(temp_x, temp_y)








def drawSchriftzug_three_Top(x, y, instrokeLen=1):
        
    Schriftzug_three_Top = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x+3, y+2)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    
    instroke = drawInstroke(*Grund_a, instrokeLen)


    HSL_size = 0
    HSL_start = 16
        
    G1, G2 = line_G_vonH_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
    Schriftzug_three_Top.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    Schriftzug_three_Top.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    Schriftzug_three_Top += instroke
   
    drawPath(Schriftzug_three_Top)
    return Schriftzug_three_Top

#drawSchriftzug_three_Top(temp_x, temp_y)







def drawSchriftzug_five_Top(x, y):
        
    Schriftzug_five_Top = BezierPath()   
        
    # drawGrundelOrient(A1, A2, offset, x, y-4.5)
    # drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    # drawGrundelOrient(A1, A2, offset, x+4, y+2)  
    # Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    
    
    HSL_size = 0
    HSL_start = 16
   
    # Positionierung hier relativ (also x, y), damit ich das Teil direkt ansetzen kann   
    G1, G2 = line_G_vonH_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    E1, E2 = line_E_vonF_u_gr(G1, *angles, part, HSL_size, HSL_start)

    
    Schriftzug_five_Top.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_13))



    drawPath(Schriftzug_five_Top)
    return Schriftzug_five_Top

#drawSchriftzug_five_Top(temp_x, temp_y)






def drawSchriftzug_seven_Stem(x, y):
        
    Schriftzug_seven_Stem = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4, y+2)
    

    HSL_size = 14
    HSL_start = 76
        
    E1, E2 = line_E_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_seven_Stem.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_10))
    ### beim letzten gschummelt und die Linie einfach länger gemacht damit Baseline trifft


    drawPath(Schriftzug_seven_Stem)
    return Schriftzug_seven_Stem

#drawSchriftzug_seven_Stem(temp_x, temp_y)





def drawSchriftzug_zero_BogenLinks(x, y):
        
    Schriftzug_zero_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.75)
    

    HSL_size = 24
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))



    drawPath(Schriftzug_zero_BogenLinks)
    return Schriftzug_zero_BogenLinks

#drawSchriftzug_zero_BogenLinks(temp_x, temp_y)





def drawSchriftzug_zero_BogenRechts(x, y, version="zero"):
        
    Schriftzug_zero_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.75)
    

    HSL_size = 24
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))

    if version == "zero":
    
        Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_13, angle_11, True))


    drawPath(Schriftzug_zero_BogenRechts)
    return Schriftzug_zero_BogenRechts

#drawSchriftzug_zero_BogenRechts(temp_x, temp_y, version="zero")
#drawSchriftzug_zero_BogenRechts(temp_x, temp_y, version="nine")




def drawSchriftzug_six_BogenRechts(x, y):
        
    Schriftzug_six_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y-0.75)
    

    HSL_size = 4
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    #G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))


    drawPath(Schriftzug_six_BogenRechts)
    return Schriftzug_six_BogenRechts

#drawSchriftzug_six_BogenRechts(temp_x, temp_y)






def drawSchriftzug_nine_BogenLinks(x, y):
        
    Schriftzug_nine_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.75)
    

    HSL_size = 4
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))



    drawPath(Schriftzug_nine_BogenLinks)
    return Schriftzug_nine_BogenLinks

#drawSchriftzug_nine_BogenLinks(temp_x, temp_y)





def drawSchriftzug_eight(x, y):
        
    Schriftzug_eight_BogenMiddle = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    

    HSL_size = 0
    HSL_start = 13 

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonH_u_kl(E1, *angles, part, HSL_size, HSL_start)

    straight_middle = drawGrundelementC(*G2, length=1.625)
    
    G3, G4 = line_G_vonF_o_gr_s(straight_middle.points[-1], *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonD_u_gr(G3, *angles, part, HSL_size, HSL_start)
    
    
    Schriftzug_eight_BogenMiddle.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_11))
    Schriftzug_eight_BogenMiddle.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_13, True))



    ### Teil rechts oben
    Schriftzug_eight_BogenRechtsOben = BezierPath()   

    HSL_size = 0
    HSL_start = 13 

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonD_u_kl(E1, *angles, part, HSL_size, HSL_start)
    
    Schriftzug_eight_BogenRechtsOben.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_13, True))
    
    straight_middle2 = drawGrundelementA(*E4, 1.625, "down")



    ### Teil links unten
    HSL_size = 0
    HSL_start = 13

    E5, E6 = line_E_vonF_o_gr_s(straight_middle2.points[-1], *angles, part, HSL_size, HSL_start)
    E7, E8 = line_E_vonF_u_kl(E5, *angles, part, HSL_size, HSL_start)
    
    Schriftzug_eight_BogenRechtsOben.arc(*drawKreisSeg(E5, HSL_start, angle_5, angle_13))


    Schriftzug_eight = Schriftzug_eight_BogenMiddle + Schriftzug_eight_BogenRechtsOben
    drawPath(Schriftzug_eight)

    return Schriftzug_eight

#drawSchriftzug_eight(temp_x, temp_y)




def drawSchriftzug_seven_Top(x, y, instrokeLen=1):
        
    Schriftzug_seven_Top = BezierPath()   
    
    drawGrundelOrient(A1, A2, offset, x+4, y+2)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    
    instroke = drawInstroke(*Grund_a, instrokeLen)
    top = drawSchneckenzug(*Grund_a, LOWER_G, 2, HSL_size=0, HSL_start=32, clockwise=False, inward=False)

    Schriftzug_seven_Top += instroke + top
    drawPath(Schriftzug_seven_Top)
    return Schriftzug_seven_Top

#drawSchriftzug_seven_Top(temp_x, temp_y)















##################################################################################
###            Ab hier Versalien
##################################################################################



temp_x = 3


def drawSchriftzug_BG_Versalien(x, y):
    ### Hintergrund
    
    ### Reihe unten            
    drawGrundelOrientMittig(A1, A2, offset, x, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)  # für J
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-5) 
     
    ### Reihe mitte (x-Höhe)
    drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+3, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+6, y) 
    
    ### Element oben
    drawGrundelOrientMittig(A1, A2, offset, x+3, y+3) 
    
    ### 1. Extra für M, W und breite Glyphen
    drawGrundelOrientMittig(A1, A2, offset, x+9, y-5) #unten
    drawGrundelOrientMittig(A1, A2, offset, x+9, y) #mitte
    drawGrundelOrientMittig(A1, A2, offset, x+9, y+3)  #oben 

    ### Extra für M, W und breite Glyphen
    drawGrundelOrientMittig(A1, A2, offset, x+12, y-5) #unten
    drawGrundelOrientMittig(A1, A2, offset, x+12, y) #mitte
    drawGrundelOrientMittig(A1, A2, offset, x+12, y+3)  #oben 


drawSchriftzug_BG_Versalien(temp_x, temp_y)













def drawSchriftzug_F_stehenderSchwung(x, y):
        
    Schriftzug_F_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+0.25) 


    
    Einsatz = drawGrundelementE(*Grund_a, length=0.5)
    
    ### stehenderSchwung OBEN
    HSL_size = 1
    HSL_start = 10
   

    A3, A4 = line_A_vonB_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    instroke = drawInstroke(*E2, 0.5, "down")
    
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))




    ### stehenderSchwung UNTEN
    HSL_size = 4
    HSL_start = 36


    A5, A6 = line_A_vonH_o_kl_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*4, angle_14, angle_13, True))

    Schriftzug_F_stehenderSchwung += instroke

    drawPath(Schriftzug_F_stehenderSchwung)

    return Schriftzug_F_stehenderSchwung

#drawSchriftzug_F_stehenderSchwung(temp_x, temp_y)







def drawSchriftzug_F_liegenderSchwung(x, y):
        
    Schriftzug_F_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+5.5, y+3.5) 

    ### Eigentlich steht bei Roßberg Schriftteil 9, daber der scheint irgendwie zu klein
    #left = drawSchriftteil9(*Grund_a)
    
    
    HSL_size = 1
    HSL_start = 5
    
    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))   
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))      
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))      
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(A3, HSL_start, angle_2, angle_1, True))      

    
    
    Einsatz = drawGrundelementE(*Grund_a, length = 0)


    HSL_size = 1
    HSL_start = 5
    
    A5, A6 = line_A_vonB_u_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))      
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))      
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))      
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    

    Schriftzug_F_liegenderSchwung += Einsatz

    drawPath(Schriftzug_F_liegenderSchwung)

    return Schriftzug_F_liegenderSchwung

#drawSchriftzug_F_liegenderSchwung(temp_x, temp_y)











def drawSchriftzug_I_stehenderSchwung(x, y):
        
    Schriftzug_I_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+0.25) 

    Einsatz = drawGrundelementE(*Grund_a, length=0.5)
    
    ### stehenderSchwung OBEN
    HSL_size = 1.25
    HSL_start = 6
   

    A3, A4 = line_A_vonB_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))




    ### stehenderSchwung UNTEN
    HSL_size = 4
    HSL_start = 36


    A5, A6 = line_A_vonH_o_kl_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*4, angle_14, angle_13, True))

    
    drawPath(Schriftzug_I_stehenderSchwung)

    return Schriftzug_I_stehenderSchwung

#drawSchriftzug_I_stehenderSchwung(temp_x, temp_y)







def drawSchriftzug_I_liegenderSchwung(x, y):
        
    Schriftzug_I_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4.25, y+3.25) 

    ### Eigentlich steht bei Roßberg Schriftteil 9, daber der scheint irgendwie zu klein
    #left = drawSchriftteil9(*Grund_a)
    
    
    HSL_size = 2
    HSL_start = 4
    
    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_I_liegenderSchwung.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_2))      
    Schriftzug_I_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_3))      
    Schriftzug_I_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_4))      
    Schriftzug_I_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_4, angle_5))   
    
    
    
    Einsatz = drawGrundelementE(*Grund_a, length = 0.5)
    right = drawSchriftteil10(*Einsatz.points[4])

    Schriftzug_I_liegenderSchwung += Einsatz + right

    drawPath(Schriftzug_I_liegenderSchwung)

    return Schriftzug_I_liegenderSchwung

#drawSchriftzug_I_liegenderSchwung(temp_x, temp_y)








def drawSchriftzug_J_stehenderSchwung(x, y):
        
    Schriftzug_J_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+0.25) 

    Einsatz = drawGrundelementE(*Grund_a, length=1)
    
    ### stehenderSchwung OBEN
    HSL_size = 1.5
    HSL_start = 6
   

    A3, A4 = line_A_vonB_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))




    ### stehenderSchwung UNTEN
    HSL_size = 2
    HSL_start = 42


    A5, A6 = line_A_vonH_o_kl_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*4, angle_14, angle_13, True))

    
    drawPath(Schriftzug_J_stehenderSchwung)

    return Schriftzug_J_stehenderSchwung

#drawSchriftzug_J_stehenderSchwung(temp_x, temp_y)







def drawSchriftzug_J_liegenderSchwung(x, y):
        
    Schriftzug_J_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4.25, y+3.25) 

    ### Eigentlich steht bei Roßberg Schriftteil 9, daber der scheint irgendwie zu klein
    #left = drawSchriftteil9(*Grund_a)
    
    
    HSL_size = 2
    HSL_start = 10
    
    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_2))      
    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_3))      
    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_4))      
    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_4, angle_5))   
    
    
    
    Einsatz = drawGrundelementE(*Grund_a, length = 0.5)
    
    right = drawSchriftteil10(*Einsatz.points[4])

    Schriftzug_J_liegenderSchwung += Einsatz + right

    drawPath(Schriftzug_J_liegenderSchwung)

    return Schriftzug_J_liegenderSchwung

#drawSchriftzug_J_liegenderSchwung(temp_x, temp_y)







def drawSchriftzug_L_stehenderSchwung(x, y):
        
    Schriftzug_L_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1.5) 
    Einsatz = drawGrundelementE(*Grund_a, length=0.5, pos="unten")
    oben = drawSchneckenzug(*Einsatz.points[-1], LOWER_A, 4, HSL_size=2, HSL_start=26, clockwise=True, inward=True)
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=2, HSL_start=19, clockwise=True, inward=True)
    
    Schriftzug_L_stehenderSchwung += unten + Einsatz + oben
    drawPath(Schriftzug_L_stehenderSchwung)
    return Schriftzug_L_stehenderSchwung

#drawSchriftzug_L_stehenderSchwung(temp_x, temp_y)








def drawSchriftzug_L_Fuss(x, y):
    
    Schriftzug_L_Fuss = BezierPath() 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2.5, y-5.25) 

    
    HSL_size = 1
    HSL_start = 2

    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)  
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_L_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug_L_Fuss.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug_L_Fuss.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug_L_Fuss.arc(*drawKreisSeg(A3, HSL_start, angle_2, angle_1, True))

    Einsatz = drawGrundelementE(*Grund_a, 0.5)
    
    # Wendung
    A5, A6 = line_A_vonB_u_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)        
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_L_Fuss.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_L_Fuss.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_L_Fuss.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_L_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))

    outstroke = drawOutstroke(*E2, 0.5)
    Schriftzug_L_Fuss += Einsatz + outstroke
    drawPath(Schriftzug_L_Fuss)
    return Schriftzug_L_Fuss

#drawSchriftzug_L_Fuss(temp_x, temp_y)









def drawSchriftzug_T_stehenderSchwung(x, y):
        
    Schriftzug_T_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3, y-1.5) 


    Einsatz = drawGrundelementE(*Grund_a, length=0)
    
    ### stehenderSchwung OBEN
    HSL_size = 1
    HSL_start = 10
   

    A3, A4 = line_A_vonB_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    instroke = drawInstroke(*E2, 0.25, "down")
    
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))


    ### stehenderSchwung UNTEN
    HSL_size = 4
    HSL_start = 23


    A5, A6 = line_A_vonH_o_kl_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*4, angle_14, angle_13, True))
    
    outstroke = drawOutstroke(*E4, 0.5, "down")


    Schriftzug_T_stehenderSchwung += instroke + outstroke

    drawPath(Schriftzug_T_stehenderSchwung)

    return Schriftzug_T_stehenderSchwung

#drawSchriftzug_T_stehenderSchwung(temp_x, temp_y)









def drawSchriftzug_S_stehenderSchwung(x, y):
        
    Schriftzug_S_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.25, y-1.35)  
    Einsatz = drawGrundelementE(*Grund_a, length=0.65, pos="unten")   
    oben = drawSchneckenzug(*Einsatz.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=17, clockwise=True, inward=False)    
    instroke = drawInstroke(*oben.points[-1], 1, "down")
    connection = drawOutstroke(*Grund_a, 2.5)
    unten_1 = drawSchneckenzug(*connection.points[0], UPPER_A, 1, HSL_size=4, HSL_start=30, clockwise=True, inward=False)
    unten_2 = drawSchneckenzug(*unten_1.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=27.7, clockwise=True, inward=False)
    outstroke = drawOutstroke(*unten_2.points[-1], 0.5, "down")

    Schriftzug_S_stehenderSchwung += Einsatz + oben + connection + unten_1 + unten_2 + instroke + outstroke 
    drawPath(Schriftzug_S_stehenderSchwung)
    return Schriftzug_S_stehenderSchwung

#drawSchriftzug_S_stehenderSchwung(temp_x, temp_y)



def drawSchriftzug_S_liegenderSchwung(x, y, instrokeLen=0, outstrokeLen=0.5):
    #x +=3   
    Schriftzug_S_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1.11, y-5) 
    
    
    HSL_size = 1
    HSL_start = 3

    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)  
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(A3, HSL_start, angle_2, angle_1, True))

    instroke = drawInstroke(*E2, instrokeLen)

    Einsatz = drawGrundelementE(*Grund_a, 0.5)
    
    HSL_start = 4
    # Wendung
    A5, A6 = line_A_vonB_u_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)        
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    outstroke = drawOutstroke(*E2, outstrokeLen)

    Schriftzug_S_liegenderSchwung += instroke + Einsatz + outstroke
    drawPath(Schriftzug_S_liegenderSchwung)
    return Schriftzug_S_liegenderSchwung

#drawSchriftzug_S_liegenderSchwung(temp_x, temp_y)










def drawSchriftzug_K_stehenderSchwung(x, y):
        
    Schriftzug_K_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1) 
    oben = drawSchneckenzug(*Grund_a, LOWER_A, 4, HSL_size=4, HSL_start=30, clockwise=True, inward=True)
    unten = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=2, HSL_start=26, clockwise=True, inward=True)
    instroke = drawInstroke(*oben.points[-1], 0.5, "down")
    outstroke = drawOutstroke(*unten.points[-1], 0.5, "down")

    Schriftzug_K_stehenderSchwung += unten + oben + instroke + outstroke
    drawPath(Schriftzug_K_stehenderSchwung)
    return Schriftzug_K_stehenderSchwung

#drawSchriftzug_K_stehenderSchwung(temp_x, temp_y)










def drawSchriftzug_B_Hauptbogen(x, y):
        
    Schriftzug_B_Hauptbogen = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2, y+2) 

    
    HSL_size = 1
    HSL_start = 29
        
    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*8)
        
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_2, angle_1, True))
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_1, angle_0, True))
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(C1, HSL_start-HSL_size*6, angle_16, angle_15, True))
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(D1, HSL_start-HSL_size*7, angle_15, angle_14, True))
    Schriftzug_B_Hauptbogen.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_14, angle_13, True))


    drawPath(Schriftzug_B_Hauptbogen)

    return Schriftzug_B_Hauptbogen

#drawSchriftzug_B_Hauptbogen(temp_x, temp_y)





    
    
def drawSchriftzug_B_Bauch(x, y):
        
    Schriftzug_B_Bauch = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.25, y-0.375) 
    Anfang = drawSchriftteil9(*Grund_a)
    Weiter = drawSchriftteil3(*Grund_a)
    Einsatz= drawGrundelementF(*Weiter.points[-1], 0.45)
    Ende = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=28, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Ende.points[-1], 1.1, "down")

    Schriftzug_B_Bauch += Anfang + Weiter + Einsatz + Ende + outstroke
    drawPath(Schriftzug_B_Bauch)
    return Schriftzug_B_Bauch
    
    
#drawSchriftzug_B_Bauch(temp_x, temp_y)

    
    


 



def drawSchriftzug_G_Hauptbogen(x, y):
        
    Schriftzug_G_Hauptbogen = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-1.75, y+2.5) 

    
    HSL_size = 1
    HSL_start = 29.5
        
    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*8)
        
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_2, angle_1, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_1, angle_0, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(C1, HSL_start-HSL_size*6, angle_16, angle_15, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(D1, HSL_start-HSL_size*7, angle_15, angle_14, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_14, angle_13, True))
    

    drawPath(Schriftzug_G_Hauptbogen)

    return Schriftzug_G_Hauptbogen

#drawSchriftzug_G_Hauptbogen(temp_x, temp_y)





def drawSchriftzug_G_Deckung(x, y):
        
    Schriftzug_G_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.75, y-3.5) 

    
    HSL_size = 0.5
    HSL_start = 5

    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug_G_Deckung.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug_G_Deckung.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))

    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug_G_Deckung.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    
    A5, A6 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    Schriftzug_G_Deckung.arc(*drawKreisSeg(A5, HSL_start-HSL_size*4, angle_2, angle_1, True))  
    
    
    ### zweiter Teil
    HSL_size = 0.5
    HSL_start = 5
    
    A4, A5 = line_A_vonH_o_kl_s(A6, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_kl(A4, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_G_Deckung.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))


    drawPath(Schriftzug_G_Deckung)

    return Schriftzug_G_Deckung

#drawSchriftzug_G_Deckung(temp_x, temp_y)








def drawSchriftzug_G_Fuss(x, y):
        
    Schriftzug_G_Fuss = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.4, y-4) 

    links = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=2, HSL_start=4, clockwise=False, inward=False)
    Einsatz = drawGrundelementE(*Grund_a, 1.5)
    rechts = drawSchneckenzug(*Einsatz.points[4], LOWER_A, 4, HSL_size=2, HSL_start=10, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], 1)
    
    Schriftzug_G_Fuss = links + Einsatz + rechts + outstroke
    drawPath(Schriftzug_G_Fuss)

    return Schriftzug_G_Fuss
    
#drawSchriftzug_G_Fuss(temp_x, temp_y)

    
    
    
def drawSchriftzug_G_Bauch(x, y):
        
    Schriftzug_G_Bauch = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5, y+0.5) 

    
    HSL_size = 1
    HSL_start = 9.75

    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug_G_Bauch.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug_G_Bauch.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))

    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug_G_Bauch.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    Schriftzug_G_Bauch.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_2, angle_1, True)) 
    
    


    # ### stehenderSchwung UNTEN
    HSL_size = 6
    HSL_start = 11.35

    A5, A6 = line_A_vonH_o_gr_s(A4, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C3, C4 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D3, D4 = line_D_vonC_u_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E3, E4 = line_E_vonD_u_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_G_Bauch.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(C3, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(D3, HSL_start+HSL_size*5, angle_14, angle_13, True))
    
    outstroke = drawOutstroke(*E4, 0.25, "down")

    drawPath(Schriftzug_G_Bauch)

    return Schriftzug_G_Bauch
    
    
#drawSchriftzug_G_Bauch(temp_x, temp_y)

    
    

def drawSchriftzug_G_stehenderSchwung(x, y, instrokeLen=0.5):
        
    Schriftzug_G_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y) 
    Einsatz = drawGrundelementE(*Grund_a, length=0.5)
    Einsatz2 = drawGrundelementE(*Grund_a, length=0.25, pos="unten")
    oben = drawSchneckenzug(*Einsatz2.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=9, clockwise=True, inward=False)
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=2, HSL_start=18, clockwise=True, inward=True)
    instroke = drawInstroke(*oben.points[-1], instrokeLen, "down")

    Schriftzug_G_stehenderSchwung += unten + oben +  Einsatz + Einsatz2 + instroke
    drawPath(Schriftzug_G_stehenderSchwung)
    return Schriftzug_G_stehenderSchwung

#drawSchriftzug_G_stehenderSchwung(temp_x, temp_y)

   


def drawSchriftzug_D_stehenderSchwung(x, y, instrokeLen=0):
        
    Schriftzug_D_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y) 
    
    Einsatz = drawGrundelementE(*Grund_a, length=0.5)
    oben = drawSchneckenzug(*Grund_a, LOWER_A, 4, HSL_size=0.25, HSL_start=10, clockwise=True, inward=False)
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1, HSL_start=18, clockwise=True, inward=True)
    instroke = drawInstroke(*oben.points[-1], instrokeLen, "down")

    Schriftzug_D_stehenderSchwung += instroke + unten + oben +  Einsatz
    drawPath(Schriftzug_D_stehenderSchwung)
    return Schriftzug_D_stehenderSchwung

#drawSchriftzug_D_stehenderSchwung(temp_x, temp_y)




def drawSchriftzug_R_Hauptbogen(x, y):
        
    Schriftzug_R_Hauptbogen = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2, y+2) 
    
    Hauptbogen_top = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=30, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*Hauptbogen_top.points[-1], 1)
    Hauptbogen_btm = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=24.5, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Hauptbogen_btm.points[-1], 0.5, "down")
    
    Schriftzug_R_Hauptbogen += Hauptbogen_top + Einsatz + Hauptbogen_btm + outstroke
    drawPath(Schriftzug_R_Hauptbogen)

    return Schriftzug_R_Hauptbogen

#drawSchriftzug_R_Hauptbogen(temp_x, temp_y)








def drawSchriftzug_R_Deckung(x, y):
        
    Schriftzug_R_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-1.1, y-5) 

    
    HSL_size = 0.5
    HSL_start = 7.5

    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug_R_Deckung.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug_R_Deckung.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))

    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug_R_Deckung.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    
    A5, A6 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    Schriftzug_R_Deckung.arc(*drawKreisSeg(A5, HSL_start-HSL_size*4, angle_2, angle_1, True))  
    
    
    ### zweiter Teil
    
    A4, A5 = line_A_vonH_o_kl_s(A6, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_kl(A4, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_R_Deckung.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))

    

    drawPath(Schriftzug_R_Deckung)

    return Schriftzug_R_Deckung

#drawSchriftzug_R_Deckung(temp_x, temp_y)








def drawSchriftzug_R_Fuss(x, y):
        
    Schriftzug_R_Fuss = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4.4, y-5) 
 
    HSL_size = 1
    HSL_start = 5

    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)  
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_R_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug_R_Fuss.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug_R_Fuss.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug_R_Fuss.arc(*drawKreisSeg(A3, HSL_start, angle_2, angle_1, True))

    Einsatz = drawGrundelementE(*Grund_a, 0.6)
    

    # Wendung
    A5, A6 = line_A_vonB_u_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)        
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_R_Fuss.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_R_Fuss.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_R_Fuss.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_R_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    
    outstroke = drawOutstroke(*E2,0.25)   

    drawPath(Schriftzug_R_Fuss)

    return Schriftzug_R_Fuss
    
    
#drawSchriftzug_R_Fuss(temp_x, temp_y)





def drawSchriftzug_X_BogenRechts(x, y):
    
    Schriftzug_X_BogenRechts = BezierPath()   

    drawGrundelOrientMittig(A1, A2, offset, x+6, y+3) 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3, y-2.5) 
    
    Mitte_nachUnten = drawSchneckenzug(*Grund_a, UPPER_B, 5, HSL_size=6, HSL_start=37.2, clockwise=False, inward=True)
    Einsatz = drawGrundelementF(*Grund_a, 1, "up")
    Mitte_nachOben = drawSchneckenzug(*Einsatz.points[-1], UPPER_B, 3, HSL_size=6, HSL_start=43, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Mitte_nachOben.points[-1], 0.25)
    
    Schriftzug_X_BogenRechts += Mitte_nachUnten + Einsatz + Mitte_nachOben + outstroke
    drawPath(Schriftzug_X_BogenRechts)
    return Schriftzug_X_BogenRechts

#drawSchriftzug_X_BogenRechts(temp_x, temp_y)




  
 
    
def drawSchriftzug_X_DeckungOben(x, y):
        
    Schriftzug_X_DeckungOben = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.25, y+3.7) 
        
    ### Teil oben bis A
    HSL_size = 0.5
    HSL_start = 2.5
        
    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))


    ### Teil unten ab A
    HSL_size = 0.5
    HSL_start = 5.5
    
    A5, A6 = line_A_vonB_u_gr_s(A4, *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))

    G3, G4 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(G3, HSL_start+HSL_size*2, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))


    outstroke = drawOutstroke(*E2, 0)


    drawPath(Schriftzug_X_DeckungOben)

    return Schriftzug_X_DeckungOben

#drawSchriftzug_X_DeckungOben(temp_x, temp_y)
 
    
    
    
    
    






def drawSchriftzug_O_stehenderSchwung(x, y):
        
    Schriftzug_O_stehenderSchwung = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1) 

    Einsatz = drawGrundelementE(*Grund_a, length=0.5, pos="unten")


    ### stehenderSchwung OBEN
    HSL_size = 2
    HSL_start = 19.75
   
    A3, A4 = line_A_vonH_u_kl_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    
    instroke = drawInstroke(*E2, 0.5, "down")

    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_5, angle_6))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_6, angle_7))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_8))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_8, angle_9))



    ### stehenderSchwung UNTEN
    HSL_size = 2
    HSL_start = 18

    A5, A6 = line_A_vonH_o_kl_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*4, angle_14, angle_13, True))



    Schriftzug_O_stehenderSchwung += instroke + Einsatz
    drawPath(Schriftzug_O_stehenderSchwung)

    return Schriftzug_O_stehenderSchwung


#drawSchriftzug_O_stehenderSchwung(temp_x, temp_y)




def drawSchriftzug_O_liegenderSchwung(x, y):
        
    Schriftzug_O_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-1.5, y-5) 
    
    ### linkes Element
    HSL_size = 1
    HSL_start = 3.5

    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)  
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(A3, HSL_start, angle_2, angle_1, True))


    Einsatz = drawGrundelementE(*Grund_a, 1)
    

    # rechtes Element
    A5, A6 = line_A_vonB_u_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)        
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    
    outstroke = drawOutstroke(*E2, 1.25)
    
    Schriftzug_O_liegenderSchwung += Einsatz + outstroke
    drawPath(Schriftzug_O_liegenderSchwung)

    return Schriftzug_O_liegenderSchwung

#drawSchriftzug_O_liegenderSchwung(temp_x, temp_y)












def drawSchriftzug_O_BogenRechts(x, y):
        
    Schriftzug_O_BogenRechts = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2.75, y+2.5)


    ### Spitze ganz OBEN
    HSL_size = 1
    HSL_start = 12
   
    A3, A4 = line_A_vonH_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    
    instroke = drawInstroke(*E2, 0.1, "down")

    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_5, angle_6))
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_6, angle_7))
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_8))
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_8, angle_9))
    
    
    Einsatz = drawGrundelementE(*Grund_a, length=1.55, pos="oben")
 

    ### von OBEN aus nach MITTE
    HSL_size = 0
    HSL_start = 46

    A5, A6 = line_A_vonH_o_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)
    
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(B3, HSL_start+HSL_size, angle_1, angle_0, True))
    
    
    ### von Mitte aus nach UNTEN
    HSL_size = 2
    HSL_start = 30
    
    B5, B6 = line_B_vonA_u_kl_s(B4, *angles, part, HSL_size, HSL_start)
    C3, C4 = line_C_vonB_u_kl(B5, *angles, part, HSL_size, HSL_start-HSL_size)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*3)

    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(C3, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(D3, HSL_start-HSL_size*2, angle_15, angle_14, True))
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(E3, HSL_start-HSL_size*3, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E4, 1.25, "down")


    Schriftzug_O_BogenRechts += outstroke
    drawPath(Schriftzug_O_BogenRechts)
    return Schriftzug_O_BogenRechts


#drawSchriftzug_O_BogenRechts(temp_x, temp_y)










def drawSchriftzug_Q_liegenderSchwung(x, y):
        
    Schriftzug_Q_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2.5, y-4.5) 
    
    ### linkes Element
    HSL_size = 1
    HSL_start = 4

    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)  
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(A3, HSL_start, angle_2, angle_1, True))


    Einsatz = drawGrundelementE(*Grund_a, 1.5)
    

    # rechtes Element
    A5, A6 = line_A_vonB_u_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)        
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    Schriftzug_Q_liegenderSchwung += Einsatz

    drawPath(Schriftzug_Q_liegenderSchwung)

    return Schriftzug_Q_liegenderSchwung

#drawSchriftzug_Q_liegenderSchwung(temp_x, temp_y)








def drawSchriftzug_D_liegenderSchwung(x, y):
        
    Schriftzug_D_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.75, y-5.4) 
    
    ### linkes Element
    HSL_size = 1
    HSL_start = 3.5

    A3, A4 = line_A_vonB_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)  
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(A3, HSL_start, angle_2, angle_1, True))


    Einsatz = drawGrundelementE(*Grund_a, 0.5)
    

    # rechtes Element
    A5, A6 = line_A_vonB_u_gr_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)        
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    
    outstroke = drawOutstroke(*E2, 2.1)
    
    Schriftzug_D_liegenderSchwung += Einsatz + outstroke


    
    outstroke = drawOutstroke(*E2, 0.7)
    
    
    Schriftzug_D_liegenderSchwung += outstroke

    drawPath(Schriftzug_D_liegenderSchwung)

    return Schriftzug_D_liegenderSchwung

#drawSchriftzug_D_liegenderSchwung(temp_x, temp_y)

 
 
 
 
def drawSchriftzug_D_Hauptbogen(x, y):
        
    Schriftzug_D_Hauptbogen = BezierPath()   
    
    #drawGrundelOrientMittig(A1, A2, offset, x-2, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1, y+3) 
    
    HSL_size = 1
    HSL_start = 31.55
        
    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    Einsatz = drawGrundelementF(*B2, 1.5)

    B3, B4 = line_B_vonA_u_kl_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start-HSL_size*5)
    C1, C2 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*8)
        
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_2, angle_1, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_1, angle_0, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(C1, HSL_start-HSL_size*6, angle_16, angle_15, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(D1, HSL_start-HSL_size*7, angle_15, angle_14, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_14, angle_13, True))
    
    #outstroke = drawOutstroke(*E4, 0.25, "down")

    drawPath(Schriftzug_D_Hauptbogen)

    return Schriftzug_D_Hauptbogen

#drawSchriftzug_D_Hauptbogen(temp_x, temp_y)




    
def drawSchriftzug_D_Deckung(x, y):
        
    Schriftzug_D_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2) 
    Schriftzug_D_Deckung = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=0.5, HSL_start=1, clockwise=False, inward=False)

    drawPath(Schriftzug_D_Deckung)
    return Schriftzug_D_Deckung

#drawSchriftzug_D_Deckung(temp_x, temp_y)


 

    
    




def drawSchriftzug_A_Hauptstrich_links(x, y):
        
    Schriftzug_A_Hauptstrich_links = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+4.5, y+2.25) 

    instroke  = drawInstroke(*Grund_a, 0.25, "down")


    ### Bogen OBEN
    HSL_size = 0.25
    HSL_start = 19
   
    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)

    downstroke = drawGrundelementF(*B2, 3)

    Schriftzug_A_Hauptstrich_links.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))
    Schriftzug_A_Hauptstrich_links.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))
    Schriftzug_A_Hauptstrich_links.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))


    ### Bogen UNTEN
    HSL_size = 1
    HSL_start = 17
    
    B3, B4 = line_B_vonC_u_kl_s(downstroke.points[-1], *angles, part, HSL_size, HSL_start)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*3)

    Schriftzug_A_Hauptstrich_links.arc(*drawKreisSeg(C3, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug_A_Hauptstrich_links.arc(*drawKreisSeg(D3, HSL_start-HSL_size*2, angle_15, angle_14, True))
    Schriftzug_A_Hauptstrich_links.arc(*drawKreisSeg(E3, HSL_start-HSL_size*3, angle_14, angle_13, True))
    

    outstroke  = drawOutstroke(*E4, 0.25, "down")


    Schriftzug_A_Hauptstrich_links += instroke + downstroke + outstroke

    drawPath(Schriftzug_A_Hauptstrich_links)

    return Schriftzug_A_Hauptstrich_links


#drawSchriftzug_A_Hauptstrich_links(temp_x, temp_y)








    




def drawSchriftzug_A_Hauptstrich_rechts(x, y):
        
    Schriftzug_A_Hauptstrich_rechts = BezierPath()   
        


    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1, y+3.25) 


    ### Bogen OBEN
    HSL_size = 1
    HSL_start = 23.25
   
    E1, E2 = line_E_vonD_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)


    downstroke = drawGrundelementF(*B2, 3.75)

    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))


    ### Bogen UNTEN
    HSL_size = 1
    HSL_start = 22
    
    B3, B4 = line_B_vonC_o_kl_s(downstroke.points[-1], *angles, part, HSL_size, HSL_start)
    A5, A6 = line_A_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
    H3, H4 = line_H_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*2)

    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(A5, HSL_start-HSL_size, angle_8, angle_9))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(H3, HSL_start-HSL_size*2, angle_9, angle_10))

    outstroke = drawOutstroke(*H4, 0.5)

    Schriftzug_A_Hauptstrich_rechts += downstroke + outstroke

    drawPath(Schriftzug_A_Hauptstrich_rechts)

    return Schriftzug_A_Hauptstrich_rechts


#drawSchriftzug_A_Hauptstrich_rechts(temp_x, temp_y)



      
      
      
  
 
    
def drawSchriftzug_A_DeckungOben(x, y):
        
    Schriftzug_A_DeckungOben = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+7.75, y+4.35)     
    
    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=0.5, HSL_start=5, clockwise=False, inward=False)

    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=6, clockwise=False, inward=False)

    Schriftzug_A_DeckungOben  = DeckungOben_left + DeckungOben_right
    drawPath(Schriftzug_A_DeckungOben)

    return Schriftzug_A_DeckungOben

#drawSchriftzug_A_DeckungOben(temp_x, temp_y)









def drawSchriftzug_H_stehenderSchwung(x, y):
        
    Schriftzug_H_stehenderSchwung = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+3.75)   
    instroke = drawInstroke(*Grund_d, 1, "down")
    Schwung_oben = drawSchneckenzug(*Grund_d, UPPER_E, 4, HSL_size=1, HSL_start=16, clockwise=False, inward=False)
    Einsatz = drawGrundelementE(*Schwung_oben.points[-1], 1.25)
    Schwung_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1, HSL_start=18.5, clockwise=True, inward=True)

    Schriftzug_H_stehenderSchwung += instroke + Schwung_oben + Einsatz + Schwung_unten
    drawPath(Schriftzug_H_stehenderSchwung)
    return Schriftzug_H_stehenderSchwung

#drawSchriftzug_H_stehenderSchwung(temp_x, temp_y)








def drawSchriftzug_H_Hauptstrich_rechts(x, y):
        
    Schriftzug_H_Hauptstrich_rechts = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+1) 
    instroke = drawInstroke(*Grund_a, 0)
    Schwung_Anfang = drawSchneckenzug(*Grund_a, UPPER_B, 1, HSL_size=1, HSL_start=12, clockwise=False, inward=True)
    Schwung_Mitte = drawSchneckenzug(*Schwung_Anfang.points[-1], UPPER_A, 1, HSL_size=2, HSL_start=58, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*Schwung_Mitte.points[-1], 0.25)
    Schwung_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=38, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Schwung_unten.points[-1], 0, "down")

    Schriftzug_H_Hauptstrich_rechts += instroke + Schwung_Anfang + Schwung_Mitte + Einsatz + Schwung_unten + outstroke
    drawPath(Schriftzug_H_Hauptstrich_rechts)
    return Schriftzug_H_Hauptstrich_rechts

#drawSchriftzug_H_Hauptstrich_rechts(temp_x, temp_y)








def drawSchriftzug_M_Bogen_links(x, y):
        
    Schriftzug_M_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-0.5, y+2.25) 
    Hauptbogen = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=1, HSL_start=32.5, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Hauptbogen.points[-1], 0.5, "down")
    
    Schriftzug_M_Bogen_links = Hauptbogen + outstroke
    drawPath(Schriftzug_M_Bogen_links)
    return Schriftzug_M_Bogen_links

#drawSchriftzug_M_Bogen_links(temp_x, temp_y)


def drawSchriftzug_M_Bogen_mitte(x, y):
        
    Schriftzug_M_Bogen_mitte = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+8.75, y+3.75) 
    Bogen_oben = drawSchneckenzug(*Grund_c, UPPER_E, 4, HSL_size=1, HSL_start=16, clockwise=False, inward=False)
    Einsatz = drawGrundelementE(*Bogen_oben.points[-1], 1)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1, HSL_start=19, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 1, "down")
    
    Schriftzug_M_Bogen_mitte = Bogen_oben + Bogen_unten + Einsatz + outstroke
    drawPath(Schriftzug_M_Bogen_mitte)
    return Schriftzug_M_Bogen_mitte

#drawSchriftzug_M_Bogen_mitte(temp_x, temp_y)
    

def drawSchriftzug_M_Bogen_rechts(x, y):
        
    Schriftzug_M_rechts = BezierPath()   


    ### OBEN
    Schriftzug_M_rechts_oben = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+12.5, y+2.75) 
    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=6, clockwise=True, inward=False)
    Einsatz_rechts = drawGrundelementE(*liegenderSchwung_links.points[-1], 0.25)
    liegenderSchwung_rechts = drawSchneckenzug(*Einsatz_rechts.points[4], LOWER_A, 4, HSL_size=1, HSL_start=5, clockwise=False, inward=False)
    Einsatz_links = drawOutstroke(*liegenderSchwung_links.points[0], 0.75, "down")
        
    Schriftzug_M_rechts_oben = liegenderSchwung_links + liegenderSchwung_rechts + Einsatz_rechts + Einsatz_links
    
    
    ### UNTEN    
    Schriftzug_M_rechts_unten = BezierPath()  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+12.25, y+1.125) 
    instroke = drawOutstroke(*Grund_c, 0.5)
    Bogen_oben = drawSchneckenzug(*Grund_c, UPPER_E, 3, HSL_size=1, HSL_start=9, clockwise=False, inward=False)
    Einsatz = drawGrundelementF(*Bogen_oben.points[-1], 3.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[-1], UPPER_B, 2, HSL_size=1, HSL_start=19, clockwise=False, inward=False)
    outstroke = drawOutstroke(*Bogen_unten.points[-1])
    
    Schriftzug_M_rechts_unten = instroke + Bogen_oben + Einsatz + Bogen_unten + outstroke
  
    Schriftzug_M_rechts = Schriftzug_M_rechts_oben + Schriftzug_M_rechts_unten
    drawPath(Schriftzug_M_rechts)

    return Schriftzug_M_rechts

#drawSchriftzug_M_Bogen_rechts(temp_x, temp_y)












def drawSchriftzug_W_Bogen_links(x, y):
        
    Schriftzug_W_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2.25, y+1.75)     
    Schriftzug_W_Bogen_links = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=1, HSL_start=29.5, clockwise=True, inward=True)
    
    drawPath(Schriftzug_W_Bogen_links)
    return Schriftzug_W_Bogen_links

#drawSchriftzug_W_Bogen_links(temp_x, temp_y)





def drawSchriftzug_W_Bogen_mitte(x, y):
        
    Schriftzug_W_Bogen_mitte = BezierPath()   
 
    drawGrundelOrientMittig(A1, A2, offset, x+6, y+4)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.5, y+4.25) 
    
    Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
    
    Einsatz = drawGrundelementE(*Bogen_oben.points[-1], 1.75)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=4, HSL_start=22, clockwise=True, inward=False)

    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")
    
    Schriftzug_W_Bogen_mitte = Bogen_oben + Bogen_unten + Einsatz + outstroke
    drawPath(Schriftzug_W_Bogen_mitte)

    return Schriftzug_W_Bogen_mitte

#drawSchriftzug_W_Bogen_mitte(temp_x, temp_y)
    
    
    

def drawSchriftzug_W_Bogen_rechts(x, y):
        
    Schriftzug_W_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_W_rechts_unten = BezierPath()    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9, y+1) 
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True)
    Einsatz = drawGrundelementE(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=6, HSL_start=22, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.9, "down")
    
    Schriftzug_W_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    
    ### OBEN
    Schriftzug_W_rechts_oben = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9.25, y+3) 
    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=6, clockwise=True, inward=False)    
    Einsatz_rechts = drawGrundelementE(*liegenderSchwung_links.points[-1], 0.25)  
    liegenderSchwung_rechts = drawSchneckenzug(*Einsatz_rechts.points[4], LOWER_A, 4, HSL_size=1, HSL_start=8, clockwise=False, inward=False)
    Einsatz_links = drawOutstroke(*liegenderSchwung_links.points[0], 0.75, "down")

    Schriftzug_W_rechts_oben = liegenderSchwung_links + liegenderSchwung_rechts + Einsatz_rechts + Einsatz_links

    Schriftzug_W_rechts = Schriftzug_W_rechts_unten + Schriftzug_W_rechts_oben
    drawPath(Schriftzug_W_rechts)
    return Schriftzug_W_rechts

#drawSchriftzug_W_Bogen_rechts(temp_x, temp_y)





    
    
    
    






def drawSchriftzug_V_Bogen_links(x, y):
        
    Schriftzug_V_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2.5)   
    Schriftzug_V_Bogen_links = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=1.25, HSL_start=30, clockwise=True, inward=True)

    drawPath(Schriftzug_V_Bogen_links)
    return Schriftzug_V_Bogen_links

#drawSchriftzug_V_Bogen_links(temp_x, temp_y)





def drawSchriftzug_V_Bogen_rechts(x, y):
        
    Schriftzug_V_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_W_rechts_unten = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9, y+0.5) 
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True)
    Einsatz = drawGrundelementE(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=6, HSL_start=18.8, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.85, "down")
    Schriftzug_V_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke
    
    ### OBEN
    Schriftzug_V_rechts_oben = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+10, y+3.5) 
    stehenderSchwung_oben = drawSchneckenzug(*Grund_b, UPPER_E, 4, HSL_size=1, HSL_start=5.55, clockwise=False, inward=False)
    Einsatz = drawGrundelementE(*stehenderSchwung_oben.points[-1], 1.125)
    stehenderSchwung_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1.5, HSL_start=6, clockwise=True, inward=False) 
    Schriftzug_V_rechts_oben = stehenderSchwung_oben + Einsatz + stehenderSchwung_unten
 
    Schriftzug_V_rechts = Schriftzug_V_rechts_unten + Schriftzug_V_rechts_oben
    drawPath(Schriftzug_V_rechts)
    return Schriftzug_V_rechts

#drawSchriftzug_V_Bogen_rechts(temp_x, temp_y)









def drawSchriftzug_Y_Bogen_links(x, y):
        
    Schriftzug_Y_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2.5) 
    Hauptbogen = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=0, HSL_start=28.75, clockwise=True, inward=True)
    
    Schriftzug_Y_Bogen_links = Hauptbogen 
    drawPath(Schriftzug_Y_Bogen_links)
    return Schriftzug_Y_Bogen_links

#drawSchriftzug_Y_Bogen_links(temp_x, temp_y)



def drawSchriftzug_Y_Bogen_rechts(x, y):
        
    Schriftzug_Y_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_Y_rechts_unten = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9.5, y+1) 
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 4, HSL_size=1, HSL_start=12, clockwise=False, inward=True)
    Einsatz = drawGrundelementE(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=7.5, HSL_start=28, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")
    
    Schriftzug_Y_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke
  
    
    ### OBEN
    Schriftzug_Y_rechts_oben = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9.25, y+2.875) 
    instroke = drawInstroke(*Grund_a, 0.5)
    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=9, clockwise=True, inward=False)
    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=7, clockwise=False, inward=False)
        
    Schriftzug_Y_rechts_oben = instroke + liegenderSchwung_links + liegenderSchwung_rechts


    Schriftzug_Y_rechts = Schriftzug_Y_rechts_unten + Schriftzug_Y_rechts_oben
    drawPath(Schriftzug_Y_rechts)
    return Schriftzug_Y_rechts

#drawSchriftzug_Y_Bogen_rechts(temp_x, temp_y)









    

def drawSchriftzug_P_Bogen_links(x, y):
        
    Schriftzug_P_links = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.25, y+3.5) 
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 5, HSL_size=2, HSL_start=4, clockwise=False, inward=False)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 1)
    Bogen_mitte = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 2, HSL_size=1, HSL_start=25.4, clockwise=True, inward=False)  
    Einsatz_2 = drawGrundelementF(*Bogen_mitte.points[-1], 4.25)
    Bogen_unten = drawSchneckenzug(*Einsatz_2.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=26, clockwise=True, inward=False) 

    Schriftzug_P_links = Bogen_oben + Einsatz + Bogen_mitte + Einsatz_2 + Bogen_unten
    drawPath(Schriftzug_P_links)
    return Schriftzug_P_links

#drawSchriftzug_P_Bogen_links(temp_x, temp_y)




    

def drawSchriftzug_P_Bogen_rechts(x, y):
        
    Schriftzug_P_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_P_rechts_unten = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5.5, y+0.75) 
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 4, HSL_size=1, HSL_start=12, clockwise=False, inward=True)
    Einsatz = drawGrundelementE(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=6, HSL_start=20, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 2.5, "down")
    Schriftzug_P_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    ### OBEN
    Schriftzug_P_rechts_oben = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5.5, y+2.75) 
    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=8, clockwise=True, inward=False)    
    Einsatz_rechts = drawGrundelementE(*liegenderSchwung_links.points[-1], 0.25)  
    liegenderSchwung_rechts = drawSchneckenzug(*Einsatz_rechts.points[4], LOWER_A, 4, HSL_size=1, HSL_start=6, clockwise=False, inward=False)
    #Einsatz_links = drawOutstroke(*liegenderSchwung_links.points[0], 0.75, "down")
    Schriftzug_P_rechts_oben = liegenderSchwung_links + liegenderSchwung_rechts + Einsatz_rechts #+ Einsatz_links
   
    Schriftzug_P_rechts = Schriftzug_P_rechts_unten + Schriftzug_P_rechts_oben
    drawPath(Schriftzug_P_rechts)
    return Schriftzug_P_rechts

#drawSchriftzug_P_Bogen_rechts(temp_x, temp_y)



    
    
    
    






def drawSchriftzug_U_Hauptstrich_links(x, y):
        
    Schriftzug_U_Hauptstrich_links = BezierPath()

    drawGrundelOrientMittig(A1, A2, offset, x-2.5, y+0.5)
    drawGrundelOrientMittig(A1, A2, offset, x-1.5, y+1)
    drawGrundelOrientMittig(A1, A2, offset, x-0.5, y+1.5)


    ### Liegender Schwung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.75, y+3.5) 
    links = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=1, HSL_start=6, clockwise=False, inward=False)
    Einsatz = drawGrundelementE(*Grund_a, 0.875)
    right = drawSchriftteil10(*Einsatz.points[4])
    
    Schriftzug_U_Hauptstrich_links += links + Einsatz + right
    
    ### Stehender Schwung nach unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+1.75) 
    instroke  = drawInstroke(*Grund_a, 1.5, "down")
    Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
    downstroke = drawGrundelementE(*Bogen_oben.points[-1], 0.75)
    Bogen_unten = drawSchneckenzug(*downstroke.points[4], UPPER_A, 4, HSL_size=1, HSL_start=12, clockwise=True, inward=False)
    outstroke  = drawOutstroke(*Bogen_unten.points[-1], 0, "down")
    
    Schriftzug_U_Hauptstrich_links += instroke + Bogen_oben + downstroke + Bogen_unten + outstroke
    drawPath(Schriftzug_U_Hauptstrich_links)
    return Schriftzug_U_Hauptstrich_links

#drawSchriftzug_U_Hauptstrich_links(temp_x, temp_y)



def drawSchriftzug_U_Hauptstrich_rechts(x, y):
        
    Schriftzug_U_Hauptstrich_rechts = BezierPath()
    
    ### Rechts, gerade
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y+2)    
    stem = drawSchriftzug3_Figures(x+3, y, instrokeLen=0)
    serife_nach_rechts = drawSchriftteil2(*Raute_a)
    serife_nach_unten = drawSchriftteil5(*Raute_d)

    ### Liegender Schwung unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1.75, y-5) 
    links = drawSchneckenzug(*Grund_a, UPPER_A,4, HSL_size=1, HSL_start=3, clockwise=False, inward=False)
    Einsatz = drawGrundelementE(*Grund_a, 0.1)
    right = drawSchneckenzug(*Einsatz.points[4], LOWER_A, 4, HSL_size=1, HSL_start=7, clockwise=False, inward=False)
    
    Schriftzug_U_Hauptstrich_rechts += links + Einsatz + right + stem + serife_nach_rechts + serife_nach_unten
    drawPath(Schriftzug_U_Hauptstrich_rechts)
    return Schriftzug_U_Hauptstrich_rechts

#drawSchriftzug_U_Hauptstrich_rechts(temp_x, temp_y)






def drawSchriftzug_Z_Bogen_oben(x, y):
        
    Schriftzug_Z_Bogen_oben = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y+3.25) 
    top = drawSchneckenzug(*Grund_a, UPPER_E, 2, HSL_size=1.5, HSL_start=19, clockwise=True, inward=True)
    btm = drawSchneckenzug(*top.points[-1], UPPER_G, 6, HSL_size=1, HSL_start=9, clockwise=True, inward=False)

    # # Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.75, y+0.5) 
    # # Deckung = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=0.25, HSL_start=4, clockwise=True, inward=False)

    Schriftzug_Z_Bogen_oben += top + btm #+ Deckung
    drawPath(Schriftzug_Z_Bogen_oben)
    return Schriftzug_Z_Bogen_oben

#drawSchriftzug_Z_Bogen_oben(temp_x, temp_y)



def drawSchriftzug_Z_Bogen_unten(x, y):
        
    Schriftzug_Z_Bogen_unten = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.75, y-1) 
    top = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=0.5, HSL_start=15, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*top.points[-1], 0.65)
    btm = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=15, clockwise=True, inward=False)
    
    Schriftzug_Z_Bogen_unten += top + Einsatz + btm 
    drawPath(Schriftzug_Z_Bogen_unten)
    return Schriftzug_Z_Bogen_unten


#drawSchriftzug_Z_Bogen_unten(temp_x, temp_y)








def drawSchriftzug_z_Initial(x, y, version="Endspitze"):
        
    Schriftzug_z_Initial = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y+2.25) 
    oben1 = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=0.5, HSL_start=10, clockwise=False, inward=True)
    oben2 = drawSchneckenzug(*oben1.points[-1], UPPER_A, 4, HSL_size=0.5, HSL_start=8, clockwise=True, inward=False)

    if version == "Endspitze":
        Einsatz = drawGrundelementA(*oben2.points[-1], 0.5, "down")
        #Endspitze = drawSchneckenzug(*Einsatz.points[-1], LOWER_E, 10, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 11)])
        Schriftzug_z_Initial += Einsatz #+ Endspitze

    if version == "ohneEndspitze":
        pass
        
        
    Schriftzug_z_Initial += oben1 + oben2
    drawPath(Schriftzug_z_Initial)

    return Schriftzug_z_Initial

#drawSchriftzug_z_Initial(temp_x, temp_y, version="Endspitze")







def drawSchriftzug_C_stehenderSchwung(x, y):
        
    Schriftzug_C_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1) 
    Einsatz = drawGrundelementE(*Grund_a, length=0.5, pos="unten")
    oben = drawSchneckenzug(*Einsatz.points[-1], LOWER_A, 4, HSL_size=2, HSL_start=14, clockwise=True, inward=False)
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=2, HSL_start=11, clockwise=True, inward=False)
    outstroke = drawOutstroke(*oben.points[-1], 0.5)

    Schriftzug_C_stehenderSchwung += unten + Einsatz + oben + outstroke
    drawPath(Schriftzug_C_stehenderSchwung)
    return Schriftzug_C_stehenderSchwung

#drawSchriftzug_C_stehenderSchwung(temp_x, temp_y)


def drawSchriftzug_C_liegenderSchwung(x, y):
        
    Schriftzug_C_liegenderSchwung = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1.7, y-4.5) 

    links = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=1, HSL_start=5, clockwise=False, inward=False)
    Einsatz = drawGrundelementE(*Grund_a, 1)
    rechts = drawSchneckenzug(*Einsatz.points[4], LOWER_A, 4, HSL_size=2, HSL_start=4, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], 0.75)
    
    Schriftzug_C_liegenderSchwung = links + Einsatz + rechts + outstroke
    drawPath(Schriftzug_C_liegenderSchwung)

    return Schriftzug_C_liegenderSchwung
    
#drawSchriftzug_C_liegenderSchwung(temp_x, temp_y)





def drawAuge(x, y):
    
    Auge = BezierPath()

    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    start_oben = drawSchriftteil2(x, y)
    bow = drawSchriftteil8(*start_oben.points[-1])

    Auge += start_oben + bow
    drawPath(Auge)  
    return Auge
        
#drawAuge(temp_x, temp_y)




