import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente
import version_3.creation.Schwuenge_GatC
import version_3.creation.Schriftteile_GatC
#import Fraktur_Schriftzuege_GatC

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schwuenge_GatC)
importlib.reload(version_3.creation.Schriftteile_GatC)
#importlib.reload(Fraktur_Schriftzuege_GatC)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.Schwuenge_GatC import *
from version_3.creation.Schriftteile_GatC import *
#from Fraktur_Schriftzuege_GatC import *

    

from nibLib.pens.rectNibPen import RectNibPen
from math import radians
import math as m
import collections
import glyphContext




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 10
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)





# _____________ Pen for drawing shape in glyph window _______________

penWidth = 10
penThickness = 4

#    +--------- width --------+
#    |                        |  thickness
#    +------------------------+





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)




# ___________ Sidebearings, Margin _______________

# # font = CurrentFont()

# # for key in sidebearingFunctions:
# #     glyph = font[[key][0]]  
    




# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)



stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 9



# ________________________________________________________


  

def drawFutura_a(x, y):      
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)

    Strich_rechts = drawGrundelementF(*Raute_a, 5.5)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-2.75)
    Bogen_1 = drawSchneckenzug(*Raute_a, LOWER_B, 4, HSL_size=0.4, HSL_start=21.25, clockwise=False, inward=True)
    Bogen_2 = drawSchneckenzug(*Bogen_1.points[-1], UPPER_F, 4, HSL_size=2, HSL_start=16.75, clockwise=False, inward=False)
    Bogen_3 = drawSchneckenzug(*Bogen_2.points[-1], UPPER_B, 4, HSL_size=1, HSL_start=22, clockwise=False, inward=True)
    Bogen_4 = drawSchneckenzug(*Bogen_3.points[-1], LOWER_F, 4, HSL_size=0.8, HSL_start=18.95, clockwise=False, inward=False)
    
    Futura_a = Strich_rechts + Bogen_1 + Bogen_2 + Bogen_3 + Bogen_4
    trans_scale(Futura_a, 3.75)
    return Futura_a
 


    
    
    
def drawFutura_b(x,y):        
    
    Strich_links = drawSchriftzug5(x, y, top="ascender")
    Bogen_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5)
    
    Futura_b = Strich_links + Bogen_rechts
    trans_scale(Futura_b, valueToMoveGlyph)
    return Futura_b
    
    
def drawFutura_b_dot(x, y):      
    #y += -3
    glyph_b = drawFutura_b(x, y)
    trans_scale_invert(glyph_b, valueToMoveGlyph)

    pkt_Auslauf = glyph_b.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Futura_b_dot = glyph_b
    trans_scale(Futura_b_dot, valueToMoveGlyph)
    return Futura_b_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    


def drawFutura_c(x,y, outstrokeLen=0.5):
           
    Bogen_links = drawSchriftzug6(x, y, outstrokeLen=1)
    Haeckchen_oben = drawSchriftteil2(*Bogen_links.points[-4])

    if version_mod == 1:
        outstroke = drawOutstroke(*Haeckchen_oben.points[-1], outstrokeLen)
    else:
        outstroke = drawOutstroke(*Haeckchen_oben.points[11], outstrokeLen)

    Futura_c = Bogen_links + Haeckchen_oben + outstroke
    trans_scale(Futura_c, valueToMoveGlyph)
    return Futura_c
    
    
    



def drawFutura_d(x, y, Endspitze=12):      

    Bogen_links = drawSchriftzug6(x, y)
    Bogen_rechts = drawSchwung6(x, y)
    
    Futura_d = Bogen_links + Bogen_rechts
    trans_scale(Futura_d, valueToMoveGlyph)
    return Futura_d
    

 


def drawFutura_e(x, y):
    
    Futura_e = BezierPath()      
    
    Bogen_links = drawSchriftzug6(x, y, outstrokeLen=1.5)
    Haken_rechts = drawSchriftteil8(*Bogen_links.points[-4])
    outstroke_oben = drawOutstroke(*Haken_rechts.points[-1], 1.5, "down")

    Futura_e = Bogen_links + Haken_rechts + outstroke_oben 
    trans_scale(Futura_e, valueToMoveGlyph)
    return Futura_e
    
    
       

 
    
def drawFutura_f(x,y):

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    trans_scale(Querstrich, valueToMoveGlyph)

    Futura_longs = drawFutura_longs(x, y)[0]
    values_to_return = drawFutura_longs(x, y)[1]
    Futura_f = Futura_longs + Querstrich
    return Futura_f, values_to_return
    
    
    
    
    
    
def drawFutura_f_f(x, y):
    
    Querstrich_left = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich_right = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich = Querstrich_left + Querstrich_right
    trans_scale(Querstrich, valueToMoveGlyph)
    
    ligature_longs_longs = drawFutura_longs_longs(x, y)[0]
    values_to_return = drawFutura_longs_longs(x, y)[1]
    
    Futura_f_f = ligature_longs_longs + Querstrich
    return Futura_f_f, values_to_return
    
     
     
     
     

def drawFutura_f_t(x, y):
        
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    trans_scale(Querstrich, valueToMoveGlyph)
        
    glyph_longs_t = drawFutura_longs_t(x, y)[0]
    values_to_return = drawFutura_longs_t(x, y)[1]
    
    Futura_f_t = glyph_longs_t + Querstrich
    return Futura_f_t, values_to_return 
    

    
    
def drawFutura_f_f_t(x, y):
    
    Querstrich_links = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich_rechts = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)

    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=2, ligatureLen=2)
    pkt_Ausstrich = main_stroke_left.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    pkt_Auslauf = main_stroke_left.points[-13]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    con = drawSchriftzug_f_f_con(x, y+0.25)
    main_stroke_middle = drawSchriftzug2(x+3, y, instrokeLen=1, ligatureLen=2, AuslaufKehlung=False)

    Spitze = drawSchriftzug_f_t_con(x, y)
    
    Futura_f_f_t = main_stroke_left + con + main_stroke_middle + Spitze + Querstrich_links + Querstrich_rechts
    trans_scale(Futura_f_f_t, valueToMoveGlyph)
        
    glyph_t = drawFutura_t(x+6, y)
    Futura_f_f_t += glyph_t
    
    return Futura_f_f_t, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
        

    
    

    
def drawFutura_g(x, y):

    Bogen_links = drawSchriftzug7(x, y, outstrokeLen=2.5) 
    Strich_rechts = drawSchriftzug4(x+3, y, bottom="Schwung")
    pkt_Auslauf = Strich_rechts.points[19]
    #text("pkt_Auslauf", pkt_Auslauf)

    Futura_g = Bogen_links + Strich_rechts
    trans_scale(Futura_g, valueToMoveGlyph)
    return Futura_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    

    
def drawFutura_g_thinStroke(x, y, *, pass_from_thick=None):

    Futura_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    #Futura_g_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1)
    
    Futura_g_thinStroke += Auslauf + Endpunkt
    drawPath(Futura_g_thinStroke)
    trans_thinStroke_down_left(Futura_g_thinStroke)
    trans_scale(Futura_g_thinStroke, valueToMoveGlyph)    
    return Futura_g_thinStroke
    






           


def drawFutura_h(x, y):      
    
    Strich_links = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0)
    Bogen_rechts = drawSchriftzug9(x+3, y)
    
    Futura_h = Strich_links + Bogen_rechts
    trans_scale(Futura_h, valueToMoveGlyph)
    return Futura_h
    
    
def drawFutura_h_dot(x, y):      
    #y += -3
    glyph_h = drawFutura_h(x, y)
    trans_scale_invert(glyph_h, valueToMoveGlyph)

    pkt_Auslauf_up = glyph_h.points[5]
    #text("pkt_Auslauf_up", pkt_Auslauf_up)
    pkt_Auslauf_down = glyph_h.points[-3]
    #text("pkt_Auslauf_down", pkt_Auslauf_down)
        
    Futura_h_dot = glyph_h
    trans_scale(Futura_h_dot, valueToMoveGlyph)
    return Futura_h_dot, collections.namedtuple('dummy', 'pkt_Auslauf_up pkt_Auslauf_down')(pkt_Auslauf_up, pkt_Auslauf_down)
    
    

    
    
    
    
        

def drawFutura_i(x, y):      
    
    #Futura_i = BezierPath()
    Strich = drawSchriftzug4(x, y)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    iPunkt = drawSchriftteil2(*Raute_a)
    
    Futura_i = Strich + iPunkt
    trans_scale(Futura_i, valueToMoveGlyph)
    return Futura_i
        
    

        
    
def drawFutura_j(x, y):      
    
    x  += 3
    Strich = drawSchriftzug4(x, y, bottom="Schwung")
    pkt_Auslauf = Strich.points[19]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    Punkt = drawSchriftteil2(*Raute_a)
    
    Futura_j = Strich + Punkt
    trans_scale(Futura_j, valueToMoveGlyph)
    return Futura_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    




def drawFutura_k(x, y):
    
    Strich_links = drawSchriftzug5(x, y, top="ascender", outstrokeLen=1)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.25)

    Schleife_instroke = drawInstroke(*Grund_a, 1.5, "down")
    Schleife = drawSchriftteil8(*Schleife_instroke.points[-1])
    Schleife_outstroke = drawOutstroke(*Schleife.points[-1], 1.5, "down")

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    
    Futura_k = Strich_links + Schleife_instroke + Schleife + Schleife_outstroke + Querstrich
    trans_scale(Futura_k, valueToMoveGlyph)
    return Futura_k
    
    
def drawFutura_k_dot(x, y):      
    #y += -3
    glyph_k = drawFutura_k(x, y)
    trans_scale_invert(glyph_k, valueToMoveGlyph)

    pkt_Auslauf = glyph_k.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Futura_k_dot = glyph_k
    trans_scale(Futura_k_dot, valueToMoveGlyph)
    return Futura_k_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
        
        
def drawFutura_l(x, y):    
    
    Schriftzug5 = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0.5)
    
    Futura_l = Schriftzug5
    trans_scale(Futura_l, valueToMoveGlyph)
    return Futura_l
    
    
def drawFutura_l_dot(x, y):      
    #y += -3
    glyph_l = drawFutura_l(x, y)
    trans_scale_invert(glyph_l, valueToMoveGlyph)

    pkt_Auslauf = glyph_l.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Futura_l_dot = glyph_l
    trans_scale(Futura_l_dot, valueToMoveGlyph)
    return Futura_l_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
        
        
def drawFutura_m(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    
    # Raute Abstand 2. Strich Mitte
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
        
    Strich_mitte = drawSchriftzug5(x+3, y, instrokeLen=2.5, outstrokeLen=0)
    
    # Raute Abstand 3. Strich rechts
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
        
    Strich_rechts = drawSchriftzug4(x+6, y, instrokeLen=2.5)
    
    Futura_m = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(Futura_m, valueToMoveGlyph) 
    return Futura_m
    







def drawFutura_n(x, y):

    Futura_n = BezierPath()
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)

    # Raute Abstand 2. Strich
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
    Strich_rechts = drawSchriftzug4(x+3, y, instrokeLen=2.5)

    Futura_n = Strich_links + Strich_rechts
    trans_scale(Futura_n, valueToMoveGlyph)    
    return Futura_n
    





def drawFutura_o(x, y):
        
    # Weil aber bei Zusammensetzung der beiden Schrift-Züge Fig. VI. und VIII. das daraus    # entstehende o in der Gattung C durch ihre starken Krümmungen zu viel Zwischenraum erhalten, [...] 
    # so muß hier in C jedem dieser Schrift-Züge 1/4 Bestandtheil Gerades mehr eingesetzet, 
    # und dann nur 3. halbe Breiten oder 1 1/2 Hauptmaas Zwischenraum (nehmlich nach den  
    # Grundbestandtheilen dieser gebogenen Schrift-Züge gemessen,) dazu genommen werden.    
    
    Bogen_links = drawSchriftzug6(x, y, "for o", outstrokeLen=0)
    Bogen_rechts = drawSchriftzug8(x+2.5, y, "for o", outstrokeLen=1.225)
    
    Futura_o = Bogen_links + Bogen_rechts
    trans_scale(Futura_o, valueToMoveGlyph) 
    return Futura_o
    
    
    
    

    
def drawFutura_p(x, y):
        
    # Weil aber bei Zusammensetzung der beiden Schrift-Züge Fig. VI. und VIII. das daraus    # entstehende o in der Gattung C durch ihre starken Krümmungen zu viel Zwischenraum erhalten, [...] 
    # so muß hier in C jedem dieser Schrift-Züge 1/4 Bestandtheil Gerades mehr eingesetzet, 
    # und dann nur 3. halbe Breiten oder 1 1/2 Hauptmaas Zwischenraum (nehmlich nach den  
    # Grundbestandtheilen dieser gebogenen Schrift-Züge gemessen,) dazu genommen werden.
   
    Strich_links = drawSchriftzug3(x, y, bottom="Kehlung")
    pkt_Ausstrich = Strich_links.points[-1]
    
    Bogen_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5)
    
    #Hilfe Positionierung Signatur
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)
    Signatur = drawSchriftteil1(*Raute_d)
        
    Futura_p = Strich_links + Signatur + Bogen_rechts
    trans_scale(Futura_p, valueToMoveGlyph) 
    return Futura_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    


    
    
def drawFutura_q(x, y):      
    
    Bogen_links = drawSchriftzug7(x, y)
    Strich_rechts = drawSchriftzug4(x+3, y, bottom="Kehlung")
    pkt_Ausstrich = Strich_rechts.points[29]
    #text("pkt_Ausstrich", pkt_Ausstrich)
  
    Futura_q = Bogen_links + Strich_rechts
    trans_scale(Futura_q, valueToMoveGlyph) 
    return Futura_q, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)






def drawFutura_r(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)

    Strich = drawGrundelementF(*Raute_a, 5.5)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-1.875)
    Bogen_1 = drawSchneckenzug(*Raute_a, UPPER_B, 4, HSL_size=5, HSL_start=24, clockwise=True, inward=True)
    Bogen_2 = drawSchneckenzug(*Bogen_1.points[-1], UPPER_F, 1, HSL_size=8, HSL_start=17, clockwise=True, inward=True)

    Futura_r = Strich + Bogen_1 + Bogen_2
    trans_scale(Futura_r, 3.75) 
    return Futura_r
    

    

def drawFutura_rc(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, baseline-0.35)

    Strich = drawSchriftzug8(x, y, Einsatz="1", outstrokeLen=0)
    #Verbindung = drawInstroke(*Raute_a, 1.5)
    #Haken = drawSchriftteil2(*Raute_a)
    #outstroke = drawOutstroke(*Haken.points[-1])
    Fuss = drawSchriftteil11(*Raute_a)
    
    Futura_rc = Strich + Fuss #+ Verbindung + Haken + outstroke
    trans_scale(Futura_rc, valueToMoveGlyph) 
    
    letter_c = drawFutura_c(x+3, y, outstrokeLen=0)
    Futura_rc += letter_c
    return Futura_rc
    
    
    
    
    
    
    
def drawFutura_s(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-3)
    Bogen_oben = drawHalbbogen8(*Raute_a, instrokeLen=1.72, outstrokeLen=2)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y)
    Deckung_oben = drawSchriftteil2(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y-2)
    Bogen_unten = drawHalbbogen4(*Raute_a, outstrokeLen=1.67)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1, y-5)
    Deckung_unten = drawSchriftteil1(*Raute_d)
    
    Futura_s = Bogen_oben + Bogen_unten + Deckung_oben + Deckung_unten
    trans_scale(Futura_s, valueToMoveGlyph) 
    return Futura_s
    
      
  

    

def drawFutura_longs(x, y):
        
    main_stroke_down = drawSchriftzug2(x, y, instrokeLen=1.5)
    pkt_Ausstrich = main_stroke_down.points[-17]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+3.25)
    Spitze = drawSchriftteil11(*Raute_a)

    Futura_longs = main_stroke_down + Spitze
    trans_scale(Futura_longs, valueToMoveGlyph)
    return Futura_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    

    

def drawFutura_longs_longs(x, y):
        
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=2.5)
    pkt_Ausstrich = main_stroke_left.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    pkt_Auslauf = main_stroke_left.points[-13]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    main_stroke_right = drawSchriftzug2(x+3, y, instrokeLen=1.5, AuslaufKehlung=False)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y+3.235)
    Spitze = drawSchriftteil11(*Raute_a)
    con = drawSchriftzug_longs_longs_con(x, y)

    Futura_longs_longs = main_stroke_left + main_stroke_right + con + Spitze
    trans_scale(Futura_longs_longs, valueToMoveGlyph)
    return Futura_longs_longs, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    

def drawFutura_longs_longs_thinStroke(x, y, *, pass_from_thick=None):
    
    Futura_longs_longs_thinStroke = BezierPath()   

    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
    #Futura_longs_longs_thinStroke.oval(Auslauf_oben.points[-1][0]-modul_width*0.5, Auslauf_oben.points[-1][1]-modul_height*0.65, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.5, Auslauf_oben.points[-1][1]-modul_height*0.65)


    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
    trans_thinStroke_down_left(Zierstrich) 
    trans_thinStroke_up_right(Auslauf_oben)
        
    Futura_longs_longs_thinStroke += Auslauf_oben + Zierstrich + Endpunkt

    drawPath(Futura_longs_longs_thinStroke)
    trans_scale(Futura_longs_longs_thinStroke, valueToMoveGlyph)
    return Futura_longs_longs_thinStroke   
    
    
    
    
    
    


def drawFutura_longs_t(x, y):
        
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=1, ligatureLen=2)
    pkt_Ausstrich = main_stroke_left.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+2.5)
    Spitze = drawSchriftteil12(*Raute_a)
    
    Futura_longs_t = main_stroke_left + Spitze
    trans_scale(Futura_longs_t, valueToMoveGlyph)
        
    glyph_t = drawFutura_t(x+3, y)
    Futura_longs_t += glyph_t
    return Futura_longs_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    

def drawFutura_germandbls(x,y):
    
    Futura_germandbls = BezierPath()


    ### linker Teil
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=1, ligatureLen=2)
    pkt_Ausstrich = main_stroke_left.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+2.5)
    Spitze = drawSchriftteil12(*Raute_a)
           
       
    ### rechter Teil        
    Halbbogen_oben = drawSchriftzug_z_Initial(x+2.5, y-1)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y-2.5)
    Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_G, 6, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
    instroke = drawInstroke(*Halbbogen_unten.points[0], 0.5)
    outstroke = drawOutstroke(*Halbbogen_unten.points[-1], 0.25, "down")
    
    pkt_Auslauf = outstroke.points[0]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Futura_germandbls += main_stroke_left + Spitze + Halbbogen_oben + Halbbogen_unten + instroke + outstroke

    drawPath(Futura_germandbls)
    trans_scale(Futura_germandbls, valueToMoveGlyph)
    return Futura_germandbls, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
    
    

def drawFutura_germandbls_Endspitze(x, y, *, pass_from_thick=None):
    
    Futura_germandbls_Endspitze = BezierPath()   
    
    Endspitze = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 7, HSL_size=1, HSL_start=13, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 8)])
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
    #Futura_germandbls_Endspitze.oval(Endspitze.points[-1][0]-modul_width*0.5, Endspitze.points[-1][1]-modul_height*0.015, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Endspitze.points[-1][0]-modul_width*0.5, Endspitze.points[-1][1]-modul_height*0.015)

    Futura_germandbls_Endspitze += Endspitze + Zierstrich + Endpunkt   
    drawPath(Futura_germandbls_Endspitze)
    trans_thinStroke_down_left(Futura_germandbls_Endspitze)
    trans_scale(Futura_germandbls_Endspitze, valueToMoveGlyph)
    return Futura_germandbls_Endspitze   
      
    
    
    
    
def drawFutura_t(x, y):    
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)

    Strich = drawGrundelementF(Raute_a[0], Raute_a[1]+2.1*modul_height, 7.6)
    Querstrich  = drawGrundelementB(Raute_a[0]-1.05*modul_width, Raute_a[1], 2.15)
    
    Futura_t = Strich + Querstrich
    trans_scale(Futura_t, 3.75)
    return Futura_t
    
    
    
    
    
    

def drawFutura_u(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
       
    Strich_links = drawGrundelementF(*Raute_a, 3.75)
    Bogen_links = drawSchneckenzug(*Strich_links.points[-1], UPPER_B, 4, HSL_size=1, HSL_start=14.8, clockwise=False, inward=True)
    Einsatz = drawGrundelementB(*Bogen_links.points[-1], 0.25)
    Bogen_rechts = drawSchneckenzug(*Einsatz.points[-1], LOWER_F, 4, HSL_size=1, HSL_start=14.75, clockwise=False, inward=True)
    Strich_rechts = drawGrundelementF(*Bogen_rechts.points[-1], 3.92, "up")

    
    Futura_u = Strich_links + Bogen_links + Einsatz + Bogen_rechts + Strich_rechts
    trans_scale(Futura_u, 3.75)
    return Futura_u
    
    
    
    
    
    
def drawFutura_adieresis(x, y):
    
    Dieresis = drawDieresis(x+0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_a = drawFutura_a(x, y)
        
    Futura_adieresis = Grundschriftzug_a + Dieresis
    return Futura_adieresis
    

def drawFutura_odieresis(x, y):
    
    Dieresis = drawDieresis(x+0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_o = drawFutura_o(x, y)
        
    Futura_odieresis = Grundschriftzug_o + Dieresis
    return Futura_odieresis
    

def drawFutura_udieresis(x, y):
    
    Dieresis = drawDieresis(x+0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_u = drawFutura_u(x, y)
        
    Futura_udieresis = Grundschriftzug_u + Dieresis
    return Futura_udieresis
    
    
    
    
    
    
def drawFutura_v(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5)
    
    Futura_v = Strich_links + Strich_rechts
    trans_scale(Futura_v, valueToMoveGlyph)
    return Futura_v
    
    
    

    
def drawFutura_w(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    Strich_mitte = drawSchriftzug5(x+3, y, instrokeLen=2.5)
    Strich_rechts = drawSchriftzug8(x+6, y, instrokeLen=2.5)

    Futura_w = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(Futura_w, valueToMoveGlyph)
    return Futura_w
    
    
    
    
    
def drawFutura_x(x, y):
    
    glyph_r = drawFutura_r(x, y)
    trans_scale_invert(glyph_r, valueToMoveGlyph)  ### erstmal rückwärts skalieren um den hook zu positionieren!
    
    pkt_Ausstrich = glyph_r.points[-40]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    Futura_x = glyph_r 
    trans_scale(Futura_x, valueToMoveGlyph)
    return Futura_x, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)

    
    
    
def drawFutura_x_thinStroke(x, y, *, pass_from_thick=None):
    
    Futura_x_thinStroke = BezierPath()

    hook = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich, UPPER_E, 15, HSL_size=1, HSL_start=17, clockwise=False, inward=True)
    Endpunkt = drawThinstroke_Endpunkt(hook.points[-1][0]-modul_width*0.7, hook.points[-1][1]-modul_height*1.075)

    Futura_x_thinStroke += hook + Endpunkt
    drawPath(Futura_x_thinStroke)
    trans_thinStroke_down_left(Futura_x_thinStroke)
    trans_scale(Futura_x_thinStroke, valueToMoveGlyph)
    return Futura_x_thinStroke
      

    
    
    



def drawFutura_y(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    Strich_rechts = drawSchriftzug9(x+3, y, instrokeLen=2.5)
    
    Futura_y = Strich_links + Strich_rechts
    trans_scale(Futura_y, valueToMoveGlyph)
    return Futura_y
    

def drawFutura_y_dot(x, y):      
    
    glyph_y = drawFutura_y(x, y)
    trans_scale_invert(glyph_y, valueToMoveGlyph)

    pkt_Auslauf = glyph_y.points[-3]
    text("pkt_Auslauf", pkt_Auslauf)
    
    Futura_y_dot = glyph_y
    trans_scale(Futura_y_dot, valueToMoveGlyph)
    return Futura_y_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    


def drawFutura_z(x,y, style="final"):
    #x +=3    
    Futura_z = BezierPath()
        
    if style == "initial":
        Halbbogen_oben = drawSchriftzug_z_Initial(x, y)
        pkt_Ausstrich = Halbbogen_oben.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf) 
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, y-1.5)
        Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_G, 6, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
        instroke = drawInstroke(*Halbbogen_unten.points[0], 0.5)
        outstroke = drawOutstroke(*Halbbogen_unten.points[-1], 1.75, "down")

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-2.5, y-5)
        Deckung_unten = drawSchriftteil1(*Raute_d)
        Futura_z += instroke + outstroke + Deckung_unten
    
        drawPath(Halbbogen_unten)

   
    if style == "final":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
        Halbbogen_oben = drawHalbbogen4(*Raute_a)
    
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-3.5)
        Halbbogen_unten = drawHalbbogen5(*Raute_a)
        pkt_Ausstrich = Halbbogen_unten.points[-1]
        text("pkt_Ausstrich", pkt_Ausstrich) 


    Futura_z += Halbbogen_oben + Halbbogen_unten
    trans_scale(Futura_z, valueToMoveGlyph)
    return Futura_z, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    

def drawFutura_z_thinStroke(x, y, *, pass_from_thick=None):
    
    Futura_z_thinStroke = BezierPath()

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich, LOWER_E, 10, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 11)])
    #Futura_z_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.05, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.05)

    Futura_z_thinStroke += Auslauf + Endpunkt
    drawPath(Futura_z_thinStroke)
    trans_thinStroke_down_left(Futura_z_thinStroke)
    trans_scale(Futura_z_thinStroke, valueToMoveGlyph)
    return Futura_z_thinStroke      
        







def drawFutura_thinstroke_Straight(x, y, *, pass_from_thick=None):     
    
    Futura_thinstroke_Straight = BezierPath()
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
          
    Futura_thinstroke_Straight +=  Zierstrich
    trans_thinStroke_down_left(Futura_thinstroke_Straight)
    trans_scale(Futura_thinstroke_Straight, valueToMoveGlyph)
    return Futura_thinstroke_Straight
    
    

    
    
    
    
    
    
    
###################################
###### ab hier Interpunktion ######
###################################
    
    
    
    
def drawFutura_period(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    period = drawGrundelementC(*Raute_a)
        
    Futura_period = period
    trans_scale(Futura_period, valueToMoveGlyph)
    return Futura_period
    
    
    
    
    
def drawFutura_colon(x, y):
        
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    colon_top = drawGrundelementC(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)    
    colon_btm = drawGrundelementC(*Raute_a)  
            
    Futura_colon = colon_top + colon_btm
    trans_scale(Futura_colon, valueToMoveGlyph)    
    return Futura_colon
    
    
    
    
    
def drawFutura_semicolon(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    semicolon_top = drawGrundelementC(*Raute_a)      
    semicolon_btm = drawSchriftzug_semicolon_btm(x-0.5, baseline+0.25)  

    Futura_semicolon = semicolon_top + semicolon_btm
    trans_scale(Futura_semicolon, valueToMoveGlyph)    
    return Futura_semicolon
    
    
    
    

def drawFutura_quoteright(x, y):

    quoteright = drawSchriftzug_semicolon_btm(x, y+0.75) 
    
    Futura_quoteright = quoteright
    trans_scale(Futura_quoteright, valueToMoveGlyph)    
    return Futura_quoteright
    




    
def drawFutura_quotesingle(x, y):

    Futura_quotesingle = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.5)
    fake = drawGrundelementA(Grund_c[0], Grund_c[1]-10, 0.1)   ### this has to be deleted manually
    pkt_fake = Grund_c
    
    Futura_quotesingle = fake
    drawPath(Futura_quotesingle)
    trans_scale(Futura_quotesingle, valueToMoveGlyph)
    return Futura_quotesingle, collections.namedtuple('dummy', 'pkt_fake')(pkt_fake)
    

def drawFutura_quotesingle_thinstroke(x, y, *, pass_from_thick=None):

    Futura_quotesingle_thinstroke = BezierPath()

    quotesingle = drawSchneckenzug(*pass_from_thick.pkt_fake, LOWER_B, 3, HSL_size=2, HSL_start=6, clockwise=True, inward=False)
    Endpunkt = drawThinstroke_Endpunkt(pass_from_thick.pkt_fake[0]-modul_width*1.11, pass_from_thick.pkt_fake[1]-modul_height*0.55)

    Futura_quotesingle_thinstroke += quotesingle + Endpunkt
    drawPath(Futura_quotesingle_thinstroke)
    trans_scale(Futura_quotesingle_thinstroke, valueToMoveGlyph)
    return Futura_quotesingle_thinstroke
    
    
    
    
    
    
def drawFutura_comma(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    comma = drawGrundelementG(*Grund_a, 2, "down") 

    Futura_comma = comma
    trans_scale(Futura_comma, valueToMoveGlyph)    
    return Futura_comma
    
    



    
def drawFutura_endash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-5.25)

    endash = drawGrundelementB(*Grund_a, 2) 

    Futura_endash = endash
    trans_scale(Futura_endash, valueToMoveGlyph)
    return Futura_endash
    
    
    
    
    
def drawFutura_hyphen(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.25)
    hyphen_top = drawGrundelementH(*Grund_b, 2, "down") 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.25)
    hyphen_btm = drawGrundelementH(*Grund_b, 2, "down") 
    
    Futura_hyphen = hyphen_top + hyphen_btm
    trans_scale(Futura_hyphen, valueToMoveGlyph)
    return Futura_hyphen
     
    
    
    
def drawFutura_exclam(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3.25)
    exclam = drawGrundelementF(*Grund_a, 7) 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    period = drawGrundelementC(*Raute_a)

    Futura_exclam = exclam + period
    trans_scale(Futura_exclam, valueToMoveGlyph)
    return Futura_exclam
    
    



def drawFutura_question(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    period = drawGrundelementC(*Raute_a)

    question = drawSchriftzug_question(x, y) 

    Futura_question = question + period
    trans_scale(Futura_question, valueToMoveGlyph)
    return Futura_question
    








###################################
######     ab hier Zahlen    ######
###################################




def drawFutura_zero(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y)

    Futura_zero = Bogen_links + Bogen_rechts
    trans_scale(Futura_zero, valueToMoveGlyph)
    return Futura_zero
    





    
    
 
def drawFutura_one(x, y): 
    x += 3
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    
    stem = drawSchriftzug3_Figures(x, y)

    Serife_nach_rechts = drawSchriftteil2(*Raute_a)
    Serife_nach_unten = drawSchriftteil5(*Raute_d)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    transition = drawSchneckenzug(*Grund_a, LOWER_B, 3, HSL_size=1, HSL_start=16.25, clockwise=True, inward=True)
    pkt_Auslauf = transition.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
        
    Futura_one = stem + Serife_nach_rechts + Serife_nach_unten + transition
    drawPath(Futura_one)
    trans_scale(Futura_one, valueToMoveGlyph)
    return Futura_one, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)


def drawFutura_one_thinStroke(x, y, *, pass_from_thick=None): 
    
    Futura_one_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    #Futura_one_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    Futura_one_thinStroke += Auslauf + Endpunkt
    drawPath(Futura_one_thinStroke)
    trans_thinStroke_down_left(Futura_one_thinStroke)
    trans_scale(Futura_one_thinStroke, valueToMoveGlyph)
    return Futura_one_thinStroke
    
    
    




def drawFutura_two(x, y):
     
    Bogen = drawSchriftzug_two_Bogen(x, y)
    pkt_Auslauf = Bogen.points[0]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    Schwung_unten = drawSchriftzug_two_Schwung (x, y)
    
    Futura_two = Bogen + Schwung_unten    
    trans_scale(Futura_two, valueToMoveGlyph)
    return Futura_two, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawFutura_two_thinStroke(x, y, *, pass_from_thick=None): 
    
    Futura_two_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 12, HSL_size=1, HSL_start=18, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.001 * i      for  i in range(0, 13)])

    Futura_two_thinStroke += Auslauf 
    drawPath(Futura_two_thinStroke)
    trans_thinStroke_down_left(Futura_two_thinStroke)
    trans_scale(Futura_two_thinStroke, valueToMoveGlyph)
    return Futura_two_thinStroke
    
    
    
    
    
    
    


def drawFutura_three(x, y):

    Futura_three = BezierPath()
         
    Three_Top = drawSchriftzug_three_Top(x, y)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    con = drawGrundelementA(*Three_Top.points[-1], 4, "down")
    
    drawPath(Futura_three)
    Futura_three = Three_Top + Three_Schwung + con
    trans_scale(Futura_three, valueToMoveGlyph)
    return Futura_three, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawFutura_three_thinStroke(x, y, *, pass_from_thick=None):

    Futura_three_thinStroke = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, pass_from_thick.pkt_Auslauf[0]/modul_width, pass_from_thick.pkt_Auslauf[1]/modul_height)  
    Auslauf = drawSchneckenzug(*Grund_c, LOWER_E, 11, HSL_size=1, HSL_start=18, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 12)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.9, Auslauf.points[-1][1]-modul_height*1)    
    #Futura_three_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.9, Auslauf.points[-1][1]-modul_height*1, part*8, part*8)
    
    Futura_three_thinStroke += Auslauf + Endpunkt
    drawPath(Futura_three_thinStroke)
    trans_thinStroke_down_left(Futura_three_thinStroke) 
    trans_scale(Futura_three_thinStroke, valueToMoveGlyph)
    return Futura_three_thinStroke
    
    
    
    
    
    
def drawFutura_four(x, y):
    
    x+=5
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-3.5, y)

    stem = drawSchriftzug3_Figures(x, y, instrokeLen=0)
    stroke_down = drawGrundelementH(*stem.points[0], 3, "down")
    stroke_hor = drawGrundelementB(*Grund_d, 5)

    Futura_four = stem + stroke_down + stroke_hor
    trans_scale(Futura_four, valueToMoveGlyph)
    return Futura_four   
    
    
    
    
def drawFutura_five(x, y):

    Futura_five = BezierPath()
       
    Five_Top = drawSchriftzug_five_Top(x, y)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    con = drawGrundelementA(*Five_Top.points[0], 4, "down")
        
    Futura_five = Five_Top + Three_Schwung + con
    drawPath(Futura_five)
    trans_scale(Futura_five, valueToMoveGlyph)
    return Futura_five, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    


def drawFutura_six(x, y):
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    pkt_Auslauf_top = Bogen_links.points[0]
    #text("pkt_Auslauf_top", pkt_Auslauf_top) 
    
    Bogen_rechts = drawSchriftzug_six_BogenRechts(x, y)
    pkt_Auslauf_inner = Bogen_rechts.points[0]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 

    Futura_six = Bogen_links + Bogen_rechts
    trans_scale(Futura_six, valueToMoveGlyph)
    return Futura_six, collections.namedtuple('dummy', 'pkt_Auslauf_top pkt_Auslauf_inner')(pkt_Auslauf_top, pkt_Auslauf_inner)
    
    
def drawFutura_six_thinStroke(x, y, *, pass_from_thick=None):

    Futura_six_thinStroke = BezierPath() 

    
    ########## this to draw real line like in drawing Tab 21 #########
    # Einsatz = drawInstroke(*pass_from_thick.pkt_Auslauf_top, 1, "down")
    # Auslauf_top = drawSchneckenzug(*Einsatz.points[-1], UPPER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    # Futura_six_thinStroke += Einsatz 

    ##################################################################
    
    
    Auslauf_top = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_top, UPPER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    
    ### wegen blödem Absatz – siehe Tab21
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+2, y-0.25)
    Auslauf_inner = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=2, HSL_start=15, clockwise=False, inward=False)
    #Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, UPPER_E, 3, HSL_size=2, HSL_start=10, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf_inner) 
    trans_thinStroke_down_left(Auslauf_inner) ### zwei halbe = 1 ganzes verschoben siehe Tab 21
    trans_thinStroke_up_right(Auslauf_top)
        
    Futura_six_thinStroke += Auslauf_top + Auslauf_inner 
    drawPath(Futura_six_thinStroke)
    trans_scale(Futura_six_thinStroke, valueToMoveGlyph)
    return Futura_six_thinStroke 
    
    





    
    
def drawFutura_seven(x, y):

    Futura_seven = BezierPath()

    Seven_Top = drawSchriftzug_three_Top(x-1, y)
    pkt_Stem = Seven_Top.points[-1]
    #text("pkt_Stem", pkt_Stem) 
        
    Futura_seven = Seven_Top
    drawPath(Futura_seven)
    trans_scale(Futura_seven, valueToMoveGlyph)
    return Futura_seven, collections.namedtuple('dummy', 'pkt_Stem')(pkt_Stem)
        
    
def drawFutura_seven_thinStroke(x, y, *, pass_from_thick=None):

    Futura_seven_thinStroke = BezierPath() 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4, y+2)
    
    stem_top = drawSchneckenzug(*pass_from_thick.pkt_Stem, UPPER_E, 4, HSL_size=14, HSL_start=76, clockwise=False, inward=True)
    stem_btm = drawSchneckenzug(*stem_top.points[-1], LOWER_A, 3, HSL_size=4, HSL_start=16.7, clockwise=False, inward=True)
    
    #Futura_seven_thinStroke.oval(stem_btm.points[-1][0]-modul_width*0.55, stem_btm.points[-1][1], part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(stem_btm.points[-1][0]-modul_width*0.55, stem_btm.points[-1][1])
    
    Futura_seven_thinStroke += stem_top + stem_btm + Endpunkt
    drawPath(Futura_seven_thinStroke)
    trans_scale(Futura_seven_thinStroke, valueToMoveGlyph)
    return Futura_seven_thinStroke        







def drawFutura_eight(x, y):

    Futura_eight = drawSchriftzug_eight(x, y)
    trans_scale(Futura_eight, valueToMoveGlyph)
    return Futura_eight
    
    
    
    
    


def drawFutura_nine(x, y):
    
    Bogen_links = drawSchriftzug_nine_BogenLinks(x, y)
    pkt_Auslauf_inner = Bogen_links.points[-1]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 
    
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y, version="nine")
    pkt_Auslauf_btm = Bogen_rechts.points[-1]
    #text("pkt_Auslauf_btm", pkt_Auslauf_btm) 
    
    Futura_nine = Bogen_links + Bogen_rechts
    trans_scale(Futura_nine, valueToMoveGlyph)
    return Futura_nine, collections.namedtuple('dummy', 'pkt_Auslauf_btm pkt_Auslauf_inner')(pkt_Auslauf_btm, pkt_Auslauf_inner)


def drawFutura_nine_thinStroke(x, y, *, pass_from_thick=None):

    Futura_nine_thinStroke = BezierPath() 
    
    Auslauf_btm = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_btm, LOWER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, LOWER_E, 4, HSL_size=6, HSL_start=9, clockwise=False, inward=False)
        
    trans_thinStroke_down_left(Auslauf_btm) 
    trans_thinStroke_up_right(Auslauf_inner)
    
    Futura_nine_thinStroke += Auslauf_btm + Auslauf_inner 
    drawPath(Futura_nine_thinStroke)
    trans_scale(Futura_nine_thinStroke, valueToMoveGlyph)
    return Futura_nine_thinStroke 
    
    
    
    
    





##################################################################################
###            Ab hier Versalien
##################################################################################


def drawSchriftzug_BG_Versalien(x, y):
                
    drawGrundelOrientMittig(A1, A2, offset, x, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)  # für J
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-5) 
     
    drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+3, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+6, y) 
    
    drawGrundelOrientMittig(A1, A2, offset, x+3, y+3) 


#drawSchriftzug_BG_Versalien(temp_x, temp_y)





def drawFutura_A(x, y):

    
    Hauptstrich_links = drawSchriftzug_A_Hauptstrich_links(x, y)
    Hauptstrich_rechts = drawSchriftzug_A_Hauptstrich_rechts(x, y)

    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x, y)
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y-5.35)
    Signatur = drawSchriftteil11(*Raute_a)
    
    Futura_A = Hauptstrich_links + Hauptstrich_rechts + Deckung_LinksUnten + Deckung_Rechtsoben + Signatur
    trans_scale(Futura_A, valueToMoveGlyph)
    return Futura_A





        
    
def drawFutura_B(x, y):
    #x += 5  
    Hauptbogen = drawSchriftzug_B_Hauptbogen(x, y)
    pkt_Auslauf = Hauptbogen.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5, y+2.5)   
    ##### Auge nach Roßberg
    # # # # # # # #instroke = drawInstroke(*Grund_a, 1.5)
    # # # # # # # middleElement = drawSchriftteil2(*Grund_a)
    # # # # # # # pkt_instrokeAuge = middleElement.points[0]
    # # # # # # # #text("pkt_instrokeAuge", middleElement.points[0])
    # # # # # # # Auge = drawSchriftteil8(*middleElement.points[-1])
    # # # # # # # pkt_outstrokeAuge = Auge.points[-1]
    # # # # # # # #text("pkt_outstrokeAuge", Auge.points[-1])

    ### Auge Petra
    Auge = drawAuge(*Grund_a)    
    #middleElement = drawSchriftteil2(*Grund_a)
    pkt_instrokeAuge = Auge.points[0]
    #text("pkt_instrokeAuge", pkt_instrokeAuge)
    pkt_outstrokeAuge = Auge.points[-1]
    #text("pkt_outstrokeAuge", pkt_outstrokeAuge)

    Bauch = drawSchriftzug_B_Bauch(x, y)
    Fuss = drawSchneckenzug(*Bauch.points[-2], UPPER_H, 3, HSL_size=3, HSL_start=24, clockwise=False, inward=True)

    Futura_B = Hauptbogen + Fuss +  Auge + Bauch
    drawPath(Futura_B)
    trans_scale(Futura_B, valueToMoveGlyph)      
    return Futura_B, collections.namedtuple('dummy', 'pkt_Auslauf pkt_instrokeAuge pkt_outstrokeAuge')(pkt_Auslauf, pkt_instrokeAuge, pkt_outstrokeAuge)

    
def drawFutura_B_thinStroke(x, y, *, pass_from_thick=None):
    
    Futura_B_thinStroke = BezierPath()
        
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    #Futura_B_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)

    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.25)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=16, clockwise=False, inward=True)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=10, clockwise=True, inward=False)

    Futura_B_thinStroke += Auslauf + instroke + curve_toInstroke + curve_fromAuge + Endpunkt
    drawPath(Futura_B_thinStroke)
    trans_thinStroke_down_left(Futura_B_thinStroke) 
    trans_scale(Futura_B_thinStroke, valueToMoveGlyph)
    return Futura_B_thinStroke
    




def drawFutura_C(x, y, version="Substantival"):
   
    stehender_Schwung = drawSchriftzug_C_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-2]
    #text("pkt_Auslauf", stehender_Schwung.points[-2])
    liegender_Schwung = drawSchriftzug_C_liegenderSchwung(x, y)
    Deckung = drawSchriftzug_A_DeckungOben(x-2.5, y+0.25)

    Futura_C = stehender_Schwung + liegender_Schwung + Deckung
    
    if version == "Substantival":
        trans_scale(Futura_C, valueToMoveGlyph)
    if version == "Versal":
        trans_scale(Futura_C, valueToMoveGlyph)
        Futura_C.scale(1.25)
    if version == "Initial":
        trans_scale(Futura_C, valueToMoveGlyph)
        Futura_C.scale(1.75)        
    if version == "Capital":
        trans_scale(Futura_C, valueToMoveGlyph)
        Futura_C.scale(2.5)
    
    return Futura_C, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)


def drawFutura_C_thinStroke(x, y, *, pass_from_thick=None):

    Futura_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)  

    Futura_C_thinStroke += Auslauf + Endpunkt
    drawPath(Futura_C_thinStroke)
    trans_thinStroke_down_left(Futura_C_thinStroke) 
    trans_scale(Futura_C_thinStroke, valueToMoveGlyph)    
    return Futura_C_thinStroke
    

def drawFutura_C_thinStroke_Versal(x, y, *, pass_from_thick=None):

    Futura_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2.5, Auslauf.points[-1][1]-5.8
    amount = 15
    radius = part*7
    
    i = 0.4
    for i in range(amount):
        Futura_C_thinStroke.oval(position[0]+i*0.2, position[1]+i*0.2, radius-i*0.4, radius-i*0.4)
        i += 0.4
    Futura_C_thinStroke += Auslauf
    drawPath(Futura_C_thinStroke)
    Futura_C_thinStroke.translate(-(modul_width*0.5)+1, -(modul_height*0.25-0.5))
    trans_scale(Futura_C_thinStroke, valueToMoveGlyph)    
    Futura_C_thinStroke.scale(1.25)
    return Futura_C_thinStroke


def drawFutura_C_thinStroke_Initial(x, y, *, pass_from_thick=None):

    Futura_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2, Auslauf.points[-1][1]-4.15
    amount = 15
    radius = part*5
    
    i = 0.4
    for i in range(amount):
        Futura_C_thinStroke.oval(position[0]+i*0.2, position[1]+i*0.2, radius-i*0.4, radius-i*0.4)
        i += 0.4
        
    Futura_C_thinStroke += Auslauf
    drawPath(Futura_C_thinStroke)
    Futura_C_thinStroke.translate(-(modul_width*0.5)+1.5, -(modul_height*0.25-0.75))
    trans_scale(Futura_C_thinStroke, valueToMoveGlyph)    
    Futura_C_thinStroke.scale(1.75)
    return Futura_C_thinStroke
        
        
def drawFutura_C_thinStroke_Capital(x, y, *, pass_from_thick=None):

    Futura_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2, Auslauf.points[-1][1]-4.15
    amount = 20
    radius = part*5
    
    i = 0.3
    for i in range(amount):
        Futura_C_thinStroke.oval(position[0]+i*0.15, position[1]+i*0.15, radius-i*0.3, radius-i*0.3)
        i += 0.3

    Futura_C_thinStroke += Auslauf
    drawPath(Futura_C_thinStroke)
    Futura_C_thinStroke.translate(-(modul_width*0.5)+2, -(modul_height*0.25-1))
    trans_scale(Futura_C_thinStroke, valueToMoveGlyph)    
    Futura_C_thinStroke.scale(2.5)
    return Futura_C_thinStroke
    
    
    
    
          
    


def drawFutura_D(x, y):
   
    stehender_Schwung = drawSchriftzug_G_stehenderSchwung(x,y-1)
    pkt_Auslauf = stehender_Schwung.points[-3]
    #text("pkt_Auslauf", stehender_Schwung.points[-3])
    stehender_Schwung_Deckung = drawSchriftzug_D_Deckung(x-3, y)
    liegender_Schwung = drawSchriftzug_D_liegenderSchwung(x-3,y)
    Hauptbogen = drawSchriftzug_D_Hauptbogen(x-3, y)

    Futura_D = stehender_Schwung + stehender_Schwung_Deckung + liegender_Schwung + Hauptbogen
    trans_scale(Futura_D, valueToMoveGlyph)
    return Futura_D, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    

    
    
    

    

def drawFutura_E(x, y):
   
    stehender_Schwung = drawSchriftzug_E_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-2]
    #text("pkt_Auslauf", stehender_Schwung.points[-2])

    liegender_Schwung = drawSchriftzug_E_liegenderSchwung(x, y)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4.5, y+3.5)  
    Deckung_top = drawSchneckenzug(*Grund_a, LOWER_G, 6, HSL_size=0.5, HSL_start=6, clockwise=True, inward=True)
    Einsatz = drawGrundelementC(*Grund_a, 0.5)
    Deckung_btm = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=0.5, HSL_start=9, clockwise=True, inward=True)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y-0.25)
    Signatur = drawSchriftteil11(*Raute_a)

    Futura_E = stehender_Schwung + liegender_Schwung + Deckung_top + Einsatz + Deckung_btm + Signatur
    drawPath(Futura_E)
    trans_scale(Futura_E, valueToMoveGlyph)
    return Futura_E, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    



    
      
   
def drawFutura_F(x, y):
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)

    Strich = drawGrundelementF(Raute_a[0], Raute_a[1]+3.85*modul_height, 9.35)
    Deckstrich  = drawGrundelementB(Raute_a[0], Raute_a[1]+3.85*modul_height, 3.35)
    Querstrich  = drawGrundelementB(Raute_a[0], Raute_a[1]-0.3*modul_height, 3.25)
    
    Futura_F = Strich + Deckstrich + Querstrich
    trans_scale(Futura_F, 3.75)
    return Futura_F    
    
    
    
def drawFutura_F_thinStroke(x, y, *, pass_from_thick=None):

    Futura_F_thinStroke = BezierPath()
    
    Futura_F_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.25*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 1, HSL_size=1, HSL_start=28, clockwise=True, inward=False)

    Futura_F_thinStroke += Auslauf
    drawPath(Futura_F_thinStroke)
    trans_thinStroke_down_left(Futura_F_thinStroke) 
    trans_scale(Futura_F_thinStroke, valueToMoveGlyph)
    return Futura_F_thinStroke 



    
    
def drawFutura_G(x, y):

    Hauptbogen = drawSchriftzug_G_Hauptbogen(x, y)
    Hauptbogen_Deckung = drawSchriftzug_G_Deckung(x, y)
    
    Fuss = drawSchriftzug_G_Fuss(x, y)

    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5, y+3)
    # # # # # # # # #instroke = drawInstroke(*Grund_a, 1.5)
    # # # # # # # # middleElement = drawSchriftteil2(*Grund_a)
    # # # # # # # # pkt_instrokeAuge = middleElement.points[0]
    # # # # # # # # #text("pkt_instrokeAuge", middleElement.points[0])
    
    # # # # # # # # Auge = drawSchriftteil8(*middleElement.points[-1])
    # # # # # # # # pkt_outstrokeAuge = Auge.points[-1]
    # # # # # # # # #text("pkt_outstrokeAuge", Auge.points[-1])
    
    ### Auge Petra
    Auge = drawAuge(*Grund_a)    
    #middleElement = drawSchriftteil2(*Grund_a)
    pkt_instrokeAuge = Auge.points[0]
    #text("pkt_instrokeAuge", pkt_instrokeAuge)
    pkt_outstrokeAuge = Auge.points[-1]
    #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
    
    
    Bauch = drawSchriftzug_G_Bauch(x, y)
    
    stehender_Schwung = drawSchriftzug_G_stehenderSchwung(x, y, instrokeLen=1.5)
    pkt_Auslauf_unten = stehender_Schwung.points[-3]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    pkt_Auslauf_oben = stehender_Schwung.points[-1]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.25, y-3)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.4, Signatur.points[-8][1]+modul_height*1.25
    #text("pkt_Signatur", pkt_Signatur)

    Futura_G = Hauptbogen + Hauptbogen_Deckung + Fuss + Auge + Bauch + stehender_Schwung + Signatur
    trans_scale(Futura_G, valueToMoveGlyph)
    return Futura_G, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_outstrokeAuge pkt_Auslauf_unten pkt_Auslauf_oben pkt_Signatur')(pkt_instrokeAuge, pkt_outstrokeAuge, pkt_Auslauf_unten, pkt_Auslauf_oben, pkt_Signatur)
    
    
    
def drawFutura_G_thinStroke(x, y, *, pass_from_thick=None):

    Futura_G_thinStroke = BezierPath()
    
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt_unten = drawThinstroke_Endpunkt(Auslauf_unten.points[-1][0]-modul_width*1, Auslauf_unten.points[-1][1]-modul_height*1.35)
    
    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_oben, LOWER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
    Endpunkt_oben = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.5, Auslauf_oben.points[-1][1]-modul_height*0.6)
 
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.25)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=16, clockwise=False, inward=True)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=10, clockwise=True, inward=False)

    Futura_G_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.25*modul_height))

    move_down = Auslauf_unten + instroke + curve_toInstroke + curve_fromAuge ### didn't work. Why???
    trans_thinStroke_down_left(Auslauf_unten) 
    trans_thinStroke_down_left(instroke) 
    trans_thinStroke_down_left(curve_toInstroke) 
    trans_thinStroke_down_left(curve_fromAuge) 
    trans_thinStroke_down_left(Futura_G_thinStroke) 
    trans_thinStroke_up_right(Auslauf_oben)
        
    Futura_G_thinStroke += Auslauf_unten + Auslauf_oben + instroke + curve_toInstroke + curve_fromAuge + Endpunkt_unten + Endpunkt_oben  
    drawPath(Futura_G_thinStroke)
    trans_scale(Futura_G_thinStroke, valueToMoveGlyph)
    return Futura_G_thinStroke
    
    
    
    




def drawFutura_H(x, y):
    
    x += 3
    
    stehender_Schwung = drawSchriftzug_H_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    Hauptstrich_rechts = drawSchriftzug_H_Hauptstrich_rechts(x, y)
    pkt_Einlauf = Hauptstrich_rechts.points[0]
    #text("pkt_Einlauf", pkt_Einlauf)
    pkt_Auslauf_unten = Hauptstrich_rechts.points[-2]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x-1.25-3.5, y+0.25)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5.5)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)
    
    Futura_H = stehender_Schwung + Hauptstrich_rechts + Deckung_Rechtsoben + liegender_Schwung_unten
    
    trans_scale(Futura_H, valueToMoveGlyph)
    return Futura_H, collections.namedtuple('dummy', 'pkt_Einlauf pkt_Auslauf pkt_Auslauf_unten')(pkt_Einlauf, pkt_Auslauf, pkt_Auslauf_unten)
    
    
def drawFutura_H_thinStroke(x, y, *, pass_from_thick=None):

    Futura_H_thinStroke = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Einlauf, 0.5)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 1, HSL_size=1, HSL_start=28, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 3, HSL_size=1.1, HSL_start=20, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 4)])
     
    Futura_H_thinStroke += instroke + curve_toInstroke + Auslauf + Endpunkt + Auslauf_unten
    drawPath(Futura_H_thinStroke)
    trans_thinStroke_down_left(Futura_H_thinStroke)
    trans_scale(Futura_H_thinStroke, valueToMoveGlyph)
    return Futura_H_thinStroke
       

    
    
    


def drawFutura_I(x, y):

    liegender_Schwung = drawSchriftzug_I_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_I_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, y-5.25)
    Deckung_left = drawSchriftteil9(*Raute_a)
    Deckung_btm = drawSchriftteil8(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.5, y-0.25)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.25
    #text("pkt_Signatur", pkt_Signatur)
    
    Futura_I = liegender_Schwung + stehender_Schwung + Deckung_left + Deckung_btm + Signatur
    trans_scale(Futura_I, valueToMoveGlyph)
    return Futura_I, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
        
       
def drawFutura_I_thinStroke(x, y, *, pass_from_thick=None):

    Futura_I_thinStroke = BezierPath()
    
    Futura_I_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 1, HSL_size=1, HSL_start=28, clockwise=True, inward=False)

    Futura_I_thinStroke += Auslauf
    drawPath(Futura_I_thinStroke)
    trans_scale(Futura_I_thinStroke, valueToMoveGlyph)
    return Futura_I_thinStroke 
    
    
    
    


def drawFutura_J(x, y):

    #x += 5
    liegender_Schwung = drawSchriftzug_J_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_J_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.5, y-0.75)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.25
    #text("pkt_Signatur", pkt_Signatur)
    
    Futura_J = liegender_Schwung + stehender_Schwung + Signatur
    trans_scale(Futura_J, valueToMoveGlyph)
    return Futura_J, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
    
    
def drawFutura_J_thinStroke(x, y, *, pass_from_thick=None):

    Futura_J_thinStroke = BezierPath()
    
    Futura_J_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.25*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.25, HSL_start=20, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    Futura_J_thinStroke += Auslauf + Endpunkt
    drawPath(Futura_J_thinStroke)
    trans_thinStroke_down_left(Futura_J_thinStroke) 
    trans_scale(Futura_J_thinStroke, valueToMoveGlyph)
    return Futura_J_thinStroke 
    
    




def drawFutura_K(x, y):
    x += 3
    stehender_Schwung = drawSchriftzug_K_stehenderSchwung(x, y)

    ### Deckung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3.25, y+2.75)
    Einsatz = drawGrundelementC(*Grund_a, 0.25, "unten")
    Deckung_oben_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_oben_btm = drawHalbbogen4(*Grund_a, instrokeLen=0)
    Deckung_oben = Einsatz + Deckung_oben_top + Deckung_oben_btm


    ### Deckung unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-3, y-5.5)
    Einsatz = drawGrundelementC(*Grund_a, 0.5, "unten")
    Deckung_unten_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_unten_btm = drawHalbbogen1(*Grund_a, instrokeLen=0)
    Deckung_unten = Einsatz + Deckung_unten_top + Deckung_unten_btm
    
    ### Schwung unten
    liegender_Schwung_unten = drawSchriftzug_S_liegenderSchwung(x-1.25, y)
    outstroke = drawOutstroke(*liegender_Schwung_unten.points[0], 0.3, "down")
    
    ### Querstrich
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.8, y-2)
    Querstrich = drawGrundelementB(*Grund_a, 3)
    
    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1, y+1.5)
    instroke = drawInstroke(*Grund_a, 1)
    middleElement = drawGrundelementC(*Grund_a, 0.4)
    Auge = drawSchriftteil8(*middleElement.points[-1])
    Auge_all = instroke + middleElement + Auge
    
    Futura_K = stehender_Schwung + liegender_Schwung_unten + Deckung_oben + Deckung_unten + Querstrich + Auge_all + outstroke
    trans_scale(Futura_K, valueToMoveGlyph)
    return Futura_K
    
    
    
    
    
    

def drawFutura_L(x, y):

    stehender_Schwung = drawSchriftzug_L_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-9]
    #text("pkt_Auslauf", pkt_Auslauf)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+2.5)
    Einsatz = drawGrundelementC(*Grund_a, 0.25, "unten")
    Deckung_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_btm = drawHalbbogen3(*Grund_a, instrokeLen=0)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5.35)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    Futura_L = stehender_Schwung + Einsatz + Deckung_top + Deckung_btm + liegender_Schwung_unten
    trans_scale(Futura_L, valueToMoveGlyph)
    return Futura_L, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    
def drawFutura_M(x, y):

    links = drawSchriftzug_M_Bogen_links(x, y)
    mitte = drawSchriftzug_M_Bogen_mitte(x, y)
    rechts = drawSchriftzug_M_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[30]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x+3, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+8.75, y-5.35)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    Futura_M = links + mitte + rechts + Deckung_LinksUnten + liegender_Schwung_unten
    trans_scale(Futura_M, valueToMoveGlyph)
    return Futura_M, collections.namedtuple('dummy', 'pkt_Einlauf')(pkt_Einlauf)
    

    
def drawFutura_M_thinStroke(x, y, *, pass_from_thick=None):

    Futura_M_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    pkt_Einlauf = pass_from_thick.pkt_Einlauf[0]- 3*modul_width, pass_from_thick.pkt_Einlauf[1]

    Einlauf_davor = drawSchneckenzug(*pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
    
    Futura_M_thinStroke += Einlauf + Einlauf_davor
    drawPath(Futura_M_thinStroke)
    trans_thinStroke_down_left(Futura_M_thinStroke) 
    trans_scale(Futura_M_thinStroke, valueToMoveGlyph)
    return Futura_M_thinStroke
       
    
    
    
    
def drawFutura_N(x, y):

    links = drawSchriftzug_M_Bogen_links(x, y)
    rechts = drawSchriftzug_M_Bogen_rechts(x-3, y)
    pkt_Einlauf = rechts.points[30]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x+3, y)

    Futura_N = links + rechts + Deckung_LinksUnten 
    trans_scale(Futura_N, valueToMoveGlyph)
    return Futura_N, collections.namedtuple('dummy', 'pkt_Einlauf')(pkt_Einlauf)
    

def drawFutura_N_thinStroke(x, y, *, pass_from_thick=None):

    Futura_N_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    Futura_N_thinStroke += Einlauf
    drawPath(Futura_N_thinStroke)
    trans_thinStroke_down_left(Futura_N_thinStroke) 
    trans_scale(Futura_N_thinStroke, valueToMoveGlyph)
    return Futura_N_thinStroke
    
    
     
    
    

    
def drawFutura_O(x, y):

    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-9]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    Futura_O = stehender_Schwung + liegender_Schwung + Bogen_rechts
    trans_scale(Futura_O, valueToMoveGlyph)
    return Futura_O, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
    
    

    
    
    
    
    
def drawFutura_P(x, y):

    links = drawSchriftzug_P_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_P_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[-24]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung = drawGrundelementB(x+4*modul_width, y+1.25*modul_height, 3.75)
    #drawSchriftzug_R_Deckung(x+4.5, y+0.25)

    Futura_P = links + rechts + Deckung 
    trans_scale(Futura_P, valueToMoveGlyph)
    return Futura_P, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
    

def drawFutura_P_thinStroke(x, y, *, pass_from_thick=None):

    Futura_P_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    Futura_P_thinStroke += Einlauf + Auslauf + Endpunkt
    drawPath(Futura_P_thinStroke)
    trans_thinStroke_down_left(Futura_P_thinStroke)
    trans_scale(Futura_P_thinStroke, valueToMoveGlyph)
    return Futura_P_thinStroke
    
    
    
    

def drawFutura_Q(x, y):
    
    ''' dieser obere Teil ist von O kopiert'''
    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-9]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    Futura_O = stehender_Schwung + liegender_Schwung + Bogen_rechts
    
    ''' ab hier neue Form'''
    liegender_Schwung = drawSchriftzug_Q_liegenderSchwung(x-2, y)

    Futura_Q = Futura_O + liegender_Schwung
    trans_scale(Futura_Q, valueToMoveGlyph)
    return Futura_Q, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    



    
def drawFutura_R(x, y):

    Hauptbogen = drawSchriftzug_R_Hauptbogen(x, y)
    Hauptbogen_Deckung = drawSchriftzug_R_Deckung(x, y)
    
    Fuss = drawSchriftzug_R_Fuss(x, y)
    
    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5, y+2.5)
    Auge = drawAuge(*Grund_a)    
    #middleElement = drawSchriftteil2(*Grund_a)
    pkt_instrokeAuge = Auge.points[0]
    #text("pkt_instrokeAuge", pkt_instrokeAuge)
    pkt_outstrokeAuge = Auge.points[-1]
    #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.75, y-1.5)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.4
    #text("pkt_Signatur", pkt_Signatur)

    Futura_R = Hauptbogen + Hauptbogen_Deckung + Fuss + Auge + Signatur
    trans_scale(Futura_R, valueToMoveGlyph)
    return Futura_R, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_outstrokeAuge pkt_Signatur')(pkt_instrokeAuge, pkt_outstrokeAuge, pkt_Signatur)
    
    
def drawFutura_R_thinStroke(x, y, *, pass_from_thick=None):
    
    Futura_R_thinStroke = BezierPath()
    
    Futura_R_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.25)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=16, clockwise=False, inward=True)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=10, clockwise=True, inward=False)

    Futura_R_thinStroke += instroke + curve_toInstroke + curve_fromAuge
    drawPath(Futura_R_thinStroke)
    trans_thinStroke_down_left(Futura_R_thinStroke)
    trans_scale(Futura_R_thinStroke, valueToMoveGlyph)
    return Futura_R_thinStroke
    
    
    
    
    


def drawFutura_S(x, y):

    
    stehender_Schwung = drawSchriftzug_S_stehenderSchwung(x, y)

    ### Deckung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6, y+3)
    Einsatz = drawGrundelementC(*Grund_a, 0.25, "unten")
    Deckung_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_btm = drawHalbbogen3(*Grund_a, instrokeLen=0)
 
    liegender_Schwung_unten = drawSchriftzug_S_liegenderSchwung(x, y, outstrokeLen=0.7)

    Futura_S = stehender_Schwung + Einsatz + Deckung_top +  Deckung_btm + liegender_Schwung_unten
    trans_scale(Futura_S, valueToMoveGlyph)
    return Futura_S
    
    
    
     
    
def drawFutura_T(x, y):
    
    stehender_Schwung = drawSchriftzug_T_stehenderSchwung(x, y)
    liegender_Schwung = drawSchriftzug_T_liegenderSchwung(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y-5.35)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    Futura_T = stehender_Schwung + liegender_Schwung + liegender_Schwung_unten
    trans_scale(Futura_T, valueToMoveGlyph)
    return Futura_T
    
    




def drawFutura_U(x, y):

    Futura_U = BezierPath()
    
    links = drawSchriftzug_U_Hauptstrich_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    rechts = drawSchriftzug_U_Hauptstrich_rechts(x, y)
    

    Futura_U = links + rechts 
    trans_scale(Futura_U, valueToMoveGlyph)
    return Futura_U, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    


    
    
    



def drawFutura_V(x, y):

    Futura_V = BezierPath()
    
    links = drawSchriftzug_V_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_V_Bogen_rechts(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.5, y-5.5)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    Futura_V = links + rechts + liegender_Schwung_unten 
    trans_scale(Futura_V, valueToMoveGlyph)
    return Futura_V, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    

    
    
def drawFutura_W(x, y):
    
    Futura_W = BezierPath()

    links = drawSchriftzug_W_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    mitte = drawSchriftzug_W_Bogen_mitte(x, y)
    rechts = drawSchriftzug_W_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[-24]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Fuss = drawSchneckenzug(*rechts.points[-26], UPPER_H, 3, HSL_size=4, HSL_start=11, clockwise=False, inward=False)
    
    Futura_W = links + mitte + rechts + Fuss 
    drawPath(Futura_W)
    trans_scale(Futura_W, valueToMoveGlyph)
    return Futura_W, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
    
    


def drawFutura_W_thinStroke(x, y, *, pass_from_thick=None):

    Futura_W_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
    pkt_Einlauf_vorher = pass_from_thick.pkt_Einlauf[0]-modul_width*3.75, pass_from_thick.pkt_Einlauf[1]
    Einlauf_vorher = drawSchneckenzug(*pkt_Einlauf_vorher, UPPER_E, 1, HSL_size=2, HSL_start=28, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
        
    Futura_W_thinStroke += Einlauf + Einlauf_vorher + Auslauf + Endpunkt
    drawPath(Futura_W_thinStroke)
    trans_thinStroke_down_left(Futura_W_thinStroke)
    trans_scale(Futura_W_thinStroke, valueToMoveGlyph)
    return Futura_W_thinStroke
    
    
    
    
 
    

def drawFutura_X(x, y):

    Hauptbogen_Links = drawSchriftzug_R_Hauptbogen(x, y)
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x, y)
    
    Hauptbogen_Rechts = drawSchriftzug_X_BogenRechts(x, y)
    Deckung_Rechtsoben = drawSchriftzug_X_DeckungOben(x, y)

    ### Querstrich
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.5, y-2)
    Querstrich = drawGrundelementB(*Grund_a, 3.5)
    
    Futura_X = Hauptbogen_Links + Deckung_LinksUnten + Hauptbogen_Rechts + Deckung_Rechtsoben + Querstrich
    trans_scale(Futura_X, valueToMoveGlyph)
    return Futura_X
    
    
    
      

def drawFutura_Y(x, y):

    Futura_Y = BezierPath()
    
    links = drawSchriftzug_Y_Bogen_links(x, y)
    rechts = drawSchriftzug_Y_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[-24]
    #text("pkt_Einlauf", pkt_Einlauf)
    pkt_Auslauf = rechts.points[-26]
    #text("pkt_Auslauf", pkt_Auslauf)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.45, y-5.5)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    Futura_Y = links + rechts + liegender_Schwung_unten 
    trans_scale(Futura_Y, valueToMoveGlyph)
    return Futura_Y, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
   

def drawFutura_Y_thinStroke(x, y, *, pass_from_thick=None):

    Futura_Y_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1 + 0.075 * i  for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.75, Auslauf.points[-1][1]-modul_height*0.04)
        
    Futura_Y_thinStroke += Einlauf + Auslauf + Endpunkt
    drawPath(Futura_Y_thinStroke)
    trans_thinStroke_down_left(Futura_Y_thinStroke)
    trans_scale(Futura_Y_thinStroke, valueToMoveGlyph)
    return Futura_Y_thinStroke
    
    
    
    
    
    
    
    
def drawFutura_Z(x, y):

    Futura_Z = BezierPath()
    
    oben = drawSchriftzug_Z_Bogen_oben(x, y)
    outstroke = drawOutstroke(*oben.points[-1], 0.25, "down")
    pkt_Auslauf_oben = outstroke.points[0]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)
    
    unten = drawSchriftzug_Z_Bogen_unten(x, y)
    pkt_Auslauf_unten = unten.points[-3]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    
    Deckung_unten = drawSchriftzug_R_Deckung(x, y)

    Futura_Z = oben + unten + Deckung_unten + outstroke
    trans_scale(Futura_Z, valueToMoveGlyph)
    return Futura_Z, collections.namedtuple('dummy', 'pkt_Auslauf_oben pkt_Auslauf_unten')(pkt_Auslauf_oben, pkt_Auslauf_unten)
    
    
def drawFutura_Z_thinStroke(x, y, *, pass_from_thick=None):

    Futura_Z_thinStroke = BezierPath()

    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_oben, LOWER_E, 8, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.25, Auslauf_oben.points[-1][1]-modul_height*1.04)
    
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 1, HSL_size=1, HSL_start=20, clockwise=True, inward=True)
    
    Futura_Z_thinStroke += Auslauf_oben + Auslauf_unten + Endpunkt
    drawPath(Futura_Z_thinStroke)
    trans_thinStroke_down_left(Futura_Z_thinStroke)
    trans_scale(Futura_Z_thinStroke, valueToMoveGlyph)
    return Futura_Z_thinStroke




def drawFutura_dotStrokeUp(x, y, *, pass_from_thick=None):

    Futura_dotStrokeUp = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 1, "down")
    Auslauf = drawSchneckenzug(*instroke.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.6, Auslauf.points[-1][1]-modul_height*1.1)
    
    Futura_dotStrokeUp += instroke + Auslauf + Endpunkt
    drawPath(Futura_dotStrokeUp)
    trans_thinStroke_up_right(Futura_dotStrokeUp)
    trans_scale(Futura_dotStrokeUp, valueToMoveGlyph)    
    return Futura_dotStrokeUp
    
    
def drawFutura_dotStrokeDown(x, y, *, pass_from_thick=None):

    Futura_dotStrokeDown = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 0.75)
    Auslauf = drawSchneckenzug(*instroke.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.75, Auslauf.points[-1][1]-modul_height*0.05)
    
    Futura_dotStrokeDown += instroke + Auslauf + Endpunkt
    drawPath(Futura_dotStrokeDown)
    trans_thinStroke_down_left(Futura_dotStrokeDown)
    trans_scale(Futura_dotStrokeDown, valueToMoveGlyph)    
    return Futura_dotStrokeDown
    
    
    
def drawFutura_dotStrokeUpDown(x, y, *, pass_from_thick=None):

    Futura_dotStrokeUpDown = BezierPath()
    
    ### UP
    instroke_up = drawInstroke(*pass_from_thick.pkt_Auslauf_up, 1, "down")
    Auslauf_up = drawSchneckenzug(*instroke_up.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_up = drawThinstroke_Endpunkt(Auslauf_up.points[-1][0]-modul_width*0.6, Auslauf_up.points[-1][1]-modul_height*1.1)
    
    ### DOWN
    instroke_down = drawInstroke(*pass_from_thick.pkt_Auslauf_down, 0.75)
    Auslauf_down = drawSchneckenzug(*instroke_down.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_down = drawThinstroke_Endpunkt(Auslauf_down.points[-1][0]-modul_width*0.75, Auslauf_down.points[-1][1]-modul_height*0.05)

    Futura_dotStrokeUp = instroke_up + Auslauf_up + Endpunkt_up
    Futura_dotStrokeDown = instroke_down + Auslauf_down + Endpunkt_down

    trans_thinStroke_up_right(Futura_dotStrokeDown)
    trans_thinStroke_down_left(Futura_dotStrokeDown)  

    Futura_dotStrokeUpDown = Futura_dotStrokeUp + Futura_dotStrokeDown
    drawPath(Futura_dotStrokeUpDown)
    trans_scale(Futura_dotStrokeUpDown, valueToMoveGlyph)    
    return Futura_dotStrokeUpDown
    
    
    
    
    
     
    


# ______________________________________________________    
    
    
margin_1modul = 60
margin_2modul = 120
marginStr = 30+30
marginRnd = 60 
margin_r=-30
margin_t=0

# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {

   'a' : [ drawFutura_a, [temp_x, temp_y], 70, marginStr ],
   #  'b' : [ drawFutura_b, [temp_x, temp_y], marginStr, marginRnd ],
   #  'c' : [ drawFutura_c, [temp_x, temp_y], marginRnd, margin_r ],
   #  'd' : [ drawFutura_d, [temp_x, temp_y, 12], marginRnd, marginRnd ],
   #  'e' : [ drawFutura_e, [temp_x, temp_y], marginRnd, marginStr ],
   #  'f' : [ drawFutura_f, [temp_x, temp_y], marginStr, -153 ],
   #  'g' : [ drawFutura_g, [temp_x, temp_y], marginRnd, marginStr+22 ],
   #  'h' : [ drawFutura_h, [temp_x, temp_y], marginStr, marginRnd ],
   #  'i' : [ drawFutura_i, [temp_x, temp_y], marginStr, marginStr ],
   #  'j' : [ drawFutura_j, [temp_x, temp_y], marginStr, marginStr ],
   #  'k' : [ drawFutura_k, [temp_x, temp_y], marginStr, marginStr-30 ],
   #  'l' : [ drawFutura_l, [temp_x, temp_y], marginStr, marginStr-30],
   #  'm' : [ drawFutura_m, [temp_x, temp_y], marginStr, marginStr ],
   #  'n' : [ drawFutura_n, [temp_x, temp_y], marginStr, marginStr ],
   #  'o' : [ drawFutura_o, [temp_x, temp_y], marginRnd, marginRnd ],
   #  'p' : [ drawFutura_p, [temp_x, temp_y], marginStr, marginRnd ],
   #  'q' : [ drawFutura_q, [temp_x, temp_y], marginRnd, marginStr ],
    'r' : [ drawFutura_r, [temp_x, temp_y], 75, 20 ],
   #  's' : [ drawFutura_s, [temp_x, temp_y], marginStr, marginStr ],
   #  'longs' : [ drawFutura_longs, [temp_x, temp_y], marginStr, -153 ],
   #  'germandbls' : [ drawFutura_germandbls, [temp_x, temp_y], marginStr, marginStr-30 ],
    't' : [ drawFutura_t, [temp_x, temp_y], 30, 30],
    'u' : [ drawFutura_u, [temp_x, temp_y], 75, 75 ],
   #  'v' : [ drawFutura_v, [temp_x, temp_y], marginStr, marginRnd ],
   #  'w' : [ drawFutura_w, [temp_x, temp_y], marginStr, marginRnd ],
   #  'x' : [ drawFutura_x, [temp_x, temp_y], marginStr, margin_r ],
   #  'y' : [ drawFutura_y, [temp_x, temp_y], marginStr, marginRnd ],
   #  'z' : [ drawFutura_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd ],
   #  'z.init' : [ drawFutura_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd ],
   #  'z.fina' : [ drawFutura_z, [temp_x, temp_y, "final"], marginStr, marginRnd ],

   #  'adieresis' : [ drawFutura_adieresis, [temp_x, temp_y], marginRnd, marginStr ],
   #  'odieresis' : [ drawFutura_odieresis, [temp_x, temp_y], marginRnd, marginRnd ],
   #  'udieresis' : [ drawFutura_udieresis, [temp_x, temp_y], marginStr, marginStr ],

   #   'b.ss01' : [ drawFutura_b_dot, [temp_x, temp_y], marginStr, marginRnd ],
   #   'h.ss01' : [ drawFutura_h_dot, [temp_x, temp_y], marginStr, marginRnd ],
   #   'k.ss01' : [ drawFutura_k_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
   #   'l.ss01' : [ drawFutura_l_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
   #   't.ss01' : [ drawFutura_t_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
   #   'y.ss01' : [ drawFutura_y_dot, [temp_x, temp_y], marginStr, marginRnd ],


   #  #### Ligatures
    
   # 'r_c' : [ drawFutura_rc, [temp_x, temp_y], marginStr, marginStr-60 ],
   # 'longs_longs' : [ drawFutura_longs_longs, [temp_x, temp_y], marginStr-30, -153 ],
   # 'f_f' : [ drawFutura_f_f, [temp_x, temp_y], marginStr-30, -153 ],
   # 'longs_t' : [ drawFutura_longs_t, [temp_x, temp_y], marginStr, margin_t ],
   # 'f_t' : [ drawFutura_f_t, [temp_x, temp_y], marginStr, margin_t ],
   # 'f_f_t' : [ drawFutura_f_f_t, [temp_x, temp_y], marginStr, margin_t-56 ],


    
   #  ##### Interpunction
    
   #  'period' : [ drawFutura_period, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'colon' : [ drawFutura_colon, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'semicolon' : [ drawFutura_semicolon, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'quoteright' : [ drawFutura_quoteright, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'quotesingle' : [ drawFutura_quotesingle, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'comma' : [ drawFutura_comma, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'endash' : [ drawFutura_endash, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'hyphen' : [ drawFutura_hyphen, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'exclam' : [ drawFutura_exclam, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'question' : [ drawFutura_question, [temp_x, temp_y], margin_1modul, margin_1modul ],


   #  ## Numbers
    
   #  'zero' : [ drawFutura_zero, [temp_x, temp_y], marginRnd, marginRnd-60 ],
   #  'one' : [ drawFutura_one, [temp_x, temp_y], marginStr+140, marginStr ],
   #  'two' : [ drawFutura_two, [temp_x, temp_y], marginStr+18, marginStr ],
   #  'three' : [ drawFutura_three, [temp_x, temp_y], marginStr+60, marginStr-60 ],
   #  'four' : [ drawFutura_four, [temp_x, temp_y], marginStr, marginStr-15 ],
   #  'five' : [ drawFutura_five, [temp_x, temp_y], marginStr+120, marginStr-210 ],
   #  'six' : [ drawFutura_six, [temp_x, temp_y], marginRnd, marginStr ],
   #  'seven' : [ drawFutura_seven, [temp_x, temp_y], marginStr, -120 ],
   #  'eight' : [ drawFutura_eight, [temp_x, temp_y], marginRnd, marginRnd-15 ],    
   #  'nine' : [ drawFutura_nine, [temp_x, temp_y], marginRnd, marginStr ],    



   #  ##### Uppercase
    
    # 'A' : [ drawFutura_A, [temp_x, temp_y], marginStr, marginStr-90 ],
    # 'B' : [ drawFutura_B, [temp_x, temp_y], marginStr, marginRnd ],
    # 'C' : [ drawFutura_C, [temp_x, temp_y], marginStr, marginStr-120 ],
    # 'D' : [ drawFutura_D, [temp_x, temp_y], marginStr, marginRnd ],
    # 'E' : [ drawFutura_E, [temp_x, temp_y], marginStr, marginStr-60 ],
    # 'F' : [ drawFutura_F, [temp_x, temp_y], marginStr, 15 ],
    # 'G' : [ drawFutura_G, [temp_x, temp_y], marginStr, marginRnd ],
    # 'H' : [ drawFutura_H, [temp_x, temp_y], marginStr, 0 ],
    # 'I' : [ drawFutura_I, [temp_x, temp_y], marginStr, marginStr-60],
    # 'J' : [ drawFutura_J, [temp_x, temp_y], marginStr, marginRnd-150],
    # 'K' : [ drawFutura_K, [temp_x, temp_y], marginRnd, marginStr-60 ],
    # 'L' : [ drawFutura_L, [temp_x, temp_y], marginStr+60, marginStr-60 ],
    # 'M' : [ drawFutura_M, [temp_x, temp_y], marginStr, marginStr-90 ],
    # 'N' : [ drawFutura_N, [temp_x, temp_y], marginStr, marginStr-90 ],
    # 'O' : [ drawFutura_O, [temp_x, temp_y], marginStr+60, marginStr+30 ],
    # 'P' : [ drawFutura_P, [temp_x, temp_y], marginStr+30, marginStr-90 ],
    # 'Q' : [ drawFutura_Q, [temp_x, temp_y], marginStr, marginStr ],
    # 'R' : [ drawFutura_R, [temp_x, temp_y], marginStr, marginStr ],
    # 'S' : [ drawFutura_S, [temp_x, temp_y], marginStr, marginRnd ],
    # 'T' : [ drawFutura_T, [temp_x, temp_y], marginStr, marginStr-120 ],
    # 'U' : [ drawFutura_U, [temp_x, temp_y], marginStr, marginStr ],
    # 'V' : [ drawFutura_V, [temp_x, temp_y], marginStr, marginStr ],
    # 'W' : [ drawFutura_W, [temp_x, temp_y], marginStr, marginStr-95 ],
    # 'X' : [ drawFutura_X, [temp_x, temp_y], marginStr, marginStr-90 ],
    # 'Y' : [ drawFutura_Y, [temp_x, temp_y], marginStr, marginStr-95 ],
    # 'Z' : [ drawFutura_Z, [temp_x, temp_y], marginStr, marginRnd ],

    # 'C.ss02' : [ drawFutura_C, [temp_x, temp_y, "Versal"], marginStr, marginStr-150 ],
    # 'C.ss03' : [ drawFutura_C, [temp_x, temp_y, "Initial"], marginStr, marginStr-200 ],
    # 'C.ss04' : [ drawFutura_C, [temp_x, temp_y, "Capital"], marginStr, marginStr-225 ],

    }



drawFunctions_thinStroke = {

    'f' : [ drawFutura_thinstroke_Straight, [temp_x, temp_y] ],
    'g' : [ drawFutura_g_thinStroke, [temp_x, temp_y] ],
    'j' : [ drawFutura_g_thinStroke, [temp_x, temp_y] ],
    'p' : [ drawFutura_thinstroke_Straight, [temp_x, temp_y] ],
    'q' : [ drawFutura_thinstroke_Straight, [temp_x, temp_y] ],
    'x' : [ drawFutura_x_thinStroke, [temp_x, temp_y] ],
    'z' : [ drawFutura_z_thinStroke, [temp_x, temp_y] ],
    'z.init' : [ drawFutura_z_thinStroke, [temp_x, temp_y] ],
    'z.fina' : [ drawFutura_x_thinStroke, [temp_x, temp_y] ],

    'b.ss01' : [ drawFutura_dotStrokeUp, [temp_x, temp_y] ],
    'h.ss01' : [ drawFutura_dotStrokeUpDown, [temp_x, temp_y] ],
    'k.ss01' : [ drawFutura_dotStrokeUp, [temp_x, temp_y] ],
    'l.ss01' : [ drawFutura_dotStrokeUp, [temp_x, temp_y] ],
    't.ss01' : [ drawFutura_dotStrokeUp, [temp_x, temp_y] ],
    'y.ss01' : [ drawFutura_dotStrokeDown, [temp_x, temp_y] ],

    'longs' : [ drawFutura_thinstroke_Straight, [temp_x, temp_y] ],
    'longs_longs' : [ drawFutura_longs_longs_thinStroke, [temp_x, temp_y] ],
    'longs_t' : [ drawFutura_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f' : [ drawFutura_longs_longs_thinStroke, [temp_x, temp_y] ],
    'f_t' : [ drawFutura_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f_t' : [ drawFutura_longs_longs_thinStroke, [temp_x, temp_y] ],
    
    'germandbls' : [ drawFutura_germandbls_Endspitze, [temp_x, temp_y] ],
    'quotesingle' : [ drawFutura_quotesingle_thinstroke, [temp_x, temp_y] ],  


    'B' : [ drawFutura_B_thinStroke, [temp_x, temp_y] ],
    'C' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    'C.ss02' : [ drawFutura_C_thinStroke_Versal, [temp_x, temp_y] ],
    'C.ss03' : [ drawFutura_C_thinStroke_Initial, [temp_x, temp_y] ],
    'C.ss04' : [ drawFutura_C_thinStroke_Capital, [temp_x, temp_y] ],
    'D' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    'E' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    #'F' : [ drawFutura_F_thinStroke, [temp_x, temp_y] ],
    'G' : [ drawFutura_G_thinStroke, [temp_x, temp_y] ],
    'H' : [ drawFutura_H_thinStroke, [temp_x, temp_y] ],
    'I' : [ drawFutura_F_thinStroke, [temp_x, temp_y] ],
    'J' : [ drawFutura_J_thinStroke, [temp_x, temp_y] ],
    'L' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    'M' : [ drawFutura_M_thinStroke, [temp_x, temp_y] ],
    'N' : [ drawFutura_N_thinStroke, [temp_x, temp_y] ],
    'O' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    'P' : [ drawFutura_P_thinStroke, [temp_x, temp_y] ],
    'Q' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    'R' : [ drawFutura_R_thinStroke, [temp_x, temp_y] ],
    'U' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    'V' : [ drawFutura_C_thinStroke, [temp_x, temp_y] ],
    'W' : [ drawFutura_W_thinStroke, [temp_x, temp_y] ],
    'Y' : [ drawFutura_Y_thinStroke, [temp_x, temp_y] ],
    'Z' : [ drawFutura_Z_thinStroke, [temp_x, temp_y] ],

    'one' : [ drawFutura_one_thinStroke, [temp_x, temp_y] ],
    'two' : [ drawFutura_two_thinStroke, [temp_x, temp_y] ],
    'three' : [ drawFutura_three_thinStroke, [temp_x, temp_y] ],
    'five' : [ drawFutura_three_thinStroke, [temp_x, temp_y] ],
    'six' : [ drawFutura_six_thinStroke, [temp_x, temp_y] ],
    'seven' : [ drawFutura_seven_thinStroke, [temp_x, temp_y] ],
    'nine' : [ drawFutura_nine_thinStroke, [temp_x, temp_y] ],


    }
    
    
   
    
  
    
    
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	

    if key in drawFunctions_thinStroke:

        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    #print("margin left:", glyph.leftMargin,"_", "right:", glyph.rightMargin)
    #print("supposed to be:", drawFunctions[key][2],"_", drawFunctions[key][3])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_before = margin_ts - margin_fg
    #print("difference before:", difference_before)
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_mid = margin_ts - margin_fg
    #print("difference mid:", difference_mid)

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid
    #print("move thinStroke by:", move_thinStroke)
    

    glyph.copyLayerToLayer('foreground', 'background')
   
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): 
        margin_ts = 0 
        #print("(no thin line)")   
    #print("fg:", margin_fg, "__ ts:", margin_ts)  
    difference_final = margin_ts - margin_fg
    #print("difference before:", difference_before, "=== difference final:", difference_final , "?")
    
    foreground.clear()

    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    #print("drawn with:  __Angle:", a, "__Width:", w)

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 

        #print("drawn with:  __Angle:", a, "__Width:", w)

        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    
    ### keep a copy in background layer
    # newLayerName = "original_math"
    # glyph.layers[0].copyToLayer(newLayerName, clear=True)
    # glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    # glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)
    
    
    
    
    
    
    
    
    
    
    