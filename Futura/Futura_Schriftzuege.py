import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente
import version_3.creation.Halbboegen_GatC
import version_3.creation.Schriftteile_GatC

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Halbboegen_GatC)
importlib.reload(version_3.creation.Schriftteile_GatC)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.Halbboegen_GatC import *
from version_3.creation.Schriftteile_GatC import *


import math as m




temp_x = 3        ## 3
temp_y = 9        ## 9


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 20
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)







# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)


stroke(.1)
strokeWidth(.1)





def drawSchriftzug1(x,y, instroke=True, outstroke=True):
    
    Schriftzug1 = BezierPath()  
    pos = x *modul_width, (y+0.75) *modul_height
        
    if instroke == True:
        instroke_start = pos[0] - offset[0]/2,  pos[1] - offset[1]/2
        Schriftzug1.line(instroke_start, pos)

    downstroke_end = pos[0], pos[1] - (x_height)*modul_width
    Schriftzug1.line(pos, downstroke_end)

    if outstroke == True:
        line(downstroke_end, (downstroke_end[0] + offset[0]/2,    downstroke_end[1] + offset[1]/2))
    
    drawPath(Schriftzug1)
    
    return Schriftzug1
   
#drawSchriftzug1 (temp_x, temp_y, instroke=True, outstroke=True)

  
  
  
  
    
'''
AuslaufKehlung siehe Grundelemente
'''       
    





def drawSchriftzug2(x, y, instrokeLen=1, AuslaufKehlung=False, ligatureLen=2.5):
    
    Schriftzug2 = BezierPath()  
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    pos_stroke_left = drawGrundelementA(Raute_a[0]-offset[0]/2,     Raute_a[1]-offset[1]/2)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1, y+ligatureLen)    
    instroke_top = drawInstroke(*Raute_a, instrokeLen, "down")
    
    Rundung_oben = drawSchriftteil5(*Raute_a)
    
    downstroke_start = Rundung_oben.points[-1]
    downstroke_end = downstroke_start[0],          (baseline-0.25)*modul_height
    
    #if AuslaufKehlung == True:
        #AuslaufKehlung = drawAuslaufKehlung(*downstroke_end)
        #AuslaufKehlung = drawAuslaufKehlung(x, baseline-0.75)

    Schriftzug2.line(downstroke_start, downstroke_end)

    Schriftzug2 += pos_stroke_left + instroke_top + Rundung_oben
    drawPath(Schriftzug2)
    return Schriftzug2
    
    
#drawSchriftzug2(temp_x, temp_y, instrokeLen=2.5)
#drawSchriftzug2(temp_x+3, temp_y, AuslaufKehlung=False)
  
  

  
  
  
def drawSchriftzug_longs_longs_con(x, y):
    
    Schriftzug_longs_longs_con = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1, y+3) 
    
    connection_left = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=1, HSL_start=30, clockwise=True, inward=False)
    
    connection_transition = drawSchneckenzug(*connection_left.points[-1], UPPER_G, 3, HSL_size=2, HSL_start=12.25, clockwise=True, inward=True)

    outstroke = drawGrundelementF(*connection_transition.points[-1], 0.5)

    
    Schriftzug_longs_longs_con += connection_left + connection_transition + outstroke
    drawPath(Schriftzug_longs_longs_con)
    return Schriftzug_longs_longs_con
    
  
    
#drawSchriftzug_longs_longs_con(temp_x, temp_y)



def drawSchriftzug_f_f_con(x, y):
    
    Schriftzug_f_f_con = BezierPath() 
  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+4)
    connection_left = drawSchneckenzug(*Grund_a, UPPER_E, 7, HSL_size=2, HSL_start=1, clockwise=False, inward=False)
    Einsatz = drawGrundelementB(*connection_left.points[-1], 0.5)
    connection_transition = drawSchneckenzug(*Einsatz.points[-1], UPPER_F, 4, HSL_size=1, HSL_start=10.1, clockwise=True, inward=True)
    outstroke = drawGrundelementF(*connection_transition.points[-1], 1)

    Schriftzug_f_f_con += connection_left + connection_transition + outstroke + Einsatz
    drawPath(Schriftzug_f_f_con)
    return Schriftzug_f_f_con
    
#drawSchriftzug_f_f_con(temp_x, temp_y)
    
    
    
def drawSchriftzug_f_t_con(x, y):
    
    Schriftzug_f_t_con = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5, y+2.5)  
    links = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=1, HSL_start=16, clockwise=True, inward=False)
    rechts = drawSchneckenzug(*links.points[-1], LOWER_G, 2, HSL_size=1, HSL_start=16.5, clockwise=False, inward=True)
    straight_con = drawInstroke(*rechts.points[-1], 1)
    
    Schriftzug_f_t_con += links + rechts + straight_con
    drawPath(Schriftzug_f_t_con)
    return Schriftzug_f_t_con
    
#drawSchriftzug_f_t_con(temp_x, temp_y)





def drawSchriftzug3(x,y, top="standard", bottom="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug3 = BezierPath()   
    
        
    if top == "ascender":
    
        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1, y+1.5)    

        pos_stroke = drawGrundelementA(Raute_a[0]-1.5*modul_width,     Raute_a[1]-1.75*modul_height)
        instroke = drawGrundelementA(*Raute_a)

        # 1st bend
        Schriftteil5 = drawSchriftteil5(*Raute_a)
 
        stroke_start = Schriftteil5.points[-1]
        
        Schriftzug3_top = pos_stroke + instroke + Schriftteil5
        
    else:
        
        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y) 

        Rundung_oben = drawSchriftteil3(*Raute_a)
        stroke_start = Rundung_oben.points[-1]
        
        instroke = drawInstroke(*Raute_a, instrokeLen)
        
        Schriftzug3_top = Rundung_oben + instroke



    # draw Modul + Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)    
            
    
    if bottom == "gerade":    # für g und j
        stroke_end = stroke_start[0],    Raute_a[1]+modul_height*0.25
        
        Schriftzug3_bottom = Schriftzug3_top
        
    elif bottom == "Kehlung":
        stroke_end = stroke_start[0],    Raute_a[1]-modul_height*0.75
        AuslaufKehlung = drawAuslaufKehlung(*stroke_end)
        
        Schriftzug3_bottom = Schriftzug3_top #+ AuslaufKehlung
    
    else:
        
        Rundung_unten = drawSchriftteil4(*Raute_d)
        stroke_end = Rundung_unten.points[-1]
        
        outstroke = drawOutstroke(*Raute_d, outstrokeLen)
        
        Schriftzug3_bottom = Schriftzug3_top + Rundung_unten + outstroke
            

        
    
    Schriftzug3 = Schriftzug3_top + Schriftzug3_bottom

    Schriftzug3.line(stroke_start, stroke_end)
    
    drawPath(Schriftzug3)
    return Schriftzug3
    


# drawSchriftzug3(temp_x, temp_y, "standard", instrokeLen=1, outstrokeLen=1)
# drawSchriftzug3(temp_x, temp_y, "ascender", instrokeLen=1, outstrokeLen=1)
#drawSchriftzug3(temp_x, temp_y, bottom="gerade", instrokeLen=1)    
#drawSchriftzug3(temp_x, temp_y, bottom="Kehlung", instrokeLen=1.5)     







def drawSchriftzug4(x, y, bottom="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug4 = BezierPath() 
    
    if bottom == "Schwung":             # für g und j  
    
        main_stroke = drawSchriftzug3(x, y, instrokeLen=instrokeLen, bottom="gerade")  

        Transition_to_right = drawSchneckenzug(*main_stroke.points[-1], UPPER_B, 1, HSL_size=4, HSL_start=12, clockwise=False, inward=False)
 
        Bogen_unten = drawSchneckenzug(*Transition_to_right.points[-1], UPPER_A, 4, HSL_size=2, HSL_start=12.5, clockwise=True, inward=False)

        ### Endspitze = drawSchneckenzug(*Bogen_unten.points[-1], LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])

### Vergebliche Versuche, den Endpunkt zu zeichen        
#         #arcTo(point1, point2, radius)
#         Schriftzug4.moveTo((Endspitze.points[-1]))
#         #Schriftzug4.arcTo(Endspitze.points[-1], Endspitze.points[0], 30)
#         # Verzierungspunkt  
#         Schriftzug4.arc(*drawKreisSeg(Endspitze.points[-1], 4, angle_12, angle_2, True))

#         ctr = Endspitze.points[-1]    
#         #Schriftzug4.oval(ctr[0]-part*3.8, ctr[1]-part*4, part*8, part*8)
#         oval((ctr[0]-part*3.8)+1.5, (ctr[1]-part*4)-(part*8)/2, part*8, part*8)  


        Schriftzug4 += Transition_to_right + Bogen_unten #+ Endspitze
    
    if bottom == "Kehlung":
        
        main_stroke = drawSchriftzug3(x, y, instrokeLen=instrokeLen, bottom="Kehlung")
    
    
    if bottom == "standard":
        
        main_stroke = drawSchriftzug3(x, y, "standard", instrokeLen=instrokeLen, outstrokeLen=outstrokeLen)
                
    
    Serife_nach_rechts = drawSchriftteil2((x-1.5)*modul_width, (y+0.5)*modul_height)
    Serife_nach_unten = drawSchriftteil5((x-0.5)*modul_width, y*modul_height)
    
    Schriftzug4 += main_stroke + Serife_nach_rechts + Serife_nach_unten

    drawPath(Schriftzug4)
    
    return Schriftzug4
    

#drawSchriftzug4(temp_x, temp_y, bottom = "standard", instrokeLen=.5, outstrokeLen=.5)        
#drawSchriftzug4(temp_x+3, temp_y, bottom = "Schwung", instrokeLen=2)       
#drawSchriftzug4(temp_x, temp_y, instrokeLen=0, bottom = "Kehlung")        

   


   
    
   
    
    
    
    
def drawSchriftzug5(x, y, top="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug5 = BezierPath()
    

    if top == "ascender":

        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+3)    

        pos_stroke = drawPosStroke(x, y)
        instroke = drawInstroke(*Raute_d, 1, "down")
        bend_top = drawSchriftteil5(*Raute_d)
        stroke_start = bend_top.points[-1]        ### keep as 1st reference
        
        # draw Modul + Raute
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)  

        bend_bottom = drawSchriftteil4(*Raute_d)
        stroke_end = bend_bottom.points[-1]        ### keep as 2nd reference

        Serife_nach_links = drawSchriftteil6(*Raute_a)
        Serife_nach_unten = drawSchriftteil1(*Raute_d)
        
        Schriftzug5.line(stroke_start, stroke_end)        ### Hauptlinie nach unten
        
        outstroke = drawOutstroke(*Raute_d, outstrokeLen)
        
        Schriftzug5 += pos_stroke + instroke + bend_top + bend_bottom + Serife_nach_links + Serife_nach_unten + outstroke
        


    else:        # >>> if top == "standard":
            
        Schriftzug3 = drawSchriftzug3(x, y, instrokeLen=instrokeLen, outstrokeLen=outstrokeLen)

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)
    
        Serife_nach_links = drawSchriftteil6(*Raute_a)
        Serife_nach_unten = drawSchriftteil1(*Raute_d)

        Schriftzug5 = Schriftzug3 + Serife_nach_links + Serife_nach_unten
    
    
    
    drawPath(Schriftzug5)
    return Schriftzug5
    
    
#drawSchriftzug5(temp_x+3, temp_y, top="standard", outstrokeLen=0.5)      
#drawSchriftzug5(temp_x, temp_y, top="ascender", outstrokeLen=0.5)      








def drawSchriftzug6(x,y, Einsatz="standard", instrokeLen=1, outstrokeLen=1):
    
    Schriftzug6 = BezierPath()
    
    if Einsatz == "for o":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-0.25)    
    
    if Einsatz == "standard":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    
    print(x, y)  
    instroke = drawInstroke(*Raute_c, instrokeLen)
    
    
    HSL_size = 2
    HSL_start = 16
    
    E1, E2 = line_E_vonF_o_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug6.arc(*drawKreisSeg(D1, 14, angle_5, angle_6))
    
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug6.arc(*drawKreisSeg(C1, 12, angle_6, angle_7))
    
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug6.arc(*drawKreisSeg(B1, 10, angle_7, angle_8))
        
        
    # Das an den 5. Bestandtheilen Ermangelnde, ist zwischen den bei den Bögen 
    # durch gerade Bestandtheile ersetzt, welches hier aus 2 1/8 Grundbestandtheil 
    # oder 6 1/4 Modul, bestehet, wodurch dieser Schrift-Zug eine Vollkommenheit erreicht. 

    # Raute unten
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)  
    G1, G2 = line_G_vonF_u_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
    A1, A2 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    Schriftzug6.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_9))
    Schriftzug6.arc(*drawKreisSeg(A1, HSL_start-HSL_size*2, angle_9, angle_10))  
    Schriftzug6.arc(*drawKreisSeg(H1, HSL_start-HSL_size, angle_10, angle_11))

    
    outstroke = drawOutstroke(*G2, outstrokeLen)  
    
    Schriftzug6 += instroke + outstroke
    drawPath(Schriftzug6)
    
    return Schriftzug6


# drawSchriftzug6(temp_x,temp_y)       
# drawSchriftzug6(temp_x,temp_y, Einsatz="for o")       





def drawSchriftzug7(x, y, instrokeLen=2, outstrokeLen=2):
    
    Schriftzug7 = BezierPath()
        
    HSL_size = 2    
    HSL_start = 14
            
    # draw Modul + Raute
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-0.5)  

    instroke = drawInstroke(*Raute_d, instrokeLen, "down")

    E1, E2 = line_E_vonF_o_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)
    
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug7.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug7.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug7.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))


    # Das an den 4 Bestandtheilen Ermangelnde istzwischen diesen beiden Bögen 
    # mit 2 1/8 Grundbestandtheile oder 6 1/4 Modul, auch ergänzet. 

    # draw Modul + Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)  

    G1, G2 = line_G_vonF_u_kl_s(Raute_d, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
    A1, A2 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    Schriftzug7.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_9))
    Schriftzug7.arc(*drawKreisSeg(A1, HSL_start-HSL_size*2, angle_9, angle_10))  
    Schriftzug7.arc(*drawKreisSeg(H1, HSL_start-HSL_size, angle_10, angle_11))


    outstroke = drawOutstroke(*G2, outstrokeLen)    
    
    Schriftzug7 += instroke + outstroke
    drawPath(Schriftzug7) 
    return Schriftzug7


#drawSchriftzug7(temp_x, temp_y)







def drawSchriftzug8(x,y, Einsatz="standard", instrokeLen=1, outstrokeLen=1.4):
    
    Schriftzug8 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1, y-5)  

    # C setzt dieser Schrift-Zug mit einem Stück Consructions-Bogen der dritten Hauptschneckenlinie von    # der Richtlinie G mit einem Radio von 8. Part an, und erweitert sich bis 14. Part auf der Richtlinie B.
    # Der untere aber fängt von der Richtlinie B wieder mit 14. Part an, und formiret die
    # Endspitze eines Constructions-Bogens der nehmlichen Hauptschneckenlinie mit 20. Part. 
    # oben: 3. HSL; G>B; 8>14     unten: 3. HSL; B>E; 14>20
    
    instroke = drawInstroke(*Raute_a, instrokeLen)
    

    if Einsatz == "standard":
        # Diesen beiden Bögen darf nur, zu Ergänzung des Schrift-Zugs 3   2/3 Modul eingesetzt werden. 
        bend_top = drawSchneckenzug(*Raute_a, UPPER_G, 3, HSL_size=3, HSL_start=8, clockwise=True, inward=False)
        Element = drawGrundelementF(*bend_top.points[-1], 1.3)
        bend_btm = drawSchneckenzug(*Element.points[-1], LOWER_B, 3, HSL_size=3, HSL_start=14, clockwise=True, inward=False)

   
    if Einsatz == "for o":           
        ### hier sehr verändert damit zu Tab 17 passt
        #helper = drawGrundelementF(*Raute_d, 5)
        drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1, y-5.25)  
        bend_top = drawSchneckenzug(*Raute_a, UPPER_G, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=False)
        Element = drawGrundelementF(*bend_top.points[-1], 1.3)
        bend_btm = drawSchneckenzug(*Element.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=14, clockwise=True, inward=False)
        
        
    if Einsatz == "1":
        bend_top = drawSchneckenzug(*Raute_a, UPPER_G, 3, HSL_size=3, HSL_start=8, clockwise=True, inward=False)
        Element = drawGrundelementF(*bend_top.points[-1], 1)
        bend_btm = drawSchneckenzug(*Element.points[-1], LOWER_B, 3, HSL_size=3, HSL_start=14, clockwise=True, inward=False)
    

    outstroke = drawOutstroke(*bend_btm.points[-1], outstrokeLen, "down")
    
    Schriftzug8 += instroke + bend_top + Element + bend_btm + outstroke
    drawPath(Schriftzug8)
    return Schriftzug8


#drawSchriftzug8(temp_x+2, temp_y)
#drawSchriftzug8(temp_x+2, temp_y, Einsatz="for o", instrokeLen=2, outstrokeLen=0.8)
#drawSchriftzug8(temp_x+2, temp_y, Einsatz="1")






def drawSchriftzug9(x, y, instrokeLen=2, outstrokeLen=0):
    
    Schriftzug9 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    
    # Es ist also der obere Bogen dieses Schrift-Zugs bei dem Hauptbestandtheile C mit einem 
    # Constructions-Bogen-Stück der vierten Hauptschneckenlinie von der Richtlinie G mit 8. Part 
    # angefangen, und mit 2. Bogenmaas zur Richtlinie B gezogen.
    
    HSL_size = 4    HSL_start = 8

    instroke = drawInstroke(*Raute_a, instrokeLen)

    G1, G2 = line_G_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
    Schriftzug9.arc(*drawKreisSeg(G1, 8, angle_3, angle_2, True))  
    Schriftzug9.arc(*drawKreisSeg(H1, 12, angle_2, angle_1, True))  
    Schriftzug9.arc(*drawKreisSeg(A1, 16, angle_1, angle_0, True))  


    # Dieser Schrift-Zug bekommt wegen seiner Länge 7. Modul oder 2 1/3 Grundbestandtheil Einsatz. . 
    Einsatz = 2.333 * modul_height
    B2 = B2[0], B2[1] - Einsatz
    
    # Der untere Bogen aber fängt auf der Richtlinie B mit dem Maase an, womit der obere auhöret, 
    # nehmlich mit 16. Part, und erweitert sich zur Endspitze eines Constructions-Bogens der 
    # vierten Hauptschneckenlinie mit einem 24. Part grössern Radio.
    
    HSL_start = 16

    B3, B4 = line_B_vonA_u_gr_s(B2, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
    Schriftzug9.arc(*drawKreisSeg(B3, HSL_start, angle_16, angle_15, True))  
    Schriftzug9.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_15, angle_14, True))  
    Schriftzug9.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_14, angle_13, True))  
    
    Schriftzug9 += instroke
    
    drawPath(Schriftzug9)
    return Schriftzug9
    
    
    
#drawSchriftzug9(temp_x, temp_y)
 
 
 
 
   


    
    



def drawSchriftzug_x_hook(x, y):
    
    Schriftzug_x_hook = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-0.225) 
    Schriftzug_x_hook = drawSchneckenzug(*Raute_a, UPPER_E, 15, HSL_size=1, HSL_start=18, clockwise=False, inward=True)
        
    drawPath(Schriftzug_x_hook) 
    return Schriftzug_x_hook
    
    
    
#drawSchriftzug_x_hook(temp_x, temp_y)




def drawDieresis(x, y):
    
    Dieresis = BezierPath() 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2) 
    links = drawSchriftteil8(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y+2)
    rechts = drawSchriftteil8(*Raute_a)

    Dieresis = links + rechts
    drawPath(Dieresis) 
    return Dieresis
    
#drawDieresis(temp_x, temp_y)











##################################################################################
###            Ab hier Satzzeichen
##################################################################################






def drawSchriftzug_semicolon_btm(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x, y-0.5) 

    Schriftzug_semicolon_btm = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=0.5, HSL_start=3.15, clockwise=True, inward=False)

    drawPath(Schriftzug_semicolon_btm)
    return Schriftzug_semicolon_btm
    
    
#drawSchriftzug_semicolon_btm(temp_x, temp_y)  







def drawSchriftzug_question(x, y):
    
    Schriftzug_question = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25)
    drawGrundelOrient(A1, A2, offset, x-0.5, y+3)
    drawGrundelOrient(A1, A2, offset, x, y-3.75)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)   
    oben = drawSchneckenzug(*Grund_a, LOWER_E, 8, HSL_size=1.125, HSL_start=14.85, clockwise=False, inward=True)
    unten = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=1.125, HSL_start=18, clockwise=False, inward=True)

    Schriftzug_question = oben + unten
    drawPath(Schriftzug_question)
    return Schriftzug_question
    
#drawSchriftzug_question(temp_x, temp_y)









##################################################################################
###            Ab hier Zahlen
##################################################################################






def drawSchriftzug3_Figures(x, y, instrokeLen=0.5, outstrokeLen=0.5):
    
    ### dieser Schriftzug ist wie 3, nur dass er 8 hoch ist, so hoch wie Zahlen/Ziffern
    
    Schriftzug3_fullHeight = BezierPath()   
        
        
    # TOP
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2) 

    Rundung_oben = drawSchriftteil3(*Raute_a)
    stroke_start = Rundung_oben.points[-1]

    instroke = drawInstroke(*Raute_a, instrokeLen)



    # BOTTOM 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)    
        
    Rundung_unten = drawSchriftteil4(*Raute_d)
    stroke_end = Rundung_unten.points[-1]
    
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
    
    Schriftzug3_Figures = instroke + Rundung_oben + Rundung_unten + outstroke

    Schriftzug3_Figures.line(stroke_start, stroke_end)

    drawPath(Schriftzug3_Figures)
    return Schriftzug3_Figures
    
    
#drawSchriftzug3_Figures(temp_x, temp_y)
    
    
    
    
    
    
def drawSchriftzug_two_Bogen(x, y, instrokeLen=0, outstrokeLen=2.15):
    
    ### dieser Schriftzug ist wie 3, nur dass er 8 hoch ist, so hoch wie Zahlen/Ziffern
    
    Schriftzug_two_Bogen = BezierPath()   
           
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x+1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+1, y-3.5)
    drawGrundelOrient(A1, A2, offset, x-1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-4.5)

    instroke = drawInstroke(*Grund_a, instrokeLen)
    Bogen = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=2, HSL_start=12.6, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Bogen.points[-1], outstrokeLen, "down")
        
    Schriftzug_two_Bogen += instroke + Bogen + outstroke
    drawPath(Schriftzug_two_Bogen)
    return Schriftzug_two_Bogen
    
    
#drawSchriftzug_two_Bogen(temp_x, temp_y)




def drawSchriftzug_two_Schwung(x, y, instrokeLen=0.25):
    
    Schriftzug_two_Schwung = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-4.5)

    instroke = drawInstroke(*Grund_a, instrokeLen, "down")


    ### Teil 1 – links
    HSL_size = 1
    HSL_start = 4.5
        
    E1, E2 = line_E_vonD_o_gr_s(instroke.points[-1], *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftzug_two_Schwung.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))



    ### Teil 2 – rechts
    HSL_size = 2.25
    HSL_start = 10.25
        
    A5, A6 = line_A_vonB_u_gr_s(A4, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E3, E4 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_two_Schwung.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))


        
    Schriftzug_two_Schwung += instroke

    drawPath(Schriftzug_two_Schwung)
    return Schriftzug_two_Schwung
    
    
#drawSchriftzug_two_Schwung(temp_x, temp_y)






def drawSchriftzug_three_Bogen(x, y):
        
    Schriftzug_three_Bogen = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y)
    

    HSL_size = 2
    HSL_start = 12.6
        
    E1, E2 = line_E_vonD_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    E3, E4 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)

    Schriftzug_three_Bogen.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_15, angle_14, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_14, angle_13, True))


    drawPath(Schriftzug_three_Bogen)
    return Schriftzug_three_Bogen

#drawSchriftzug_three_Bogen(temp_x, temp_y)






def drawSchriftzug_three_Top(x, y, instrokeLen=1):
        
    Schriftzug_three_Top = BezierPath()   
    
    drawGrundelOrient(A1, A2, offset, x+4, y+2)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    
    instroke = drawInstroke(*Grund_a, instrokeLen)
    top = drawSchneckenzug(*Grund_a, LOWER_G, 2, HSL_size=0, HSL_start=32, clockwise=False, inward=False)

    Schriftzug_three_Top += instroke + top
    drawPath(Schriftzug_three_Top)
    return Schriftzug_three_Top

#drawSchriftzug_three_Top(temp_x, temp_y)







def drawSchriftzug_five_Top(x, y):
        
    Schriftzug_five_Top = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+5, y+2)  
    drawGrundelOrient(A1, A2, offset, x+1, y+2)
    
    
    HSL_size = 1
    HSL_start = 12
   
    A5, A6 = line_A_vonB_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E3, E4 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug_five_Top.arc(*drawKreisSeg(A5, HSL_start, angle_9, angle_10))
    Schriftzug_five_Top.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftzug_five_Top.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftzug_five_Top.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))


    drawPath(Schriftzug_five_Top)
    return Schriftzug_five_Top

#drawSchriftzug_five_Top(temp_x, temp_y)










def drawSchriftzug_zero_BogenLinks(x, y):
        
    Schriftzug_zero_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.75)
    

    HSL_size = 24
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))



    drawPath(Schriftzug_zero_BogenLinks)
    return Schriftzug_zero_BogenLinks

#drawSchriftzug_zero_BogenLinks(temp_x, temp_y)





def drawSchriftzug_zero_BogenRechts(x, y, version="zero"):
        
    Schriftzug_zero_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.75)
    

    HSL_size = 24
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))

    if version == "zero":
    
        Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_13, angle_11, True))


    drawPath(Schriftzug_zero_BogenRechts)
    return Schriftzug_zero_BogenRechts

# drawSchriftzug_zero_BogenRechts(temp_x, temp_y, version="zero")
#drawSchriftzug_zero_BogenRechts(temp_x, temp_y, version="nine")




def drawSchriftzug_six_BogenRechts(x, y):
        
    Schriftzug_six_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y-0.75)
    

    HSL_size = 4
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    #G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))


    drawPath(Schriftzug_six_BogenRechts)
    return Schriftzug_six_BogenRechts

#drawSchriftzug_six_BogenRechts(temp_x, temp_y)






def drawSchriftzug_nine_BogenLinks(x, y):
        
    Schriftzug_nine_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.75)
    

    HSL_size = 4
    HSL_start = 16

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))



    drawPath(Schriftzug_nine_BogenLinks)
    return Schriftzug_nine_BogenLinks

#drawSchriftzug_nine_BogenLinks(temp_x, temp_y)





def drawSchriftzug_eight(x, y):
        
    Schriftzug_eight_BogenMiddle = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+4, y-4.5)  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    

    HSL_size = 0
    HSL_start = 13 

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonH_u_kl(E1, *angles, part, HSL_size, HSL_start)

    straight_middle = drawGrundelementC(*G2, length=1.625)
    
    G3, G4 = line_G_vonF_o_gr_s(straight_middle.points[-1], *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonD_u_gr(G3, *angles, part, HSL_size, HSL_start)
    
    Schriftzug_eight_BogenMiddle.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_11))
    Schriftzug_eight_BogenMiddle.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_13, True))



    ### Teil rechts oben
    Schriftzug_eight_BogenRechtsOben = BezierPath()   

    HSL_size = 0
    HSL_start = 13 

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonD_u_kl(E1, *angles, part, HSL_size, HSL_start)
    
    Schriftzug_eight_BogenRechtsOben.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_13, True))
    
    straight_middle2 = drawGrundelementA(*E4, 1.625, "down")



    ### Teil links unten
    HSL_size = 0
    HSL_start = 13

    E5, E6 = line_E_vonF_o_gr_s(straight_middle2.points[-1], *angles, part, HSL_size, HSL_start)
    E7, E8 = line_E_vonF_u_kl(E5, *angles, part, HSL_size, HSL_start)
    
    Schriftzug_eight_BogenRechtsOben.arc(*drawKreisSeg(E5, HSL_start, angle_5, angle_13))


    Schriftzug_eight = Schriftzug_eight_BogenMiddle + Schriftzug_eight_BogenRechtsOben
    drawPath(Schriftzug_eight)

    return Schriftzug_eight

#drawSchriftzug_eight(temp_x, temp_y)






    
    
# # # def drawSchriftzug_one_AuslaufEndpunkt(x, y, instrokeLen=0.5, outstrokeLen=0.5):
    
# # #     ### dieser Schriftzug ist wie 3, nur dass er 8 hoch ist, so hoch wie Zahlen/Ziffern
# # #     ### aber für was habe ich ihn entworfen?
        
    
# # #     Schriftzug3_fullHeight = BezierPath()   
        
        
# # #     # TOP
# # #     Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2) 

# # #     Rundung_oben = drawSchriftteil3(*Raute_a)
# # #     stroke_start = Rundung_oben.points[-1]

# # #     instroke = drawInstroke(*Raute_a, instrokeLen)



# # #     # BOTTOM 
# # #     Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)    
        
# # #     Rundung_unten = drawSchriftteil4(*Raute_d)
# # #     stroke_end = Rundung_unten.points[-1]
    
# # #     outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
    
# # #     Schriftzug3_Figures = instroke + Rundung_oben + Rundung_unten + outstroke

# # #     Schriftzug3_Figures.line(stroke_start, stroke_end)

# # #     drawPath(Schriftzug3_Figures)
# # #     return Schriftzug3_Figures
    
    
# # # #drawSchriftzug3_Figures(temp_x, temp_y)







##################################################################################
###            Ab hier Versalien
##################################################################################





def drawSchriftzug_BG_Versalien(x, y):
    ### Hintergrund
    
    ### Reihe unten            
    drawGrundelOrientMittig(A1, A2, offset, x, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)  # für J
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-5) 
     
    ### Reihe mitte (x-Höhe)
    drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+3, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+6, y) 
    
    ### Element oben
    drawGrundelOrientMittig(A1, A2, offset, x+3, y+3) 
    
    ### 1. Extra für M, W und breite Glyphen
    drawGrundelOrientMittig(A1, A2, offset, x+9, y-5) #unten
    drawGrundelOrientMittig(A1, A2, offset, x+9, y) #mitte
    drawGrundelOrientMittig(A1, A2, offset, x+9, y+3)  #oben 

    ### Extra für M, W und breite Glyphen
    drawGrundelOrientMittig(A1, A2, offset, x+12, y-5) #unten
    drawGrundelOrientMittig(A1, A2, offset, x+12, y) #mitte
    drawGrundelOrientMittig(A1, A2, offset, x+12, y+3)  #oben 

drawSchriftzug_BG_Versalien(temp_x, temp_y)










def drawSchriftzug_C_stehenderSchwung(x, y):
        
    Schriftzug_C_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y-1) 


    oben = drawSchneckenzug(*Grund_a, LOWER_H, 5, HSL_size=1, HSL_start=17, clockwise=True, inward=True)
    instroke = drawInstroke(*oben.points[-1], 1.25, "down")
    Einsatz = drawGrundelementC(*Grund_a, 0, "unten")
    unten = drawSchneckenzug(*Einsatz.points[-1], UPPER_H, 5, HSL_size=1, HSL_start=12, clockwise=True, inward=True)
    outstroke = drawOutstroke(*unten.points[-1], 0.75, "down")
    
    Schriftzug_C_stehenderSchwung += oben + instroke + Einsatz + unten + outstroke
    drawPath(Schriftzug_C_stehenderSchwung)

    return Schriftzug_C_stehenderSchwung

#drawSchriftzug_C_stehenderSchwung(temp_x, temp_y)






def drawSchriftzug_C_liegenderSchwung(x, y):
        
    Schriftzug_C_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, baseline+0.25) 

    links = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=2, HSL_start=14, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*Grund_a, 1)
    right = drawSchneckenzug(*Einsatz.points[-1], LOWER_G, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)
    outstroke = drawOutstroke(*right.points[-1], 1)

    Schriftzug_C_liegenderSchwung +=  links + Einsatz + right + outstroke
    drawPath(Schriftzug_C_liegenderSchwung)
    return Schriftzug_C_liegenderSchwung

#drawSchriftzug_C_liegenderSchwung(temp_x, temp_y)










def drawSchriftzug_E_stehenderSchwung(x, y):
        
    Schriftzug_E_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y-1) 


    oben = drawSchneckenzug(*Grund_a, LOWER_H, 5, HSL_size=1, HSL_start=14.5, clockwise=True, inward=True)
    instroke = drawInstroke(*oben.points[-1], 1.5, "down")
    Einsatz = drawGrundelementC(*Grund_a, 0, "unten")
    unten = drawSchneckenzug(*Einsatz.points[-1], UPPER_H, 5, HSL_size=1, HSL_start=12, clockwise=True, inward=True)
    outstroke = drawOutstroke(*unten.points[-1], 0.75, "down")
    
    Schriftzug_E_stehenderSchwung += oben + instroke + Einsatz + unten + outstroke
    drawPath(Schriftzug_E_stehenderSchwung)

    return Schriftzug_E_stehenderSchwung

#drawSchriftzug_E_stehenderSchwung(temp_x, temp_y)






def drawSchriftzug_E_liegenderSchwung(x, y):
        
    Schriftzug_E_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, baseline+0.25) 

    links = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=2, HSL_start=14, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*Grund_a, 1)
    right = drawSchneckenzug(*Einsatz.points[-1], LOWER_G, 2, HSL_size=2, HSL_start=23.5, clockwise=False, inward=False)
    outstroke = drawOutstroke(*right.points[-1], 0.75)

    Schriftzug_E_liegenderSchwung +=  links + Einsatz + right + outstroke
    drawPath(Schriftzug_E_liegenderSchwung)
    return Schriftzug_E_liegenderSchwung

#drawSchriftzug_E_liegenderSchwung(temp_x, temp_y)












def drawSchriftzug_F_stehenderSchwung(x, y):
        
    Schriftzug_F_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25) 

    Einsatz = drawGrundelementC(*Grund_a, 0.5)


    ### stehenderSchwung OBEN
    HSL_size = 1.25
    HSL_start = 5.5
   
    G1, G2 = line_G_vonF_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    instroke = drawInstroke(*E2, 0.5, "down")


    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_9, angle_10))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_10, angle_11))



    ### stehenderSchwung UNTEN
    HSL_size = 4
    HSL_start = 10

    G3, G4 = line_G_vonF_o_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    A5, A6 = line_A_vonH_o_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C3, C4 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D3, D4 = line_D_vonC_u_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E3, E4 = line_E_vonD_u_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_2, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_F_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start+HSL_size*5, angle_14, angle_13, True))

    #outstroke = drawOutstroke(*E4, 1, "down")

    Schriftzug_F_stehenderSchwung += instroke + Einsatz #+ outstroke
    drawPath(Schriftzug_F_stehenderSchwung)

    return Schriftzug_F_stehenderSchwung

#drawSchriftzug_F_stehenderSchwung(temp_x, temp_y)







def drawSchriftzug_F_liegenderSchwung(x, y):
        
    Schriftzug_F_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+2, y+3.25) 

    ### Eigentlich steht bei Roßberg Schriftteil 9, daber der scheint irgendwie zu klein
    #left = drawSchriftteil9(*Grund_a)
    
    
    HSL_size = 2
    HSL_start = 14
          
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_4))
    
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug_F_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_5))
    
    
    
    Einsatz = drawGrundelementC(*Grund_a, 0)
    right = drawSchriftteil10(*Einsatz.points[-1])

    Schriftzug_F_liegenderSchwung += Einsatz + right

    drawPath(Schriftzug_F_liegenderSchwung)

    return Schriftzug_F_liegenderSchwung

#drawSchriftzug_F_liegenderSchwung(temp_x, temp_y)












def drawSchriftzug_I_stehenderSchwung(x, y):
        
    Schriftzug_I_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+0.25) 

    
    ### stehenderSchwung OBEN
    HSL_size = 1
    HSL_start = 4.5
   
    G1, G2 = line_G_vonF_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_9, angle_10))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_10, angle_11))



    ### stehenderSchwung UNTEN
    HSL_size = 2
    HSL_start = 16

    G3, G4 = line_G_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    A5, A6 = line_A_vonH_o_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C3, C4 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D3, D4 = line_D_vonC_u_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E3, E4 = line_E_vonD_u_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_2, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_I_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start+HSL_size*5, angle_14, angle_13, True))

    
    drawPath(Schriftzug_I_stehenderSchwung)

    return Schriftzug_I_stehenderSchwung

#drawSchriftzug_I_stehenderSchwung(temp_x, temp_y)







def drawSchriftzug_I_liegenderSchwung(x, y):
        
    Schriftzug_I_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3.5, y+3) 

    ### Eigentlich steht bei Roßberg Schriftteil 9, daber der scheint irgendwie zu klein
    #left = drawSchriftteil9(*Grund_a)
    
    links = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*Grund_a, 0.5)
    right = drawSchriftteil10(*Einsatz.points[-1])

    Schriftzug_I_liegenderSchwung += links + Einsatz + right
    drawPath(Schriftzug_I_liegenderSchwung)
    return Schriftzug_I_liegenderSchwung

#drawSchriftzug_I_liegenderSchwung(temp_x, temp_y)






def drawSchriftzug_J_stehenderSchwung(x, y):
        
    Schriftzug_J_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.25) 

    
    ### stehenderSchwung OBEN
    HSL_size = 1
    HSL_start = 6.5
   
    G1, G2 = line_G_vonF_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_9, angle_10))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_10, angle_11))



    ### stehenderSchwung UNTEN
    HSL_size = 4
    HSL_start = 18

    G3, G4 = line_G_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    A5, A6 = line_A_vonH_o_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C3, C4 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D3, D4 = line_D_vonC_u_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E3, E4 = line_E_vonD_u_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_2, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_J_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start+HSL_size*5, angle_14, angle_13, True))

    
    drawPath(Schriftzug_J_stehenderSchwung)

    return Schriftzug_J_stehenderSchwung

#drawSchriftzug_J_stehenderSchwung(temp_x, temp_y)







def drawSchriftzug_J_liegenderSchwung(x, y):
        
    Schriftzug_J_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.5, y+3) 

    ### Eigentlich steht bei Roßberg Schriftteil 9, daber der scheint irgendwie zu klein
    #left = drawSchriftteil9(*Grund_a)
    
    
    HSL_size = 1
    HSL_start = 32
          
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    
    Einsatz = drawGrundelementC(*Grund_a, 0.75)



    HSL_size = 1
    HSL_start = 22 
    
    G1, G2 = line_G_vonF_u_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug_J_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))



    Schriftzug_J_liegenderSchwung += Einsatz

    drawPath(Schriftzug_J_liegenderSchwung)

    return Schriftzug_J_liegenderSchwung

#drawSchriftzug_J_liegenderSchwung(temp_x, temp_y)







def drawSchriftzug_L_stehenderSchwung(x, y):
        
    Schriftzug_L_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1.5) 

    Einsatz = drawGrundelementC(*Grund_a, 0, "unten")


    ### stehenderSchwung OBEN
    HSL_size = 2
    HSL_start = 24.5
   
    H1, H2 = line_H_vonG_u_kl_s(Einsatz.points[0], *angles, part, HSL_size, HSL_start)
    A3, A4 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size)
    B1, B2 = line_B_vonA_o_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    instroke = drawInstroke(*E2, 0.1, "down")

    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(E1, HSL_start-HSL_size*5, angle_5, angle_6))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start-HSL_size*4, angle_6, angle_7))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_8, angle_9))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start-HSL_size*1, angle_9, angle_10))




    ### stehenderSchwung UNTEN
    HSL_size = 1
    HSL_start = 14.5

    H3, H4 = line_H_vonG_o_kl_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*2)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start-HSL_size, angle_2, angle_1, True))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*2, angle_1, angle_0, True))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*3, angle_16, angle_15, True))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*4, angle_15, angle_14, True))
    Schriftzug_L_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*5, angle_14, angle_13, True))

    Schriftzug_L_stehenderSchwung += instroke + Einsatz
    drawPath(Schriftzug_L_stehenderSchwung)

    return Schriftzug_L_stehenderSchwung

#drawSchriftzug_L_stehenderSchwung(temp_x, temp_y)






def drawSchriftzug_T_stehenderSchwung(x, y):
        
    Schriftzug_T_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3, y-1.5) 

    Einsatz = drawGrundelementC(*Grund_a, length=0)
    
    ### stehenderSchwung OBEN
    HSL_size = 1.5
    HSL_start = 8
   
    H1, H2 = line_H_vonG_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    A3, A4 = line_A_vonB_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
    instroke = drawInstroke(*E2, 0.5, "down")


    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*4, angle_5, angle_6))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*3, angle_6, angle_7))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*2, angle_7, angle_8))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_8, angle_9))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_10))




    ### stehenderSchwung UNTEN
    HSL_size = 1
    HSL_start = 14

    H3, H4 = line_H_vonG_o_kl_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*2)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start-HSL_size, angle_2, angle_1, True))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*2, angle_1, angle_0, True))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*3, angle_16, angle_15, True))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*4, angle_15, angle_14, True))
    Schriftzug_T_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*5, angle_14, angle_13, True))

    outstroke = drawInstroke(*E4, 0.25)

    Schriftzug_T_stehenderSchwung += instroke + outstroke

    drawPath(Schriftzug_T_stehenderSchwung)

    return Schriftzug_T_stehenderSchwung

#drawSchriftzug_T_stehenderSchwung(temp_x, temp_y)





def drawSchriftzug_T_liegenderSchwung(x, y):
        
    Schriftzug_T_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4, y+2.75) 
    
    
    HSL_size = 2
    HSL_start = 17
          
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_T_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_4))
    
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug_T_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_5))
    

    
    Einsatz = drawGrundelementC(*Grund_a, 1)
    right = drawSchriftteil10(*Einsatz.points[-1])

    Schriftzug_T_liegenderSchwung += Einsatz + right

    drawPath(Schriftzug_T_liegenderSchwung)

    return Schriftzug_T_liegenderSchwung

#drawSchriftzug_T_liegenderSchwung(temp_x, temp_y)









def drawSchriftzug_S_stehenderSchwung(x, y):
        
    Schriftzug_S_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3.5, y-1.25) 

    Einsatz = drawGrundelementC(*Grund_a, length=0)
    
    ### stehenderSchwung OBEN
    HSL_size = 1
    HSL_start = 12
    
    G1, G2 = line_G_vonF_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)

    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_9, angle_10))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_10, angle_11))

    instroke = drawOutstroke(*E2, 0.5)
    outstroke = drawOutstroke(*Grund_a, 2)


    ### stehenderSchwung UNTEN
    HSL_size = 4
    HSL_start = 10

    G3, G4 = line_G_vonF_o_gr_s(outstroke.points[0], *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    A5, A6 = line_A_vonH_o_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C3, C4 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D3, D4 = line_D_vonC_u_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E3, E4 = line_E_vonD_u_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_2, True))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_S_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start+HSL_size*5, angle_14, angle_13, True))


    Schriftzug_S_stehenderSchwung += instroke + outstroke

    drawPath(Schriftzug_S_stehenderSchwung)

    return Schriftzug_S_stehenderSchwung

#drawSchriftzug_S_stehenderSchwung(temp_x, temp_y)







def drawSchriftzug_S_liegenderSchwung(x, y, outstrokeLen=0.5):
        
    Schriftzug_S_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.75, y-5.09) 
    
    
    HSL_size = 1
    HSL_start = 11.5

    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))
    
    Einsatz = drawGrundelementC(*Grund_a, 0.5)



    # Wendung
    G3, G4 = line_G_vonH_u_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_S_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    
    outstroke = drawOutstroke(*E2, outstrokeLen)
    
    Schriftzug_S_liegenderSchwung += Einsatz + outstroke

    drawPath(Schriftzug_S_liegenderSchwung)

    return Schriftzug_S_liegenderSchwung

#drawSchriftzug_S_liegenderSchwung(temp_x, temp_y)










def drawSchriftzug_K_stehenderSchwung(x, y):
        
    Schriftzug_K_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1) 
    
    Einsatz1 = drawGrundelementD(*Grund_a, 0.5, "unten")
    Einsatz2 = drawGrundelementD(*Grund_a, 0.5)
    Schwung_oben = drawSchneckenzug(*Einsatz1.points[-1] , LOWER_H, 5, HSL_size=1, HSL_start=13, clockwise=True, inward=False)
    Schwung_unten = drawSchneckenzug(*Einsatz2.points[4] , UPPER_H, 5, HSL_size=2, HSL_start=11, clockwise=True, inward=False)
     
    instroke = drawInstroke(*Schwung_oben.points[-1], 1, "down")
    outstroke = drawOutstroke(*Schwung_unten.points[-1], 1, "down")
    
    Schriftzug_K_stehenderSchwung = Schwung_oben + Schwung_unten + instroke + outstroke + Einsatz1 + Einsatz2
    drawPath(Schriftzug_K_stehenderSchwung)
    return Schriftzug_K_stehenderSchwung

#drawSchriftzug_K_stehenderSchwung(temp_x, temp_y)






def drawSchriftzug_B_Hauptbogen(x, y):
        
    Schriftzug_B_Hauptbogen = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2, y+2) 
    Hauptbogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=2, HSL_start=32.5, clockwise=True, inward=True)
    Hauptbogen_unten = drawSchneckenzug(*Hauptbogen_oben.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=26, clockwise=True, inward=True)
    
    Schriftzug_B_Hauptbogen = Hauptbogen_oben + Hauptbogen_unten
    drawPath(Schriftzug_B_Hauptbogen)
    return Schriftzug_B_Hauptbogen

#drawSchriftzug_B_Hauptbogen(temp_x, temp_y)





    
    
    
####### --- ALT BUT KEEP #####
def drawSchriftzug_B_Bauch_VORHER(x, y):
        
    Schriftzug_B_Bauch = BezierPath()   
    

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.25, y-0.5) 


    HSL_size = 2
    HSL_start = 13
                   
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug_B_Bauch.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_B_Bauch.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))


    ### stehenderSchwung UNTEN
    HSL_size = 3
    HSL_start = 9.6

    G3, G4 = line_G_vonF_o_gr_s(G2, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    A5, A6 = line_A_vonH_o_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C3, C4 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D3, D4 = line_D_vonC_u_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E3, E4 = line_E_vonD_u_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_B_Bauch.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_2, True))
    Schriftzug_B_Bauch.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_B_Bauch.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_B_Bauch.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_B_Bauch.arc(*drawKreisSeg(C3, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_B_Bauch.arc(*drawKreisSeg(D3, HSL_start+HSL_size*5, angle_14, angle_13, True))
    
    outstroke = drawOutstroke(*E4, 1.95, "down")

    Schriftzug_B_Bauch += outstroke
    drawPath(Schriftzug_B_Bauch)

    return Schriftzug_B_Bauch
    
    
#drawSchriftzug_B_Bauch(temp_x, temp_y)





def drawSchriftzug_B_Bauch(x, y):
        
    Schriftzug_B_Bauch = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.25, y-0.375) 
    Anfang = drawSchriftteil9(*Grund_a)
    Weiter = drawSchneckenzug(*Anfang.points[0], UPPER_G, 3, HSL_size=1, HSL_start=10, clockwise=True, inward=False)

    Einsatz= drawGrundelementF(*Weiter.points[-1], 0.25)
    Ende = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=21.5, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Ende.points[-1], 1.7, "down")

    Schriftzug_B_Bauch += Anfang + Weiter + Einsatz + Ende + outstroke
    drawPath(Schriftzug_B_Bauch)
    return Schriftzug_B_Bauch

#drawSchriftzug_B_Bauch(temp_x, temp_y)







def drawSchriftzug_G_Hauptbogen(x, y):
        
    Schriftzug_G_Hauptbogen = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2.5, y+2.25) 
    
    HSL_size = 2
    HSL_start = 35
        
    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*8)
        
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_2, angle_1, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_1, angle_0, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(C1, HSL_start-HSL_size*6, angle_16, angle_15, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(D1, HSL_start-HSL_size*7, angle_15, angle_14, True))
    Schriftzug_G_Hauptbogen.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_14, angle_13, True))
    

    drawPath(Schriftzug_G_Hauptbogen)

    return Schriftzug_G_Hauptbogen

#drawSchriftzug_G_Hauptbogen(temp_x, temp_y)







def drawSchriftzug_G_Deckung(x, y):
        
    Schriftzug_G_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.5, y-3.75) 

    
    # fast wie Schriftteil 9
    HSL_size = 2
    HSL_start = 8
          
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug_G_Deckung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    ### zweiter Teil
    HSL_size = 0.5
    HSL_start = 5
    
    G3, G4 = line_G_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*3)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    
    Schriftzug_G_Deckung.arc(*drawKreisSeg(H3, HSL_start-HSL_size, angle_3, angle_2, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(A5, HSL_start-HSL_size*2, angle_2, angle_1, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*3, angle_1, angle_0, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*4, angle_16, angle_15, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*5, angle_15, angle_14, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*6, angle_14, angle_13, True))
    


    drawPath(Schriftzug_G_Deckung)

    return Schriftzug_G_Deckung

#drawSchriftzug_G_Deckung(temp_x, temp_y)








def drawSchriftzug_G_Fuss(x, y):
        
    Schriftzug_G_Fuss = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.4, y-4.25) 
 
    HSL_size = 1
    HSL_start = 20
          
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug_G_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_G_Fuss.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    Einsatz = drawGrundelementC(*Grund_a, 1.5)


    HSL_size = 1
    HSL_start = 26
    
    G1, G2 = line_G_vonF_u_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_G_Fuss.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug_G_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    outstroke = drawOutstroke(*E2, 1)


    drawPath(Schriftzug_G_Fuss)

    return Schriftzug_G_Fuss
    
    
#drawSchriftzug_G_Fuss(temp_x, temp_y)





    
    
    
def drawSchriftzug_G_Bauch(x, y):
        
    Schriftzug_G_Bauch = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.5, y+0.25) 


    HSL_size = 2
    HSL_start = 13
                   
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug_G_Bauch.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))


    ### stehenderSchwung UNTEN
    HSL_size = 4
    HSL_start = 10

    G3, G4 = line_G_vonF_o_gr_s(G2, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    A5, A6 = line_A_vonH_o_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C3, C4 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D3, D4 = line_D_vonC_u_gr(C3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E3, E4 = line_E_vonD_u_gr(D3, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    Schriftzug_G_Bauch.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_2, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(B3, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(C3, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_G_Bauch.arc(*drawKreisSeg(D3, HSL_start+HSL_size*5, angle_14, angle_13, True))
    
    outstroke = drawOutstroke(*E4, 1, "down")

  
    drawPath(Schriftzug_G_Bauch)

    return Schriftzug_G_Bauch 
    
#drawSchriftzug_G_Bauch(temp_x, temp_y)

    
    
    

def drawSchriftzug_G_stehenderSchwung(x, y, instrokeLen=0.5):
        
    Schriftzug_G_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y) 


    
    Einsatz = drawGrundelementC(*Grund_a, length=0)
    
    ### stehenderSchwung OBEN
    HSL_size = 1
    HSL_start = 6
   
    G1, G2 = line_G_vonF_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    
    instroke = drawInstroke(*E2, instrokeLen, "down")
    
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_9, angle_10))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_10, angle_11))




    ### stehenderSchwung UNTEN
    HSL_size = 1
    HSL_start = 15

    G3, G4 = line_G_vonF_o_kl_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*3)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(H3, HSL_start-HSL_size, angle_3, angle_2, True))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start-HSL_size*2, angle_2, angle_1, True))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*3, angle_1, angle_0, True))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*4, angle_16, angle_15, True))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*5, angle_15, angle_14, True))
    Schriftzug_G_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*6, angle_14, angle_13, True))

    Schriftzug_G_stehenderSchwung += instroke

    drawPath(Schriftzug_G_stehenderSchwung)

    return Schriftzug_G_stehenderSchwung

#drawSchriftzug_G_stehenderSchwung(temp_x, temp_y)

   







def drawSchriftzug_R_Hauptbogen(x, y):
        
    Schriftzug_R_Hauptbogen = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2, y+2) 
    
    Hauptbogen_top = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=30, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*Hauptbogen_top.points[-1], 1)
    Hauptbogen_btm = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=24.5, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Hauptbogen_btm.points[-1], 0.35, "down")
    
    Schriftzug_R_Hauptbogen += Hauptbogen_top + Einsatz + Hauptbogen_btm + outstroke
    drawPath(Schriftzug_R_Hauptbogen)

    return Schriftzug_R_Hauptbogen

#drawSchriftzug_R_Hauptbogen(temp_x, temp_y)






def drawSchriftzug_R_Deckung(x, y):
        
    Schriftzug_R_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-5.5) 

    
    # fast wie Schriftteil 9
    HSL_size = 2
    HSL_start = 14
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug_R_Deckung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    ### zweiter Teil
    HSL_size = 0.5
    HSL_start = 6
    
    G3, G4 = line_G_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*3)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    
    Schriftzug_R_Deckung.arc(*drawKreisSeg(H3, HSL_start-HSL_size, angle_3, angle_2, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(A5, HSL_start-HSL_size*2, angle_2, angle_1, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*3, angle_1, angle_0, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*4, angle_16, angle_15, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*5, angle_15, angle_14, True))
    Schriftzug_R_Deckung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*6, angle_14, angle_13, True))
    


    drawPath(Schriftzug_R_Deckung)

    return Schriftzug_R_Deckung

#drawSchriftzug_R_Deckung(temp_x, temp_y)








def drawSchriftzug_R_Fuss(x, y):
        
    Schriftzug_R_Fuss = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4.25, y-4.825) 
 
    HSL_size = 1
    HSL_start = 11.5

    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
    Schriftzug_R_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_R_Fuss.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))
    
    Einsatz = drawGrundelementC(*Grund_a, 1)



    # Wendung
    G3, G4 = line_G_vonH_u_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_R_Fuss.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_R_Fuss.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    
    outstroke = drawOutstroke(*E2, 0.7)   


    drawPath(Schriftzug_R_Fuss)

    return Schriftzug_R_Fuss
    
    
#drawSchriftzug_R_Fuss(temp_x, temp_y)





   



def drawSchriftzug_X_BogenRechts(x, y):
    
    Schriftzug_X_BogenRechts = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6, y+3) 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3, y-2.5) 
    
    Mitte_nachUnten = drawSchneckenzug(*Grund_a, UPPER_B, 5, HSL_size=2, HSL_start=27, clockwise=False, inward=True)
    Einsatz = drawGrundelementF(*Grund_a, 0.25, "up")
    Mitte_nachOben = drawSchneckenzug(*Einsatz.points[-1], UPPER_B, 3, HSL_size=12, HSL_start=61, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Mitte_nachOben.points[-1], 0.25)
    
    Schriftzug_X_BogenRechts += Mitte_nachUnten + Einsatz + Mitte_nachOben + outstroke
    drawPath(Schriftzug_X_BogenRechts)
    return Schriftzug_X_BogenRechts

#drawSchriftzug_X_BogenRechts(temp_x, temp_y)






  
 
    
def drawSchriftzug_X_DeckungOben(x, y):
        
    Schriftzug_X_DeckungOben = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6, y+3.5)    
    

    # Wendepunkt ab Raute_b nach links
    HSL_size = 0.5
    HSL_start = 1
        
    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))



    HSL_size = 1
    HSL_start = 14
    
    G3, G4 = line_G_vonH_u_gr_s(G2, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_X_DeckungOben.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))


    outstroke = drawOutstroke(*E2, 0.25)
    

    drawPath(Schriftzug_X_DeckungOben)

    return Schriftzug_X_DeckungOben

#drawSchriftzug_X_DeckungOben(temp_x, temp_y)








def drawSchriftzug_O_stehenderSchwung(x, y):
        
    Schriftzug_O_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-0.75) 
    
    Einsatz = drawGrundelementD(*Grund_a, length=0.5)


    ### stehenderSchwung OBEN
    HSL_size = 1
    HSL_start = 10.25
    
    H1, H2 = line_H_vonG_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)

    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_5, angle_6))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_6, angle_7))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_7, angle_8))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_8, angle_9))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_9, angle_10))

    instroke = drawInstroke(*E2, 0.5, "down")


    ### stehenderSchwung UNTEN
    HSL_size = 1
    HSL_start = 14

    H3, H4 = line_H_vonG_o_kl_s(Einsatz.points[4], *angles, part, HSL_size, HSL_start)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*2)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*3)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(A5, HSL_start-HSL_size, angle_2, angle_1, True))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*2, angle_1, angle_0, True))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*3, angle_16, angle_15, True))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*4, angle_15, angle_14, True))
    Schriftzug_O_stehenderSchwung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*5, angle_14, angle_13, True))
    

    Schriftzug_O_stehenderSchwung += instroke + Einsatz

    drawPath(Schriftzug_O_stehenderSchwung)

    return Schriftzug_O_stehenderSchwung

#drawSchriftzug_O_stehenderSchwung(temp_x, temp_y)




def drawSchriftzug_O_liegenderSchwung(x, y):
        
    Schriftzug_O_liegenderSchwung = BezierPath()   
            
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-5.09) 
    
    
    HSL_size = 1
    HSL_start = 11.5

    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))
    
    Einsatz = drawGrundelementC(*Grund_a, 0.5)



    # Wendung
    G3, G4 = line_G_vonH_u_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_O_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    
    outstroke = drawOutstroke(*E2, 0.7)
    

    Schriftzug_O_liegenderSchwung += outstroke
    drawPath(Schriftzug_O_liegenderSchwung)

    return Schriftzug_O_liegenderSchwung

#drawSchriftzug_O_liegenderSchwung(temp_x, temp_y)




def drawSchriftzug_O_BogenRechts(x, y):
        
    Schriftzug_O_BogenRechts = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5-1.75, y+2.5)
    Spitze = drawHalbbogen9(*Grund_a, outstrokeLen=0)
    
    
    ### von Mitte aus nach OBEN
    HSL_size = 4
    HSL_start = 24

    G3, G4 = line_G_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    A5, A6 = line_A_vonH_o_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B3, B4 = line_B_vonA_u_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*3)

    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(G3, HSL_start, angle_3, angle_2, True))
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(H3, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_O_BogenRechts.arc(*drawKreisSeg(A5, HSL_start+HSL_size*2, angle_1, angle_0, True))
    
    
    
    ### von Mitte aus nach UNTEN
    
    Bogen_unten = drawSchneckenzug(*B4, LOWER_B, 3, HSL_size=4, HSL_start=37.8, clockwise=True, inward=True)

    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")


    Schriftzug_O_BogenRechts += Spitze + outstroke + Bogen_unten

    drawPath(Schriftzug_O_BogenRechts)

    return Schriftzug_O_BogenRechts

#drawSchriftzug_O_BogenRechts(temp_x, temp_y)










def drawSchriftzug_Q_liegenderSchwung(x, y):
        
    Schriftzug_Q_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4.85, y-4.55) 
    
    
    HSL_size = 1
    HSL_start = 11.5

    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))
    
    Einsatz = drawGrundelementC(*Grund_a, 1.5)



    # Wendung
    G3, G4 = line_G_vonH_u_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_Q_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    
    outstroke = drawOutstroke(*E2, 0.7)
    
    Schriftzug_Q_liegenderSchwung += Einsatz

    drawPath(Schriftzug_Q_liegenderSchwung)

    return Schriftzug_Q_liegenderSchwung

#drawSchriftzug_Q_liegenderSchwung(temp_x, temp_y)






def drawSchriftzug_D_liegenderSchwung(x, y):
        
    Schriftzug_D_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.75, y-5) 
    
    
    HSL_size = 1
    HSL_start = 10

    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))
    
    Einsatz = drawGrundelementC(*Grund_a, 0.75)



    # Wendung
    G3, G4 = line_G_vonH_u_gr_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start)
    
    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_D_liegenderSchwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    
    outstroke = drawOutstroke(*E2, 1.25)
    
    Schriftzug_D_liegenderSchwung += Einsatz + outstroke

    drawPath(Schriftzug_D_liegenderSchwung)

    return Schriftzug_D_liegenderSchwung

#drawSchriftzug_D_liegenderSchwung(temp_x, temp_y)

 
 
 
 
def drawSchriftzug_D_Hauptbogen(x, y):
        
    Schriftzug_D_Hauptbogen = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1, y+3) 
    
    HSL_size = 1
    HSL_start = 31.55
        
    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    Einsatz = drawGrundelementF(*B2, 1.5)

    B3, B4 = line_B_vonA_u_kl_s(Einsatz.points[-1], *angles, part, HSL_size, HSL_start-HSL_size*5)
    C1, C2 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*8)
        
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_2, angle_1, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_1, angle_0, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(C1, HSL_start-HSL_size*6, angle_16, angle_15, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(D1, HSL_start-HSL_size*7, angle_15, angle_14, True))
    Schriftzug_D_Hauptbogen.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_14, angle_13, True))
    
    #outstroke = drawOutstroke(*E4, 0.25, "down")

    drawPath(Schriftzug_D_Hauptbogen)

    return Schriftzug_D_Hauptbogen

#drawSchriftzug_D_Hauptbogen(temp_x, temp_y)




    
def drawSchriftzug_D_Deckung(x, y):
        
    Schriftzug_D_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+4, y+2)    
    

    # Wendepunkt ab Raute_b nach links
    HSL_size = 0.5
    HSL_start = 1
        
    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))



    HSL_size = 1
    HSL_start = 6
    
    G3, G4 = line_G_vonH_u_gr_s(G2, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))


    outstroke = drawOutstroke(*E2, 0.25)

    Schriftzug_D_Deckung+= outstroke

    drawPath(Schriftzug_D_Deckung)

    return Schriftzug_D_Deckung

#drawSchriftzug_D_Deckung(temp_x, temp_y)










def drawSchriftzug_A_Hauptstrich_links(x, y):
        
    Schriftzug_A_Hauptstrich_links = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+4.5, y+2.25) 

    instroke  = drawInstroke(*Grund_a, 1.5, "down")
    
    Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=0.25, HSL_start=19, clockwise=False, inward=False)
    
    downstroke = drawGrundelementF(*Bogen_oben.points[-1], 3.125)

    Bogen_unten = drawSchneckenzug(*downstroke.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=16, clockwise=True, inward=True)

    outstroke  = drawOutstroke(*Bogen_unten.points[-1], 0.25, "down")

    Schriftzug_A_Hauptstrich_links += instroke + downstroke + outstroke + Bogen_oben + Bogen_unten

    drawPath(Schriftzug_A_Hauptstrich_links)

    return Schriftzug_A_Hauptstrich_links


#drawSchriftzug_A_Hauptstrich_links(temp_x, temp_y)



    




def drawSchriftzug_A_Hauptstrich_rechts(x, y):
        
    Schriftzug_A_Hauptstrich_rechts = BezierPath()   
        


    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y+3) 


    ### Bogen OBEN
    HSL_size = 3
    HSL_start = 34.25
   
    E1, E2 = line_E_vonD_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)


    downstroke = drawGrundelementF(*B2, 4.475)

    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_2, angle_1, True))
    Schriftzug_A_Hauptstrich_rechts.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_1, angle_0, True))

    
    Bogen_unten = drawSchneckenzug(*downstroke.points[-1], UPPER_B, 3, HSL_size=1, HSL_start=15, clockwise=False, inward=True)

    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5)

    Schriftzug_A_Hauptstrich_rechts += downstroke + Bogen_unten + outstroke

    drawPath(Schriftzug_A_Hauptstrich_rechts)

    return Schriftzug_A_Hauptstrich_rechts


#drawSchriftzug_A_Hauptstrich_rechts(temp_x, temp_y)



      
          
def drawSchriftzug_A_DeckungOben(x, y):
        
    Schriftzug_A_DeckungOben = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+7.25, y+4.25) 


    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1.25, HSL_start=1, clockwise=False, inward=False)

    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=10, clockwise=False, inward=False)

    Schriftzug_A_DeckungOben  = DeckungOben_left + DeckungOben_right
    drawPath(Schriftzug_A_DeckungOben)

    return Schriftzug_A_DeckungOben

#drawSchriftzug_A_DeckungOben(temp_x, temp_y)







def drawSchriftzug_H_stehenderSchwung(x, y):
        
    Schriftzug_H_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+3.65) 
    
    instroke = drawInstroke(*Grund_d, 1, "down")
    
    Schwung_oben = drawSchneckenzug(*Grund_d, UPPER_E, 5, HSL_size=1, HSL_start=15, clockwise=False, inward=False)


    Schwung_unten = drawSchneckenzug(*Schwung_oben.points[-1], UPPER_H, 5, HSL_size=1, HSL_start=18.5, clockwise=True, inward=True)
    
    
    #Endspitze = drawSchneckenzug(*Schwung_unten.points[-1], LOWER_E, 10, HSL_size=1, HSL_start=16, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.08 * i      for  i in range(0, 11)])
     

    Schriftzug_H_stehenderSchwung += instroke + Schwung_oben + Schwung_unten #+ Endspitze

    drawPath(Schriftzug_H_stehenderSchwung)

    return Schriftzug_H_stehenderSchwung

#drawSchriftzug_H_stehenderSchwung(temp_x, temp_y)








def drawSchriftzug_H_Hauptstrich_rechts(x, y):
        
    Schriftzug_H_Hauptstrich_rechts = BezierPath()     

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+1.25) 
    #instroke = drawInstroke(*Grund_a, 2)

    Schwung_Anfang = drawSchneckenzug(*Grund_a, LOWER_A, 1, HSL_size=1, HSL_start=20, clockwise=False, inward=True)
    Schwung_Mitte = drawSchneckenzug(*Schwung_Anfang.points[-1], UPPER_H, 2, HSL_size=1, HSL_start=26, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*Schwung_Mitte.points[-1], 1.5)   
    Schwung_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=38, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Schwung_unten.points[-1], 0.5, "down")

    Schriftzug_H_Hauptstrich_rechts += Schwung_Anfang + Schwung_Mitte + Einsatz + Schwung_unten + outstroke
    drawPath(Schriftzug_H_Hauptstrich_rechts)
    return Schriftzug_H_Hauptstrich_rechts


#drawSchriftzug_H_Hauptstrich_rechts(temp_x, temp_y)







def drawSchriftzug_M_Bogen_links(x, y):
        
    Schriftzug_M_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2.25) 
    
    Hauptbogen = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=1, HSL_start=32.5, clockwise=True, inward=True)
    
    outstroke = drawOutstroke(*Hauptbogen.points[-1], 0.5, "down")
    
    Schriftzug_M_Bogen_links = Hauptbogen + outstroke
    drawPath(Schriftzug_M_Bogen_links)

    return Schriftzug_M_Bogen_links

#drawSchriftzug_M_Bogen_links(temp_x, temp_y)




def drawSchriftzug_M_Bogen_mitte(x, y):
        
    Schriftzug_M_Bogen_mitte = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+8.75, y+3.5) 
    
    Bogen_oben = drawSchneckenzug(*Grund_c, UPPER_E, 5, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0.5)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=1, HSL_start=20, clockwise=True, inward=False)

    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")
    
    Schriftzug_M_Bogen_mitte = Bogen_oben + Bogen_unten + Einsatz + outstroke
    drawPath(Schriftzug_M_Bogen_mitte)

    return Schriftzug_M_Bogen_mitte

#drawSchriftzug_M_Bogen_mitte(temp_x, temp_y)
    
    
    

def drawSchriftzug_M_Bogen_rechts(x, y):
        
    Schriftzug_M_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_M_rechts_unten = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+12.75, y+1.125) 
    
    instroke = drawOutstroke(*Grund_c, 0.5)
    Bogen_oben = drawSchneckenzug(*Grund_c, UPPER_E, 3, HSL_size=1, HSL_start=9, clockwise=False, inward=False)
    Einsatz = drawGrundelementF(*Bogen_oben.points[-1], 3.5)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[-1], UPPER_B, 3, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
    outstroke = drawOutstroke(*Bogen_unten.points[-1])
    
    Schriftzug_M_rechts_unten = instroke + Bogen_oben + Einsatz + Bogen_unten + outstroke

    
    
    ### OBEN
    Schriftzug_M_rechts_oben = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+12, y+2.5) 

    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=False)

    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
        
    Schriftzug_M_rechts_oben = liegenderSchwung_links + liegenderSchwung_rechts


   
    Schriftzug_M_rechts = Schriftzug_M_rechts_unten + Schriftzug_M_rechts_oben
    drawPath(Schriftzug_M_rechts)

    return Schriftzug_M_rechts

#drawSchriftzug_M_Bogen_rechts(temp_x, temp_y)







def drawSchriftzug_W_Bogen_links(x, y):
        
    Schriftzug_W_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+1.75) 
    Hauptbogen = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=1, HSL_start=28, clockwise=True, inward=True)
    
    Schriftzug_W_Bogen_links = Hauptbogen
    drawPath(Schriftzug_W_Bogen_links)

    return Schriftzug_W_Bogen_links

#drawSchriftzug_W_Bogen_links(temp_x, temp_y)





def drawSchriftzug_W_Bogen_mitte(x, y):
        
    Schriftzug_W_Bogen_mitte = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+7.75, y+3.75) 
    
    Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=8, clockwise=False, inward=False)
    
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 1.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=1, HSL_start=20, clockwise=True, inward=False)

    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")
    
    Schriftzug_W_Bogen_mitte = Bogen_oben + Bogen_unten + Einsatz + outstroke
    drawPath(Schriftzug_W_Bogen_mitte)

    return Schriftzug_W_Bogen_mitte

#drawSchriftzug_W_Bogen_mitte(temp_x, temp_y)
    
    
    

def drawSchriftzug_W_Bogen_rechts(x, y):
        
    Schriftzug_W_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_W_rechts_unten = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+11, y+0.75) 
    
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 5, HSL_size=1, HSL_start=10, clockwise=False, inward=True)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=7.5, HSL_start=7.75, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 1.13, "down")
    
    Schriftzug_W_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    
    
    ### OBEN
    Schriftzug_W_rechts_oben = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+10.5, y+2.5) 

    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=False)

    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
    
    # instroke_special = drawSchneckenzug(*liegenderSchwung_links.points[0], UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
        
    Schriftzug_W_rechts_oben = liegenderSchwung_links + liegenderSchwung_rechts


   
    Schriftzug_W_rechts = Schriftzug_W_rechts_unten + Schriftzug_W_rechts_oben
    drawPath(Schriftzug_W_rechts)

    return Schriftzug_W_rechts

#drawSchriftzug_W_Bogen_rechts(temp_x, temp_y)







def drawSchriftzug_V_Bogen_links(x, y):
        
    Schriftzug_V_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2.5) 
    Hauptbogen = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=1.25, HSL_start=30, clockwise=True, inward=True)

    Schriftzug_V_Bogen_links = Hauptbogen
    drawPath(Schriftzug_V_Bogen_links)

    return Schriftzug_V_Bogen_links

#drawSchriftzug_V_Bogen_links(temp_x, temp_y)





def drawSchriftzug_V_Bogen_rechts(x, y):
        
    Schriftzug_V_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_V_rechts_unten = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+8.75, y+0.25) 
    
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 5, HSL_size=1, HSL_start=8, clockwise=False, inward=True)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=7.5, HSL_start=7.75, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 1, "down")
    
    Schriftzug_V_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    
    
    ### OBEN
    Schriftzug_V_rechts_oben = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+10, y+3.5) 

    stehenderSchwung_oben = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=1, HSL_start=2, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*stehenderSchwung_oben.points[-1], 0.5)

    stehenderSchwung_unten = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 5, HSL_size=1.5, HSL_start=4, clockwise=True, inward=False)
    
        
    Schriftzug_V_rechts_oben = stehenderSchwung_oben +  Einsatz + stehenderSchwung_unten


   
    Schriftzug_V_rechts = Schriftzug_V_rechts_unten + Schriftzug_V_rechts_oben
    drawPath(Schriftzug_V_rechts)

    return Schriftzug_V_rechts

#drawSchriftzug_V_Bogen_rechts(temp_x, temp_y)









def drawSchriftzug_Y_Bogen_links(x, y):
        
    Schriftzug_Y_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2.5) 
    
    Hauptbogen = drawSchneckenzug(*Grund_c, UPPER_E, 8, HSL_size=0, HSL_start=28.8, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Hauptbogen.points[-1], 1, "down")

    Schriftzug_Y_Bogen_links = Hauptbogen + outstroke
    drawPath(Schriftzug_Y_Bogen_links)

    return Schriftzug_Y_Bogen_links

#drawSchriftzug_Y_Bogen_links(temp_x, temp_y)






def drawSchriftzug_Y_Bogen_rechts(x, y):
        
    Schriftzug_Y_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_Y_rechts_unten = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9, y+0.75) 
    
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 5, HSL_size=1, HSL_start=10, clockwise=False, inward=True)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=7.5, HSL_start=15, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")
    
    
    Schriftzug_Y_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    
    
    ### OBEN
    Schriftzug_Y_rechts_oben = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+8.5, y+2.5) 

    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=False)

    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
        
    Schriftzug_Y_rechts_oben = liegenderSchwung_links + liegenderSchwung_rechts

    Schriftzug_Y_rechts = Schriftzug_Y_rechts_unten + Schriftzug_Y_rechts_oben
    drawPath(Schriftzug_Y_rechts)

    return Schriftzug_Y_rechts

#drawSchriftzug_Y_Bogen_rechts(temp_x, temp_y)







    

def drawSchriftzug_P_Bogen_links(x, y):
        
    Schriftzug_P_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2.75) 
    
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 7, HSL_size=0, HSL_start=8, clockwise=False, inward=True)
    Einsatz = drawGrundelementB(*Bogen_oben.points[-1], 0.75)
    Bogen_mitte = drawSchneckenzug(*Einsatz.points[-1], UPPER_F, 4, HSL_size=2, HSL_start=15, clockwise=True, inward=False)  
    Einsatz_2 = drawGrundelementF(*Bogen_mitte.points[-1], 3.75)
  
    Bogen_unten = drawSchneckenzug(*Einsatz_2.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=26, clockwise=True, inward=False)  
 
    Schriftzug_P_links_unten = Bogen_oben + Einsatz + Bogen_mitte + Einsatz_2 + Bogen_unten
   
    Schriftzug_P_links = Schriftzug_P_links_unten 
    drawPath(Schriftzug_P_links)

    return Schriftzug_P_links

#drawSchriftzug_P_Bogen_links(temp_x, temp_y)




    

def drawSchriftzug_P_Bogen_rechts(x, y):
        
    Schriftzug_P_rechts = BezierPath()   

    ### RECHTS UNTEN = Stehender Schwung
    Schriftzug_P_rechts_unten = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.6, y+0.5) 
    
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 5, HSL_size=1, HSL_start=10, clockwise=False, inward=True)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=4, HSL_start=13.45, clockwise=True, inward=False)    
    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 3.3, "down")
    Schriftzug_P_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    
    
    ### RECHTS OBEN = Liegender Schwung
    Schriftzug_P_rechts_oben = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6, y+2.25) 

    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=False)

    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
        
    Schriftzug_P_rechts_oben = liegenderSchwung_links + liegenderSchwung_rechts

    Schriftzug_P_rechts = Schriftzug_P_rechts_unten + Schriftzug_P_rechts_oben
    drawPath(Schriftzug_P_rechts)

    return Schriftzug_P_rechts

#drawSchriftzug_P_Bogen_rechts(temp_x, temp_y)








def drawSchriftzug_U_Hauptstrich_links(x, y):
        
    Schriftzug_U_Hauptstrich_links = BezierPath()


    ### Liegender Schwung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.25, y+3.15) 
    links = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*Grund_a, 1.05)
    right = drawSchriftteil10(*Einsatz.points[-1])
    
    Schriftzug_U_Hauptstrich_links += links + Einsatz + right
    
    ### Stehender Schwung nach unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.25, y+1.5) 
    instroke  = drawInstroke(*Grund_a, 2, "down")
    Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=11, clockwise=False, inward=False)
    downstroke = drawGrundelementD(*Bogen_oben.points[-1], 0)
    Bogen_unten = drawSchneckenzug(*downstroke.points[0], UPPER_H, 5, HSL_size=1, HSL_start=8, clockwise=True, inward=False)
    outstroke  = drawOutstroke(*Bogen_unten.points[-1], 0, "down")

    
    Schriftzug_U_Hauptstrich_links += instroke + Bogen_oben + downstroke + Bogen_unten + outstroke

    drawPath(Schriftzug_U_Hauptstrich_links)

    return Schriftzug_U_Hauptstrich_links


#drawSchriftzug_U_Hauptstrich_links(temp_x, temp_y)



    


def drawSchriftzug_U_Hauptstrich_rechts(x, y):
        
    Schriftzug_U_Hauptstrich_rechts = BezierPath()
    
    
    ### Rechts, gerade
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y+2)    
    stem = drawSchriftzug3_Figures(x+4.5, y, instrokeLen=0)
    serife_nach_rechts = drawSchriftteil2(*Raute_a)
    serife_nach_unten = drawSchriftteil5(*Raute_d)


    ### Liegender Schwung unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.5, y-5.15) 
    links = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=2, HSL_start=10, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*Grund_a, 0.35)
    right = drawSchneckenzug(*Einsatz.points[-1], LOWER_G, 2, HSL_size=2, HSL_start=10, clockwise=False, inward=False)
    Einsatz2 = drawGrundelementA(*right.points[-1], 1)

    up = drawSchneckenzug(*Einsatz2.points[-1], LOWER_E, 3, HSL_size=8, HSL_start=43.2, clockwise=False, inward=True)

    
    
    Schriftzug_U_Hauptstrich_rechts += stem + serife_nach_rechts + serife_nach_unten + links + Einsatz + right + Einsatz2 + up

    drawPath(Schriftzug_U_Hauptstrich_rechts)

    return Schriftzug_U_Hauptstrich_rechts


#drawSchriftzug_U_Hauptstrich_rechts(temp_x, temp_y)








def drawSchriftzug_Z_Bogen_oben(x, y):
        
    Schriftzug_Z_Bogen_oben = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.25, y+3.125) 
    top = drawSchneckenzug(*Grund_a, UPPER_E, 2, HSL_size=1.5, HSL_start=21, clockwise=True, inward=True)
    btm = drawSchneckenzug(*top.points[-1], UPPER_G, 6, HSL_size=0.5, HSL_start=11, clockwise=True, inward=False)

    ### Ich habe mich für den Punkt anstatt Deckung entschieden, ganz simpel weil schöner :)
    #Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y+0.25) 
    #Deckung = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=1.25, HSL_start=10, clockwise=True, inward=True)
    
    Schriftzug_Z_Bogen_oben += top + btm #+ Deckung
    drawPath(Schriftzug_Z_Bogen_oben)
    return Schriftzug_Z_Bogen_oben


#drawSchriftzug_Z_Bogen_oben(temp_x, temp_y)





def drawSchriftzug_Z_Bogen_unten(x, y):
        
    Schriftzug_Z_Bogen_unten = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y-1.25) 
    top = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=2, HSL_start=20, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*top.points[-1], 0.75)
    btm = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=14, clockwise=True, inward=False)
    
    Schriftzug_Z_Bogen_unten += top + btm + Einsatz
    drawPath(Schriftzug_Z_Bogen_unten)
    return Schriftzug_Z_Bogen_unten


#drawSchriftzug_Z_Bogen_unten(temp_x, temp_y)




def drawSchriftzug_z_Initial(x, y):
        
    Schriftzug_z_Initial = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y+2) 
    oben1 = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=0.5, HSL_start=6, clockwise=False, inward=True)
    oben2 = drawSchneckenzug(*oben1.points[-1], UPPER_H, 5, HSL_size=0.5, HSL_start=7, clockwise=True, inward=False)
    Einsatz = drawGrundelementA(*oben2.points[-1], 0.25, "down")

    Schriftzug_z_Initial += oben1 + oben2 + Einsatz
    drawPath(Schriftzug_z_Initial)

    return Schriftzug_z_Initial

#drawSchriftzug_z_Initial(temp_x, temp_y)




def drawAuge(x, y):
    
    Auge = BezierPath()

    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    start_oben = drawSchriftteil2(x, y)
    bow = drawSchriftteil8(*start_oben.points[-1])

    Auge += start_oben + bow
    drawPath(Auge)  
    return Auge
        
#drawAuge(temp_x, temp_y)






