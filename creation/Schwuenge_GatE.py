import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Halbboegen_GatE

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Halbboegen_GatE)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Halbboegen_GatE import *

import math as m
from drawBot import *





temp_x = 3
temp_y = 9


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 7
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

    
strokeWidth(.1)







def drawSchwung1(x,y):
    
    Schwung1 = BezierPath()
    Schwung1 = drawHalbbogen1(x, y, instroke=False) + drawHalbbogen10(x, y, outstroke=False)
    
    return Schwung1
    



   
def drawSchwung2(x,y):
    
    Schwung2 = BezierPath()    
    Schwung2 = drawHalbbogen2(x, y, instroke=False) + drawHalbbogen9(x, y, outstroke=False)

    return Schwung2
    
    
    
    
    
def drawSchwung3(x,y):
    
    Schwung3 = BezierPath()
    Schwung3 = drawHalbbogen3(x, y, instroke=False) + drawHalbbogen8(x, y, outstroke=False)
    return Schwung3

   
   
   
def drawSchwung4(x,y):
    
    Schwung4 = BezierPath()
    Schwung4 = drawHalbbogen4(x, y, instroke=False) + drawHalbbogen7(x, y, outstroke=False)

    return Schwung4
    
    
    
    
def drawSchwung5(x,y):
    
    Schwung5 = BezierPath()
    Schwung5 = drawHalbbogen5(x, y, instroke=False) + drawHalbbogen6(x, y, outstroke=False)

    return Schwung5
    
    
    
    
    
    
def drawSchwung6(x,y, Endspitze=11):
    
    Schwung6 = BezierPath()

    # ___________ 1. Schwung unten anfangen _______________

    # So ist auch der zur Gattung E bestimmte Schwung gleichfalls einen Bestandtheil hoch 
    # ueber den besagten Anfang ab mit der obern Krümmung dc, aber nur bis zur Richtlinie H, 
    # von unten hinauf construiert, und auf dieser Richtlinie in eben dergleichen Halbbogen 
    # also geschwungen, daß dessen Endspitze auch auf dieser Gränzlinie gh ausgehet.
        # Dieser Schwung ist unten von der Richtlinie D mit einem Radio von 3 1/2 Bogenmaas 
    # bis an die Richtlinie C, von dieser aber mit einem Radio von 4. Bogenmaas bis an die Hauptlinie, 
    # und von dieser wieder mit einem Radio von 3 1/2 Bogenmaas bis zur Richtlinie H construiert ...
  
    # 1. HSL; D>C; 3.5BM, 4BM, 3.5BM
    

    HSL_size = 4
    HSL_start = 28

    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y) 
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1, y-4.5)
    
    instroke = drawInstroke(*Raute_c, 1)
    
    D1, D2 = line_D_vonE_u_gr_s(Raute_c, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size)
    
    Schwung6.arc(*drawKreisSeg(D1, HSL_start, angle_14, angle_15))

    HSL_size = 4
    HSL_start = 32
    
    # Mittelsegment
    A3, A4 = line_A_vonB_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schwung6.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_1))

    H1, H2 = line_H_vonA_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schwung6.arc(*drawKreisSeg(H1, HSL_start-HSL_size, angle_1, angle_2))

    
    # Übergang nach oben

    if Endspitze == 11:  
        # auf welcher derselbe seine Wendung ebenfalls in einen Halbbogen mit einem Radio von 1 1/2 Bogenmaas anfängt 
        # und nach der nehmlichen Schneckenlinie, wie vorhin, abfallend zur Spitze mit 8. Part ausgehet.
        # 1.5 BM = 8+4 = 12, daher 12>8
        HSL_start = 13
  
        
    if Endspitze == 12:
        # Oder nach der völligen Größe mit einem Radio von 17. bis 13. Part.
        HSL_start = 17


    HSL_size = 1
        
    H3, H4 = line_H_vonG_u_kl_s(H2, *angles, part, HSL_size, HSL_start)

    A1, A2 = line_A_vonH_u_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size)
    Schwung6.arc(*drawKreisSeg(A1, HSL_start-HSL_size, angle_10, angle_9, True))

    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schwung6.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_9, angle_8, True))

    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schwung6.arc(*drawKreisSeg(C1, HSL_start-HSL_size*3, angle_8, angle_7, True))
 
    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    Schwung6.arc(*drawKreisSeg(D1, HSL_start-HSL_size*4, angle_7, angle_6, True))

    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*5)
    Schwung6.arc(*drawKreisSeg(E1, HSL_start-HSL_size*5, angle_6, angle_5, True))

    Schwung6 += instroke
    drawPath(Schwung6)
    
    return Schwung6
    
    

#drawSchwung6(temp_x, temp_y)     # Endspitze kann optional auch 11 sein, 12 ist default




