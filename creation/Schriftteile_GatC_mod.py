import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *

import math as m
from drawBot import *





temp_x = 2 * modul_width
temp_y = 9 * modul_height



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)






# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)







def drawSchriftteil1(x, y):
    
    Schriftteil1 = BezierPath()

    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height)

    regular = drawSchneckenzug(x, y, UPPER_A, 2, HSL_size=4, HSL_start=4, clockwise=False, inward=False)    
    modified = drawSchneckenzug(*regular.points[-1], UPPER_G, 1, HSL_size=3.5, HSL_start=9.7, clockwise=False, inward=False)
    
    Schriftteil1 += regular + modified
    drawPath(Schriftteil1)    
    return Schriftteil1
    
#drawSchriftteil1(temp_x, temp_y)






 
 
def drawSchriftteil2(x, y):
    
    Schriftteil2 = BezierPath()

    #Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    regular = drawSchneckenzug(x, y, LOWER_A, 2, HSL_size=4.5, HSL_start=4, clockwise=False, inward=False)
    modified = drawSchneckenzug(*regular.points[-1], LOWER_G, 1, HSL_size=5, HSL_start=9.35, clockwise=False, inward=False)

    # # # pt1 = regular.points[-1]
    # # # pt2 = regular.points[-1][0]+1.5, regular.points[-1][1]-0.75
    # # # pt3 = x+modul_width, y-modul_height/2
    # # # Schriftteil2.moveTo(pt1)
    # # # Schriftteil2.curveTo(pt1, pt2, pt3)
    # # # #text("pt1", pt1)
    # # # #text("pt2", pt2)
    # # # #text("pt3", pt3)

    Schriftteil2 += regular + modified
    drawPath(Schriftteil2)  
    return Schriftteil2
        
#drawSchriftteil2(temp_x, temp_y)
    
    
    




def drawSchriftteil3(x, y, version="standard"):
    
    Schriftteil3 = BezierPath()
    
    # C in No. 3.
    # nach der zweiten Hauptschneckenlinie von G bis B rechts 
    # mit 8. Part angefangen und mit 4. Part geschlossen. (Th1, S. 36)
    # 2. HSL, G>B, 8>4
    
    if version == "standard":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=2, HSL_start=10, clockwise=True, inward=True) 
        
    if version == "0.75":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=1, HSL_start=10, clockwise=True, inward=True) 

    if version == "1":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=4, HSL_start=19.85, clockwise=True, inward=True) 
    
    drawPath(Schriftteil3)
    return Schriftteil3
    
    
#drawSchriftteil3(temp_x, temp_y, "standard")



 




    
def drawSchriftteil4(x, y, version="standard"):
    
    Schriftteil4 = BezierPath()

    if version == "standard":
        HSL_size = 2
        HSL_start = 10 
        
    if version == "1":
        HSL_size = 4
        HSL_start = 19.85

    G1, G2 = line_G_vonF_u_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil4.arc(*drawKreisSeg(H1, HSL_start-HSL_size, angle_11, angle_10, True))

    A1, A2 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftteil4.arc(*drawKreisSeg(A1, HSL_start-HSL_size*2, angle_10, angle_9, True))

    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftteil4.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_9, angle_8, True))

    drawPath(Schriftteil4)
        
    return Schriftteil4
    
    
#drawSchriftteil4(temp_x, temp_y, "1") 






   
def drawSchriftteil5(x, y, version="standard"):
    
    Schriftteil5 = BezierPath()
    
    # C in No. 5. 
    # nach der zweiten Hauptschneckenlinie mit einem Bogenmaase 
    # von der Theilinie E bis mit 4. Part zur Theilinie B.
    # 2. HSL, E>B, 8>4
    
    #Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    if version == "standard":   ### 0.5 modul
        bend = drawSchneckenzug(x, y , UPPER_E, 3, HSL_size=2, HSL_start=9.875, clockwise=False, inward=True)

    if version == "1":
        bend = drawSchneckenzug(x, y , UPPER_E, 3, HSL_size=5, HSL_start=21.55, clockwise=False, inward=True)
        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt

    Schriftteil5 += bend
    drawPath(Schriftteil5)
    return Schriftteil5
    
    
#drawSchriftteil5(temp_x, temp_y, "standard")
# drawSchriftteil5(temp_x, temp_y, "1")




    
    
def drawSchriftteil6(x, y, version="standard"):
    
    Schriftteil6 = BezierPath()   
    
    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height)
    
    
    if version == "standard":   ### 0.5 modul
        HSL_size = 2
        HSL_start = 9.735
        
    if version == "1":
        HSL_size = 5             # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        HSL_start = 21.55        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
    
    if version == "1.5":
        HSL_size = 10    # angelegt für KurrentKanzlei 
        HSL_start = 40      # Schriftzug 8c (TAB 49)


    E1, E2 = line_E_vonF_u_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil6.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))

    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftteil6.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))

    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftteil6.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))


    drawPath(Schriftteil6)
        
    return Schriftteil6
    

#drawSchriftteil6(temp_x, temp_y, "standard")
#drawSchriftteil6(temp_x, temp_y, "1.5")


