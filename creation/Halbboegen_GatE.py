import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *

import math as m
from drawBot import *




temp_x = modul_width * 2.5
temp_y = modul_height * 9



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)




# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)
    
strokeWidth(.1)




drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)+2)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)+1)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height))
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-1)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-2)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-3)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-3.5)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width), (temp_y/modul_height)-3.75) ### für Halbbogen4
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-4)






def drawHalbbogen1(x, y,  instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen1 = BezierPath()
    
    instroke = drawInstroke(x, y, instrokeLen)
    
    # Fig. 1.     # der Halbbogen 1. Bestandtheil der ersten Hauptschneckenlinie groß, 
    # mit einem Radio von 5 1/2 bis 8 1/2 Part construiret. 
    # 1. HSL; A>E; 5.5>8.5
    
    HSL_size = 1
    HSL_start = 5.5
    
    A1, A2 = line_A_vonH_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)

    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen1.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen1.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen1.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen1.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen1 += instroke + outstroke
    drawPath(Halbbogen1) 
    return Halbbogen1
    
#drawHalbbogen1(temp_x, temp_y)


    
    
    

def drawHalbbogen2(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen2 = BezierPath()
        
    instroke = drawInstroke(x, y, instrokeLen)
        
    # Fig. 2
    # ist der Halbbogen 1 1/2 Bestandteil der vorigen Hauptschneckenlinie 
    # mit einem 9. bis 12. Part großen Radio construiret.
    # 1. HSL; A>E; 9>12

    HSL_size = 1
    HSL_start = 9
        
    A1, A2 = line_A_vonH_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen2.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen2.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen2.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen2.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen2 += instroke + outstroke
    drawPath(Halbbogen2)   
    return Halbbogen2

#drawHalbbogen2(temp_x, temp_y)






def drawHalbbogen3(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen3 = BezierPath()

    instroke = drawInstroke(x, y, instrokeLen)
                
    # Fig. 3
    # ist der Halbbogen aus 2. Bestandtheilen ebenfalls nach der vorigen Hauptschneckenlinie 
    # von 12 1/2 bis mit 15 1/2 Part construiret.
    # 1. HSL; A>E; 12.5>15.5

    HSL_size = 1
    HSL_start = 12.5
        
    A1, A2 = line_A_vonH_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen3.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen3.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen3.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen3.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen3 += instroke + outstroke
    drawPath(Halbbogen3)      
    return Halbbogen3
    
    
#drawHalbbogen3(temp_x, temp_y)

    

    
    
def drawHalbbogen4(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen4 = BezierPath()
    
    instroke = drawInstroke(x, y, instrokeLen)
            
    # Fig. 4
    # ist der Halbbogen aus 2 1/2 Bestandtheil    # nach einer Schneckenlinie, wie bei C und D, durch 15 1/4 bis 19. Part construiret.
    # 1.25 HSL; A>E; 15.25>19

    HSL_size = 1.25
    HSL_start = 15.25
        
    A1, A2 = line_A_vonH_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen4.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen4.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen4.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen4.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen4 += instroke + outstroke
    drawPath(Halbbogen4) 
    return Halbbogen4
    
    
drawHalbbogen4(temp_x, temp_y)
    
    
  
  
    

def drawHalbbogen5(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen5 = BezierPath()
    
    instroke = drawInstroke(x, y, instrokeLen)
            
    # Fig. 5
    # ist der Halbbogen aus. 3. Bestandtheilen nach eben dergleichen Schneckenlinie, 
    # wie bei C und D, mit 18. bis 22 1/2 Part construiret.
    # 1.5 HSL; A>E; 18>22.5

    HSL_size = 1.5
    HSL_start = 18
        
    A1, A2 = line_A_vonH_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen5.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen5.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen5.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen5.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen5 += instroke + outstroke
    drawPath(Halbbogen5) 
    return Halbbogen5
    
    
#drawHalbbogen5(temp_x, temp_y)
  
    
    
    
    
def drawHalbbogen6(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen6 = BezierPath()
        
    # siehe Fig. 5.

    HSL_size = 1.5 
    HSL_start = 18
        
    A1, A2 = line_A_vonH_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen6.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_8, True))

    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen6.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen6.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen6.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))

    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen6 += instroke + outstroke
    drawPath(Halbbogen6)
    return Halbbogen6
    
    
#drawHalbbogen6(temp_x, temp_y)


 
    
          
    
def drawHalbbogen7(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen7 = BezierPath()
        
    # siehe Fig. 4.

    HSL_size = 1.25
    HSL_start = 15.25
        
    A1, A2 = line_A_vonH_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen7.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_8, True))

    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen7.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen7.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen7.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen7 += instroke + outstroke
    drawPath(Halbbogen7)  
    return Halbbogen7
    
    
#drawHalbbogen7(temp_x, temp_y)  




def drawHalbbogen8(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen8 = BezierPath()
        
    # siehe Fig. 3.
    
    HSL_size = 1
    HSL_start = 12.5
        
    A1, A2 = line_A_vonH_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen8.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_8, True))

    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen8.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen8.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen8.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen8 += instroke + outstroke
    drawPath(Halbbogen8)
    return Halbbogen8
    
#drawHalbbogen8(temp_x, temp_y)  

    
    
    
    

def drawHalbbogen9(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen9 = BezierPath()
        
    # siehe Fig. 2.

    HSL_size = 1
    HSL_start = 9
        
    A1, A2 = line_A_vonH_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)

    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen9.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_8, True))

    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen9.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen9.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen9.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen9 += instroke + outstroke
    drawPath(Halbbogen9)   
    return Halbbogen9

#drawHalbbogen9(temp_x, temp_y) 




def drawHalbbogen10(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen10 = BezierPath()
        
    # siehe Fig. 1. 

    HSL_size = 1   
    HSL_start = 5.5
        
    A1, A2 = line_A_vonH_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)

    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Halbbogen10.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_8, True))

    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen10.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen10.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen10.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen10 += instroke + outstroke
    drawPath(Halbbogen10)     
    return Halbbogen10

#drawHalbbogen10(temp_x, temp_y)  




