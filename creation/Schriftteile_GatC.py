import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *

import math as m
from drawBot import *





temp_x = 2 * modul_width
temp_y = 9 * modul_height



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)






# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)





def drawHauptbestandteil(x, y):
    
    Hauptbestandteil = BezierPath()

    A2 = x, y          #Raute_a
    A1 = A2[0]+offsetDir[0], A2[1]-offsetDir[1] 
    Hauptbestandteil.polygon(A1, A2)
    
    drawPath(Hauptbestandteil)
        
    return Hauptbestandteil
    
#drawHauptbestandteil(temp_x, temp_y)






def drawSchriftteil1(x, y):
    
    Schriftteil1 = BezierPath()
    
    # C in No. I.
    # durch die vierte Hauptschneckenlinie rechts von der
    # Richtlinie F bis zur Hauptlinie A mit 12. Part angefangen
    # und mit 4. Part beendiget.
    # 4. HSL, F>A, 12>4
    
    Schriftteil1 = drawSchneckenzug(x, y , UPPER_A, 3, HSL_size=4, HSL_start=4, clockwise=False, inward=False)
    
    ### von links angefangen
    #Schriftteil1 = drawSchneckenzug(x, y , UPPER_F, 3, HSL_size=4, HSL_start=16, clockwise=True, inward=True)    
    
    drawPath(Schriftteil1)
    return Schriftteil1
    
#drawSchriftteil1(temp_x, temp_y)
 
 
 
 
    
    
def drawSchriftteil2(x, y):
    
    Schriftteil2 = BezierPath()
    
    HSL_size = 4 
    HSL_start = 4
        
    A1, A2 = line_A_vonB_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonA_u_gr(A1, *angles, part, HSL_size, 8)
    Schriftteil2.arc(*drawKreisSeg(A1, 4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, 12)
    Schriftteil2.arc(*drawKreisSeg(H1, 8, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, 16)
    Schriftteil2.arc(*drawKreisSeg(G1, 12, angle_11, angle_12))

    drawPath(Schriftteil2)
        
    return Schriftteil2
    
    
#drawSchriftteil2(temp_x, temp_y)
    
    


def drawSchriftteil3(x, y, version="standard"):
    
    Schriftteil3 = BezierPath()
    
    # C in No. 3.
    # nach der zweiten Hauptschneckenlinie von G bis B rechts 
    # mit 8. Part angefangen und mit 4. Part geschlossen. (Th1, S. 36)
    # 2. HSL, G>B, 8>4
    
    if version == "standard":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=2, HSL_start=10, clockwise=True, inward=True) 
        
    if version == "0.75":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=1, HSL_start=10, clockwise=True, inward=True) 

    if version == "1":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=4, HSL_start=19.85, clockwise=True, inward=True) 
    
    drawPath(Schriftteil3)
    return Schriftteil3
    
    
#drawSchriftteil3(temp_x, temp_y, "standard")





    
def drawSchriftteil4(x, y, version="standard"):
    
    Schriftteil4 = BezierPath()

    if version == "standard":
        HSL_size = 2
        HSL_start = 10 
        
    if version == "1":
        HSL_size = 4
        HSL_start = 19.85

    G1, G2 = line_G_vonF_u_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil4.arc(*drawKreisSeg(H1, HSL_start-HSL_size, angle_11, angle_10, True))

    A1, A2 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftteil4.arc(*drawKreisSeg(A1, HSL_start-HSL_size*2, angle_10, angle_9, True))

    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftteil4.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_9, angle_8, True))

    drawPath(Schriftteil4)
        
    return Schriftteil4
    
    
#drawSchriftteil4(temp_x, temp_y, "1") 





def drawSchriftteil5(x, y, version="standard"):
    
    Schriftteil5 = BezierPath()
    
    # C in No. 5. 
    # nach der zweiten Hauptschneckenlinie mit einem Bogenmaase 
    # von der Theilinie E bis mit 4. Part zur Theilinie B.
    # 2. HSL, E>B, 8>4
    
    #Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    if version == "standard":   ### 0.5 modul
        bend = drawSchneckenzug(x, y , UPPER_E, 3, HSL_size=2, HSL_start=10, clockwise=False, inward=True)

    if version == "1":
        bend = drawSchneckenzug(x, y , UPPER_E, 3, HSL_size=5, HSL_start=21.55, clockwise=False, inward=True)
        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt

    Schriftteil5 += bend
    drawPath(Schriftteil5)
    return Schriftteil5
    
    
#drawSchriftteil5(temp_x, temp_y, "standard")
# drawSchriftteil5(temp_x, temp_y, "1")
   
   
   
   
def drawSchriftteil_Test(x, y):
    
    Schriftteil_Test = BezierPath()
    
    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width, temp_y/modul_height-7)

    bend = drawSchneckenzug(*Raute_a , UPPER_B, 3, HSL_size=2, HSL_start=10, clockwise=True, inward=False)

    Schriftteil_Test += bend
    drawPath(Schriftteil_Test)
    return Schriftteil_Test
    
#drawSchriftteil_Test(temp_x, temp_y)
   



    
    
def drawSchriftteil6(x, y, version="standard"):
    
    Schriftteil6 = BezierPath()   
    
    if version == "standard":   ### 0.5 modul
        HSL_size = 2
        HSL_start = 10
        
    if version == "1":
        HSL_size = 5             # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        HSL_start = 21.55        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
    
    if version == "1.5":
        HSL_size = 10    # angelegt für KurrentKanzlei 
        HSL_start = 40      # Schriftzug 8c (TAB 49)


    E1, E2 = line_E_vonF_u_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil6.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))

    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftteil6.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))

    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftteil6.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))


    drawPath(Schriftteil6)
        
    return Schriftteil6
    

#drawSchriftteil6(temp_x, temp_y, "1.5")





def drawSchriftteil7(x, y):
    
    Schriftteil7 = BezierPath() 
    
    HSL_size = 1

    # 1. Element oben
    G2 = x, y   # Raute_b
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*8),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*8) 
    polygon(G1, G2)
    
    H1 = G1
    H2 = H1[0] - m.cos(m.radians(90-epsilon)) * (part*8),    H1[1] + m.sin(m.radians(90-epsilon)) * (part*8)
    polygon(H1, H2)
    

    # 2. Element unten
    E2 = x, y - modul_height      # Raute_d
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*8),    E2[1] + m.cos(m.radians(angle_seg_1)) * (part*8) 
    polygon(E1, E2)

    D1 = E1
    D2 = D1[0] - m.cos(m.radians(90-epsilon)) * (part*8),    D1[1] - m.sin(m.radians(90-epsilon)) * (part*8)
    polygon(D1, D2)

    # Radius über a2 + b2 = c2 ausrechnen, dann noch (wegen der Formel) durch part teilen
    center = G2[0],     G2[1] - (G2[1]-E2[1])/2
    radius = m.sqrt(pow(H2[0]-G2[0], 2) + pow(H2[1]-center[1], 2))
    
    Schriftteil7.arc(*drawKreisSeg(H1, 8, angle_5, angle_6))
    Schriftteil7.arc(*drawKreisSeg(center, radius/part, angle_6, angle_10))
    Schriftteil7.arc(*drawKreisSeg(D1, 8, angle_10, angle_11))
    
    drawPath(Schriftteil7)
        
    return Schriftteil7
    
    
#drawSchriftteil7(temp_x, temp_y)






    
    
def drawSchriftteil8(x, y, instrokeLen=0, outstrokeLen=0):
    
    Schriftteil8 = BezierPath() 
    
    HSL_size = 1

    # 1. Element oben
    G2 = x, y   # Raute_b
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*8),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*8) 
    polygon(G1, G2)
    
    H1 = G1
    H2 = H1[0] + m.cos(m.radians(90-epsilon)) * (part*8),    H1[1] + m.sin(m.radians(90-epsilon)) * (part*8)
    polygon(H1, H2)
    
    # 2. Element unten
    E2 = x, y - modul_height        # Raute_d
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*8),    E2[1] + m.cos(m.radians(angle_seg_1)) * (part*8) 
    polygon(E1, E2)
    
    D1 = E1
    D2 = D1[0] + m.cos(m.radians(90-epsilon)) * (part*8),    D1[1] - m.sin(m.radians(90-epsilon)) * (part*8)
    polygon(D1, D2)

    # Radius über a2 + b2 = c2 ausrechnen, dann noch (wegen der Formel) durch part teilen
    center = G2[0], G2[1]-(G2[1]-E2[1])/2
    radius = m.sqrt(pow(H2[0]-G2[0], 2) + pow(H2[1]-center[1], 2))
    
    Schriftteil8.arc(*drawKreisSeg(H1, 8, angle_3, angle_2, True))
    Schriftteil8.arc(*drawKreisSeg(center, radius/part, angle_2, angle_14, True))
    Schriftteil8.arc(*drawKreisSeg(D1, 8, angle_14, angle_13, True))
    
    instroke = drawInstroke(*G2, instrokeLen)
    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Schriftteil8 += instroke + outstroke
    drawPath(Schriftteil8)
    return Schriftteil8


#drawSchriftteil8(temp_x, temp_y)







def drawSchriftteil9(x, y):
    
    Schriftteil9 = BezierPath() 
    
    # zu C No. 9. 
    # eine Anfangsspitze eines Bogen nach der zweiten Hauptschneckenlinie construiret, 
    # und mit einem Radio von 17. Part von E angefangen und am Ende bis G mit 15. Part gezogen.
    # 2. HSL, E>G, 17>15
    
    HSL_size = 2
    HSL_start = 13
          
    # andersrum aufgebaut (in Photoshop kontrolliert, passt)
         
    G1, G2 = line_G_vonH_o_gr_s((x,y), *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftteil9.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_4))
    
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftteil9.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_5))
          
    drawPath(Schriftteil9)
        
    return Schriftteil9
    
   
#drawSchriftteil9(temp_x, temp_y)


    
    
def drawSchriftteil10(x, y):
    
    Schriftteil10 = BezierPath() 
    
    HSL_size = 2
    HSL_start = 13              
    
    G1, G2 = line_G_vonF_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftteil10.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftteil10.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))
          
    drawPath(Schriftteil10)
        
    return Schriftteil10
    
    
#drawSchriftteil10(temp_x, temp_y)
    
    
    
    

def drawSchriftteil11(x, y):
    
    Schriftteil11 = BezierPath() 

    # ist eine Bogenspitze, welche mit einem Radio von 8. Part bis zur Richtlinie G gezogen, 
    # die als denn umgewendet der erstern angesetzt wird.
    # G, 8
    
    ### Ich habe eine andere Lösung gewählt! Siehe version_1 für Original.
    ### in der Mitte anfangen, für bessere Passung und einfachere Positionierung (weil als Deckung verwendet)

    HSL_size = 1
    HSL_start = 8
    
    G1, G2 = line_G_vonH_o_gr_s((x,y), *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, 9)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, 10)
    
    Schriftteil11.arc(*drawKreisSeg(F1, 9, angle_5, angle_4, True))
    Schriftteil11.arc(*drawKreisSeg(G1, 8, angle_4, angle_3, True))
    
    # Wendung
    G3, G4 = line_G_vonH_u_gr_s(G2, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, 9)
    Schriftteil11.arc(*drawKreisSeg(G3, 8, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, 10)
    Schriftteil11.arc(*drawKreisSeg(F1, 9, angle_12, angle_13))
          
    drawPath(Schriftteil11)
        
    return Schriftteil11
    
    
#drawSchriftteil11(temp_x, temp_y)
  
  
    
    

def drawSchriftteil12(x, y, outstrokeLen=0):
    
    Schriftteil12 = BezierPath() 
    
    # Bei No. 12. 
    # ist dergleichen Bogenspitze mit einem Radio von 12 1/2 Part 
    # bis zur Richtlinie G mit 11 1/2 Part construiret. 
    # E>G; 12,5>11,5
    
    ### Ich habe eine andere Lösung gewählt! Siehe version_1 für Original.
    ### in der Mitte anfangen, für bessere Passung und einfachere Positionierung

    links = drawSchneckenzug(x, y, UPPER_G, 2, HSL_size=1, HSL_start=11.5, clockwise=False, inward=False)
    rechts = drawSchneckenzug(*links.points[0], LOWER_G, 2, HSL_size=1, HSL_start=11.5, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], outstrokeLen)
    
    Schriftteil12 = links + rechts + outstroke
    drawPath(Schriftteil12)    
    return Schriftteil12
    
#drawSchriftteil12(temp_x, temp_y)



