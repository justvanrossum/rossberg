"""A drop-in replacement for Jens Kutilek's RectNibPen, that builds complete
"nibbed" paths, instead of building many individual segments.
"""

# Just van Rossum, february 2021

import functools
import math
from fontTools.misc.bezierTools import calcCubicParameters, solveQuadratic, splitCubicAtT
from fontTools.misc.arrayTools import Vector
from fontTools.misc.transform import Transform
from fontTools.pens.basePen import BasePen


class RectNibPen(BasePen):

    def __init__(self, glyphSet, angle, width, height, trace=None):
        # the 'trace' argument is a dummy to be compatible with Jens Kutileks RectNibPen
        super().__init__(glyphSet)
        self.nib = RectNib(angle, width, height)
        self.contours = []

    def _moveTo(self, pt):
        self.contours.append(Contour())

    def _lineTo(self, pt):
        self._appendSegment(LineSegment(self._getCurrentPoint(), pt))

    def _curveToOne(self, pt1, pt2, pt3):
        segment = (self._getCurrentPoint(), pt1, pt2, pt3)
        for segment in splitCurveAtAngle(segment, self.nib.angle):
            for segment in splitCurveAtAngle(segment, self.nib.angle90):
                self._appendSegment(CubicSegment(*segment))

    def _closePath(self):
        self.contours[-1].close()

    def _appendSegment(self, segment):
        self.contours[-1].appendSegment(segment)

    def draw(self, pen):
        for contour in self.contours:
            contour.draw(pen, self.nib)

    def trace_path(self, glyph):
        # compatibility with Jens Kutileks RectNibPen
        # It doesn't really trace, just draws to the glyph
        pen = glyph.getPen()
        self.draw(pen)


class RectNib:

    def __init__(self, angle, width, height):
        self.angle = angle
        self.width = width
        self.height = height
        self.angle90 = angle + math.pi * 0.5
        self.tangent = Vector((math.cos(angle), math.sin(angle)))
        self.tangent90 = Vector((-math.sin(angle), math.cos(angle)))

    @functools.lru_cache(None)  # values for w and h are -1, 0 and 1
    def getVector(self, w, h):
        wv = (w * self.width * 0.5) * self.tangent
        hv = (h * self.height * 0.5) * self.tangent90
        return wv + hv


class Contour:

    def __init__(self):
        self.segments = []
        self.isClosed = False

    def appendSegment(self, segment):
        self.segments.append(segment)

    def close(self):
        self.isClosed = True

    def draw(self, pen, nib):
        segments = self.segments
        numSegments = len(segments)
        startPoint = endPoint = None
        leftSegments = []
        rightSegments = []
        prevSegEndPoint = None
        prevSegment = None
        for index, segment in enumerate(segments):
            if segment.isEmpty():
                continue
            segStartPoint, leftSegment, rightSegment, segEndPoint = segment.calcOffsets(nib)
            if index == 0:
                startPoint = segStartPoint
            if index == numSegments - 1:
                endPoint = segEndPoint
            if prevSegEndPoint is not None and abs(prevSegEndPoint - segStartPoint) < _epsilon:
                if whichTurn(prevSegment.endTangent, segment.startTangent) > 0:
                    leftSegments.append(LineSegment(segStartPoint))
                else:
                    rightSegments.append(LineSegment(segStartPoint))
            leftSegments.append(leftSegment)
            rightSegments.append(rightSegment)
            prevSegEndPoint = segEndPoint
            prevSegment = segment
        if not leftSegments:
            # This is an empty contour: there's nothing to draw
            return
        rightSegments = reversed([segment.reversed() for segment in rightSegments])
        # TODO: handle closed paths
        pen.moveTo(startPoint)
        self._drawSegments(startPoint, leftSegments, pen)
        pen.lineTo(endPoint)
        self._drawSegments(endPoint, rightSegments, pen)
        pen.closePath()

    @staticmethod
    def _drawSegments(prevPoint, segments, pen):
        for segment in segments:
            if abs(segment.points[0] - prevPoint) > _epsilon:
                pen.lineTo(segment.points[0])
            segment.draw(pen)
            prevPoint = segment.points[-1]


class Segment:

    def __init__(self, *points):
        self.points = [Vector(pt) for pt in points]
        self._coefficients = None

    def isEmpty(self):
        if len(self.points) < 2:
            return True
        length = 0
        for i in range(len(self.points) - 1):
            length += abs(self.points[i] - self.points[i + 1])
        return length < _epsilon

    @property
    def startTangent(self):
        return self.points[1] - self.points[0]

    @property
    def endTangent(self):
        return self.points[-1] - self.points[-2]

    def calcOffsets(self, nib):
        points = self.points
        assert len(points) >= 2, points

        startCurvature = sign(self.curvatureAtT(0))
        startTangent = self.startTangent

        turn = whichTurn(startTangent, nib.tangent)
        turn_90 = whichTurn(startTangent, nib.tangent90)
        if not startCurvature and (not turn or not turn_90):
            # Likely a straight line aligning with the nib: force
            # the curvature to 1 so we get the expected line cap.
            # (-1 would work as well, it just gives a different point
            # configuration.)
            startCurvature = 1
        if turn == 0:
            turn = -startCurvature * turn_90
        elif turn_90 == 0:
            turn_90 = startCurvature * turn
        penEdge = nib.getVector(turn, turn_90)
        penStart = nib.getVector(-turn, turn_90) * (turn * turn_90)

        startPoint = points[0] - penStart
        endPoint = points[-1] + penStart
        return startPoint, self - penEdge, self + penEdge, endPoint

    @property
    def coefficients(self):
        if self._coefficients is None:
            self._coefficients = self._calcCoefficients()
        return self._coefficients

    def reversed(self):
        return self.__class__(*reversed(self.points))

    def __add__(self, offset):
        return self.__class__(*(pt + offset for pt in self.points))

    def __repr__(self):
        points = ", ".join(f"({x}, {y})" for x, y in self.points)
        return f"{self.__class__.__name__}({points})"


class LineSegment(Segment):

    def draw(self, pen):
        if len(self.points) > 1:
            assert len(self.points) == 2
            pen.lineTo(self.points[1])

    def curvatureAtT(self, t):
        return 0


class CubicSegment(Segment):

    def draw(self, pen):
        assert len(self.points) == 4
        pen.curveTo(*self.points[1:])

    def _calcCoefficients(self):
        return tuple(Vector(coeff) for coeff in calcCubicParameters(*self.points))

    def curvatureAtT(self, t):
        # https://pomax.github.io/bezierinfo/#inflections
        a, b, c, d = self.coefficients
        # First derivative
        a1 = a * 3
        b1 = b * 2
        c1 = c
        x1, y1 = a1 * t*t + b1 * t + c1
        # Second derivative
        a2 = a1 * 2
        b2 = b1
        x2, y2 = a2 * t + b2
        # Cross product of first and second derivative
        return x1 * y2 - y1 * x2


_epsilon = 1e-6
_minus_epsilon = -_epsilon
_one_minus_epsilon = 1 - _epsilon


def splitCurveAtAngle(curve, angle):
    t = Transform().rotate(-angle)
    pt1, pt2, pt3, pt4 = t.transformPoints(curve)
    a, b, c, d = (y for x, y in calcCubicParameters(pt1, pt2, pt3, pt4))
    # First derivative
    a1 = a * 3.0
    b1 = b * 2.0

    yRoots = sorted(solveQuadratic(a1, b1, c))
    yRoots = [
        t for t in yRoots
        if _epsilon < t < _one_minus_epsilon
    ]
    return splitCubicAtT(*curve, *yRoots)


def whichTurn(vector1, vector2):
    """Given two vectors, return whether the second one is on the right
    hand side relative to the first (1), or on the left hand side (-1),
    or is going in the same or opposite direction (0).
    """
    x1, y1 = vector1
    x2, y2 = vector2
    dot = x1 * -y2 + y1 * x2
    if dot > _epsilon:
        # vector2 on the right of vector1
        return 1
    elif dot < _minus_epsilon:
        # vector2 on the left of vector1
        return -1
    else:
        # vector2 parallel/antiparallel to vector1
        return 0


def sign(v):
    if v < 0:
        return -1
    elif v > 0:
        return 1
    else:
        return 0


if __name__ == "__main__":
    points = [
        (170, 104),
        (122, 662),
        (784, 224),
        (886, 874),
        (292, 890),
        (451, 628),
    ]

    pen = RectNibPen(None, math.radians(220), 120, 30)
    # points[0], points[5] = points[5], points[0]
    pen.moveTo(points[0])
    pen.curveTo(*points[1:4])
    pen.lineTo(points[4])
    pen.lineTo(points[5])
    pen.moveTo((100, 500))
    pen.lineTo((300, 500))

    path = BezierPath()
    pen.draw(path)

    fill(0.6)
    stroke(0)
    # path.removeOverlap()
    drawPath(path)

    fill(0)
    stroke(None)
    for x, y in path.onCurvePoints:
        oval(x - 3, y - 3, 6, 6)
