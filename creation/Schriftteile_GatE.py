import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *

import math as m
from drawBot import *



temp_x = 2 * modul_width
temp_y = 9 * modul_height



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
baseline = backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)






def drawHauptbestandteil(x, y):
    
    Hauptbestandteil = BezierPath()

    A2 = x, y          #Raute_a
    A1 = A2[0]+offsetDir[0], A2[1]-offsetDir[1] 
    Hauptbestandteil.polygon(A1, A2)
    
    drawPath(Hauptbestandteil)
        
    return Hauptbestandteil
    
#drawHauptbestandteil(temp_x, temp_y)






def drawSchriftteil1(x, y):
    #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x/modul_width, y/modul_height)
    
    Schriftteil1 = BezierPath()
    
    #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, 2, 9)

    # E in No. I.    # nach einer Schneckenlinie mit 12. Part von der Linie A angefangen und bei B mit 6. Part geendiget.    # _. HSL, A>B, 12>6      # Im Text steht zwar „von Linie A angefangen“ aber in der Zeichnung  
    # ist von Linie H angefangen, und nur so funktioniert es auch.    # 6. HSL, H>B, 12>6
    

    HSL_size = 6
    HSL_start = 18
    
    H1, H2 = line_H_vonG_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)
    #H1, H2 = line_H_vonG_o_kl_s(Raute_a, *angles, part, HSL_size, HSL_start)

    A1, A2 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil1.arc(*drawKreisSeg(A1, HSL_start-HSL_size, angle_2, angle_1, True))

    B1, B2 = line_B_vonA_u_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*2)    
    Schriftteil1.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_1, angle_0, True))
    
    drawPath(Schriftteil1)
        
    return Schriftteil1
    
#drawSchriftteil1(temp_x, temp_y) 
 
 
 
 
    
    
def drawSchriftteil2(x, y):
    #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x/modul_width, y/modul_height)



    Schriftteil2 = BezierPath()
    
    HSL_size = 6
    HSL_start = 6
    
    B1, B2 = line_B_vonA_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)
    A1, A2 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*2)

    Schriftteil2.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))
    Schriftteil2.arc(*drawKreisSeg(A1, HSL_start+HSL_size, angle_9, angle_10))
    
    
    drawPath(Schriftteil2)
    return Schriftteil2
    
    
#drawSchriftteil2(temp_x, temp_y)  
    
    




    
    

def drawSchriftteil3(x, y, version="standard"):
    
    Schriftteil3 = BezierPath()
    
    # E in No. 3.    # nach einem Bogen von A bis B mit 15. Part.    
    # A>B, 15

    if version == "standard":
        HSL_size = 0
        HSL_start = 15
    
    
    if version == "0.75":
        HSL_size = 2
        HSL_start = 26
        
    A1, A2 = line_A_vonH_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    B1, B2 = line_B_vonA_u_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil3.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
    
    drawPath(Schriftteil3)
        
    return Schriftteil3
    


#drawSchriftteil3(temp_x, temp_y, "0.75")



    
def drawSchriftteil4(x, y, version="standard"):
    
    Schriftteil4 = BezierPath()
   
    if version == "standard": 
    
        HSL_size = 0
        HSL_start = 15
            
    if version == "1": 
    
        HSL_size = 0
        HSL_start = 26
    
    
    A1, A2 = line_A_vonH_u_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil4.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_9, angle_8, True))

    drawPath(Schriftteil4)
        
    return Schriftteil4
    
    
#drawSchriftteil4(temp_x, temp_y, "1")    
   
    




def drawSchriftteil5(x, y, version="standard"):
    
    Schriftteil5 = BezierPath()
    
    # E in No. 5.     # mit einem Bogenmaase und 7. Part von der Theilinie C bis zu B construiret.    # 1. HSL, C>B, 7
    # >>> Wenn man 13.5 anstatt 7 nimmt, passt es!

    if version == "standard":
        
        HSL_size = 0
        HSL_start = 13.5

        C1, C2 = line_C_vonD_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)

        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start)
        Schriftteil5.arc(*drawKreisSeg(B1, HSL_start, angle_7, angle_8))
        
            
    if version == "1":
        
        HSL_size = 0
        HSL_start = 26

        C1, C2 = line_C_vonD_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)

        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start)
        Schriftteil5.arc(*drawKreisSeg(B1, HSL_start, angle_7, angle_8))
        
        
        
    if version == "fakeGatC":    # angelegt für Schriftzug 2 in Kurrent GatE

        HSL_size = 5             # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        HSL_start = 21.55        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt

        E1, E2 = line_E_vonF_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        Schriftteil5.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))

        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        Schriftteil5.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        Schriftteil5.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    
    
    drawPath(Schriftteil5)
    
    return Schriftteil5
    
# drawSchriftteil5(temp_x, temp_y, "standard")    
# drawSchriftteil5(temp_x, temp_y, "1")  
# drawSchriftteil5(temp_x, temp_y, "fakeGatC")
    
    
    
    
    
    
    
    
def drawSchriftteil6(x, y, version="standard"):
    
    Schriftteil6 = BezierPath()   
    
    if version == "standard":
       
        HSL_size = 0
        HSL_start = 15

        C1, C2 = line_C_vonD_u_kl_s((x, y), *angles, part, HSL_size, HSL_start)
    
        B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start)
        Schriftteil6.arc(*drawKreisSeg(B1, HSL_start, angle_15, angle_16))
        
        
        
    if version == "1":
       
        HSL_size = 0
        HSL_start = 27
        
        C1, C2 = line_C_vonD_u_kl_s((x, y), *angles, part, HSL_size, HSL_start)

        B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start)
        Schriftteil6.arc(*drawKreisSeg(B1, HSL_start, angle_15, angle_16))
        
        
    
    if version == "fakeGatC":    # angelegt für Schriftzug 2 in Kurrent GatE

        HSL_size = 5             # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        HSL_start = 21.55        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt

        E1, E2 = line_E_vonF_u_kl_s((x, y), *angles, part, HSL_size, HSL_start)

        D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        Schriftteil6.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))

        C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        Schriftteil6.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))

        B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        Schriftteil6.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))
    
    

    drawPath(Schriftteil6)
        
    return Schriftteil6
    

#drawSchriftteil6(temp_x, temp_y)    




def drawSchriftteil7(x, y):
    
    Schriftteil7 = BezierPath() 
    
    # E in No.7.     # ist gleichfalls eine aus einem Bogenmaase bestehende Linie aus e nach der Richtlinie C, 
    # ingleichen die Linie aus d mit dem nehmliche Maase nach der Hauptlinie gezogen, 
    # welche sich beide in c durchschneiden. 
    # Aus diesem Durchschnitte c ist der Bogen ed durch den Radium ce entstanden. 

    HSL_size = 0
    HSL_start = 8
    
    A1, A2 = line_A_vonH_u_kl_s((x,y), *angles, part, HSL_size, HSL_start)
 
    C1 = A1
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),     C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)
    Schriftteil7.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_7, True))
    
    drawPath(Schriftteil7)
        
    return Schriftteil7
    

#drawSchriftteil7(temp_x, temp_y)    
   
    
    
    
def drawSchriftteil8(x, y, instrokeLen=0, outstrokeLen=0):
    
    Schriftteil8 = BezierPath() 
    
    HSL_size = 1
    HSL_start = 8
    
    A1, A2 = line_A_vonH_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    C1 = A1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)
    Schriftteil8.arc(*drawKreisSeg(C1, HSL_start, angle_1, angle_15, True))
    
    instroke = drawInstroke(*A2, instrokeLen)
    outstroke = drawOutstroke(*C2, outstrokeLen, "down")
    
    Schriftteil8 += instroke + outstroke
    drawPath(Schriftteil8)
    return Schriftteil8

#drawSchriftteil8(temp_x, temp_y) 





'''    
Dieresis sind individuell per Schrift, 
daher besser in Schriftzügen definieren
'''


# def drawDieresis(x, y):
    
#     Dieresis = BezierPath() 
    
#     dieresis_left = drawSchriftteil8(x, y)
#     dieresis_right = drawSchriftteil8(x+ 2*modul_width, y)

#     Dieresis = dieresis_left + dieresis_right 
#     drawPath(Dieresis)
        
#     return Dieresis
    
# #drawDieresis(temp_x, temp_y)







def drawSchriftteil9(x, y):
    
    Schriftteil9 = BezierPath() 
    
    # zu E No. 9.     # die Spitze eines Bogens der zweiten Hauptschneckenlinie, 
    # welche mit 10. Part angefangen und auf der Hauptlinie mit 4. Part geendiget ist.    # 2. HSL, E>A, 10>4
    
    
    #### ausgeblendet weil falsche Richtung, unlogisch
    
    # HSL_size = 2
    # HSL_start = 12

    # E1, E2 = line_E_vonD_o_kl_s((x,y), *angles, part, HSL_size, HSL_start)

    # F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    # Schriftteil9.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_5, angle_4, True))
    
    # G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    # Schriftteil9.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_4, angle_3, True))

    # H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    # Schriftteil9.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_3, angle_2, True))
    
    # A1, A2 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    # Schriftteil9.arc(*drawKreisSeg(A1, HSL_start-HSL_size*4, angle_2, angle_1, True))      

    
    HSL_size = 2
    HSL_start = 4
    
    A1, A2 = line_A_vonB_o_gr_s((x,y), *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftteil9.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_2))      
    Schriftteil9.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_3))      
    Schriftteil9.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_4))      
    Schriftteil9.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_4, angle_5))      

    
    
    drawPath(Schriftteil9)
        
    return Schriftteil9
    
   
    
#drawSchriftteil9(temp_x, temp_x)
    
    
    
    
    
    
def drawSchriftteil10(x, y):
    
    Schriftteil10 = BezierPath() 
    
    '''      #### ausgeblendet weil falsche Richtung, unlogisch
    HSL_size = 2
    HSL_start = 12

    E1, E2 = line_E_vonD_u_kl_s((x, y), *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftteil10.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_13, angle_12, True))
    
    G1, G2 = line_G_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftteil10.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_12, angle_11, True))

    H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftteil10.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_11, angle_10, True))
    
    A1, A2 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    Schriftteil10.arc(*drawKreisSeg(A1, HSL_start-HSL_size*4, angle_10, angle_9, True))
    '''
    
    HSL_size = 2
    HSL_start = 4
    
    A1, A2 = line_A_vonB_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftteil10.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_10))      
    Schriftteil10.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))      
    Schriftteil10.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))      
    Schriftteil10.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    drawPath(Schriftteil10)
        
    return Schriftteil10
    
    
#drawSchriftteil10(temp_x, temp_x)



    

def drawSchriftteil11(x, y):
    
    Schriftteil11 = BezierPath() 
    
    # Bei E No. 11. und 12.     # sind ebenfalls solche Anfangsspitzen, aber bis zur Hauptlinie, 
    # und zwar zu No. 11 vom 5. bis 2. Part, 
    # und zu No. 12. von 7. bis 3. Part construiret, 
    # und bei beiden eine dergleichen Spitze umgewendet angesetzt.
    # 11:  E>A; 5>2
    
    ### Ich habe eine andere Lösung gewählt >>> in der Mitte anfangen, 
    ### für bessere Passung und einfachere Positionierung (weil als Deckung verwendet)
    
    HSL_size = 1
    HSL_start = 2

    A1, A2 = line_A_vonB_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)  
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftteil11.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftteil11.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftteil11.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftteil11.arc(*drawKreisSeg(A1, HSL_start, angle_2, angle_1, True))

    
    # Wendung
    A3, A4 = line_A_vonB_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)          
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)        
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)

    Schriftteil11.arc(*drawKreisSeg(A3, HSL_start, angle_9, angle_10))
    Schriftteil11.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))
    Schriftteil11.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
    Schriftteil11.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))

       
    drawPath(Schriftteil11)
        
    return Schriftteil11
    
#drawSchriftteil11(temp_x, temp_x)  

    
    
    
    
    
    
    

def drawSchriftteil12(x, y, outstrokeLen=0):
    
    Schriftteil12 = BezierPath() 
    
    # Bei E No. 11. und 12.     # sind ebenfalls solche Anfangsspitzen, aber bis zur Hauptlinie, 
    # und zwar zu No. 11 vom 5. bis 2. Part, 
    # und zu No. 12. von 7. bis 3. Part construiret, 
    # und bei beiden eine dergleichen Spitze umgewendet angesetzt.
    # 12:  E>A; 7>4
    
    ### Ich habe eine andere Lösung gewählt >>> in der Mitte anfangen, 
    ### für bessere Passung und einfachere Positionierung (weil als Deckung verwendet)
    
    links = drawSchneckenzug(x, y, UPPER_A, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)
    rechts = drawSchneckenzug(x, y, LOWER_A, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], outstrokeLen)
    
    Schriftteil12 += links + rechts + outstroke
    drawPath(Schriftteil12)
        
    return Schriftteil12
    
#drawSchriftteil12(temp_x, temp_y)






