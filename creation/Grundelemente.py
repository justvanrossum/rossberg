import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *

import math as m
from drawBot import *


# # import importlib
# # import version_3.creation.create_stuff
# # import version_3.creation.create_spirals
# # import version_3.creation.Grundelemente
# # import version_3.creation.Halbboegen_GatC
# # import version_3.creation.Schriftteile_GatC

# # importlib.reload(version_3.creation.create_stuff)
# # importlib.reload(version_3.creation.create_spirals)
# # importlib.reload(version_3.creation.Grundelemente)
# # importlib.reload(version_3.creation.Halbboegen_GatC)
# # importlib.reload(version_3.creation.Schriftteile_GatC)

# # from version_3.creation.create_stuff import *
# # from version_3.creation.create_spirals import *
# # from version_3.creation.Grundelemente import *
# # from version_3.creation.Halbboegen_GatC import *
# # from version_3.creation.Schriftteile_GatC import *


# # import math as m




temp_x = modul_width * 2
temp_y = modul_height * 9



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 8
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.1)
fill(None)

fontSize(2)



# ___________ Hintergrund, Grundlinie und x-Höhe _______________

# x-Höhe
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)






# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)


 
# Raute zum Kontrollieren
#Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width, temp_y/modul_height)
    



def drawGrundelementA(x, y, length=1, direction="up"):
    
    GrundelementA = BezierPath()

    A1 = x, y
    
    if direction == "up":    
        A2 = A1[0]+offset[0]*length, A1[1]+offset[1]*length
    
    if direction == "down":    
        A2 = A1[0]-offset[0]*length, A1[1]-offset[1]*length
    
    GrundelementA.line(A1, A2)

    drawPath(GrundelementA)
    return GrundelementA
    
    
#drawGrundelementA(temp_x, temp_y, 1.2, "down")


    

def drawGrundelementB(x, y, length=1):
    
    GrundelementB = BezierPath()

    B1 = x, y        
    B2 = B1[0]+modul_width*length, B1[1]
    GrundelementB.line(B1, B2)

    drawPath(GrundelementB)
        
    return GrundelementB
    
#drawGrundelementB(temp_x, temp_y)





def drawGrundelementC(x, y, length=1, pos="oben", instrokeLen=0, outstrokeLen=0):        ### eigentlich falsch benannt, weil es automatisch
                                                                                ### E wird, wenn die andere Raute initialisiert wird.
    GrundelementC = BezierPath()
    
    ### instroke
    GrundelementC.line((x-(modul_width*instrokeLen), y-((modul_width/2)*instrokeLen)), (x, y))
        
    if pos == "oben":
        C1 = x, y         
        C2 = C1[0]+offsetDir[0]*length, C1[1]-offsetDir[1]*length
    
    if pos == "unten":
        C2 = x, y         
        C1 = C2[0]-offsetDir[0]*length, C2[1]+offsetDir[1]*length
    
    ### outstroke    
    GrundelementC.line(C2, (C2[0]+(modul_width*outstrokeLen), C2[1]+((modul_height/2)*outstrokeLen)))
    
    ### actual Element
    GrundelementC.line(C1, C2)
    
    drawPath(GrundelementC)
    return GrundelementC
    
#drawGrundelementC(temp_x, temp_y, pos="oben")




def drawGrundelementD(x, y, length=1, pos="oben", instrokeLen=0, outstrokeLen=0):   ### umständlich erzeugt, i know :/

    GrundelementD = BezierPath()
    
    ### instroke
    GrundelementD.line((x-(modul_width*instrokeLen), y-((modul_width/2)*instrokeLen)), (x, y))
    
    A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
    Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "D")

    offset = calcOffsetStroke(A1, A2)
    offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)
    
    
    if pos == "oben":
        A2 = x, y       
        A1 = A2[0]+offsetDir[0]*length, A2[1]-offsetDir[1]*length
                  
    
    if pos == "unten":
        A1 = x, y       
        A2 = A1[0]-offsetDir[0]*length, A1[1]+offsetDir[1]*length
        
    ### outstroke    
    GrundelementD.line(A1, (A1[0]+(modul_width*outstrokeLen), A1[1]+ ((modul_height/2)*outstrokeLen)))   
    
    
    
    #text("A1", A1)
    ### actual Element
    GrundelementD.line(A1, A2)
    
    drawPath(GrundelementD)   
    return GrundelementD
    
#drawGrundelementD(temp_x, temp_y, length=1) 







    


def drawGrundelementE(x, y, length=1, pos="oben", instrokeLen=0, outstrokeLen=0):   ### umständlich erzeugt, i know :/

    GrundelementE = BezierPath()
    
    ### instroke
    GrundelementE.line((x-(modul_width*instrokeLen), y- ((modul_width/2)*instrokeLen)), (x, y))
    
    A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
    Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

    offset = calcOffsetStroke(A1, A2)
    offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)
    
    
    if pos == "oben":
        A2 = x, y       
        A1 = A2[0]+offsetDir[0]*length, A2[1]-offsetDir[1]*length
                  
    
    if pos == "unten":
        A1 = x, y       
        A2 = A1[0]-offsetDir[0]*length, A1[1]+offsetDir[1]*length
        
        
    ### outstroke
    GrundelementE.line(A1, (A1[0]+(modul_width*outstrokeLen), A1[1]+ ((modul_height/2)*outstrokeLen)))   
    
    #text("A1", A1)
    ### actual Element
    GrundelementE.line(A1, A2)
    
    drawPath(GrundelementE)    
    return GrundelementE
    
#drawGrundelementE(temp_x, temp_y, length=1)   # outstroke=True

 
 
 
 
    
def drawGrundelementF(x, y, length=1, direction="down"):
    
    GrundelementF = BezierPath()
    
    if direction == "up":
        F1 = x, y          
        F2 = F1[0], F1[1]+length*modul_height
        GrundelementF.line(F1, F2)
        
    if direction == "down":   
        F1 = x, y          
        F2 = F1[0], F1[1]-length*modul_height
        GrundelementF.line(F1, F2)
        

    drawPath(GrundelementF)
        
    return GrundelementF
    
#GrundelementF = drawGrundelementF(temp_x, temp_y)
#print(GrundelementF.points)    ### ich verstehe nicht warum hier drei Punkte ausgegeben werden





def drawGrundelementG(x, y, length=1, direction="up"):
    
    GrundelementG = BezierPath()
    
    G1 = x, y     
    
    if direction == "down":
        G2 = G1[0]-length*(modul_width/2), G1[1]-length*modul_height
    else:
        G2 = G1[0]+length*(modul_width/2), G1[1]+length*modul_height
             

    GrundelementG.line(G1, G2)

    drawPath(GrundelementG)
        
    return GrundelementG
  
#drawGrundelementG(temp_x, temp_y, 1.5, "up")


 
    

def drawGrundelementH(x, y, length=1, direction="up"):
    
    GrundelementH = BezierPath()
    
    H1 = x, y          
    
    if direction == "down":
        H2 = H1[0]-modul_width*length, H1[1]-modul_height*length
    else:
        H2 = H1[0]+modul_width*length, H1[1]+modul_height*length

    GrundelementH.line(H1, H2)

    drawPath(GrundelementH)
        
    return GrundelementH
    
#drawGrundelementH(temp_x, temp_y, 1)
    



def drawInstroke(x, y, length=0.5, direction="up"):
    
    Instroke = BezierPath()
    
    A2 = x, y     
    
    if direction == "down":
        A1 = A2[0]+offset[0]*length, A2[1]+offset[1]*length
        Instroke.line(A2, A1)
        
    else:        # up
        A1 = A2[0]-offset[0]*length, A2[1]-offset[1]*length
        Instroke.line(A2, A1)
    
    

    drawPath(Instroke)
        
    return Instroke
    
    
    


def drawOutstroke(x, y, length=0.5, direction="up"):
    
    Outstroke = BezierPath()
    
    A2 = x, y        
    
    if direction == "down":
        A1 = A2[0]-offset[0]*length, A2[1]-offset[1]*length
    else:        # up
        A1 = A2[0]+offset[0]*length, A2[1]+offset[1]*length
    
    Outstroke.line(A1, A2)

    drawPath(Outstroke)
        
    return Outstroke





def drawConstroke(x, y, angle="H", length=4.5, deviation=1):
    
    ### deviation in angle “A” means the stroke end point is higher or lower
    ### deviation in angle “H” means the stroke end point moves more to left or right
    
    Constroke = BezierPath()
    
    StartBtm = x, y
    
    if angle == "A": 
        EndTop = StartBtm[0]+offset[0]*length, StartBtm[1]+offset[1]*deviation*length

    if angle == "H":
        EndTop = StartBtm[0]+modul_width*deviation*length, StartBtm[1]+modul_height*length
    
    Constroke.line(StartBtm, EndTop)
    drawPath(Constroke)
    return Constroke
    
# drawConstroke(temp_x, temp_y, angle="A", length=4.5)
# drawConstroke(temp_x, temp_y, angle="A", length=4.5, deviation=1.25)
# drawConstroke(temp_x, temp_y, angle="A", length=4.5, deviation=1.1)
# drawConstroke(temp_x, temp_y, angle="H", length=4.5)
# drawConstroke(temp_x, temp_y, angle="H", length=4.5, deviation=0.8)

    
    

def drawPosStroke(x, y, length=1):
    
    PosStroke = BezierPath()
    
    PosStroke = drawGrundelementA((x-2)*modul_width, (y+0.25)*modul_height, length)
    
    return PosStroke



def drawThinLineBtm(x, y, length=3):
    
    ThinLineBtm = BezierPath()
    
    save()
    stroke(1, 0, 0)
    strokeWidth(.001)
    ThinLineBtm = drawGrundelementF(x, y, length)  ### hier war 3.05, warum?
    restore()
    
    return ThinLineBtm





def drawAuslaufKehlung(x, y):
    
    # x=5
    # y=5

    # Kehlung und dünner Ausstrich, ohne nib sumulator!
    # der unter der Schriftlinie ab entstehende Winkel p nach der zweiten Hauptschneckenlinie 
    # von der Richtlinie E mit einem Radio von 12. Part bis zur Richtlinie B mit 16. Part abgekehlet
        
    AuslaufKehlung = BezierPath()  
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    Grund_a_btm, Grund_b_btm, Grund_c_btm, Grund_d_btm = drawGrundelOrient(A1, A2, offset, x, y-2)


    save()
    stroke(0,0,1)
    strokeWidth(.1)
            
    HSL_size = 2 
    HSL_start = 11.5  ### müsste eigentlich 12 sein laut Text

    # Strich gerade nach unten
    line(Grund_a, Grund_d_btm)
    

    E1, E2 = line_E_vonF_o_gr_s(Grund_b, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
    # AuslaufKehlung.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))
    # AuslaufKehlung.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))
    # AuslaufKehlung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))
        
    drawPath(AuslaufKehlung)
    restore()
    return AuslaufKehlung
    
 
#drawAuslaufKehlung(temp_x, temp_y)



def drawEndpunkt(x, y):     
    
    Endpunkt = BezierPath()
    
    Endpunkt.oval(x, y, part*8, part*8)
    print("Are you sure you dont want to use Thinstroke Endpunkt?")
    drawPath(Endpunkt)
    return Endpunkt

#drawEndpunkt(temp_x, temp_y)



def drawEmpty(x, y):
    
    Empty = BezierPath()

    drawPath(Empty)
    return Empty
       
#drawEmpty(temp_x, temp_y)








    
    
def drawThinstroke_Endpunkt(x, y, *, pass_from_thick=None):     
    
    Thinstroke_Endpunkt = BezierPath()
    
    amount = 9   ### auf 1 wenn “delete inner circle”
    radius = part*8
    
    i = 0.8
    for i in range(amount):
        Thinstroke_Endpunkt.oval(x+i*0.4, y+i*0.4, radius-i*0.8, radius-i*0.8)
        i += 0.8
    
    Thinstroke_Endpunkt.oval(x+i*0.4, y+i*0.4, radius-i*0.8, radius-i*0.8)
    return Thinstroke_Endpunkt






















