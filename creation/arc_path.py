"""ArcPath is a partial drawBot.BezierPath replacement, that supports sequences
of connecting arcs, approximating them with fewer Bezier segments.
"""

# Just van Rossum, february 2021

from typing import NamedTuple
import math
from fontTools.pens.basePen import BasePen
from fontTools.pens.recordingPen import RecordingPen
from fontTools.pens.reverseContourPen import ReverseContourPen
import numpy as np

try:
    from .fit_curve import fitCubic
except ImportError:
    from fit_curve import fitCubic


class ArcPath:

    def __init__(self):
        self.elements = []

    def translate(self, x=0, y=0):
        for element in self.elements:
            element.translate(x, y)

    def scale(self, sx, sy=None):
        if sy is None:
            sy = sx
        for element in self.elements:
            element.scale(sx, sy)

    def reverse(self):
        for element in self.elements:
            element.reverse()

    def moveTo(self, pt):
        self._getBezierPath().moveTo(pt)

    def lineTo(self, pt):
        self._getBezierPath().lineTo(pt)

    def curveTo(self, *pts):
        self._getBezierPath().curveTo(*pts)

    def qCurveTo(self, *pts):
        self._getBezierPath().qCurveTo(*pts)

    def closePath(self):
        self._getBezierPath().closePath()

    def endPath(self):
        self._getBezierPath().endPath()

    def _getBezierPath(self):
        if not self.elements or not isinstance(self.elements[-1], BezierPathElement):
            self.elements.append(BezierPathElement())
        return self.elements[-1]

    def arc(self, center, radius, startAngle, endAngle, clockwise):
        if not self._canAppendArc(center, radius, startAngle, endAngle, clockwise):
            self.elements.append(ArcSequenceElement())
        arcSeq = self.elements[-1]
        arcSeq.arc(center, radius, startAngle, endAngle, clockwise)

    def _canAppendArc(self, center, radius, startAngle, endAngle, clockwise):
        return (
            self.elements and
            isinstance(self.elements[-1], ArcSequenceElement) and
            self.elements[-1].canAppendArc(center, radius, startAngle, endAngle, clockwise)
        )

    def line(self, pt1, pt2):
        self.polygon(pt1, pt2, close=False)

    def polygon(self, *points, close=False):
        if not points:
            return
        self.moveTo(points[0])
        for pt in points[1:]:
            self.lineTo(pt)
        if close:
            self.closePath()
        else:
            self.endPath()

    def rect(self, x, y, w, h):
        self.polygon(
            (x, y),
            (x, y + h),
            (x + w, y + h),
            (x + w, y),
            close=True,
        )

    def oval(self, x, y, w, h):
        handle = (math.sqrt(2) - 1) * (4/3)
        rx = w / 2
        ry = h / 2
        cx = x + rx
        cy = y + ry
        basePoints = [(handle, 1), (1, handle), (1, 0)]
        self.moveTo((cx, cy + ry))
        for _ in range(4):
            curvePoints = (
                (cx + rx * dx, cy + ry * dy)
                for dx, dy in basePoints
            )
            self.curveTo(*curvePoints)
            basePoints = [(dy, -dx) for dx, dy in basePoints]
        self.closePath()

    def draw(self, pen, useArcs=False):
        if useArcs and not hasattr(pen, "arc"):
            useArcs = False
        for element in self.elements:
            if useArcs and isinstance(element, ArcSequenceElement):
                element.drawArcs(pen)
            elif hasattr(element, "drawToPen"):
                element.drawToPen(pen)
            else:
                element.draw(pen)

    drawToPen = draw

    def getNSBezierPath(self):
        # This method makes two things work in DrawBot:
        # - drawPath(thisPath)
        # - bezPath.appendPath(thisPath)
        # aka:
        # - bezPath += thisPath
        from fontTools.pens.cocoaPen import CocoaPen
        pen = CocoaPen(None)
        self.draw(pen)
        return pen.path

    @property
    def points(self):
        pen = PointCollectorPen()
        self.draw(pen)
        return pen.points

    def copy(self):
        result = ArcPath()
        result.elements = [element.copy() for element in self.elements]
        return result

    def __add__(self, other):
        result = self.copy()
        return result.__iadd__(other)

    def __iadd__(self, other):
        if isinstance(other, ArcPath):
            if (self.elements and other.elements and
                    isinstance(self.elements[-1], ArcSequenceElement) and
                    isinstance(other.elements[0], ArcSequenceElement) and
                    self.elements[-1].canAppendArc(*other.elements[0].arcs[0])):
                self.elements[-1].arcs.extend(other.elements[0].arcs)
                self.elements.extend(other.copy().elements[1:])
            else:
                self.elements.extend(other.copy().elements)
        elif hasattr(other, "drawToPen"):
            # DrawBot BezierPath
            self.elements.append(other)
        else:
            return NotImplemented
        return self


class BezierPathElement(RecordingPen):

    def copy(self):
        p = self.__class__()
        p.value = list(self.value)
        return p

    def translate(self, dx, dy):
        value = []
        for method, points in self.value:
            points = tuple(
                None if pt is None else (pt[0] + dx, pt[1] + dy)
                for pt in points
            )
            value.append((method, points))
        self.value = value

    def scale(self, sx, sy):
        value = []
        for method, points in self.value:
            points = tuple(
                None if pt is None else (pt[0] * sx, pt[1] * sy)
                for pt in points
            )
            value.append((method, points))
        self.value = value

    def reverse(self):
        rp = RecordingPen()
        rcp = ReverseContourPen(rp)
        self.draw(rcp)
        self.value = rp.value

    def draw(self, pen):
        self._ensureEndPath()
        self.replay(pen)

    def _ensureEndPath(self):
        # Add endPath() calls at the end of contours that aren't already finalized
        # with a closePath() or endPath() call.
        value = []
        for method, points in self.value:
            if method == "moveTo" and value and value[-1][0] not in {"closePath", "endPath"}:
                value.append(("endPath", ()))
            value.append((method, points))
        if value and value[-1][0] not in {"closePath", "endPath"}:
            value.append(("endPath", ()))
        self.value = value


class Arc(NamedTuple):

    center: tuple
    radius: float
    startAngle: float
    endAngle: float
    clockwise: bool

    def reversed(self):
        return Arc(
            self.center, self.radius,
            self.endAngle, self.startAngle,
            not self.clockwise
        )


_epsilon = 1e-6  # a small number


class ArcConnectionError(Exception):
    pass


class ArcSequenceElement:

    def __init__(self):
        self.arcs = []

    def copy(self):
        arcs = self.__class__()
        arcs.arcs = list(self.arcs)
        return arcs

    def translate(self, x, y):
        arcs = []
        for (cx, cy), radius, startAngle, endAngle, clockwise in self.arcs:
            arcs.append(Arc((cx + x, cy + y), radius, startAngle, endAngle, clockwise))
        self.arcs = arcs

    def scale(self, sx, sy):
        assert sx == sy
        arcs = []
        for (cx, cy), radius, startAngle, endAngle, clockwise in self.arcs:
            arcs.append(Arc((cx * sx, cy * sy), radius * sx, startAngle, endAngle, clockwise))
        self.arcs = arcs

    def reverse(self):
        self.arcs = list(reversed([arc.reversed() for arc in self.arcs]))

    @property
    def clockwise(self):
        return self.arcs[0].clockwise if self.arcs else None

    @property
    def totalSweep(self):
        if not self.arcs:
            return 0
        sweep = 0
        for arc in self.arcs:
            sweep += _getSweep(arc.startAngle, arc.endAngle, self.clockwise)
        return sweep

    def canAppendArc(self, center, radius, startAngle, endAngle, clockwise):
        if not self.arcs:
            return True
        startAngle %= 360
        endAngle %= 360
        if abs(startAngle - self.arcs[-1].endAngle) > _epsilon:
            return False
        if self.arcs[-1].clockwise != clockwise:
            return False
        x1, y1 = getArcPoint(self.arcs[-1].center, self.arcs[-1].radius, self.arcs[-1].endAngle)
        x2, y2 = getArcPoint(center, radius, startAngle)
        if abs(x2 - x1) > _epsilon or abs(y2 - y1) > _epsilon:
            return False
        return True

    def arc(self, center, radius, startAngle, endAngle, clockwise):
        if not self.canAppendArc(center, radius, startAngle, endAngle, clockwise):
            raise ArcConnectionError("arc does not connect")
        startAngle %= 360
        endAngle %= 360
        self.arcs.append(Arc(center, radius, startAngle, endAngle, clockwise))

    def drawArcs(self, path):
        if not self.arcs:
            return
        path.moveTo(getArcPoint(self.arcs[0].center, self.arcs[0].radius, self.arcs[0].startAngle))
        for center, radius, startAngle, endAngle, clockwise in self.arcs:
            path.arc(center, radius, startAngle, endAngle, clockwise)
        path.endPath()

    def draw(self, path, numBeziers=None):
        if not self.arcs:
            return
        if numBeziers is None:
            numBeziers = math.ceil(abs(self.totalSweep) / 90)
        for i, arcSeq in enumerate(self.split(numBeziers)):
            arcSeq._drawOne(path, i == 0)
        path.endPath()

    def _drawOne(self, path, doMoveTo):
        firstArc = self.arcs[0]
        points = [getArcPoint(firstArc.center, firstArc.radius, firstArc.startAngle)]
        for center, radius, startAngle, endAngle, clockwise in self.arcs:
            numMidPoints = 2  # if 2, hardcode to t = 0.5
            for i in range(1, numMidPoints):
                t = i / numMidPoints
                midAngle = interpolateAngle(startAngle, endAngle, clockwise, t)
                points.append(getArcPoint(center, radius, midAngle))
            points.append(getArcPoint(center, radius, endAngle))

        leftAngle = math.radians(firstArc.startAngle)
        rightAngle = math.radians(self.arcs[-1].endAngle)
        leftTangent = np.array([math.sin(leftAngle), -math.cos(leftAngle)])
        rightTangent = np.array([-math.sin(rightAngle), math.cos(rightAngle)])
        if not self.clockwise:
            leftTangent = -leftTangent
            rightTangent = -rightTangent
        bezierSegments = fitCubic(np.array(points), leftTangent, rightTangent, 10, maxSegments=1)
        assert len(bezierSegments) == 1
        pt1, pt2, pt3, pt4 = (tuple(pt) for pt in bezierSegments[0])
        assert pt1 == points[0]
        if doMoveTo:
            path.moveTo(pt1)
        path.curveTo(pt2, pt3, pt4)

    def split(self, numSegments):
        if numSegments <= 1:
            return [self]
        startAngle = self.arcs[0].startAngle
        clockwise = self.clockwise
        totalSweep = self.totalSweep
        arcs = list(self.arcs)
        arcSequences = [ArcSequenceElement() for i in range(numSegments)]
        for i in range(numSegments):
            if i == numSegments - 1:
                splitSweep = None  # until the end
            else:
                splitSweep = ((i + 1) / numSegments) * totalSweep
            while arcs:
                arc = arcs.pop(0)
                swp = _getSweep(startAngle, arc.endAngle, clockwise)
                if splitSweep is None:
                    arcSequences[i].arc(*arc)
                elif (clockwise and swp >= splitSweep) or (not clockwise and swp <= splitSweep):
                    arcSequences[i].arc(*arc)
                else:
                    arc1, arc2 = _splitArc(arc, startAngle + splitSweep)
                    if arc1 is not None:
                        arcSequences[i].arc(*arc1)
                    if arc2 is not None:
                        arcs.insert(0, arc2)
                    break

        return arcSequences


def getArcPoint(center, radius, angle):
    x, y = center
    x = x + radius * math.cos(math.radians(angle))
    y = y + radius * math.sin(math.radians(angle))
    return x, y


def interpolateAngle(startAngle, endAngle, clockwise, t):
    sweep = _getSweep(startAngle, endAngle, clockwise)
    return startAngle + t * sweep


def _getSweep(startAngle, endAngle, clockwise):
    sweep = (endAngle - startAngle) % 360
    if clockwise:
        sweep -= 360
    return sweep


def _splitArc(arc, splitAngle):
    splitAngle %= 360
    if abs(arc.startAngle - splitAngle) < _epsilon:
        return None, arc
    elif abs(arc.endAngle - splitAngle) < _epsilon:
        return arc, None
    arc1 = Arc(arc.center, arc.radius, arc.startAngle, splitAngle, arc.clockwise)
    arc2 = Arc(arc.center, arc.radius, splitAngle, arc.endAngle, arc.clockwise)
    return arc1, arc2


class PointCollectorPen(BasePen):

    def __init__(self, glyphSet=None):
        super().__init__(glyphSet)
        self.points = []

    def _addPoints(self, *pts):
        for x, y in pts:
            self.points.append((x, y))

    _moveTo = _addPoints
    _lineTo = _addPoints
    _curveToOne = _addPoints


if __name__ == "__main__":

    def blob(pt, r):
        x, y = pt
        d = r * 2
        oval(x - r, y - r, d, d)

    def arcify(start_point, radii, angles, clockwise):
        assert len(radii) + 1 == len(angles)
        x, y = start_point
        for i in range(len(radii)):
            startAngle = angles[i]
            endAngle = angles[i + 1]
            radius = radii[i]
            cx = x - radius * cos(math.radians(startAngle))
            cy = y - radius * sin(math.radians(startAngle))
            x = cx + radius * cos(math.radians(endAngle))
            y = cy + radius * sin(math.radians(endAngle))
            yield (cx, cy), radius, startAngle, endAngle, clockwise

    startPoint = (416, 688)
    angle1 = -234
    angle2 = 155
    angleStep = -10
    clockwise = angleStep < 0
    if clockwise:
        angles = range(angle2, angle1, angleStep)
    else:
        angles = range(angle1, angle2, angleStep)
    radius_step = 3
    radii = [80 + i * radius_step for i in range(len(angles) - 1)]

    arcp = ArcPath()
    for arcArgs in arcify(startPoint, radii, angles, clockwise):
        arcp.arc(*arcArgs)
    # print("num arcs:", len(arcp.elements[0].arcs))

    arcp.line((50, 50), (540, 120))

    arcp2 = ArcPath()
    for arcArgs in arcify((200, 500), radii, angles, clockwise):
        arcp2.arc(*arcArgs)

    arcp += arcp2

    bez = BezierPath()
    bez.rect(200, 100, 50, 60)
    arcp += bez
    arcp.moveTo((600, 150))
    arcp.lineTo((600, 200))
    arcp.curveTo((650, 300), (750, 320), (850, 100))
    arcp.oval(20, 20, 110, 170)
    arcp.rect(60, 90, 90, 120)

    print(arcp.elements)
    path = BezierPath()
    arcp.draw(path, True)
    stroke(1, 0, 0)
    fill(None)
    drawPath(path)
    for pt in path.onCurvePoints:
        blob(pt, 3)

    path = BezierPath()
    arcp.draw(path)
    stroke(0, 0, 1)
    fill(None)
    # drawPath(path)
    drawPath(arcp.copy())
    # drawPath(arcp)
    fill(1, 0, 0)
    stroke(None)
    for pt in path.points:
        blob(pt, 3)

    # print(arcp.points)
    # p = BezierPath(arcp.getNSBezierPath())
    # print(p.points)
    arcp.translate(30, 40)
    arcp.scale(1.2, 1.2)
    stroke(0, 1, 0)
    fill(None)
    drawPath(arcp)

    p1 = ArcPath()
    p1.arc((0, 0), 100, 120, 90, True)
    p2 = ArcPath()
    p2.arc((0, 0), 100, 90, 60, True)
    p2.arc((0, 0), 100, 60, 30, True)
    p1 += p2
    stroke(0)
    translate(100, 100)
    drawPath(p1)
